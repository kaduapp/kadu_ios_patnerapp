//
//  PostTicketZendesk.swift
//  Zendesk
//
//  Created by Nabeel Gulzar on 26/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//


import UIKit
import RxSwift
import RxCocoa
import RxAlamofire

class ZendeskAPI:NSObject {
    
    let disposebag = DisposeBag()
    let Zendesk_Response = PublishSubject<APIResponseModel>()
    

    /// creating new ticket
    ///
    /// - Parameter paramDict:subject, comment, priority
    func postTheNewTicket(paramDict: [String:Any]){
        Helper.showPI(message: "Creating ticket..")
        let strURL = APIEndTails.Ticket
        
        RxAlamofire
            .requestJSON(.post, strURL,parameters:paramDict, headers: APIErrors.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.hidePI()
               // Helper.showAlert(message: StringConstants.SomethingWentWrong(), head: StringConstants.Error(), type: 1)
                self.Zendesk_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    
    ///get the tickets data using request id or email id
    func getTheTicketData(id: String){
        let strURL = APIEndTails.SingleTicketDetails + id
        
        RxAlamofire
            .requestJSON(.get, strURL, headers:APIErrors.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.hidePI()
                //Helper.showAlert(message: StringConstants.SomethingWentWrong, head: StringConstants.Error(), type: 1)
                self.Zendesk_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    
    /// get the ticketid data using ticket id
    ///
    /// - Parameter id: ticket id
    func getTheTicketHistory(id: String){
        Helper.showPI(message: "loading..")
        let strURL = APIEndTails.HistoryTicket + id
        
        RxAlamofire
            .requestJSON(.get, strURL, headers: APIErrors.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.hidePI()
            //    Helper.showAlert(message: StringConstants.SomethingWentWrong(), head: StringConstants.Error(), type: 1)
                self.Zendesk_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    
    
    /// update to new comment to a ticket with id and requestid
    ///
    /// - Parameter paramDict: new comments for ticketid
    func postTheNewTicketComment(paramDict: [String:Any]){
        Helper.showPI(message: "sending..")
        let strURL = APIEndTails.Comment
        
        RxAlamofire
            .requestJSON(.put, strURL,parameters:paramDict, headers:APIErrors.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                Helper.hidePI()
            //    Helper.showAlert(message: StringConstants.SomethingWentWrong(), head: StringConstants.Error(), type: 1)
                self.Zendesk_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    
    /// response of all above Zendesks
    ///
    /// - Parameters:
    ///   - statusCode: 200 is success
    ///   - responseDict: response data after success
    func checkResponse(statusCode:Int,responseDict: [String:Any]){
        Helper.hidePI()

        print(" Response: \(responseDict)");
        let responseCodes : APICalls.ErrorCode = APICalls.ErrorCode(rawValue: statusCode)!
        Helper.hidePI()
        switch responseCodes {
        case .success:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.Zendesk_Response.onNext(responseModel)
            break
        default:
            APICalls.basicParsing(data: responseDict, status: statusCode)
            break
        }
    
    }
}



