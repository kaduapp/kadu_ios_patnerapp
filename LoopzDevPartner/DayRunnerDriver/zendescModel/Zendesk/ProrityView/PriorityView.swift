//
//  PriorityView.swift
//  LiveMPro
//
//  Created by Nabeel Gulzar on 04/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

protocol selectProrityDelegate:class
{
    func selectedPriority(priority:String,color:UIColor)
    func hideTheView() 
}


class PriorityView: UIView {
  
    @IBOutlet weak var heightOFContentView: NSLayoutConstraint!
    open weak var delegate: selectProrityDelegate?
    @IBOutlet weak var cancelTableView: UITableView!
    var isShown:Bool = false
    var bookingID:Int64 = 0
    var prioritiesArray = Priorties().reason
    var prioritiesColors = Priorties().colors
    var selectedIndex = -1
    private static var share: PriorityView? = nil
    
    static var instance: PriorityView {
        
        if (share == nil) {
            
            share = Bundle(for: self).loadNibNamed("PriorityView",
                                                   owner: nil,
                                                   options: nil)?.first as? PriorityView
     
            share?.cancelTableView.register(UINib(nibName: "PriorityCell", bundle: Bundle.main), forCellReuseIdentifier: "tablecell")
            
        }
        return share!
    }
    

    
    @IBAction func hideTheView(_ sender: Any) {
        delegate?.hideTheView()
        self.hide()
    }
    
    /// Show Network
    func show() {
        if isShown == false {
        
            heightOFContentView.constant = 5
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
            
            self.alpha = 0.0
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 1.0
                            
                            self.heightOFContentView.constant = 190
                            self.layoutIfNeeded()
            })
        }
    }
    
    /// Hide
    func hide() {
        
        if isShown == true {
            isShown = false
            heightOFContentView.constant = 190
            UIView.animate(withDuration: 0.5,
                           animations: {
                            PriorityView.share = nil
                            self.heightOFContentView.constant = 5
                            self.layoutIfNeeded()
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
        }
    }
}

extension PriorityView : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prioritiesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tablecell") as? PriorityTableCell
        cell?.cancelLabel.text = prioritiesArray[indexPath.row]
        cell?.priorityLabelColor.backgroundColor =  prioritiesColors[indexPath.row]
        cell?.selectionStyle = .none
        return cell!
    }
}

extension PriorityView : UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.selectedPriority(priority: prioritiesArray[indexPath.row], color: prioritiesColors[indexPath.row])
        self.hide()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
}

extension PriorityView : UIGestureRecognizerDelegate{
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: cancelTableView))! {
            return false
        }
        return true
    }
}



