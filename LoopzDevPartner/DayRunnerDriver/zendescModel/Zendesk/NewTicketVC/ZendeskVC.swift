//
//  ZendeskVC.swift
//  Zendesk
//
//  Created by Nabeel Gulzar on 26/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ZendeskVC: UIViewController {
    @IBOutlet weak var zendeskTableView: UITableView!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var firstLetter: UILabel!
    @IBOutlet weak var currentTime: UILabel!
    @IBOutlet weak var myName: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var moreImage: UIImageView!
    @IBOutlet weak var priorityColorLabel: UILabel!
    @IBOutlet weak var addSubject: UITextField!
    @IBOutlet weak var priorityButton: UIButton!
    @IBOutlet weak var bottomContainerConstraint: NSLayoutConstraint!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var heightOfTheTextView: NSLayoutConstraint!
    @IBOutlet weak var subjectLine: UIView!
    @IBOutlet var subjectLabel: UILabel!
    @IBOutlet var sendButton: UIButton!
    
    @IBOutlet var priorityLabel: UILabel!
    var subJuctGiven = ""
    var comments = [String]()
    var zendeskViewModel = ZendeskModel()
    var ticketHistoryData = historyModel()
    var ticketID = 0
    var curserOnTextfield = false
    var navView = NavigationView().shared

    override func viewDidLoad() {
        super.viewDidLoad()
        //textView.Semantic()
        if subJuctGiven.length > 0 {
            addSubject.text = subJuctGiven
            addSubject.isUserInteractionEnabled = false
        }else{
            addSubject.isUserInteractionEnabled = true
        }
        self.zendeskTableView.rowHeight = UITableView.automaticDimension
        self.zendeskTableView.estimatedRowHeight = 80.0
        firstLetter.text =  String(Utility.userName.first!)
        myName.text = Utility.userName
        
        headerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        headerView.layer.shadowColor = UIColor.lightGray.cgColor
        headerView.layer.shadowRadius = 1
        headerView.layer.shadowOpacity = 0.75
        let shadowFrame: CGRect = headerView.layer.bounds
        let shadowPath = (UIBezierPath(rect: shadowFrame).cgPath)
        headerView.layer.shadowPath = shadowPath
        
        priorityButton.setTitle("normal", for: .normal)
        priorityColorLabel.backgroundColor = UIColor.green
        self.currentTime.text =  Helper.getDateString(value: Date(), format: DateFormat.TimeFormatToDisplay, zone: false)
        
        // Do any additional setup after loading the view.
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: "Ticket")
        }
        textView.text = "Type message here.."
        textView.textColor = UIColor.lightGray
        subjectLabel.text = "Subject"
        priorityLabel.text = "Priority"
        sendButton.setTitle("Send" , for: .normal)
        sendButton.setTitle("Send" , for: .highlighted )
        sendButton.setTitle("Send" , for: .selected )
    }
    
      override func viewDidDisappear(_ animated: Bool) {
          super.viewDidDisappear(animated)
          removeObserver()
      }
    
    override func viewWillAppear(_ animated: Bool) {
        addObserver()
        if ticketID != 0 {
            arrowImage.isHidden = true
            zendeskViewModel.getTheTicketsHistory(userID: String(describing:ticketID)) { (success, ticketsData) in
                if success{
                    self.ticketHistoryData = ticketsData
                    self.addSubject.text = self.ticketHistoryData.subject
                    self.priorityButton.setTitle(self.ticketHistoryData.priority, for: .normal)
                    self.currentTime.text =  Helper.getDateString(value: self.ticketHistoryData.comments[0].timeStamp, format: DateFormat.DateFormatToDisplay, zone: false)
                    self.priorityButton.isEnabled = false
                    self.addSubject.isUserInteractionEnabled = false
                    switch self.ticketHistoryData.priority{
                    case "urgent":
                        self.priorityColorLabel.backgroundColor = UIColor.red
                        break
                    case "high":
                        self.priorityColorLabel.backgroundColor = UIColor.blue
                        break
                    case "normal":
                        self.priorityColorLabel.backgroundColor = UIColor.green
                        break
                    case "low":
                        self.priorityColorLabel.backgroundColor = UIColor.yellow
                        break
                    default:
                        self.priorityColorLabel.backgroundColor = UIColor.blue
                        break
                    }
                    
                    if self.ticketHistoryData.status == "solved"{
                        self.bottomContainerConstraint.constant =  -50
                    }
                    self.zendeskTableView.reloadData()
                }
            }
        }
       
    }
    
    
    // Scroll to bottom of table view for messages
    func scrollToBottomMessage() {
        if self.ticketHistoryData.comments.count == 0 {
            return
        }
        let bottomMessageIndex = IndexPath(row: self.ticketHistoryData.comments.count - 1, section: 0)
        zendeskTableView.scrollToRow(at: bottomMessageIndex, at: .bottom, animated: true)
    }
    
    @IBAction func gestureAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
         self.dismiss(animated: true, completion: nil)
    }
    

    @IBAction func sendTheText(_ sender: Any) {
        // creating new ticket
        if self.ticketHistoryData.comments.count == 0{
            self.view.endEditing(true)
            
            if textView.text == "Type message here.." {
                Helper.showAlert(message: "please add the comment to this ticket", head: "Message", type: 1)
            }else if addSubject.text?.length == 0{
                Helper.showAlert(message: "please add the subject to this ticket", head: "Message", type: 1)
            }else if priorityButton.titleLabel?.text?.length == 0{
                Helper.showAlert(message: "please add the priority to this ticket", head: "Message", type: 1)
            }
            else{
                let ticketData = NewTicketRequest.init(sub: addSubject.text!, comments: textView.text!, status: "open", ticketType: "problem", priority: priorityButton.titleLabel!.text!, req_id: Utility.TickiteID)
               
                zendeskViewModel.postTheNewTicket(newTicketData: ticketData, completionHandler: { (success) in
                   self.textView.text = ""
                  // Helper.showAlertWithOutTitle(message: StringConstants.TicketSuccessfull(), type: 1)

                })
            }
        }else{
            if textView.text == "Message" {
                Helper.showAlert(message: "please add the comment to this ticket", head: "Message", type: 1)
            }else{
              
                // adding comment to the exisiting ticket
                let newTicket = ticketHistory()
                newTicket.comment = textView.text!
                newTicket.firstLetter = String(Utility.userName.first!)
                newTicket.timeStamp = Date()
                newTicket.name = Utility.userName
                self.ticketHistoryData.comments.append(newTicket)
                self.zendeskTableView.reloadData()
                
                let params:[String:Any] = ["id":String(describing:ticketID),
                                           "body":textView.text! as Any,
                                           "author_id":Utility.TickiteID]
              
                self.textView.text = ""
                self.heightOfTheTextView.constant = 45
                zendeskViewModel.postTheNewTicketComment(params: params, completionHandler: { (success) in
                    if success{
                        self.scrollToBottomMessage()
                    }
                })
            }
        }
    }
    
    
    
    
    @IBAction func selectTheTicketPrority(_ sender: Any) {
        self.view.endEditing(true)
        DispatchQueue.main.async {
            let priorityView = PriorityView.instance
            priorityView.delegate = self
            priorityView.show()
            self.arrowImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi ));
        }
    }
}

extension ZendeskVC:selectProrityDelegate{
    
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
     /// remove Observer
    func removeObserver() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    //Mark:- Keyboard ANimation
    @objc func keyboardWillShow(_ notification: NSNotification){
        // Do something here
        
        let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as! NSNumber

        self.view.setNeedsLayout()

        UIView.animate(withDuration: TimeInterval(truncating: duration), delay: 0, options: [UIView.AnimationOptions(rawValue: UInt(truncating: curve))], animations: {

            let info = notification.userInfo!
            let inputViewFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let screenSize: CGRect = UIScreen.main.bounds
            var frame = self.view.frame
            frame.size.height = screenSize.height - inputViewFrame.size.height
            self.view.frame = frame


            self.view.layoutIfNeeded()
        }, completion: nil)

    }
    
    @objc func keyboardWillHide(_ notification: NSNotification){
        let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as! NSNumber
        
        self.view.setNeedsLayout()
        
        UIView.animate(withDuration: TimeInterval(truncating: duration), delay: 0, options: [UIView.AnimationOptions(rawValue: UInt(truncating: curve))], animations: {
            let screenSize: CGRect = UIScreen.main.bounds
            var frame = self.view.frame
            frame.size.height = screenSize.height
            self.view.frame = frame
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func selectedPriority(priority: String,color:UIColor) {
        priorityButton.setTitle(priority, for: .normal)
        priorityColorLabel.backgroundColor = color
        arrowImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi - 3.14159));
    }
    
    func  hideTheView() {
        arrowImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi - 3.14159));
    }
}


///****** tableview datasource*************//
extension ZendeskVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ticketHistoryData.comments.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"chat") as! ZendeskTableCell?
        
        cell?.firstLetyter.text = self.ticketHistoryData.comments[indexPath.row].firstLetter
        cell?.name.text = self.ticketHistoryData.comments[indexPath.row].name
        cell?.comment.text = self.ticketHistoryData.comments[indexPath.row].comment
        cell?.time.text = Helper.getDateString(value: self.ticketHistoryData.comments[indexPath.row].timeStamp, format: DateFormat.TimeFormatToDisplay, zone: false)
        cell?.date.text = Helper.getDateString(value: self.ticketHistoryData.comments[indexPath.row].timeStamp, format: DateFormat.DateFormatToDisplay, zone: false)
        
        return cell!
    }
}

///***********tableview delegate methods**************//
extension ZendeskVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ZendeskVC:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Message" {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Message"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            if let text = self.textView.text {
                if text.length > 0 {
                    self.sendTheText("")
                }
            }
            textView.resignFirstResponder()
            return false
        }
        return true;
    }
}

extension ZendeskVC:UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        curserOnTextfield = true
        self.subjectLine.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0x003A45)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (addSubject.text?.isEmpty)! {
            self.subjectLine.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0x003A45)
        }else{
            self.subjectLine.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0x003A45)
        }
        curserOnTextfield = false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        curserOnTextfield = false
        self.view.endEditing(true)
        return true
    }
    
    
}

