//
//  HeaderTableViewCell.swift
//  Zendesk
//
//  Created by Nabeel Gulzar on 26/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ZenDescHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var titleOfStatus: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
