//
//  AlertMessage.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 21/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

extension String {
    var localized: String {
        
        //return   NSLocalizedString(self, comment: self)
       // OneSkyOTAPlugin.localizedString(forKey:self , value: self, table: nil)
        return self.localised(forLanguage: Utility.getSelectedLanguegeCode())
    }
}

struct signup {
    static let InValidEmail     = "Enter Valid email id".localized
    static let InValidPhone     = "Enter Valid Phone number".localized
    static let selectOperator   = "Please select operator".localized
    static let enterFirstName   = "Please enter your first name.".localized
    static let enterLastName    = "Please enter last name".localized
    static let enterPhoneNumber = "Please enter your phone number".localized
    static let enterEmailId     = "Please enter your email address".localized
    static let enterPassword    = "Please enter a password.".localized
    static let selectLicence    = "Please upload your driving license.".localized
    static let selectBackLicence = "Please upload your driving license back image.".localized
    static let enterLicence     = "Please enter your License number.".localized
    static let licenseExp       = "Please select your License expiry date.".localized
    static let dob              = "Please select your date of birth.".localized
    static let city             = "Please select yo ur city of operation.".localized
    static let zones            = "Please select your zones of operation".localized
    static let selectProfile    = "Please upload your profile picture.".localized
    static let validPassword    = "Please input a minimum of 6 characters with atleast one numeric value(0-9).".localized
    static let validPhoneNumber = "Please enter a valid phone number".localized
    static let personDetails    = "PERSONAL DETAILS".localized
    static let confirm = "CONFIRM".localized
    static let lastName = "LAST NAME".localized
    static let phoneNumber = "PHONE NUMBER*".localized
    static let email = "EMAIL*".localized
    static let firstNAme = "FIRST NAME".localized
    static let password = "PASSWORD*".localized
    static let cityLabel = "CITY*".localized
    static let zonesLabel = "ZONES*".localized
    static let firstName = "FIRST NAME*".localized
    static let dateOfBirth = "DATE OF BIRTH*".localized
    static let licenceLabel = "LICENSE EXPIRY DATE*".localized
    static let drivingLicence = "DRIVING LICENCE*".localized
    static let takePhoto = "Take Photo".localized
    static let selectImage      = "Please select image".localized
    static let cancel           = "Cancel".localized
    static let gallery          = "Gallery".localized
    static let camera           = "Camera".localized
    static let USER_PIC         = "user_pic".localized
    static let verifyMobile     = "Verifying the MobileNumber".localized
    static let verifyEmail      = "Verifying the EmailAddress".localized
    static let logout = "LOGOUT".localized
    static let viewProfile = "View Profile".localized
    static let Version = "Version".localized
    static let noTrans = "No Recent Transactions Available.".localized
    static let all = "all".localized
    static let debit = "Debit".localized
    static let credit = "Credit".localized
    static let currentCredit = "CURRENT CREDIT".localized
    static let noBookingLabel = "No Booking history found in this week !!".localized
    static let thisWeek = "THIS WEEK".localized
    static let bookingHistory = "Booking History".localized
    static let orderCompleted = "Order Completed".localized
    static let orderDetails = "ORDER DETAILS".localized
    static let itemDetails = "ITEM DETAILS".localized
    static let items = "ITEMS".localized
    static let Qty = "QTY".localized
    static let  Price = " PRICE ".localized
    static let total = "Total".localized
    static let typeOfDelivery = "Type Of Delivery".localized
    static let paymentBreakDown = "PAYMENT BREAKDOWN".localized
    static let subtotal = "Sub Total".localized
    static let tax = "Tax".localized
    static let DeliveryCharge = "Delivery Charges".localized
    static let Discount = "Discount".localized
    static let PaymentMethod = "PAYMENT METHOD".localized
    static let weight = "Weight".localized
    static let card = "Card".localized
    static let wallet = "Wallet".localized
    static let cash = "Cash".localized
    static let earningAndCommission = "EARNINGS AND COMMISSION".localized
    static let driverEarning = "Driver earning".localized
    static let CleaningFee = "Cleaning Fee".localized
    static let support = "Support".localized
    static let recentTopLabel = "Recent Transcations".localized
    static let bankDetails = "Bank Details".localized
    static let nostripAccountDetails = "No Stripe Account Details !!".localized
    static let addStripe = "Add Stripe Account".localized
    static let signup = "SIGN UP".localized
    static let verifyCode = "A Verification Code has been sent to".localized
    static let verifBelow = ". Please enter the code below.".localized
    static let changePass = "Change Password".localized
    static let changePasswordNumber = "Thank you for verifying your phone number , please create a new password below ".localized
    static let newPassword = "NEW PASSWORD".localized
    static let reENTER = "RE-ENTER PASSWORD".localized
    static let resend = "RESEND CODE ?".localized
    static let changeName = "Change Name".localized
    static let changeyourLabel = "CHANGE YOUR NAME".localized
    static let enterName = "Enter Name".localized
    static let call = "Call".localized
    static let details = "Details".localized
}


struct vehicleDetails {
    static let vehicleNumber    = "Please enter plate number".localized
    static let vehicleColor     = "Please enter vehicle specialities".localized
    static let vehicleType      = "Please enter vehicle type".localized
    static let vehicleMake      = "Please enter vehicle make".localized
    static let vehicleModel     = "Please select vehicle model".localized
    static let insuranceNumber  = "Please enter insurance number".localized
    static let selectImage      = "Please select image".localized
    static let cancel           = "Cancel".localized
    static let gallery          = "Gallery".localized
    static let camera           = "Camera".localized
    static let USER_PIC         = "user_pic".localized
    static let arrived = "ARRIVED".localized
    static let jobCom = "JOB COMPLETED".localized
    static let pickStart = "Picked & Started".localized
    static let vehicleImg       = "Please select the vehicle image".localized
    static let vehicleIns       = "Please select driver vehicle insurance".localized
    static let vehicleRegis     = "Please select driver vehicle registation".localized
    static let carriagePermit   = "Please select driver vehicle permit".localized
}

struct VehicleMakeModelSelect {
    static let selectVehicleType   = "Please select the type".localized
    static let vehicleMake         = "Please select the make".localized
    static let selectModel         = "Please select the model".localized
    static let selectSpecialities  = "Please select the specialities".localized
    static let selectZones         = "Please select the zones".localized
    
    static let titleSpecialties = "Specialities".localized
    static let titleVehicleType = "Vehicle Types".localized
    static let titleModel       = "Vehicle Model".localized
    static let titleMake        = "Vehicle Make".localized
    static let titleZone        = "Select Zones".localized
    static let titleCity        = "Select City".localized
    static let titleSelecteService = "Service".localized
}

struct verify {
    static let fillOTP          = "Please fill the OTP".localized
}


struct alertMsgCommom {
    static let Message          = "Message".localized
}


struct signin {
    static let emailAddress     = "Please enter Email Address or Phone number".localized
    static let password         = "Please enter Password".localized
}

struct selectVehicle {
    static let vehicleID        = "Please Select VehicleID".localized
    static let password         = "Please enter Password".localized
}

struct homeVC {
    static let goOnline         = "Go Online..".localized
    static let goOffline        = "Go Offline..".localized
    static let timeLeftDrop     = "Time left for drop".localized
    static let timeLeftPickUp   = "Time left for pickup".localized
    static let bookingType      = "Booking Type".localized
    static let pickupTime = "Pickup Time:".localized
    static let customerNameLabel = "CUSTOMER NAME".localized
    static let store = "STORE".localized
    static let delivery = "DELIVERY".localized
    static let payment = "PAYMENT".localized
    static let products = "PRODUCTS".localized
    static let addOns = "Add Ons:".localized
    static let grandtotal = "Grand Total".localized
    static let jobStarted = "Start job".localized
    static let Subtotal = "Subtotal".localized
    static let jobDetails = "Job Details".localized
    static let pickup = "PICKUP".localized
    static let yourHereLabel = "YOU ARE HERE".localized
    static let delivered = "DELIVERED".localized
    static let tapProduct = "PRODUCTS (Tap on the product to edit.)".localized
    static let deliveryCompletedLabel = "Delivery Completed".localized
    static let billingAmount = "BILLABLE AMOUNT".localized
    static let signature = "SIGNATURE".localized
    static let rateLabel = "Rate the Customer".localized
    static let ordertype = "Order Type".localized
    
}

struct onBookingVC {
    static let loadingtime      = "Loading Time".localized
    static let distance         = "Distance".localized
    static let unloadingTime    = "Unloading Time".localized
    static let timeTravell      = "Time".localized
    static let drop      = "Drop".localized
    static let arrivedPickup    = "ARRIVED AT PICKUP".localized
    static let tripStart        = "On the way to pickup".localized
    static let start            = "START".localized
    static let arrivedLocation  = "ARRIVED AT LOCATION".localized
    static let reachedLocation  = "REACHED DROP LOCATION".localized
    static let unloaded         = "On the way to delivery".localized
    static let onThewayPick     = "On the way to pickup".localized
    static let lefToAccept = "left to Accept".localized
    static let deliveryFee = "DELIVERY FEE".localized
    static let dateTime = "DATE & TIME".localized
}

struct InvoiceStrings {
    static let documents        = "Photos of document".localized
    static let deliveredTo      = "Delivered to".localized
    static let handlingFee      = "Handling Fee".localized
    static let tollFee          = "Toll Fee".localized
    
    static let waitFee          = "Waiting Fee".localized
    static let discount         = "Discount".localized
    static let timeFee          = "Time Fee".localized
    static let distFee          = "Distance Fee".localized
    static let baseFee          = "Base Fee".localized
    static let billDetails      = "Bill Details".localized
    static let signature       = "Please take the signature from customer".localized
}

struct passwordViewControl {
    static let newpassword         = "Please enter new password".localized
    static let reenterPassword     = "Please Re-enter  password".localized
    static let mismatchpassword    = "Password mismatched".localized
}

struct LeftMenu {
    static let Home                = "Home".localized
    static let History             = "History".localized
    static let Support             = "Support".localized
    static let Invite              = "Invite".localized
    static let DriverPortal        = "Driver Portal".localized
    static let BankDetails         = "Bank Details".localized
    static let Logout              = "Live Chat".localized
    static let Wallet              = "Wallet".localized
    static let Language            =  "Language".localized
    
}

struct ImagePickerAlerts {
    static let selectImage         = "Please Select Image".localized
    static let cancelImageSel      = "Cancel".localized
    static let gallery             = "Gallery".localized
    static let camera              = "Camera".localized
    static let removeImage         = "Remove image".localized
}


struct HistoryAlerts {
    static let barEmptyText        = "You need to provide data for the chart.".localized
    static let Sun                 = "SUN".localized
    static let Mon                 = "MON".localized
    static let Tue                 = "TUE".localized
    static let Wed                 = "WED".localized
    static let Thu                 = "THU".localized
    static let Fri                 = "FRI".localized
    static let Sat                 = "SAT".localized
    static let earnData            = "Booking data".localized
}

struct inviteAlerts {
    static let referalCode        = "ReferralCode:".localized
    static let Account            = "Accounts".localized
    static let logtwitter         = "Please login to a Twitter account to tweet.".localized
    static let logFacebook        = "Please login to a Facebook account to share.".localized
    static let cantSentMail       = "Could Not Send Email".localized
    static let noEmailAct         = "Your device could not send e-mail.  Please check e-mail configuration and try again.".localized
    static let referralCode        = "Runner ReferralCode".localized
    static let runnerApp           = "Hi, your are using DayRunner app....".localized
    static let ok                  = "OK".localized
}

struct bankDetails {
    static let verified            = "verified".localized
    static let bankAccount         = "Bank Accounts".localized
    static let unverifed           = "Unverified".localized
    static let stripeAccount       = "Stripe Account".localized
    static let stripeHodler        = "Stripe Holder:".localized
}


struct bankDetailsMsg {
    static let photoID          = "Please Add the Photo ID".localized
    static let firstName        = "Please Add the account holder first name".localized
    
    static let holderName        = "Please Add the account holder  name".localized
    
    static let lastName         = "Please Add the account holder last name".localized
    static let accountNum       = "Please Add the Iban number".localized
    static let uniqueNum        = "Please Add the  unique identification number".localized
    static let routingNum       = "Please Add the routing number".localized
    static let address          = "Please Add the address".localized
    static let city             = "Please Add the city".localized
    static let state            = "Please Add the state".localized
    static let postal           = "Please Add the Postal code".localized
    
    static let addStripeTitle      = "Add Stripe Account".localized
    static let verifyStripeTitle   = "Verify Stripe Account".localized
    static let verifyStripe        = "Verify Stripe".localized
    static let addStripe           = "Add Stripe".localized
}

struct cancelViewStrings {
    static let writeComment         = "Write a comment here...".localized
    static let cancellation         = "Please Select Cancellation Reason (or) Provide your reason".localized
}

struct shipmentDetails {
    static let noNotes            = "No notes provided by customer.".localized
    static let quant              = "loose".localized
    static let writeComment       = "Write a comment here...".localized
}

struct forgotPasswordString{
    static let validate           = "Please enter Mobile Number or Email ID".localized
}


struct passBookingStrings{
    static let senderDetails      = "Sender Details".localized
    static let billDetails        = "Bill Details".localized
    
    
    static let baseFee            = "Base Fee".localized
    static let distFee            = "Distance Fee".localized
    static let timeFee            = "Time Fee".localized
    static let waitFee            = "Waiting Fee".localized
    
    static let tollFee            = "Toll Fee".localized
    static let handlingFee        = "Handling Fee".localized
    static let discount           = "Discount".localized
    static let yourEarn           = "Your Earnings".localized
    
    static let earnedAmt          = "Earned Ammount".localized
    static let appCom             = "App Commission".localized
    static let paymentDetails     =  "Payment Details".localized
    static let receiverDetails    = "Receiver's Details".localized
    static let documents          = "Documents".localized
}

struct AppDelegateStrings{
    
    static let newBookingWish     = "Congratulations you got New Booking".localized
    static let newBookingText     = "New booking".localized
    
}

struct loginStrings {
    static let phoneNumber = "Phone Number".localized
    static let or = "OR".localized
    static let emailAddress = "Email Address".localized
    static let Password = "PASSWORD".localized
    static let signIn = "SIGN IN".localized
    static let forgotPassword = "FORGOT PASSWORD ?".localized
    static let selectLang = "Select Language".localized
    static let dontHave = "Don't have an account? Sign Up".localized
    static let enterYourForgot = "Enter your phone number and you will get a verification code to reset Password".localized
    static let enterMail = "Enter your email and you will get a verification code to reset password".localized
    static let resetPassword = "Please Select Method to Reset Password".localized
    static let optionToSelect = "Options to select:".localized
    static let viaPhoneNumber = "Via Phone Number.".localized
    static let viaEmail = "Via Email.".localized
    static let done = "Done".localized
    static let selectCountry = "Select Country".localized
    static let typeCountry = "Type Country".localized
}
