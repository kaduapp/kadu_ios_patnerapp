//
//  APIRequestParams.swift
//  UFly
//
//  Created by 3Embed on 27/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class APIRequestParams: NSObject {
    
    //GuestLogin API
    static let Language                                 = "language"
    static let LanguageDefault                          = "0"
    static let DeviceId                                 = "deviceId"
    static let AppVersion                               = "appVersion"
    static let DevMake                                  = "deviceMake"
    static let DevModel                                 = "deviceModel"
    static let DevType                                  = "deviceType"
    static let DeviceTime                               = "deviceTime"
    static let Latitude                                 = "latitude"
    static let Longitude                                = "longitude"
    static let Version                                  = "version"
    static let Mandatory                                = "mandatory"
    
    
    
    //Login
    static let Authorization                            = "authorization"
    static let UserName                                 = "phone"
    static let Password                                 = "password"
    static let CountryCode                              = "countryCode"
    static let PushToken                                = "pushToken"

    //Validate Email
    static let Email                                    = "email"
    static let VerifyType                               = "verifyType"
    
    //ForgotPassword
    static let EmailOrMobile                            = "emailOrMobile"
    static let ProcessType                              = "processType"
    
    //Signup
    static let FirstName                                = "name"
    static let LastName                                 = "lastName"
    static let DateOfBirth                              = "dateOfBirth"
    static let About                                    = "about"
    static let ProfilePic                               = "profilePic"
    static let TermsAndCond                             = "termsAndCond"
    static let Phone                                    = "mobile"
    static let LoginType                                = "loginType"
    static let ReferralCode                             = "referralCode"

    //OTP
    static let OTPCode                                  = "code"
    
    //Addresscode
    static let Id                                       = "id"
    static let FlatNumber                               = "flatNumber"
    static let LandMark                                 = "landmark"
    static let AddLine1                                 = "addLine1"
    static let AddLine2                                 = "addLine2"
    static let City                                     = "city"
    static let State                                    = "state"
    static let Country                                  = "country"
    static let PlaceId                                  = "placeId"
    static let Pincode                                  = "pincode"
    static let Tag                                      = "taggedAs"
    static let IdAddress                                = "addressId"
    
    //Cart
    static let CustomerName                             = "customerName"
    static let ParentId                                 = "parentProductId"
    static let ChildId                                  = "childProductId"
    static let QTY                                      = "quantity"
    static let StoreId                                  = "storeId"
    static let StoreName                                = "storeName"
    static let StoreLat                                 = "latitude"
    static let StoreLong                                = "longitude"
    static let StoreLogo                                = "storeLogo"
    static let ItemName                                 = "itemName"
    static let Itemimage                                = "itemImageURL"
    static let UpcNumber                                = "upc"
    static let SKUNumber                                = "sku"
    static let ItemPrice                                = "unitPrice"
    static let CartId                                   = "cartId"
    static let Store                                    = "store"
    static let StorePrice                               = "storePrice"
    static let Product                                  = "product"
   //wishList
    static let ListId                                   = "listId"
    
   // fare
    static let Status                                   = "status"
    static let OrderType                                = "type"
    
    
    //ORder
    static let Address1                                 = "address1"
    static let Address2                                 = "address2"
    static let PaymentType                              = "paymentType"
    static let CartOrder                                = "cart"
    static let OrderTime                                = "bookingDate"
    static let DeuTime                                  = "dueDatetime"
    static let PickupType                               = "serviceType"
    static let BookingType                              = "bookingType"
    static let FCMTopic                                 = "fcmTopic"
    static let MQTTTopic                                = "mqttTopic"
    static let IPAddress                                = "ipAddress"
    static let ExtraNotes                               = "extraNote"
    static let OrderID                                  = "orderId"
    static let Reason                                   = "reason"
    static let MileageMetric                            = "mileageMetric"
    static let CurrencySymbol                           = "currencySymbol"
    static let Currency                                 = "currency"
    
    //Wishist
    static let Checked                                  = "checked"
    static let List                                     = "list"
    
    //Promocode
    static let UserId                                   = "userId"
    static let PromoCode                                = "couponCode"
    static let CityId                                   = "cityId"
    static let ZoneId                                   = "zoneId"
    static let PaymentMethod                            = "paymentMethod"
    static let VehicleType                              = "vehicleType"
    static let Amount                                   = "cartValue"
    static let DeliveryFee                              = "deliveryFee"
    static let Discount                                 = "discount"
    static let PayAmount                                = "finalPayableAmount"

    //Card Token
    static let CardToken                                = "cardToken"
    static let CardId                                   = "cardId"
}
