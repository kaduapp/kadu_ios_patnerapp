//
//  APIResponceParams.swift
//  UFly
//
//  Created by 3Embed on 27/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class APIResponceParams: NSObject {
    
    //Config
    static let AllCitiesPush                            = "aallCitiesCustomerspushTopics"
    static let AllPush                                  = "allCustomerspushTopics"
    static let GoogleMapKeys                            = "custGoogleMapKeys"
    static let GooglePlaceKeys                          = "custGooglePlaceKeys"
    static let StripeKey                                = "stripeKey"
    static let LaterBookingBuffer                       = "latebookinginterval"
    
    
    
    

    //Basic
    static let Message                                  = "message"
    static let Data                                     = "data"
    
    //Login API
    static let CountryCode                              = "countryCode"
    static let Email                                    = "email"
    static let FirstName                                = "name"
    static let Phone                                    = "mobile"
    static let Sid                                      = "sid"
    static let SessionToken                             = "token"
    static let FCMTopic                                 = "fcmTopic"
    static let MQTTTopic                                = "mqttTopic"
    static let ReferralCode                             = "referralCode"
    static let ProfilePic                               = "profilePic"
    static let MMJCard                                  = "mmjCard"
    static let IDCard                                   = "identityCard"
    static let Verified                                 = "verified"
    static let URL                                      = "url"
  
    
    //Address
    static let IdAddress                                = "_id"
    static let AddLine1                                 = "addLine1"
    static let AddLine2                                 = "addLine2"
    static let City                                     = "city"
    static let State                                    = "state"
    static let Country                                  = "country"
    static let PlaceId                                  = "placeId"
    static let Pincode                                  = "pincode"
    static let Tag                                      = "taggedAs"
    static let UserType                                 = "userType"
    static let UserId                                   = "userId"
    static let Latitude                                 = "latitude"
    static let Longitude                                = "longitude"
    static let Id                                       = "zoneId"
    static let Title                                    = "title"
    static let FlatNumber                               = "flatNumber"
    static let LandMark                                 = "landmark"
    static let CurrencySymbol                           = "currencySymbol"
    static let Currency                                 = "currency"
    static let MilageMetric                             = "mileageMetric"
    //Store
    static let Stores                                   = "stores"
    static let StoreAddress                             = "businessAddress";
    static let StoreName                                = "businessName";
    static let StoreId                                  = "businessId";
    static let StoreLat                                 = "businessLatitude";
    static let StoreLong                                = "businessLongitude";
    static let StoreImage                               = "images";
    static let DistanceKm                               = "distanceKm";
    static let DistanceMiles                            = "distanceMiles"
    static let EstimatedTime                            = "estimatedTime"
    static let Categories                               = "categories";
    static let StoreRating                              = "businessRating"
    static let FreeDeliveryAbove                        = "freeDeliveryAbove"
    static let MinimumOrder                             = "minimumOrder"
    static let BusinessImage                            = "businessImage"
    
    //Category
    static let CategoryId                               = "categoryId"
    static let CategoryName                             = "categoryName"
    static let CategoryDesc                             = "description"
    static let CategoryImage                            = "imageUrl"
    static let CategoryBannerImage                      = "bannerUrl"
    static let SubCategories                            = "subCategories"
    static let CategoryColor                            = "categoryColor"
    //SubCategories
    static let SubCategoryDesc                          = "description"
    static let SubCategoryId                            = "subCategoryId"
    static let SubCategoryName                          = "subCategoryName"
    
    //Item
    static let ItemImages                               = "mobileImage"
    static let ItemId                                   = "childProductId"
    static let ItemName                                 = "productName"
    static let ItemPrice                                = "priceValue"
    static let ItemTCH                                  = "THC"
    static let ItemCBD                                  = "CBD"
    static let Fav                                      = "isFavorite"
    static let FavCount                                 = "productFavCount"
    static let ItemDescription                          = "shortDescription"
    static let Weight                                   = "weight"
    static let ItemPCU                                  = "upc"
    static let ItemSKU                                  = "sku"
    static let DetailedDescription                      = "detailedDescription"
    static let StrainEffects                            = "strainEffects"
    static let Units                                    = "units"
    static let UnitId                                   = "unitId"
    static let UnitName                                 = "unitName"
    static let Value                                    = "value"
    
    
    //favorite
    static let ProductFav                               = "productFav"
    
    //Card
    static let Brand                                    = "brand"
    static let DefaultCard                              = "isDefault"
    static let DefaultCardWallet                        = "default"
    static let ExpMonth                                 = "exp_month"
    static let ExpYear                                  = "exp_year"
    static let ExpMonth1                                 = "expMonth"
    static let ExpYear1                                  = "expYear"
    static let Funding                                  = "funding"
    static let CardId                                   = "id"
    static let Last4                                    = "last4"
    static let Name                                     = "name"
    static let CardToken                                = "token"
    //Cart
    static let CartStoreID                              = "storeId"
    static let CartStoreName                            = "storeName"
    static let CartStoreLat                             = "storeLatitude"
    static let CartStoreLong                            = "storeLongitude"
    static let CartStoreAddress                         = "storeAddress"
    static let CartStoreLogo                            = "storeLogo"
    static let CartStoreTotal                           = "storeTotalPrice"
    static let CartStoreDelFee                          = "storeDeliveryFee"
    static let Products                                 = "products"
    static let CartId                                   = "cartId"
    static let Cart                                     = "cart"
    static let TotalPrice                               = "totalPrice"
    
    
    //History
    static let Bid                                      = "orderId"
    static let PickAddress                              = "pickAddress"
    static let DropAddress                              = "dropAddress"
    static let DriverName                               = "driverName"
    static let Status                                   = "statusCode"
    static let StatusMessage                            = "statusMessage"
    static let MessageStatus                            = "statusMessage"
    static let ReceiverName                             = "receiverName"
    static let TotalAmount                              = "totalAmount"
    static let items                                    = "items"
    static let itemName                                 = "itemName"
    static let storeName                                = "storeName"
    static let PickLat                                  = "pickupLat"
    static let PickLong                                 = "pickupLong"
    static let DropLat                                  = "dropLat"
    static let DropLong                                 = "dropLong"
    static let ServiceType                              = "serviceType"
    
    //Order
    static let BookingDate                              = "bookingDate"
    
   //selected popup
    
    static let ListName                                 = "listName"
    static let ListId                                   = "listId"
    static let ListImage                                = "listImage"
    
    //WishList
    static let WishList                                 = "wishList"
    static let Image                                    = "image"
    
    //Support
    static let Desc                                     = "desc"
    static let Link                                     = "link"
    static let SubCat                                   = "subcat"
    
    //promocode
    static let AmountCart                               = "discountAmount"
    static let FinalAmount                              = "finalAmount"
    
    static let Code                                     = "code"
    static let Description                              = "description"
    static let Discount                                 = "discount"
    static let TypeId                                   = "typeId"
    static let TypeName                                 = "typeName"
    static let EndTime                                  = "endTime"
    static let HowItWorks                               = "howItWorks"
    static let MinimunCartValue                         = "minimumPurchaseValue"
    static let StartTime                                = "startTime"
    static let TermsAndConditions                       = "termsAndConditions"
    
}
