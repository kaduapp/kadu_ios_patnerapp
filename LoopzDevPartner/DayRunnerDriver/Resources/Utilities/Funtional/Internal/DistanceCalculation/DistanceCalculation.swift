//
//  DistanceCalculation.swift
//  RunnerDev
//
//  Created by Rahul Sharma on 13/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

public class DistanceCalculation: NSObject {
    
    public override init() {
        if let rideDistance: Double = userDefault.object(forKey: "rideDistance") as? Double
        {
            distanceCalulated = rideDistance
        }
    }
    
    var userDefault: UserDefaults = UserDefaults.standard
    
    var distanceCalulated: Double = 0;
   @objc var shouldCalculateDistance: Bool = false;
    
    func calculateDistance(location: CLLocation)
    {
        if let pickLat = userDefault.object(forKey: "pickLat"), let pickLong = userDefault.object(forKey: "pickLong")
        {
            let locA = CLLocation.init(latitude: pickLat as! CLLocationDegrees, longitude: pickLong as! CLLocationDegrees)
            let locB = location
            let distance = locA.distance(from: locB) // distance returned in Meters
            if(distance > 10)
            {
                if let rideDistance: Double = userDefault.object(forKey: "rideDistance") as? Double
                {
                    let finalDistance = distance + rideDistance
                    distanceCalulated = finalDistance
                    userDefault.set(location.coordinate.latitude, forKey: "pickLat")
                    userDefault.set(location.coordinate.longitude, forKey: "pickLong")
                    userDefault.set(Double(finalDistance), forKey: "rideDistance")
                }
            }
        }
        else
        {
            userDefault.set(location.coordinate.latitude, forKey: "pickLat")
            userDefault.set(location.coordinate.longitude, forKey: "pickLong")
            userDefault.set(Double(0), forKey: "rideDistance")
        }
    }
    
    func clearDistanceCalculated()
    {
        userDefault.removeObject(forKey: "pickLat")
        userDefault.removeObject(forKey: "pickLong")
        userDefault.removeObject(forKey: "rideDistance")
        distanceCalulated = 0
    }
}

