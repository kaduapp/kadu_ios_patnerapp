//
//  DBHelper.swift
//  runner
//
//  Created by Rahul Sharma on 05/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import CoreData

class DBHelper: NSObject {
    
    //MARK: - Generic Methods
    class func getContext () ->  NSManagedObjectContext {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if #available(iOS 10.0, *) {
            return appDelegate.persistentContainer.viewContext
        }
        else {
            // Fallback on earlier versions
            return appDelegate.managedObjectContext
        }
    }
    
    //MARK: - Store Address Method
 
    class func storeInitialData(info: [String: AnyObject]) -> Bool
    {
        
        let context = getContext()
        
        //retrieve the entity that we just created
        let entity =  NSEntityDescription.entity(forEntityName: "BookingsTime", in: context)
        
        let transc = NSManagedObject(entity: entity!, insertInto: context)
                
        //set the entity values
        transc.setValue(String(describing:info["bid"]!),
                        forKey: "bid")
        
        transc.setValue(String(describing:info["bookingTime"]!),
                        forKey: "bookingTime")
        
        transc.setValue(String(describing:info["loadWaitingTime"]!),
                        forKey: "loadWaitingTime")
        
        transc.setValue(String(describing:info["startLog"]!),
                        forKey: "startLog")
        
        transc.setValue(String(describing:info["startLat"]!),
                        forKey: "startLat")

        
        //save the object
        do {
            try context.save()
            print("Saved! \(info)")
            return true
        }
        catch let error as NSError  {
            print("Could not save! \(error), \(error.userInfo)")
            return false
        }
        catch {
            return false
        }
    }
    
    
    
    //MARK: - Store Address Method
    
    class func storeStartData(info: [String: AnyObject]) -> Bool {
        
        let context = getContext()
        
        //retrieve the entity that we just created
        let entity =  NSEntityDescription.entity(forEntityName: "StartBookingdTime", in: context)
        
        let transc = NSManagedObject(entity: entity!, insertInto: context)
        
        //set the entity values
        
        
        
        transc.setValue(String(describing:info["bid"]!),
                        forKey: "bid")
        
        
        transc.setValue(String(describing:info["startTripTime"]!),
                        forKey: "startTripTime")
        
        transc.setValue(String(describing:info["unloadWaitingTime"]!),
                        forKey: "unloadWaitingTime")
        
        transc.setValue(String(describing:info["endLog"]!),
                        forKey: "endLog")
        
        transc.setValue(String(describing:info["endLat"]!),
                        forKey: "endLat")

        
        
        //save the object
        do {
            try context.save()
            print("Saved! \(info)")
            return true
        }
        catch let error as NSError  {
            print("Could not save! \(error), \(error.userInfo)")
            return false
        }
        catch {
            return false
        }
    }

    
    
    
    
    
    class func fetchAddress () -> [AnyObject] {
        
        //create a fetch request, telling it about the entity
        let fetchRequest: NSFetchRequest = BookingsTime.fetchRequest()
        
        do {
            //go get the results
            let searchResults: [AnyObject] = try getContext().fetch(fetchRequest)
            
            //I like to check the size of the returned results!
            print ("num of results = \(searchResults.count)")
            return searchResults
            
        }
        catch {
            print("Error with request: \(error)")
            return []
        }
    }
    
    class func deleteRow(tableName: String, bookingID: String) -> Bool {
        
        //create a fetch request, telling it about the entity
        let fetchRequest: NSFetchRequest = BookingsTime.fetchRequest()
        let context = getContext()
        
        
        do {
            //go get the results
            let searchResults: [AnyObject] = try context.fetch(fetchRequest)
            
            let prediction = NSPredicate(format: "bid == %@",bookingID)
            
            let results = (searchResults as NSArray).filtered(using: prediction)
            
            // Check if Find results
            if results.count == 0 {
                return false
            }
            
            // When found Results
            for address in results {
                context.delete(address as! NSManagedObject)
            }
            
            //save the object
            do {
                try context.save()
                print("Deleted!")
                return true
            }
            catch let error as NSError  {
                print("Could not save! \(error), \(error.userInfo)")
                return false
            }
            catch {
                return false
            }
         }
        catch {
            print("Error with request: \(error)")
            return false
        }
    }
    
    class func fetchData(tableName: String, bookingID: String) -> [BookingsTime] {
        
        //create a fetch request, telling it about the entity
        let fetchRequest: NSFetchRequest = BookingsTime.fetchRequest()
        let context = getContext()
        
        
        do {
            //go get the results
            let searchResults: [AnyObject] = try context.fetch(fetchRequest)
            
            let prediction = NSPredicate(format: "bid == %@",bookingID)
            
            let results: [AnyObject] = (searchResults as NSArray).filtered(using: prediction) as [AnyObject]
         
                return results as! [BookingsTime]
            }
        catch {
            print("Error with request: \(error)")
            return []
        }
    }

}
