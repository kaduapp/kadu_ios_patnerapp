//
//  BookingsTime+CoreDataProperties.swift
//  
//
//  Created by Rahul Sharma on 27/06/17.
//
//

import Foundation
import CoreData


extension BookingsTime {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BookingsTime> {
        return NSFetchRequest<BookingsTime>(entityName: "BookingsTime")
    }

    @NSManaged public var bid: String?
    @NSManaged public var bookingTime: String?
    @NSManaged public var loadWaitingTime: String?
    @NSManaged public var startLat: String?
    @NSManaged public var startLog: String?

}
