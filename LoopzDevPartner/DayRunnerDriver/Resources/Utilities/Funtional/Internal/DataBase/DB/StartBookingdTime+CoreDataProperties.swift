//
//  StartBookingdTime+CoreDataProperties.swift
//  
//
//  Created by Rahul Sharma on 27/06/17.
//
//

import Foundation
import CoreData


extension StartBookingdTime {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StartBookingdTime> {
        return NSFetchRequest<StartBookingdTime>(entityName: "StartBookingdTime")
    }

    @NSManaged public var bid: String?
    @NSManaged public var endLat: String?
    @NSManaged public var endLog: String?
    @NSManaged public var startTripTime: String?
    @NSManaged public var unloadWaitingTime: String?

}
