//
//  AppConstants.swift
//  UFly
//
//  Created by 3Embed on 27/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import AWSS3

class AppConstants: NSObject {
    enum OrderStatus:Int {
        case New                = 1
        case ManagerCancel      = 2
        case ManagerReject      = 3
        case ManagerAccept      = 4
        case OrderReady         = 5
        case OrderPicked        = 6
        case OrderCompleted     = 7
        case AcceptedByDriver   = 8
        case RejectedByDriver   = 9
        case OntheWay           = 10
        case Arrived            = 11
        case DeliveryStart      = 12
        case ReachedLocation    = 13
        case Completed          = 14
        case Submitted          = 15
    }
    static let disposeBag = DisposeBag()
    
    static let appBundleName                = Bundle.main.infoDictionary?["CFBundleName"] as! String
    static let DarkSplash                   =  false
    
    
    
    //Stripe Keys
    static var StripeKey:String {
        guard let appStripe = UserDefaults.standard.value(forKey: "stripeKey") else {
            return "pk_test_IBYk0hnidox7CDA3doY6KQGi"
        }
        return appStripe as! String
    }
    
    //Cart
    static var CartCount                = 0
    static var TotalCartPrice:Float     = 0
    
    static var Reachable                = true
    
}


//Amazon Keys
struct AmazonKeys {
    static let AmazonAccessKey      = "AKIAJBUJT7INDVJYSSDQ"
    static let AmazonSecretKey      = "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"
    static let Bucket               = "grocer"
    static let Zone                 = AWSRegionType.USEast1
    static let Url                  = "https://s3.amazonaws.com/"
}

//DateFormats
struct DateFormat {
    static let DateAndTimeFormatServer                  = "yyyy-MM-dd HH:mm:ss"
    static let DateAndTimeFormatServerGET               = "yyyyMMddHHmmss"
    static let DateFormatToDisplay                      = "dd MMM yyyy"
    static let DateFormatServer                         = "yyyy-MM-dd"
    static let TimeFormatServer                         = "HH:mm"
    static let TimeFormatToDisplay                      = "hh:mm a"
}

/// Database
struct DataBase {
    static let CartCouchDB                              = "cartDB"
    static let AddressCouchDB                           = "addressDB"
    static let SavedAddressCouchDB                      = "savedAddressDB"
    static let MYDB                                     = "mydb"
}

struct DBStructure {
    var customerName        = "DB"
    var value:[AnyObject]   = []
    var nameKey             = "Name"
    var valueKey            = "Value"
}
