//
//  StringConstants.swift
//  UFly
//
//  Created by 3Embed on 27/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class StringConstants: NSObject {
    //VNHCountryPicker
    static let TypeCountry   = NSLocalizedString("Type Country", comment: "Type Country")
    
    //checkout
    static let DeliverAt            = NSLocalizedString("Deliver At", comment: "Deliver At")
    static let DeliveryTime         = NSLocalizedString("Delivery Time", comment: "Delivery Time")
    static let Cash                 = NSLocalizedString("CASH", comment: "CASH")
    static let Card                 = NSLocalizedString("QUICK CARD", comment: "QUICK CARD")
    static let Now                  = NSLocalizedString("Now", comment: "Now")
    static let NowExtend = NSLocalizedString("Now orders are meant to place orders where the courier delivers your order ASAP.", comment: "Now orders are meant to place orders where the courier delivers your order ASAP.")
    static let CancelUpper          = NSLocalizedString("CANCEL", comment: "CANCEL")
    static let Later                = NSLocalizedString("Schedule", comment: "Schedule")
    static let LaterExtend          = NSLocalizedString("Scheduled orders are meant to place orders where the courier delivers your order at the selected delivery slot.", comment: "Scheduled orders are meant to place orders where the courier delivers your order at the selected delivery slot.")
    static let PaymentMethod        = NSLocalizedString("Payment Method", comment: "Payment Method")
    static let SelectPayment        = NSLocalizedString("Select Payment", comment: "Select Payment")
    static let AddToList            = NSLocalizedString("Add To List", comment: "Add To List")
    
    //Cart
    static let DeliveryCharge       = NSLocalizedString("Delivery Charge", comment: "Delivery Charge")
    static let Cart                 = NSLocalizedString("Cart", comment: "Cart")
    static let AddNewItems          = NSLocalizedString("Add new items", comment: "Add new items")
    static let Store                = NSLocalizedString("Store", comment: "Store")
    static let Stores                = NSLocalizedString("Stores", comment: "Stores")
    
    static let Items                = NSLocalizedString("Items", comment: "Items")
    static let Item                = NSLocalizedString("Item", comment: "Item")
    static let TotalAmount         = NSLocalizedString("Cart Total", comment: "Cart Total")
    
    static let Checkout = NSLocalizedString("CHECKOUT", comment: "CHECKOUT")
    static let CheckoutLower = NSLocalizedString("Checkout", comment: "Checkout")
    static let EmptyCart = NSLocalizedString("No Items In Your Cart", comment: "No items in your cart")
    
    //Payment ViewController
    static let AddNewCard          = NSLocalizedString("ADD NEW CARD", comment: "ADD NEW CARD")
    
    static let AddToWallet         = NSLocalizedString("Add money to wallet", comment: "Add money to wallet")
    static let Passbook            = NSLocalizedString("Passbook", comment: "Passbook")
    static let SavedCards          = NSLocalizedString("Saved Cards", comment: "Saved Cards")
    static let ChooseCards          = NSLocalizedString("Choose Your Card", comment: "Choose Your Card")
    
    
    //inviteVC
    static let ShareText                        = NSLocalizedString("\nJoin %@ today and download our app with refferal code: %@  and get access to some amazing Personalized Deals just for you !", comment: "\nJoin %@ today and download our app with refferal code: %@  and get access to some amazing Personalized Deals just for you !")
    static let YourRefCodeIs                    = NSLocalizedString("YOUR REFFERAL CODE IS ", comment: "YOUR REFFERAL CODE IS")
    static let Invite                           = NSLocalizedString("INVITE", comment: "INVITE")
    
    //Common
    static let Done                             = NSLocalizedString("DONE", comment: "DONE")
    static let DoneLower                             = NSLocalizedString("Done", comment: "Done")
    //LaunchVC
    static let DetectYourLocation               = NSLocalizedString("DETECT MY LOCATION", comment: "DETECT MY LOCATION")
    static let SetLocation                      = NSLocalizedString("Set Location", comment: "Set Location")
    static let Manually                         = NSLocalizedString("Manually", comment: "Manually")
    static let RecentLocation                   = NSLocalizedString("Recent Address", comment: "Recent Address")
    static let ResultLocation                   = NSLocalizedString("Result", comment: "Result")
    
    //Select Location
    static let DeliveryLocation                 = NSLocalizedString("DELIVERY LOCATION", comment: "DELIVERY LOCATION")
    static let EmptyLocLabelText                = NSLocalizedString("\nWe need to make sure you are in %@ Turn on the location services to get started", comment: "W\nWe need to make sure you are in %@ Turn on the location services to get started")
    
    //Login
    static let ClickHereToSignUp    = NSLocalizedString("Click here to Sign Up", comment: "Click here to Sign Up")
    static let Login                = NSLocalizedString("Login", comment: "Login")
    static let Continue             = NSLocalizedString("CONTINUE", comment: "CONTINUE")
    static let LiveFor              = NSLocalizedString("LOGIN", comment: "LOGIN")
    static let EnterPh              = NSLocalizedString("Enter your phone number to proceed", comment: "Enter your phone number to proceed")
    
    
    static let ForgotPassword       = NSLocalizedString("Forgot Password?", comment:"Forgot Password?")
    static let EmailOrPhone         = NSLocalizedString("Phone Number", comment: "Phone Number")
    static let signUp               = NSLocalizedString("SIGN UP", comment: "SIGN UP")
    static let GotoSignUp           = NSLocalizedString("Dont you have an account ? Sign Up", comment: "Dont you have an account ? Sign Up")
    static let PhoneNumber          = NSLocalizedString("PHONE NUMBER", comment: "PHONE NUMBER")
    static let Password             = NSLocalizedString("PASSWORD", comment: "PASSWORD")
    static let PhoneNumMissing      = NSLocalizedString("Please Enter Your Phone Number", comment: "Please Enter Your Phone Number")
    static let PasswordMissing      = NSLocalizedString("Password length should be minimum 6", comment: "Password length should be minimum 6")
    
    
    //ForgotPassword
    static let Email                = NSLocalizedString("EMAIL ADDRESS", comment: "EMAIL ADDRESS")
    static let OR                   = NSLocalizedString("OR", comment: "OR")
    static let Next                = NSLocalizedString("NEXT", comment: "NEXT")
    static let EnterYourMobNum     = NSLocalizedString("Enter your mobile Number / Email address and you will get verification code / link to reset password", comment: "Enter your mobile Number / Email address and you will get verification code / link to reset password")
    static let ForgotPasswordHead   = NSLocalizedString("FORGOT PASSWORD", comment: "FORGOT PASSWORD")
    
    //OTP ViewController
    static let VerifyCode           = NSLocalizedString("VERIFICATION CODE", comment: "VERIFICATION CODE")
    static let Verify               = NSLocalizedString("VERIFY AND PROCEED", comment: "VERIFY AND PROCEED")
    static let VerifyNum            = NSLocalizedString("VERIFY NUMBER", comment: "VERIFY NUMBER")
    static let OtpTitle             = NSLocalizedString("Verification code sent to", comment:"Verification code sent to")
    static let ResendOtp            = NSLocalizedString("Didn’t get code? Resend", comment: "Didn’t get code? Resend")
    static let Resend               = NSLocalizedString("Resend", comment: "Resend")
    
    //UpdatePasswordVC
    static let YourNewPW            = NSLocalizedString("YOUR NEW PASSWORD", comment: "YOUR NEW PASSWORD")
    static let EnterNewPW           = NSLocalizedString("ENTER NEW PASSWORD", comment: "ENTER NEW PASSWORD")
    static let ReEnterPW            = NSLocalizedString("RE-ENTER PASSWORD", comment: "RE-ENTER PASSWORD")
    static let Update               = NSLocalizedString("UPDATE", comment: "UPDATE")
    static let MobileNumVerified    = NSLocalizedString("Mobile Number Verified Enter Your New Password", comment: "Mobile Number Verified Enter Your New Password")
    static let PWMisMatch          =  NSLocalizedString("Password mis-matched", comment: "Password mis-matched")
    
    
    //Signup
    static let Refferal             = NSLocalizedString("REFERRAL CODE", comment: "REFERRAL CODE")
    static let RefferalCode         = NSLocalizedString("I have a refferal code", comment: "I have a refferal code")
    static let Name                 = NSLocalizedString("NAME", comment: "NAME")
    
    static let Terms                = NSLocalizedString("By creating this account, you agree to our Terms & Conditions.", comment: "By creating this account, you agree to our Terms & Conditions.")
    static let TAndC             = NSLocalizedString("Terms & Conditions.", comment:"Terms & Conditions.")
    
    //Upload id
    static let Upload               =  NSLocalizedString("Upload", comment: "Upload")
    static let Skip                 = NSLocalizedString("Skip", comment: "Skip")
    static let IDCard               = NSLocalizedString("IDENTITY CARD", comment: "IDENTITY CARD")
    static let MMJCard              = NSLocalizedString("MMJ CARD", comment: "MMJ CARD")
    static let Uploaded             = NSLocalizedString("Uploaded", comment: "Uploaded")
    static let YourIdentity         = NSLocalizedString("YOUR IDENTITY", comment: "YOUR IDENTITY")
    static let UploadId             = NSLocalizedString("UPLOAD IDENTITY", comment: "UPLOAD IDENTITY")
    static let UploadIDMMJ          = NSLocalizedString("Upload your identity & MMJ card", comment: "Upload your identity & MMJ card")
    static let UploadIDTitle        = NSLocalizedString("Upload any government issued identity Card.", comment: "Upload any government issued identity Card.")
    static let UploadMMj            = NSLocalizedString("Upload your MMJ card.", comment: "Upload your MMJ card.")
    static let GetMMJ               = NSLocalizedString("Don’t have a MMJ card? Get one", comment: "Don’t have a MMJ card? Get one")
    static let GetOne               = NSLocalizedString("Get one", comment: "Get one")
    static let UploadVCBottomText   = NSLocalizedString("Thank you for submiting the document, our account verification team will validate the document and will send notification when verification compleated", comment: "Thank you for submiting the document, our account verification team will validate the document and will send notification when verification compleated")
    
    //Home
    static let SearchData = NSLocalizedString("Search for product or dispensaries", comment: "Search for product or dispensaries")
    static let EveryDayLowPrice  = NSLocalizedString("Trending Products", comment: "Trending Products")
    static let DispensariesSearch  = NSLocalizedString("DISPENSARIES", comment: "DISPENSARIES")
    
    //Profile
    static let Logout                                   = NSLocalizedString("Logout", comment: "Logout")
    static let ManageAddress                            = NSLocalizedString("Manage Addresses", comment: "Manage Addresses")
    static let Payment                                  = NSLocalizedString("Payments", comment: "Payments")
    static let Favourite                                       = NSLocalizedString("Favorites", comment: "Favorites")
    
    static let Offers                                  = NSLocalizedString("Offers", comment: "Offers")
    static let Help                                  = NSLocalizedString("Help", comment: "Help")
    static let ReferFriends                                  = NSLocalizedString("Refer Your Friends", comment: "Refer Your Friends")
    static let Hi                                       = NSLocalizedString("Hi", comment: "Hi")
    static let ConfirmSave                           = NSLocalizedString("Do you want to Save Changes", comment: "Do you want to Save Changes")
    static let Warning                              = NSLocalizedString("Warning" , comment: "Warning")
    
    //Address
    static let GettingAddress                   = NSLocalizedString("Getting Address", comment: "Getting Address")
    static let DeletingAddress                   = NSLocalizedString("Deleting Address", comment: "Deleting Address")
    static let AddingAddress                    = NSLocalizedString("Adding Address", comment: "Adding Address")
    static let Home             = NSLocalizedString("Home", comment:"Home")
    static let Work             = NSLocalizedString("Work", comment:"Work")
    static let Others             = NSLocalizedString("Others", comment:"Others")
    static let AddNewAddress    = NSLocalizedString("ADD NEW ADDRESS", comment: "ADD NEW ADDRESS")
    static let SaveAddress             = NSLocalizedString("Save Address", comment:"Save Address")
    static let UpdateAddress             = NSLocalizedString("Update Address", comment:"Update Address")
    static let TagMissing                   = NSLocalizedString("Please Enter a Address Tag", comment: "Please Enter a Address Tag")
    static let FlatMissing                   = NSLocalizedString("Please Enter Flat Number", comment: "Please Enter Flat Number")
    static let EmptyAddress                 = NSLocalizedString("No Saved Addresses", comment: "No Saved Addresses")
    static let SaveDeliveryLoc             = NSLocalizedString("Delivery Address", comment: "Delivery Address")
    static let Address         = NSLocalizedString("ADDRESS", comment:"ADDRESS")
    
    //Item Listing
    static let CBD                                             = NSLocalizedString("% CBD", comment: "% CBD")
    static let THC                                             = NSLocalizedString("% THC", comment: "% THC")
    static let ViewAll              = NSLocalizedString("View All", comment: "View All")
    static let NearistDispensary     = NSLocalizedString("Nearest Dispensaries", comment: "Nearest Dispensaries")
    static let MileAway              = NSLocalizedString("MILES AWAY", comment: "MILES AWAY")
    static let Rating                = NSLocalizedString("RATING", comment: "RATING")
    static let Reviews               = NSLocalizedString("ESTIMATED TIME", comment: "ESTIMATED TIME")
    static let AddCart               = NSLocalizedString("Add", comment: "Add")
    static let AddToCart             = NSLocalizedString("Add To Cart", comment: "Add To Cart")
    static let RateAndReview         = NSLocalizedString("Reviews", comment: "Reviews")
    static let ExtraCharges          = NSLocalizedString("Extra charges may apply", comment: "Extra charges may apply")
    
    static let Menu                  = NSLocalizedString("MENU", comment: "MENU")
    static let GotIt                 = NSLocalizedString("GOT IT", comment: "GOT IT")
    static let ViewCart              = NSLocalizedString("View Cart", comment: "View Cart")
    static let FixedApplicable      = NSLocalizedString("fixed fee applicable", comment: "fixed fee applicable")
    static let FreeDelivery        = NSLocalizedString("away from free delivery from this store", comment: "away from free delivery from this store")
    
    //Checkout
    static let SelectAddress            = NSLocalizedString("Select Address", comment: "Select Address")
    static let PlaceOrder               = NSLocalizedString("Placing Order",comment: "Placing Order")
    static let DeliverThisOrder         = NSLocalizedString("Deliver This Order",comment: "Deliver This Order")
    
    
    //Alerts
    static let Error                            = NSLocalizedString("Error", comment: "Error")
    static let Success                          = NSLocalizedString("Success", comment: "Success")
    
    static let NoCamera                                         = NSLocalizedString("No Camera", comment: "No Camera")
    static let ChoosePhoto                                        = NSLocalizedString("Choose Photo", comment: "Choose Photo")
    static let TakePhoto                                        = NSLocalizedString("Take Photo", comment: "Take Photo")
    static let PickFromLibrary                                  = NSLocalizedString("Pick From Library", comment: "Pick From Library")
    static let Remove                                           =  NSLocalizedString("Remove", comment: "Remove")
    static let Cancel                                           = NSLocalizedString("Cancel", comment: "Cancel")
    static let NoCameraMsg                                      = NSLocalizedString("Sorry, this device has no camera", comment: "Sorry, this device has no camera")
    static let OK                                               = NSLocalizedString("OK", comment: "OK")
    static let PhoneNumberAvailable                                      = NSLocalizedString("Please enter phone number", comment: "Please enter phone number")
    static let NameAvailable                                             = NSLocalizedString("Please enter your name", comment: "Please enter your name")
    static let PasswordAvailable                                         = NSLocalizedString("Please enter your password", comment: "Please enter your password")
    static let EmailAvailable                                            = NSLocalizedString("Please enter your email", comment: "Please enter your email")
    static let ValidEmail                                       = NSLocalizedString("Please enter valid email", comment: "Please enter valid email")
    static let ValidPhoneNumber                                 = NSLocalizedString("Please enter valid phone number" , comment: "Please enter valid phone number")
    static let EnterOtp                                 = NSLocalizedString("Please enter verification code", comment: "Please enter verification code")
    static let SessionInvalid                       =   NSLocalizedString("Your session has expired,\n please login again", comment: "Your session has expired,\n please login again")
    
    //Progress
    static let ValidateLocation                 = NSLocalizedString("Validating Location", comment: "Validating Location")
    static let Loading                          = NSLocalizedString("Loading", comment: "Loading")
    static let LoggingIn                        = NSLocalizedString("Logging In", comment: "Logging In")
    static let SigningUp                        = NSLocalizedString("Signing Up", comment: "Signing Up")
    static let GetCart                          = NSLocalizedString("Fetching Cart", comment: "Fetching Cart")
    static let UpdateCart                       = NSLocalizedString("Updating Cart", comment: "Updating Cart")
    static let Updating                         = NSLocalizedString("Updating", comment: "Updating")
    static let Canceling                        = NSLocalizedString("Canceling", comment: "Canceling")
    static let Delete                           = NSLocalizedString("Deleting", comment: "Deleting")
    static let DeleteCaps                           = NSLocalizedString("DELETE", comment: "DELETE")
    
    //Titles
    static let ProfileTitle                    = NSLocalizedString("Profile", comment: "Profile")
    static let AddressListTitle                = NSLocalizedString("Saved Addresses", comment: "Saved Addresses")
    static let InviteFriends                   = NSLocalizedString("SHARE", comment: "SHARE")
    static let ReferralsTitle                  = NSLocalizedString("Referrals", comment: "Referrals")
    static let PaymentOptions                  = NSLocalizedString("Payment Options", comment: "Payment Options")
    static let WishList                        = NSLocalizedString("Wish List", comment: "Wish List")
    static let QuickCardTitle                  = NSLocalizedString("QuickCard", comment: "QuickCard")
    static let Cards                           = NSLocalizedString("Cards", comment: "Cards")
    
    //ItemDetails
    static let MilesAwayLower     = NSLocalizedString("miles away", comment: "miles away")
    static let Compare            = NSLocalizedString("Compare", comment: "Compare")
    static let Unit               = NSLocalizedString("Unit", comment: "Unit")
    static let Discription        = NSLocalizedString("Description", comment: "Description")
    static let Effects            = NSLocalizedString("Effects", comment: "Effects")
    
    static let Relaxed            = NSLocalizedString("Relaxed", comment: "Relaxed")
    static let Uplifted           = NSLocalizedString("Uplifted", comment: "Uplifted")
    static let Happy              = NSLocalizedString("Happy", comment: "Happy")
    static let Focused            = NSLocalizedString("Focused", comment: "Focused")
    static let Energetic          = NSLocalizedString("Energetic", comment: "Energetic")
    
    //Dispensaries
    static let Dispensaries     = NSLocalizedString("Dispensaries", comment: "Dispensaries")
    static let DeliveredBy      = NSLocalizedString("Delivered By Store", comment: "Delivered By Store")
    static let DeliveryBy       = NSLocalizedString("Delivery by", comment: "Delivery by")
    static let FreeDeliveryText     = NSLocalizedString("Free delivery above", comment: "Free delivery above")
    
    //editProfile
    
    static let Verified         = NSLocalizedString("Verified", comment: "Verified")
    
    //History
    static let TrackOrder       = NSLocalizedString("TRACK ORDER", comment: "TRACK ORDER")
    static let HelpUpper        = NSLocalizedString("HELP", comment: "HELP")
    static let ReOrder          = NSLocalizedString("REORDER", comment: "REORDER")
    static let ActiveOrder      = NSLocalizedString("ACTIVE ORDER", comment: "ACTIVE ORDER")
    static let PastOrder        = NSLocalizedString("PAST ORDER", comment: "PAST ORDER")
    
    //TrackingVCcomment
    static let  OrderConfirmed    =  NSLocalizedString("Order Confirmed", comment: "Order Confirmed")
    static let  DriverAssigned    =  NSLocalizedString("Driver Assigned", comment: "Driver Assigned")
    static let  DriverAtStore    =  NSLocalizedString("Driver at Store", comment: "Driver at Store")
    static let  OrderPickedUp    =  NSLocalizedString("Order Picked Up", comment: "Order Picked Up")
    static let  DriverLocation    =  NSLocalizedString("Driver at Delivery Location", comment: "Driver at Delivery Location")
    static let  OrderDelivered    =  NSLocalizedString("Order Delivered", comment: "Order Delivered")
    static let NewOrder             = NSLocalizedString("New Order", comment: "New Order")
    static let AcceptedByManager    = NSLocalizedString("Accepted By Manager", comment: "Accepted By Manager")
    static let RejectedByManager    = NSLocalizedString("Rejected By Manager", comment: "Rejected By Manager")
    static let CancelledByManager    = NSLocalizedString("Cancelled By Manager", comment: "Cancelled By Manager")
    static let OrderReady     = NSLocalizedString("Order Ready", comment: "Order Ready")
    static let OrderPicked    = NSLocalizedString("Order Picked", comment: "Order Picked")
    static let OrderCompleted = NSLocalizedString("Order Completed", comment: "Order Completed")
    static let DriverAccept     = NSLocalizedString("Driver Accept", comment: "Driver Accept")
    static let DriverCancelled  = NSLocalizedString("Cancelled By Driver", comment: "Cancelled By Driver")
    static let DriverOnTheWay   = NSLocalizedString("Driver On The Way", comment: "Driver On The Way")
    static let DriverReached    = NSLocalizedString("Driver Reached", comment: "Driver Reached")
    static let Delivered            = NSLocalizedString("Delivered", comment: "Delivered")
    static let Cancelled            = NSLocalizedString("Cancelled", comment: "Cancelled")
    static let YourOrderConfirmed  =  NSLocalizedString("Your order has been confirmed and will be delivered shortly.", comment: "Your order has been confirmed and will be delivered shortly.")
    
    //OrderDetail
    static let ItemTotal         = NSLocalizedString("Item Total", comment: "Item Total")
    static let Total             = NSLocalizedString("Total", comment: "Total")
    static let Details           = NSLocalizedString("DETAILS", comment: "DETAILS")
    static let GST               = NSLocalizedString("GST", comment: "GST")
    
    //No internet Popup
    static let NoInternet        = NSLocalizedString("Seems like you are not connected to the internet.", comment: "Seems like you are not connected to the internet.")
    
    //addNewCard
    static let CardUpper =  NSLocalizedString("CARD", comment: "CARD")
    static let Expires  = NSLocalizedString("EXPIRES", comment: "EXPIRES")
    static let Expired  = NSLocalizedString("Expired", comment: "Expired")
    static let CardNumber = NSLocalizedString("CARD NUMBER", comment: "CARD NUMBER")
    static let CVV     = NSLocalizedString("CVV", comment: "CVV")
    
    
    //////for Ufly Changes
    static let UflyMoney           = NSLocalizedString("LD Money", comment: "LD Money")
    static let NotAutherized                = NSLocalizedString("Please Setup Loopz Money", comment: "Please Setup Loopz Money")
    static let UnableToLogin =  NSLocalizedString("I am unable to log in on Loopz", comment: "I am unable to log in on Loopz")
    static let TermsOfUseForLoopz  = NSLocalizedString("Terms of Use for Loopz ON-TIME / Assured", comment: "Terms of Use for Loopz ON-TIME / Assured")
    static let CreateAnAcc          = NSLocalizedString("Create a new Loopz Delivery account.", comment: "Create a new Loopz Delivery account.")
    static let PaidBy            = NSLocalizedString("Paid via Loopz money", comment: "Paid via Loopz money")
    static let BitPay    = NSLocalizedString("BitPay", comment: "BitPay")
    static let PatentPending        =  NSLocalizedString("Patent Pending", comment: "Patent Pending")
    static let Wallet     =  NSLocalizedString("Wallet", comment: "Wallet")
    
    // **************************************************************************************************************************************************
    //not added localized file      //check terms and conditions in localized strings
    
    static let CreateNewList   =   NSLocalizedString("CREATE A NEW LIST", comment: "CREATE A NEW LIST")
    static let EnterListName   =   NSLocalizedString("Please Enter The List Name", comment: "Please Enter The List Name")
    static let Instructions    =   NSLocalizedString("Instructions", comment: "Instructions")
    static let Favouritei      =   NSLocalizedString("Favorite", comment: "Favorite")
    static let Favourited     =  NSLocalizedString("Favorited", comment: "Favorited")
    static let ChangeQty       =   NSLocalizedString("Change Qty", comment: "Change Qty")
    static let InCart          =  NSLocalizedString("In Cart", comment: "In Cart")
    
    static let Edit            =  NSLocalizedString("Edit", comment: "Edit")
    static let Change          =  NSLocalizedString("Change Password", comment: "Change Password")
    static let UpdateEmailPh   = NSLocalizedString("Update your phone number and email address.", comment: "Update your phone number and email address.")
    static let EmailAddress     =   NSLocalizedString("EMAIL ADDRESS", comment: "EMAIL ADDRESS")
    static let OldPassword      =   NSLocalizedString("OLD PASSWORD", comment: "OLD PASSWORD")
    static let NewPassword      =   NSLocalizedString("NEW PASSWORD", comment: "NEW PASSWORD")
    static let ReEnterPassword        =   NSLocalizedString("REENTER PASSWORD", comment: "REENTER PASSWORD")
    static let IdentityCard     = NSLocalizedString("Identity Card", comment: "Identity Card")
    static let UpdateLower = NSLocalizedString("Update", comment: "Update")
    
    static let HelpAndSupport      =  NSLocalizedString("Help & Support", comment: "Help & Support")
    static let HelpWithOrder  = NSLocalizedString("Help with this order", comment: "Help with this order")
    static let HelpWithQueries = NSLocalizedString("Help with other queries", comment: "Help with other queries")
    static let IssueWithPrevious = NSLocalizedString("Issue with previous orders", comment: "Issue with previous orders")
    static let GeneralQueries  = NSLocalizedString("General Queries", comment: "General Queries")
    static let Legal =  NSLocalizedString("Legal", comment: "Legal")
    
    static let GeneralQueriesUpper  = NSLocalizedString("General Queries", comment: "General Queries")
    static let LegalUpper =  NSLocalizedString("Legal", comment: "Legal")
    
    // Help and Support
    //help with this order
    static let HaveNotReceived   = NSLocalizedString("I haven’t receved this order", comment: "I haven’t receved this order")
    static let MissingOrder      = NSLocalizedString("Items are missing from my order", comment: "Items are missing from my order")
    static let DifferentOrder    = NSLocalizedString("Items are different from what i orderd", comment: "Items are different from what i orderd")
    static let SpillageOrder    = NSLocalizedString("I have packaging or spillage with this order", comment: "I have packaging or spillage with this order")
    static let Received = NSLocalizedString("I have received bad quality item", comment: "I have received bad quality item")
    static let PaymentQuery = NSLocalizedString("I have payment & refund related queries for this order", comment: "I have payment & refund related queries for this order")
    //Quick Card
    
    static let InsufficientBalance                = NSLocalizedString("Insufficient Balance QuickCard", comment: "Insufficient Balance")
    
    //    // general queries
    static let PlaceOrderQuery =  NSLocalizedString("I have query related to placing an order", comment: "I have query related to placing an order")
    
    static let PaymentRelatedQ =  NSLocalizedString("I have a payment or refund related query", comment: "I have a payment or refund related query")
    static let CouponQuery =  NSLocalizedString("I have a coupon related query", comment: "I have a coupon related query")
    
    
    // legal
    
    static let TermsOfUse = NSLocalizedString("Terms of Use", comment: "Terms of Use")
    static let PrivacyPolicy = NSLocalizedString("Privacy Policy", comment: "Privacy Policy")
    static let Cancellations = NSLocalizedString("Cancellations and Refunds", comment: "Cancellations and Refunds")
    
    
    //Confirm Order
    static let PlaceTheOrder = NSLocalizedString("PLACE THE ORDER", comment: "PLACE THE ORDER")
    static let Schedule  = NSLocalizedString("Schedule", comment: "Schedule")
    static let OrderTimeTitle = NSLocalizedString("When do you want to get your order?" , comment: "When do you want to get your order?")
    static let YourPamentBreak = NSLocalizedString("Your Payment Breakdown", comment: "Your Payment Breakdown")
    static let DeliveryType     = NSLocalizedString("Order Type", comment: "Order Type")
    static let SelectHow     = NSLocalizedString("How do you want to get your order?", comment: "How do you want to get your order?")
    static let Pickup     = NSLocalizedString("Pickup", comment: "Pickup")
    static let Delivery  = NSLocalizedString("Delivey", comment: "Delivery")
    
    static let DeliveryLocationLower                 = NSLocalizedString("Delivery Location", comment: "Delivery Location")
    
    static let Discount  = NSLocalizedString("Discount", comment: "Discount")
    static let Promocode =  NSLocalizedString("Promocode", comment: "Promocode")
    static let ConfirmOrder = NSLocalizedString("Confirm Order", comment: "Confirm Order")
    
    static let SelectDate = NSLocalizedString("Select Date", comment: "Select Date")
    static let SelectSlot = NSLocalizedString("Select Time", comment: "Select Time")
    static let ChangeType = NSLocalizedString("Change", comment: "Change")
    
    //payment methods
    static let CashLower = NSLocalizedString("Cash", comment: "Cash")
    static let QuickCard = NSLocalizedString("Quick Card", comment: "Quick Card")
    static let Money   = NSLocalizedString("Money", comment: "Money")
    static let CardLower = NSLocalizedString("Card", comment: "Card")
    static let Connect   = NSLocalizedString("Connect", comment: "Connect")
    static let Amount   =  NSLocalizedString("Amount", comment: "Amount")
    static let OrderId  = NSLocalizedString("Order Id:", comment: "Order Id:")
    
    static let KeepChange = NSLocalizedString("Please keep exact change handy to help us serve you better.", comment: "Please keep exact change handy to help us serve you better.")
    
    static let AddMoney = NSLocalizedString("Add Money", comment: "Add Money")
    static let CurrentLocation = NSLocalizedString("Current Location", comment: "Current Location")
    
    //search
    
    
    //Promocode
    static let PromocodeMissing = NSLocalizedString("Enter Your Promocode", comment: "Enter Your Promocode")
    static let Apply            = NSLocalizedString("Apply", comment: "Apply")
    static let AddressLower         = NSLocalizedString("Address", comment:"Address")
    static let Products = NSLocalizedString("PRODUCTS", comment: "PRODUCTS")
    static let Suggestions = NSLocalizedString("Suggestions", comment: "Suggestions")
    static let RelatedTo = NSLocalizedString("Related to", comment: "Related to")
    static let RecentSearches = NSLocalizedString("Recent Searches", comment: "Recent Searches")
    static let NoMatchFound = NSLocalizedString("No match found for", comment: "No match found for")
    static let ETA = NSLocalizedString("ETA", comment: "ETA")
    
    //addAddress
    static let AddressAlreadySaved  = NSLocalizedString("This address is already saved", comment: "This address is already saved")
    
    static let ApplyCoupon = NSLocalizedString("Apply Coupon", comment: "Apply Coupon")
    static let Coupon     =  NSLocalizedString("Coupons", comment: "Coupons")
    
    
    //Cancel Cart
    static let RemoveCartWarning    = NSLocalizedString("On changing your delivery location, You will lose your cart", comment: "On changing your delivery location, You will lose your cart")
    static let Message  = NSLocalizedString("Message", comment: "Message")
    
    
    static let NoDataAvailable       = NSLocalizedString("No Data Available", comment: "No Data Available")
    static let Free                  = NSLocalizedString("Free", comment: "Free")
    static let FinalAmount = NSLocalizedString("Final amount to be paid", comment: "Final amount to be paid")
    static let PlacedOn              = NSLocalizedString("Placed on", comment: "Placed on")
    static let FinalPaid             = NSLocalizedString("Final paid amount", comment: "Final paid amount")
    static let ASAP                  = NSLocalizedString("ASAP", comment: "ASAP")
    static let Scheduled             = NSLocalizedString("Scheduled On", comment: "Scheduled On")
    static let ValidThru             = NSLocalizedString("Valid Thru", comment: "Valid Thru")
    static let MakeAsDefault         = NSLocalizedString("Make As Default", comment: "Make As Default")
    static let WooHoo                       = NSLocalizedString("WooHoo", comment: "WooHoo")
    static let AppliedPromocode          = NSLocalizedString("Promocode %@ has been applied to your cart", comment: "Promocode %@ has been applied to your cart")
    static let RemovePromocode          = NSLocalizedString("Are you sure to remove promocode?", comment: "Are you sure to remove promocode?")
    static let Save          = NSLocalizedString("Save", comment: "Save")
    static let SomethingWentWrong          = NSLocalizedString("Something Went Wrong\nPlease try after sometime.", comment: "Something Went Wrong\nPlease try after sometime.")
    static let CreditAndDebit      =  NSLocalizedString("Credit & Debit Cards", comment: "Credit & Debit Cards")
    static let StartShopping    =  NSLocalizedString("Start Shopping", comment: "Start Shopping")
    static let FoodCards           = NSLocalizedString("Food Cards", comment: "Food Cards")
    static let LinkAccount        =  NSLocalizedString("LINK ACCONT", comment: "LINk ACCOUNT")
    static let PayOnDelivery      = NSLocalizedString("Pay On Delivery", comment: "Pay On Delivery")
    static let PayByCash      =  NSLocalizedString("Pay", comment: "Pay")
    static let PayByQC      =  NSLocalizedString("Pay", comment: "Pay")
    static let PayByCard    =  NSLocalizedString("Pay", comment: "Pay")
    static let AddNewCardText = NSLocalizedString("Now pay your order using Ticket Restaurants Meal Card. Add your card details\n", comment: "Now pay your order using Ticket Restaurants Meal Card. Add your card details\n")
    
    static let ConnectToQC = NSLocalizedString("Connect Your QuickCard To Experience Seamless Payments", comment: "Connect Your QuickCard To Experience Seamless Payments")
    
    static let EmptyFav                 = NSLocalizedString("No Favourite items", comment: "No Favourite items")
    static let EmptyWish                 = NSLocalizedString("No WishList", comment: "No WishList")
    static let EmptyWishItems                 = NSLocalizedString("No WishList Items", comment: "No WishList Items")
}
