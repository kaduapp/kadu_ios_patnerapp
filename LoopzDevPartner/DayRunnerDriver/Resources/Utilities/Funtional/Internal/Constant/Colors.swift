//
//  Colors.swift
//  UFly
//
//  Created by 3Embed on 26/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class Colors: NSObject {
    
    static let AppBaseColor  = "1D0869"
    @available(iOS 11.0, *)
    static let baseColor = Helper.getUIColor(color: Colors.AppBaseColor)
//        UIColor(named: Colors.AppBaseColor)
    
    static let SecondBaseColor                              = "FFFFFF"//White Color
    
    static let SeparatorDark                                = "A3A3A3"
    static let SeparatorLight                               = "DFE4EF"
    static let SeparatorLarge                               = "F5F6F9"
    
    static let PrimaryText                                  = "1A1945"//Black
    static let SectionHeader                                = "2F2E3A"
    static let SeconderyText                                = "7B8091"//Light Secound Head
    
    static let FieldHeader                                  = "A8AAB2"
    static let PlaceHolder                                  = "D8DCE5"

    
    static let PageBackground                               = "E8EBF3"
    static let PickupBtn                                    = "4F86EB"
    static let DeliveryBtn                                  = "48C16A"
    
    
    
    static let SecoundPrimaryText                           = "858B92"//Black
   // static let SecounderyText                               = "747474"//Gray
    static let HeadColor                                    = "C4C4C4"
    static let ScreenBackground                             = "F5F5F5"
    static let AddButton                                    = "4695E6"
    static let DoneBtnNormal                                = "DFE2E7"//
    static let Red                                          = "D95656" // red for cart button
    static let BlueColor                                    = "4695E6" //"4695E6" //payment ,orderDetail
    //static let SectionHeader                                = "4A4A4A" //OrderDetail
    static let Unselector                                   = "B9B9B9" // orderDetail
    static let gotItBtnColor                                = "C9A353" //ItemListinVC BotonBarText
    static let BtnBackground                                = "BBC0CE"
    static let grey                                         = "A8AAB2"
    
    static let Sender                                = Helper.getUIColor(color:"8D30F9")
    static let Receiver                                         = Helper.getUIColor(color:"ece5dd")
}

