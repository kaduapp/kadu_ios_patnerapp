//
//  Helper.swift
//  Dayrunner
//
//  Created by Rahul Sharma on 29/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//


//let activityData = ActivityData(size: CGSize(width: 30,height: 30),
//                                message: "Loading...",
//                                type: NVActivityIndicatorType.ballRotateChase,
//                                color: UIColor.white,
//                                padding: nil,
//                                displayTimeThreshold: nil,
//                                minimumDisplayTime: nil)
//NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)

import UIKit
import AVFoundation
import Stripe
import LatLongToTimezone

enum VersionError: Error {
    case invalidResponse, invalidBundleInfo
}

class Helper: NSObject {
    
    
    /********************************************************/
    //MARK: - METHODS
    /********************************************************/
    
    /// Method to get Country Code
    ///
    /// - Returns: Country code
    
    static var alertPopup:UIAlertController? = nil
    static var shared = Helper()
    static var player: AVAudioPlayer?
    class func getCountyCode() -> String {
        let currentLocale:Locale = (Locale.current as NSLocale) as Locale
        
        return String(describing:currentLocale)
      //  return (currentLocale.object(forKey: .countryCode) as? String)!
    }
    
    
    class func isValidIP(text:String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$", options: .caseInsensitive)
            return regex.firstMatch(in: text, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, text.sorted().count)) != nil
        }
        catch {
            return false
        }
    }
    //Check for app update if available?
    class func isUpdateAvailable() throws -> Bool {
        guard let url = URL(string: APIEndTails.VersionAppStore) else {
            throw VersionError.invalidBundleInfo
        }
        let data = try Data(contentsOf: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            throw VersionError.invalidResponse
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            if Utility.AppVersion == "appVersion" {
                return false
            } else {
                return version != Utility.AppVersion
            }
        }
        throw VersionError.invalidResponse
    }
    
    //This func returns final view controller on screen
    class func finalController() -> UIViewController {
        if let wd = UIApplication.shared.delegate?.window {
            var vc = wd?.rootViewController
            if(vc is UINavigationController) {
                vc = (vc as! UINavigationController).viewControllers.last
            }
            if(vc is UITabBarController) {
                vc = (vc as! UITabBarController).viewControllers?[(vc as! UITabBarController).selectedIndex]
            }
            if(vc is UINavigationController) {
                vc = (vc as! UINavigationController).viewControllers.last
            }
            while(vc?.presentedViewController != nil && (vc?.presentedViewController?.isKind(of: UIViewController.classForCoder()))!) {
                vc = vc?.presentedViewController
                if(vc is UINavigationController){
                    vc = (vc as! UINavigationController).viewControllers.last
                }
            }
            return vc!
        }
        return UIViewController()
    }
    
    class func nullKeyRemoval(data:[String:Any]) -> [String:Any] {
        var dict = data
        let keysToRemove = Array(dict.keys).filter { dict[$0] is NSNull }
        for key in keysToRemove {
            dict.removeValue(forKey: key)
        }
        let keysToUpdate = Array(dict.keys).filter { dict[$0] is [String:Any] }
        for key in keysToUpdate {
            dict.updateValue(nullKeyRemoval(data: dict[key] as! [String : Any]), forKey: key)
        }
        return dict
    }
    
   class func appVersion() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        return "\(version) build \(build)"
    }
    
    class func shadowView(view:UIView,scale: Bool) {
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 12
        view.layer.shadowColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1).cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1.0
    }
   
    //Handling Alert Messages
    class func showAlert(message:String, head:String, type:Int) {
        //        Helper.hidePI()
        //        let alert:CommonAlertView = CommonAlertView.shared
        //        alert.showAlert(message: message, head: head, type: type)
        Helper.hidePI()
        alertPopup?.dismiss(animated: false, completion: nil)
        alertPopup = UIAlertController(title:head, message: message, preferredStyle: UIAlertController.Style.alert)
        alertPopup?.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
        if UIApplication.shared.delegate?.window != nil {
            let vc = Helper.finalController()
            vc.present(alertPopup!, animated: true, completion: nil)
        }
        
        
        
    }
    
    
    //Handling Alert Messages
    class func showAlertReturn(message:String, head:String, type:String, closeHide:Bool,responce:CommonAlertView.ResponceType) {
        Helper.hidePI()
        alertPopup?.dismiss(animated: true, completion: nil)
        alertPopup = UIAlertController(title:head, message: message, preferredStyle: UIAlertController.Style.alert)
        if closeHide == false {
            alertPopup?.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: nil))
        }
        alertPopup?.addAction(UIAlertAction(title: type, style: UIAlertAction.Style.default){ action -> Void in
            CommonAlertView.AlertPopupResponse.onNext(responce)
            // Put your code here
        })
        
        if UIApplication.shared.delegate?.window != nil {
            let vc = Helper.finalController()
            vc.present(alertPopup!, animated: true, completion: nil)
        }
    }
    
    class func systemLanguage() -> String{
        return Locale.preferredLanguages[0]
    }
    
    /// Method to get Country Name
    ///
    /// - Returns: Returns Country Name
    class func getCountryName() -> String {
        
        if let name = (Locale.current as NSLocale).displayName(forKey: .
            countryCode, value: getCountyCode()) {
            // Country name was found
            return name
        }
        else {
            // Country name cannot be found
            return getCountyCode()
        }
    }
    
    /// Method to convert string dictionary to swift dictionary
    ///
    /// - Parameter text: string dictionary response from server
    /// - Returns: parsed swift dictionary [String:Any]
    class func convertToDictionary(text: String) -> [String: Any]? {
        
        if let data = text.data(using: .utf8) {
            
            do {
                
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                
            } catch {
                
                print(error.localizedDescription)
            }
            
        }
        
        return nil
    }
    
    /// Get Fonts Family
    class func fonts() {
        
        for family: String in UIFont.familyNames {
            print("\n==== : Font family : \(family)")
            
            for name: String in UIFont.fontNames(forFamilyName: family) {
                print("************ : \(name)")
            }
        }
    }
    
    /// Array for Object
    ///
    /// - Parameter object: Any Object
    /// - Returns: [AnyObject]
    class func arrayForObj(object: Any?) -> [AnyObject] {
        
        switch object {
            
        case is [AnyObject]: // Array of Type [AnyObject]
            
            return object as! [AnyObject]
            
        case is [Any]: // Array of Type [Any]
            
            return object as! [AnyObject]
            
        default:
            
            return []
        }
    }
    
    class func getStringToDate(value:String,format:String,zone:Bool) -> Date? {
        let formatter = DateFormatter()
        if zone {
            formatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        }
        formatter.locale = Locale.init(identifier: "en_US_POSIX")
        formatter.dateFormat = format
        
        if let myString = formatter.date(from: value) {
            return myString
        }
        return Date()
    }
    
    
    // alertVC
    class func alertVC(title:String, message:String)->UIAlertController {
        let alertController = UIAlertController(title: title as String, message: message as String, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
             (UIApplication.shared.delegate as! AppDelegate).alertWindow.isHidden = true
        }
        //        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
        //            UIAlertAction in
        //            NSLog("Cancel Pressed")
        //        }
        // Add the actions
        alertController.addAction(okAction)
        // alertController.addAction(cancelAction)
        
        return alertController;
    }    
    
    
    /********************************************************/
    //MARK: - Variables
    /********************************************************/
    
    /// Current Date and Time
    static var currentDateTime: String {
        
        let now = Date()
        let dateFormatter = DateFormatter()
        let indianLocale = Locale(identifier: "en_US")
        dateFormatter.locale = indianLocale as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: now)
    }
    
    /// Current Date and Time
    static var saveProfileImg: String {
        
        let now = Date()
        let dateFormatter = DateFormatter()
        let indianLocale = Locale(identifier: "en_US")
        dateFormatter.locale = indianLocale as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd_HH:mm:ss"
        return dateFormatter.string(from: now)
    }

    
    /// Current time stamp
    static var currentTimeStamp: String {
        return String(format: "%0.0f", Date().timeIntervalSince1970 * 1000)
    }
    
    /// Current time Stamp in Int64 format
    static var currentTimeStampInt64: Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    /// Get getCurrentGMTDateTime
    static var currentGMTDateTime: String {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateTimeInIsoFormatForZuluTimeZone: String = dateFormatter.string(from: now)
        return dateTimeInIsoFormatForZuluTimeZone
    }
    
    /// Change the Date format
    ///
    /// - Parameter dateString: Old formate
    /// - Returns: "yyyy-MM-dd HH:mm:ss"
    class func change(_ date: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-YYYY hh:mm a"
        let oldDate = dateFormatter.date(from: date)
        
        let indianLocale = Locale(identifier: "en_US")
        dateFormatter.locale = indianLocale as Locale!
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: oldDate!)
    }
    
    class func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    /********************************************************/
    //MARK: - Time Format
    /********************************************************/
    
    /// Change the Format to 07-Dec-2016
    ///
    /// - Parameter date: Date type
    /// - Returns: Returns 07-Dec-2016 as String
    class func changeDate(_ date: Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy" //22-Nov-2012
        let formattedDateString = dateFormatter.string(from: date)
        return formattedDateString
    }
    
    /// Get Date From String
    ///
    /// - Parameter dateString: String of Date
    /// - Returns: Date
    class func dateFromString(_ dateString: String) -> Date {
        
        let dateFormatter = DateFormatter()
        let date: Date? = dateFormatter.date(from: dateString)
        return date!
    }
    
    /// Change Date format from "yyyy-MM-dd HH:mm:ss" to "dd-MMM-yyyy"
    ///
    /// - Parameter dateString: String of Date
    /// - Returns: "10-DEC-1991"
    class func changeDateFormat(_ dateString: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date: Date? = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        let formattedDateString: String = dateFormatter.string(from: date!)
        return formattedDateString
    }
    
    class func changeDateFormatWithMonth(_ dateString: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date: Date? = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "MMM dd"
        let formattedDateString: String = dateFormatter.string(from: date!)
        return formattedDateString
    }
    
    class func changeDateFormatForHome(_ dateString: String) -> String {

        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: "en_IN")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date: Date? = dateFormatter.date(from: dateString)
        dateFormatter.timeZone = Helper.getTimeZoneFromCurrentLoaction()
        dateFormatter.dateFormat = "MMM dd , hh:mm a"
        let formattedDateString: String = dateFormatter.string(from: date!)
        return formattedDateString
    }
    
    class func yearMonthDateTime(timeStamp:Int64) -> String{
        
        let date = NSDate(timeIntervalSince1970:TimeInterval(timeStamp))
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        df.timeZone = Helper.getTimeZoneFromCurrentLoaction()
        
        let dateString = df.string(from: date as Date)
        return dateString
    }
    
    class func changeDateFormatForHistory(_ dateString: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: "en_IN")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date: Date? = dateFormatter.date(from: dateString)
        dateFormatter.timeZone = Helper.getTimeZoneFromCurrentLoaction()
        dateFormatter.dateFormat = "d MMM yyyy , hh:mm a"
        let formattedDateString: String = dateFormatter.string(from: date!)
        return formattedDateString
    }
    
    
    /********************************************************/
    //MARK: - Current Symbol
    /********************************************************/
    class func getCurrency(_ amount: Float) -> String {
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.locale = Locale.current
        currencyFormatter.maximumFractionDigits = 2
        currencyFormatter.minimumFractionDigits = 2
        currencyFormatter.alwaysShowsDecimalSeparator = true
        currencyFormatter.numberStyle = .currency
        let someAmount: NSNumber = NSNumber(value: amount)
        let currencystring: String? = currencyFormatter.string(from: someAmount)
        
        return currencystring!
    }
    

    //MARK: - Progress Ideicator
    // Show with Default Message
    class func showPI(message: String) {
        
        let activityData = ActivityData(size: CGSize(width: 30,height: 30),
                                        message: message ,
                                        type: NVActivityIndicatorType.ballRotateChase,
                                        color: UIColor.white,
                                        padding: nil,
                                        displayTimeThreshold: nil,
                                        minimumDisplayTime: nil)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)

    }
    // Hide
    class func hidePI() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
      //************ add border shadow*****************//
    class func addBorderShadow(view : UIView) -> UIView {
        
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 5
        view.layer.shadowPath = UIBezierPath(rect: view.bounds).cgPath
        view.layer.shouldRasterize = true
        return view
    }
    
     //************ background color gradient added *****************//
    class func setViewBackgroundGradient(sender: UIView, _ topColor:UIColor, _ bottomColor:UIColor)->UIView {
        
        let gradient = CAGradientLayer()
        
        gradient.frame = sender.bounds
        gradient.colors = [topColor, bottomColor]
        
        sender.layer.insertSublayer(gradient, at: 0)
        return sender
    }
    
    //******Get current Timezone from your lat longs*******//
    class func getTimeZoneFromCurrentLoaction() -> TimeZone? {
//        let manager = LocationManager.shared
        let ud = UserDefaults.standard

        let location = CLLocationCoordinate2D(latitude: CLLocationDegrees(ud.double(forKey:"currentLat")), longitude: CLLocationDegrees(ud.double(forKey:"currentLog")))
        return TimezoneMapper.latLngToTimezone(location)
    }
    
      //************ Add shadow to the view*****************//
    class func shadowView(sender: UIView, width:CGFloat, height:CGFloat ) {
        
        sender.layer.cornerRadius = 4
        // Drop Shadow
        let squarePath = UIBezierPath(rect:CGRect.init(x: 0, y:0 , width: width, height: height)).cgPath
            
        sender.layer.shadowPath = squarePath;
        //sender.layer.shadowRadius = 4
        sender.layer.shadowOffset = CGSize(width: CGFloat(0.5), height: CGFloat(0.5))
        sender.layer.shadowColor = UIColor.lightGray.cgColor
        sender.layer.shadowOpacity = 5.0
        sender.layer.masksToBounds = true
        sender.layer.borderWidth = 1.0
       // sender.layer.borderColor = UIColor.rbg(r: 244, g: 121, b: 62).cgColor
        sender.layer.borderColor = UIColor.white.cgColor
    }
    
      //************ time differene to drop*****************//
    class func getTheTimeDifferenceDrop(_ stringDate: String) -> String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date1: Date? = df.date(from: stringDate)
        let date2 = Date()
        let interval: TimeInterval = date2.timeIntervalSince(date1!)
        var hours = Int(interval) / 3600
        // integer division to get the hours part
        
        
        var minutes: Int = (Int(interval) - (hours * 3600)) / 60
        // interval minus hours part (in seconds) divided by 60 yields minutes
        var timeDiff: String
        if minutes < 0 || hours <= 0 {
            if hours < 0 {
                hours = -(hours)
            }
            minutes = -(minutes)
            timeDiff = "-" + String(describing: hours) + ":" + String(describing: minutes) + " Hrs"
            //  timeDiff = "-\(hours):%02d"
        }
        else {
            timeDiff =  String(describing: hours) + ":" + String(describing: minutes) + " Hrs"
            //  timeDiff = "\(hours):%02d"
        }
        return timeDiff
    }

    class  func validateMobile(value: String) -> Bool {

        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }

       //************ time differene to pickup*****************//
    class func getTheTimeDifference(_ stringDate: String) -> String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date1: Date? = df.date(from: stringDate)
        let date2 = Date()
        let interval: TimeInterval? = date1?.timeIntervalSince(date2)
        var hours = 2 /*Int(interval!) / 3600*/
        // integer division to get the hours part
        let minutes: Int = 3/*(Int(interval!) - (hours * 3600)) / 60*/
        // interval minus hours part (in seconds) divided by 60 yields minutes
        var timeDiff: String
        if minutes < 0 || hours <= 0 {
            
            var min = ""
            var hor = ""
            var days = ""
            if minutes > 1 {
                min = String(describing: minutes) + " Mins "
            }else{
                min = String(describing: minutes) + " Min "
            }
            
            hours = hours - (hours/24)*24
            
            if hours > 1 {
                hor = String(describing: hours) + " Hrs "
            }else{
                hor = String(describing: hours) + " Hr "
            }
            
            if hours/24 > 1 {
                days =  String(describing: hours/24) + " Days "
            }else{
                days =  String(describing: hours/24) + " Day "
            }
            timeDiff = "-" + days + hor + min
            
//            if hours < 0 {
//                hours = -(hours)
//            }
//            minutes = -(minutes)
//            timeDiff = "-" + String(describing: hours) + " Hr" + ":" + String(describing: minutes) + " Mins"
        }
        else {
   
                var min = ""
                var hor = ""
                var days = ""
                if minutes > 1 {
                    min = String(describing: minutes) + " Mins "
                }else{
                    min = String(describing: minutes) + " Min "
                }
                
                hours = hours - (hours/24)*24
                
                if hours > 1 {
                    hor = String(describing: hours) + " Hrs "
                }else{
                    hor = String(describing: hours) + " Hr "
                }
                
                if hours/24 > 1 {
                    days =  String(describing: hours/24) + " Days "
                }else{
                    days =  String(describing: hours/24) + " Day "
                }
                timeDiff = days + hor + min
        }
        return timeDiff
    }

     //************ to show toast view *****************//
    class  func toastView(messsage : String, view: UIView ){
        let toastLabel = UILabel(frame: CGRect(x: 0, y: 100, width: view.frame.size.width,  height : 35))
        toastLabel.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0x484848)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = NSTextAlignment.center;
        view.addSubview(toastLabel)
        toastLabel.text = messsage
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        UIView.animate(withDuration: 2.0, delay: 0.3, options: UIView.AnimationOptions.curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        })
    }
    
    //************ show alert view on root viewController window *****************//
    class func alertVC(errMSG:String){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.alertWindow.rootViewController = UIViewController()
        appDelegate.alertWindow.windowLevel = UIWindow.Level.alert + 1
        appDelegate.alertWindow.makeKeyAndVisible(); appDelegate.alertWindow.rootViewController?.present(Helper.alertVC(title: "Message", message: errMSG), animated: true, completion: { ()
            in
        })
    }
    
    
    //************ Resize the width and height of image *****************//
    class func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }

     //************ To get difference between two times *****************//
    class func getDifferenceForTwoTimes(currentDate:String,previousDate:String)->AnyObject {
        let dateFormatter = DateFormatter()
        let userCalendar = Calendar.current
        
        let requestedComponent: Set<Calendar.Component> = [.month,.day,.hour,.minute,.second]
        dateFormatter.locale = Locale.init(identifier: "en_IN")
        TimeZone.ReferenceType.default = TimeZone(abbreviation: "GMT")!
        dateFormatter.timeZone = TimeZone.ReferenceType.default
//        dateFormatter.timeZone = Helper.getTimeZoneFromCurrentLoaction()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let startTime = dateFormatter.date(from: previousDate)
        let endTime = dateFormatter.date(from: currentDate)
        let timeDifference = userCalendar.dateComponents(requestedComponent, from: startTime!, to: endTime!)
        return timeDifference as AnyObject
    }

    
    //************ To get the IP address *****************//
    class func getIPAddresses() -> [String] {
        var addresses = [String]()
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return [] }
        guard let firstAddr = ifaddr else { return [] }
        
        // For each interface ...
        for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let flags = Int32(ptr.pointee.ifa_flags)
            let addr = ptr.pointee.ifa_addr.pointee
            
            // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
            if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    if (getnameinfo(ptr.pointee.ifa_addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                        let address = String(cString: hostname)
                        addresses.append(address)
                    }
                }
            }
        }
        
        freeifaddrs(ifaddr)
        return addresses
    }
    
    class func playSound(soundName: String) {
        guard let url = Bundle.main.url(forResource: soundName, withExtension: "wav") else {
            print("error")
            return
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player.play()
            player.numberOfLoops = 1
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    //
    class func attributedString(from string: String, nonBoldRange: NSRange?) -> NSAttributedString {

        let attrs = [
            NSAttributedString.Key.font: UIFont(name: "ClanPro-NarrMedium", size: 14.0 ),
                           // UIFont.boldSystemFont(ofSize: fontSize),
            NSAttributedString.Key.foregroundColor: Helper.UIColorFromRGB(rgbValue: 0x333333)
        ]
        let nonBoldAttribute = [
            NSAttributedString.Key.font: UIFont(name: "ClanPro-NarrNews", size: 14.0 ),
            NSAttributedString.Key.foregroundColor: Helper.UIColorFromRGB(rgbValue: 0x555555)
            ]
        let attrStr = NSMutableAttributedString(string: string, attributes: attrs )
        if let range = nonBoldRange {
            attrStr.setAttributes(nonBoldAttribute, range: range)
        }
        return attrStr
    }
    
    class func historyAttributedString(from string: String, nonBoldRange: NSRange?) -> NSAttributedString {
        
        let attrs = [
            NSAttributedString.Key.font: UIFont(name: "ClanPro-NarrMedium", size: 13.0 ),
            // UIFont.boldSystemFont(ofSize: fontSize),
            NSAttributedString.Key.foregroundColor: Helper.UIColorFromRGB(rgbValue: 0x333333)
        ]
        let nonBoldAttribute = [
            NSAttributedString.Key.font: UIFont(name: "ClanPro-NarrNews", size: 13.0 ),
            NSAttributedString.Key.foregroundColor: Helper.UIColorFromRGB(rgbValue: 0x555555)
        ]
        let attrStr = NSMutableAttributedString(string: string, attributes: attrs )
        if let range = nonBoldRange {
            attrStr.setAttributes(nonBoldAttribute, range: range)
        }
        return attrStr
    }
    
    class func clipDigit(value:Float, digits: Int) -> String {
        return String(format:"%.\(digits)f", value)
    }
    
    //Set Button Title
    class func setButtonTitle(normal:String,highlighted:String,selected:String,button:UIButton) {
        button.setTitle(normal, for: .normal)
        button.setTitle(highlighted, for: .highlighted)
        button.setTitle(selected, for: .selected)
    }
    
    ///Adding Done Button On Keyboard
    class func addDoneButtonOnTextField(tf:UITextField, vc: UIView){
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem.init(title: StringConstants.Done, style: .plain, target: vc, action: #selector(UIView.endEditing(_:)))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        tf.inputAccessoryView = keyboardToolbar
    }
    
    /// Get Card Image
    ///
    /// - Parameter type: Type of Car
    /// - Returns: Return Value is Card Image
    class func cardImage(with type: String) -> UIImage {
        
        switch type {
            
        case "visa":
            return STPImageLibrary.brandImage(for: STPCardBrand.visa)
        case "mastercard":
            return STPImageLibrary.brandImage(for: STPCardBrand.masterCard)
        case "discover":
            return STPImageLibrary.brandImage(for: STPCardBrand.discover)
        case "american express":
            return STPImageLibrary.brandImage(for: STPCardBrand.amex)
        case "amex":
            return STPImageLibrary.brandImage(for: STPCardBrand.amex)
        case "jcb":
            return STPImageLibrary.brandImage(for: STPCardBrand.JCB)
        case "diners club":
            return STPImageLibrary.brandImage(for: STPCardBrand.dinersClub)
        default:
            return STPImageLibrary.brandImage(for: STPCardBrand.unknown)
        }
    }

    
    ///Set ui element Border and corner
    class func setUiElementBorderWithCorner(element:UIView, radius:Float, borderWidth:Float , color:UIColor) {
        element.layer.cornerRadius = CGFloat(radius)
        element.layer.borderWidth = CGFloat(borderWidth)
        element.layer.borderColor = color.cgColor
        element.clipsToBounds = true
    }
    
    //Get UIColor from HexColorCode
    class func getUIColor(color:String) -> UIColor {
        var cString:String = color.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.sorted().count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    //Get Image From Color
    static func GetImageFrom(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    //Add Shadow
    class func setShadow(sender:UIView) {
        
        sender.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(0))
        sender.layer.shadowColor = UIColor.lightGray.cgColor
        sender.layer.shadowOpacity = 0.5
    }
    
    //Set Button
    class func setButton(button:UIButton,view:UIView,primaryColour:UIColor,seconderyColor:UIColor,shadow:Bool) {
        //Set Background
        button.setBackgroundImage(Helper.GetImageFrom(color: primaryColour), for: .normal)
        button.setBackgroundImage(Helper.GetImageFrom(color: seconderyColor), for: .highlighted)
        button.setBackgroundImage(Helper.GetImageFrom(color: seconderyColor), for: .selected)
        
        //Set Border and Corner
        button.layer.cornerRadius = CGFloat(UIConstants.ButtonCornerRadius)
        button.layer.borderWidth = CGFloat(UIConstants.ButtonBorderWidth)
        button.layer.borderColor = primaryColour.cgColor
        if primaryColour == UIColor.white || primaryColour == Helper.getUIColor(color: Colors.SecondBaseColor) || primaryColour == UIColor.clear {
            button.layer.borderColor = seconderyColor.cgColor
        }
        button.clipsToBounds = true
        
        
        if view.isKind(of: UIView.classForCoder()) {
            if shadow {
                self.setShadow(sender: view)
            }
            view.backgroundColor = UIColor .clear
        }
        
        //Set Title
        var pC = primaryColour
        var sC = seconderyColor
        if seconderyColor == UIColor.clear {
            sC = UIColor.white
        }
        if primaryColour == UIColor.clear {
            pC = UIColor.white
        }
        button.setTitleColor(sC, for: .normal)
        button.setTitleColor(pC, for: .highlighted)
        button.setTitleColor(pC, for: .selected)
    }
    
    //Multiple ColourText
    class func updateText(text: String,subText:String,_ sender: Any,color:UIColor,link: String) {
        
        let range = (text.lowercased() as NSString).range(of: subText.lowercased())
        let attributedString = NSMutableAttributedString(string:text)
//        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value:color , range: range)
        var size = 0
        if let element = sender as? UITextView {
            size = Int((element.font?.pointSize)! + CGFloat(2))
        }else if let element = sender as? UIButton {
            size = Int((element.titleLabel?.font.pointSize)! + CGFloat(2))
        }else if let element = sender as? UILabel {
            size = Int((element.font?.pointSize)! + CGFloat(2) )
        }
        else if let element = sender as? UITextField {
            size = Int((element.font?.pointSize)! + CGFloat(2) )
        }
        
        
        let font = UIFont(name: Fonts.Roboto.Medium , size: CGFloat(size))
//        attributedString.addAttribute(NSAttributedStringKey.font, value:font!, range: range)
        if let element = sender as? UIButton {
            element.setAttributedTitle(attributedString, for: .normal)
            let range2 = (text as NSString).range(of: text)
            let attributedString2 = NSMutableAttributedString(string:text)
//            attributedString2.addAttribute(NSAttributedStringKey.foregroundColor, value:color , range: range2)
            element.setAttributedTitle(attributedString2, for: .highlighted)
            element.setAttributedTitle(attributedString2, for: .selected)
        }else if let element = sender as? UILabel {
            element.attributedText = attributedString
        }
        else if let element = sender as? UITextView {
//            attributedString.addAttribute(.link, value: link, range: range)
            element.attributedText = attributedString
//            element.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: color]
        }
        else if let element = sender as? UITextField {
            element.attributedText = attributedString
        }
    }
    
    //Add shadow to NavigationController
    class func addShadowToNavigationBar(controller:UINavigationController){
        controller.navigationBar.layer.masksToBounds = false
        controller.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        controller.navigationBar.layer.shadowOpacity = 0.8
        controller.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        controller.navigationBar.layer.shadowRadius = 2
    }
    
    class func removeShadowToNavigationBar(controller:UINavigationController){
        controller.navigationBar.layer.masksToBounds = true
        controller.navigationBar.layer.shadowColor = UIColor.white.cgColor
        controller.navigationBar.layer.shadowOpacity = 0
        controller.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        controller.navigationBar.layer.shadowRadius = 0
    }

    
    //
    class func highlightedString(from string: String, nonBoldRange: NSRange?) -> NSAttributedString {
        
        let attrs = [
            NSAttributedString.Key.font: UIFont(name: "ClanPro-NarrMedium", size: 15.0 ),
            // UIFont.boldSystemFont(ofSize: fontSize),
            NSAttributedString.Key.foregroundColor: Helper.UIColorFromRGB(rgbValue: 0x3498DB)
        ]
        let nonBoldAttribute = [
            NSAttributedString.Key.font: UIFont(name: "ClanPro-NarrNews", size: 14.0 ),
            NSAttributedString.Key.foregroundColor: Helper.UIColorFromRGB(rgbValue: 0x555555)
        ]
        let attrStr = NSMutableAttributedString(string: string, attributes: attrs )
        if let range = nonBoldRange {
            attrStr.setAttributes(nonBoldAttribute, range: range)
        }
        return attrStr
    }
    class func dayDifference(from interval : TimeInterval) -> (String , String, Date)
    {
        let calendar = Calendar.current
        let date:Date = NSDate(timeIntervalSince1970: interval) as Date
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "hh:mm a"
        let dateStringFirst = dayTimePeriodFormatter.string(from: date as Date)
        
        if calendar.isDateInYesterday(date ) { return ("Yesterday" ,dateStringFirst,date) }
        else if calendar.isDateInToday(date) { return ("Today",dateStringFirst,date) }
        else if calendar.isDateInTomorrow(date) { return ("Tomorrow",dateStringFirst,date) }
        else {
            let startOfNow = calendar.startOfDay(for: Date())
            let startOfTimeStamp = calendar.startOfDay(for: date)
            let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
            let day = components.day!
            if day < 1 { return ("\(-day) days ago",dateStringFirst,date) }
            else { return ("In \(day) days",dateStringFirst,date) }
        }
    }
    class func withDate(from interval : Date) -> (String , String, Date)
    {
       let calendar = Calendar.current
        let date:Date = interval
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "hh:mm a"
        let dateStringFirst = dayTimePeriodFormatter.string(from: date as Date)
        
        if calendar.isDateInYesterday(date ) { return ("Yesterday" ,dateStringFirst,date) }
        else if calendar.isDateInToday(date) { return ("Today",dateStringFirst,date) }
        else if calendar.isDateInTomorrow(date) { return ("Tomorrow",dateStringFirst,date) }
        else {
            let startOfNow = calendar.startOfDay(for: Date())
            let startOfTimeStamp = calendar.startOfDay(for: date)
            let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
            let day = components.day!
            if day < 1 { return ("\(-day) days ago",dateStringFirst,date) }
            else { return ("In \(day) days",dateStringFirst,date) }
        }
    }
    class func getDateString(value:Date,format:String,zone:Bool) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale.init(identifier: "en_US_POSIX")// for 24 hours format
        if zone {
            //            formatter.timeZone = Helper.getTimeZoneFromPickUpLoaction()
        }
        let myString = formatter.string(from: value)
        return myString
    }
    
    class func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "d MMMM yyyy h:mm a"
        
        return dateFormatter.string(from: dt!)
    }
    
    
}


