//
//  ReachabilityCheck.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 01/10/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Alamofire

@objc protocol ReachabilityHelperDelegate {
    @objc optional func didNetworkReachabilityChange(reachable: Bool)
}

class ReachabilityHelper: NSObject {
    
    var reachabilityObj: Reachability?
    var isReachable:Bool {
        return (reachabilityObj?.isReachable)!
    }
    
    var delegate: ReachabilityHelperDelegate? = nil
    
    private static var helper: ReachabilityHelper? = nil
    static var shared: ReachabilityHelper {
        if helper == nil {
            helper = ReachabilityHelper()
        }
        return helper!
    }
    override init() {
        
    }
    
    func startMonitoring() {
        
        // Listen Notification
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reachabilityChanged(_:)),
                                               name: ReachabilityChangedNotification,
                                               object: reachabilityObj)
        
        // Start reachability without a hostname intially
        setupReachability(nil)
        startNotifier()
        
        // After 5 seconds, stop and re-start reachability, this time using a hostname
        let dispatchTime = DispatchTime.now() + DispatchTimeInterval.seconds(5)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
            self.stopNotifier()
            self.setupReachability("www.google.com")
            self.startNotifier()
        }
    }
    
    private func setupReachability(_ hostName: String?) {
        
        let reachability = hostName == nil ? Reachability() : Reachability(hostname: hostName!)
        self.reachabilityObj = reachability
        
        reachability?.whenReachable = { reachability in
            DispatchQueue.main.async {
                self.reachable(reachability)
            }
        }
        reachability?.whenUnreachable = { reachability in
            DispatchQueue.main.async {
                self.notReachable(reachability)
            }
        }
    }
    
    private func startNotifier() {
        do {
            try reachabilityObj?.startNotifier()
        }
        catch {
            return
        }
    }
    
    private func stopNotifier() {
        reachabilityObj?.stopNotifier()
    }
    
    private func reachable(_ reachability: Reachability) {
        
        if reachability.isReachableViaWiFi {
            //print("\nConnected through Wifi\n")
        }
        else {
            //print("\nNetwork Connected\n")
        }
       // ReachabilityView.instance.hide()  // hidden
    }
    
    private func notReachable(_ reachability: Reachability) {
        //print("\nNetwork not Connected\n")
        ReachabilityView.instance.show()
    }
    
    @objc private func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
        self.reachabilityObj = reachability
        
        print("Network Status : ",reachability.currentReachabilityString)
        if reachability.isReachable {
            reachable(reachability)
        }
        else {
            notReachable(reachability)
        }
    }
    
    deinit {
        stopNotifier()
    }
}
