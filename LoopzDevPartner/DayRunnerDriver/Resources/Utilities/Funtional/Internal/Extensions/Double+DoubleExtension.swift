//
//  Double+DoubleExtension.swift
//  DayRunnerDriverDev
//
//  Created by Rahul Sharma on 22/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
