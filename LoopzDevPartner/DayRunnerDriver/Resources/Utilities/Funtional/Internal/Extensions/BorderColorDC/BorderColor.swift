//
//  BorderColor.swift
//  DayRunner
//
//  Created by Rahul Sharma on 30/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit


@IBDesignable final class BorderColor: UIView{
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        // Get the Graphics Context
        let context = UIGraphicsGetCurrentContext()
        
        // Set the outerline width
        context!.setLineWidth(1.0)
        
        //CGContextSetLineWidth(context, lineWidth)
        // Set the outerline colour
         UIColor.lightGray.set()
    
       // UIColor.init(red: 178, green: 178, blue: 178).set()
        // Create square
       // context!.addRect(rect)
        
        // Draw
        context!.strokePath()
    }
}
