//
//  UIViewController+Session.swift
//  DayRunner
//
//  Created by 3Embed on 29/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import FirebaseMessaging
extension UIViewController {
    
    /// Handle Session Expired
    func sessionExpired() {
        Session.expired()
    }
}

class Session {
    
    /// Handle Session Expired
    class func expired() {
        
        print("\nSession Token: \(Utility.sessionToken)\n")
        
        guard Utility.sessionToken.length != 0 else {
            return
        }
       // QuickCardAPI.removePaymentCode()
        ///unsubscribing Message channel
        let mqtt = MQTT.sharedInstance
        
        if let mqttManager = mqtt.manager {
            
            if !(mqttManager.subscriptions.isEmpty) {
                mqttManager.subscriptions = nil
                mqttManager.connect(toLast: { (error) in
                    if let error = error{
                        print("error ",error.localizedDescription)
                    }
                })
                mqtt.disconnectMQTTConnection()
                MQTT.sharedInstance.manager = nil
            }
            
        }
        
        if Utility.sessionToken != USER_INFO.SESSION_TOKEN {
            let pushTopicName = UserDefaults.standard.value(forKey: USER_INFO.DRIVERPUBCHANNEL)
      //      Messaging.messaging().unsubscribe(fromTopic: pushTopicName as! String)
        }
        if let pushTopicArray: [Any] = UserDefaults.standard.object(forKey: "pushTopicArray") as? [Any]
        {
            for x in 0..<pushTopicArray.count
            {
                print("Push Topic \(x + 1): \(pushTopicArray[x])")
                let pushTopicName: String = pushTopicArray[x] as! String
                Messaging.messaging().unsubscribe(fromTopic: pushTopicName)
            }
        }
        UserDefaults.standard.removeObject(forKey: "pushTopicArray")
        let locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
        locationtracker.stopLocationUpdateTimer()
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let signIN: SplashViewController? = storyboard.instantiateViewController(withIdentifier: storyBoardIDs.splashVC!) as? SplashViewController
        
        let window = UIApplication.shared.keyWindow
        window?.rootViewController = signIN
        _ = Utility.deleteSavedData()
    }
}
