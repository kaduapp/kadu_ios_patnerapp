//
//  LoadingProgress.swift
//  Channel 40
//
//  Created by STARK on 04/02/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class LoadingProgress: UIView {
    
    @IBOutlet weak var progressImage: UIImageView!
    private static var obj: LoadingProgress? = nil
    static var flagClose    = false
    static var shared: LoadingProgress {

        if obj == nil {
            obj = UINib(nibName: "LoadingProgress", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as? LoadingProgress
            obj?.frame = UIScreen.main.bounds
        }
        LoadingProgress.flagClose = false
        return obj!
    }
    @IBOutlet weak var loadingHead: UILabel!
    
   private func setup() {
        let window:UIWindow = UIApplication.shared.delegate!.window!!
        window.addSubview(self)
        
//        self.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
//
//        UIView.animate(withDuration: 0.0, delay: 0.0, options: .beginFromCurrentState, animations: {() -> Void in
//            self.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
//
//        }, completion: {(_ finished: Bool) -> Void in
//        })
    
        let when = DispatchTime.now() + 30
        
        DispatchQueue.main.asyncAfter(deadline: when){
            self.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
//            UIView.animate(withDuration: 0.0, delay: 0.0, options: .beginFromCurrentState, animations: {() -> Void in
//                self.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
//            }, completion: {(_ finished: Bool) -> Void in
                self.removeFromSuperview()
//            })
        }
    
    }
    
    func showPI(message: String) {
        setup()
//        Fonts.setPrimaryMedium(loadingHead)
        loadingHead.text = message
        let jeremyGif = UIImage.gifImageWithName("loader")
        progressImage.image = jeremyGif
        let when = DispatchTime.now() + 1
        
        DispatchQueue.main.asyncAfter(deadline: when){
            if LoadingProgress.flagClose {
                if (LoadingProgress.obj != nil) {
                    self.removeFromSuperview()
                }
            }else{
                LoadingProgress.flagClose = true
            }
        }
    }
    
    func hide() {
        if LoadingProgress.flagClose {
            if (LoadingProgress.obj != nil) {
                self.removeFromSuperview()
                
            }
            LoadingProgress.flagClose = false
        }else{
            LoadingProgress.flagClose = true
        }
    }
    
    
    func setLabel(text:String) {
        loadingHead.text = text
    }
}
