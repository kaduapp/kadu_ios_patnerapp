//
//  LocksmithWrapper.swift
//  KarruPro
//
//  Created by Rahul Sharma on 13/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Locksmith
class LocksmithWrapper {
    
    internal class func getAccountName() -> String
    {
        let appIdentifierPrefix = Bundle.main.infoDictionary!["AppIdentifierPrefix"] as! String
        let appBundleId = Bundle.main.infoDictionary?["CFBundleIdentifier"] as! String
        print(appIdentifierPrefix + appBundleId)
        return (appIdentifierPrefix + appBundleId)
        
    }
    
    class func addObjectToKeychain(data key:String, value: String)
    {
        let accountName = getAccountName()
        
        do {
            try Locksmith.saveData(data: [key: value], forUserAccount: accountName)
        } catch LocksmithError.duplicate {
            print("Got Duplication Error")
        } catch {
            print("Got Error")
            print(error)
        }
        
    }
    
    class func getObjectFromKeychain(data key:String ) -> String
    {
        let accountName = getAccountName()
        let dictionary = Locksmith.loadDataForUserAccount(userAccount: accountName)
        print(dictionary!)
        if let value = dictionary![key] as? String
        {
            return value
        }
        else
        {
            return ""
        }
    }
    
    class func deleteData()
    {
        let accountName = getAccountName()
        do{
            try Locksmith.deleteDataForUserAccount(userAccount: accountName)
        }
        catch{
            print("Some Error while deleting Data")
        }
    }
}
