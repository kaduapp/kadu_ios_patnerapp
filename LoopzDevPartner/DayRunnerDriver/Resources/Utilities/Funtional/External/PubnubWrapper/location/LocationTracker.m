//
//  LocationTracker.h
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


#import "LocationTracker.h"
#import "Kadu_Partner-Swift.h"


#define LATITUDE @"latitude"
#define LONGITUDE @"longitude"
#define ACCURACY @"theAccuracy"


@interface LocationTracker()
{
    double heading;
    MQTT *mqtt;
    
    
}
@property(nonatomic,strong)NSString *pubNubChannel;
@property CBLManager * manager;
@property LocationEnableView *locationEnabled;
//@property (retain) DistanceCalculation *distanceCalculation;

@end
static LocationTracker *locationtracker;
@implementation LocationTracker
{
    NSTimer *customLocationUpdateTimer;
}

+ (id)sharedInstance {
    if (!locationtracker) {
        locationtracker  = [[self alloc] init];
    }
    
    return locationtracker;
}


+ (CLLocationManager *)sharedLocationManager {
    static CLLocationManager *_locationManager;
    
    @synchronized(self) {
        if (_locationManager == nil) {
            _locationManager = [[CLLocationManager alloc] init];
            
            _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_locationManager requestAlwaysAuthorization];
            }
            
            if([_locationManager respondsToSelector:@selector(allowsBackgroundLocationUpdates)]){
                [_locationManager setAllowsBackgroundLocationUpdates:YES];
            }
        }
    }
    return _locationManager;
}


///******instantiate the location tracker**********/////
- (id)init {
    if (self==[super init]) {
        //Get the share model and also initialize myLocationArray
        self.shareModel = [LocationShareModel sharedModel];
//        self.manager = [_manager sharedInstance];  //***couch db initialisation *****//
//        _locationEnabled = [_locationEnabled shared];
        self.shareModel.myLocationArray = [[NSMutableArray alloc]init];
        //  mqtt = [MQTT sharedInstance];
        ///********** did calls when the app enter background*************//
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}


////********** when the app enters background ************//
-(void)applicationEnterBackground{
    
    self.globalLocationManager = [LocationTracker sharedLocationManager];
    self.globalLocationManager.delegate = self;
    self.globalLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    
    self.globalLocationManager.distanceFilter = kCLDistanceFilterNone;
    [self.globalLocationManager startUpdatingLocation];
    [self.globalLocationManager startUpdatingHeading];
    float iosVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if(iosVersion >= 9.0f)
    {
        [self.globalLocationManager setAllowsBackgroundLocationUpdates:YES];
    }
    
    //Use the BackgroundTaskManager to manage all the background Task
    self.shareModel.bgTask = [BackgroundTaskManager sharedBackgroundTaskManager];
    [self.shareModel.bgTask beginNewBackgroundTask];
}

- (void) restartLocationUpdates
{
    
    if (self.shareModel.timer) {
        [self.shareModel.timer invalidate];
        self.shareModel.timer = nil;
    }
    
    self.globalLocationManager = [LocationTracker sharedLocationManager];
    self.globalLocationManager.delegate = self;
    self.globalLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    self.globalLocationManager.distanceFilter = kCLDistanceFilterNone;
    [self.globalLocationManager startUpdatingLocation];
    [self.globalLocationManager startUpdatingHeading];
    float iosVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if(iosVersion >= 9.0f)
    {
        [self.globalLocationManager setAllowsBackgroundLocationUpdates:YES];
    }
}


- (void)startLocationTracking {
    
    if ([CLLocationManager locationServicesEnabled] == NO) {
        [_locationEnabled show];
        
    } else {
        [_locationEnabled hide];
        CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
        
        if(authorizationStatus == kCLAuthorizationStatusDenied || authorizationStatus == kCLAuthorizationStatusRestricted){
        }
        else
        {
            self.globalLocationManager = [LocationTracker sharedLocationManager];
            self.globalLocationManager.delegate = self;
            self.globalLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
            self.globalLocationManager.distanceFilter = kCLDistanceFilterNone;
            [self.globalLocationManager startUpdatingLocation];
            [self.globalLocationManager startUpdatingHeading];
            float iosVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
            if(iosVersion >= 9.0f)
            {
                [self.globalLocationManager setAllowsBackgroundLocationUpdates:YES];
            }
        }
    }
}

- (void)stopLocationTracking
{
    if (self.shareModel.timer)
    {
        [self.shareModel.timer invalidate];
        self.shareModel.timer = nil;
    }
    self.globalLocationManager = [LocationTracker sharedLocationManager];
    [self.globalLocationManager stopUpdatingLocation];
    [self.globalLocationManager stopUpdatingHeading];
}



#pragma mark - CLLocationManagerDelegate Methods

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    NSLog(@"status: %d",status);
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
            [_locationEnabled show];
            break;
            
        case kCLAuthorizationStatusAuthorizedAlways:
            [_locationEnabled hide];
            break;
            
        case  kCLAuthorizationStatusAuthorizedWhenInUse:
            NSLog(@"");
            break;
            
        default:
            break;
    }
}


-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading{
    heading = newHeading.trueHeading;
    float heading = newHeading.trueHeading;
    [self.delegate didUpdateHeadingTo:heading];
}



-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    NSString * str = [ud valueForKey:@"transit"];
    NSString * transValue;
    if (str == nil) {
        transValue = @"0";
    }else {
        transValue = [ud valueForKey:@"transit"];
    }
    [ud synchronize];
    
    CLLocation * newLocation2 = [locations objectAtIndex:0];
    CLLocationCoordinate2D theLocation2 = newLocation2.coordinate;
    
    
    //    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    //    NSString * str = [ud valueForKey:@"transit"];
    //    NSString * transValue;
    //    if (str == nil) {
    //        transValue = @"0";
    //    }else {
    //        transValue = [ud valueForKey:@"transit"];
    //    }
    //    [ud synchronize];
    
    //    self.lastLatitude = [NSString stringWithFormat:@"%f",theLocation2.latitude];
    //    self.lastLongitude = [NSString stringWithFormat:@"%f",theLocation2.longitude];
    
    CLLocation* currentLocation = [locations lastObject];
    
    [self.delegate didUpdateLocationWithLatitude:theLocation2.latitude andLongitude:theLocation2.longitude];
    
    
    NSDictionary *message = @{
                              @"action" : [NSNumber numberWithInt:4],
                              @"lt": [NSNumber numberWithDouble:currentLocation.coordinate.latitude],
                              @"lg": [NSNumber numberWithDouble:currentLocation.coordinate.longitude],
                              @"vt"            : Utility.vehicleTypeID,
                              @"battery_Per"   :[NSString stringWithFormat:@"%f", Utility.batteryLevel],
                              @"app_version"   : Utility.appVersion,
                              @"device_type"   : @"1",
                              @"location_check": @"1",
                              @"pubnubStr"     : Utility.onGoingBid,
                              @"location_Heading": [NSString stringWithFormat:@"%f",heading],
                              @"presenceTime"  :Utility.presence,
                              @"transit":transValue
                              };
    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"locationUpdated" object:currentLocation];
    NSError *error;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:message options:NSJSONWritingPrettyPrinted error:&error];
    NSString *publishChannel = [[NSUserDefaults standardUserDefaults] objectForKey:@"driver_channel"];

    NSMutableArray * arr = [[NSMutableArray alloc]init];
    arr = [[NSUserDefaults standardUserDefaults] objectForKey:@"SavedStringArray"];
    
    
    
    
    if(arr.count == 1) {
        
    }else {
        for (int i = 0; i<arr.count; i++) {
        }
    }
    if(self.distanceCalulation!=nil && self.distanceCalulation.shouldCalculateDistance == YES)
    {
//        [self.distanceCalulation calculateDistanceWithLocation:[locations firstObject]];
    }
    
    for(int i=0;i<locations.count;i++)
    {
        CLLocation * newLocation = [locations objectAtIndex:i];
        CLLocationCoordinate2D theLocation = newLocation.coordinate;
        CLLocationAccuracy theAccuracy = newLocation.horizontalAccuracy;
        //
        float distanceChange = [newLocation distanceFromLocation:_lastLocaiton];
        if(distanceChange >= 20.00)
        {
            self.distance += distanceChange;
        }
        // [Helper showAlertWithTitle:@"Distance" Message:[NSString stringWithFormat:@"%f",self.distance]];
        if (self.distanceCallback)
        {
            if (self.distance >= 20.00)
            {
                self.distanceCallback(self.distance, newLocation);
            }
        }
        
        if (self.distanceCallbackOnOff)
        {
            self.distanceCallbackOnOff(self.distanceOnOff,newLocation);
        }
        _lastLocaiton = newLocation;
        
        
        
        
        NSTimeInterval locationAge = - [newLocation.timestamp timeIntervalSinceNow];
        
        if (locationAge > 30.0)
        {
            continue;
        }
        //NSLog(@"the accuaracy %f",theAccuracy);
        
        //Select only valid location and also location with good accuracy
        if(newLocation!=nil && theAccuracy>0
           &&theAccuracy<2000
           &&(!(theLocation.latitude==0.0&&theLocation.longitude==0.0))){
            
            self.myLastLocation = theLocation;
            self.myLastLocationAccuracy= theAccuracy;
            
            NSMutableDictionary * dict = [[NSMutableDictionary alloc]init];
            [dict setObject:[NSNumber numberWithFloat:theLocation.latitude] forKey:@"latitude"];
            [dict setObject:[NSNumber numberWithFloat:theLocation.longitude] forKey:@"longitude"];
            [dict setObject:[NSNumber numberWithFloat:theAccuracy] forKey:@"theAccuracy"];
            
            //Add the vallid location with good accuracy into an array
            //Every 1 minute, I will select the best location based on accuracy and send to server
            [self.shareModel.myLocationArray addObject:dict];
        }
    }
    
    //If the timer still valid, return it (Will not run the code below)
    if (self.shareModel.timer) {
        return;
    }
    
    /// background task manager begins
    self.shareModel.bgTask = [BackgroundTaskManager sharedBackgroundTaskManager];
    [self.shareModel.bgTask beginNewBackgroundTask];
    
    //Will only restart the locationManager after 20 seconds, so that we can get some accurate locations
    //The location manager will restart because its been stopped for 10 seconds
    NSInteger newOne = [[NSUserDefaults standardUserDefaults] integerForKey:@"5"];
    self.shareModel.timer = [NSTimer scheduledTimerWithTimeInterval:newOne target:self
                                                           selector:@selector(restartLocationUpdates)
                                                           userInfo:nil
                                                            repeats:NO];
    
    //Will only stop the locationManager after 10 seconds, so that we can get some accurate locations
    //The location manager will only operate for 10 seconds to save battery
    
    if (self.shareModel.delay10Seconds)
    {
        [self.shareModel.delay10Seconds invalidate];
        self.shareModel.delay10Seconds = nil;
    }
    
    
    self.shareModel.delay10Seconds = [NSTimer scheduledTimerWithTimeInterval:10 target:self
                                                                    selector:@selector(stopLocationDelayBy10Seconds)
                                                                    userInfo:nil
                                                                     repeats:NO];
    
}


//Stop the locationManager
-(void)stopLocationDelayBy10Seconds{
    self.globalLocationManager = [LocationTracker sharedLocationManager];
    [self.globalLocationManager stopUpdatingLocation];
    [self.globalLocationManager stopUpdatingHeading];
}



//Send the location to Server
- (void)updateLocationToServer
{
    // Find the best location from the array based on accuracy
    NSMutableDictionary * myBestLocation = [[NSMutableDictionary alloc]init];
    
    for(int i=0;i<self.shareModel.myLocationArray.count;i++){
        
        NSMutableDictionary * currentLocation = [self.shareModel.myLocationArray objectAtIndex:i];
        
        if(i==0)
            myBestLocation = currentLocation;
        else
        {
            if([[currentLocation objectForKey:ACCURACY]floatValue]<=[[myBestLocation objectForKey:ACCURACY]floatValue]){
                myBestLocation = currentLocation;
            }
        }
    }
    //If the array is 0, get the last location
    //Sometimes due to network issue or unknown reason, you could not get the location during that  period, the best you can do is sending the last known location to the server
    if(self.shareModel.myLocationArray.count==0)
    {
        self.myLocation=self.myLastLocation;
        self.myLocationAccuracy=self.myLastLocationAccuracy;
        [self startPubNubSream:self.myLocation.latitude Longitude:self.myLocation.longitude]; //*** if location disabled, then last longs will update ********//
    }
    else
    {
        CLLocationCoordinate2D theBestLocation;
        theBestLocation.latitude =[[myBestLocation objectForKey:LATITUDE]floatValue];
        theBestLocation.longitude =[[myBestLocation objectForKey:LONGITUDE]floatValue];
        self.myLocation=theBestLocation;
        self.myLocationAccuracy =[[myBestLocation objectForKey:ACCURACY]floatValue];
        
        [self startPubNubSream:self.myLocation.latitude Longitude:self.myLocation.longitude];
        
        //After sending the location to the server successful, remember to clear the current array with the following code. It is to make sure that you clear up old location in the array and add the new locations from locationManager
        [self.shareModel.myLocationArray removeAllObjects];
        self.shareModel.myLocationArray = nil;
        self.shareModel.myLocationArray = [[NSMutableArray alloc]init];
    }
}

#pragma mark server API

-(void)startPubNubSream:(double)latitude Longitude:(double)longitude
{
    NSError *error;
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    NSString * str = [ud valueForKey:@"transit"];
    [ud synchronize];
    CBLDatabase* database =  [_manager databaseNamed: @"livedayrunner" error: &error];
    NSString *locationCheck;
    if ([CLLocationManager locationServicesEnabled] == NO) { // location check
        locationCheck = @"0"; // location disabled
    }else{
        locationCheck = @"1";// location Enabled
    }
    
    /// saving the last updated lat longs to update accepted time
    [ud setDouble:latitude forKey:@"currentLat"];
    [ud setDouble:longitude forKey:@"currentLog"];
    [ud synchronize];
    
    
    
    NSDictionary *message = @{
                              @"latitude": [NSNumber numberWithDouble:latitude],
                              @"longitude": [NSNumber numberWithDouble:longitude],
                              //                              @"vehicleType"            : Utility.vehicleTypeID,
                              @"batteryPer"   :[NSString stringWithFormat:@"%f", Utility.batteryLevel],
                              @"appVersion"   : Utility.appVersion,
                              @"deviceType"   : @"1",
                              @"locationCheck": locationCheck,
                              @"pubnubStr"     : Utility.onGoingBid,
                              @"locationHeading": [NSString stringWithFormat:@"%f",heading],
                              @"presenceTime"  :Utility.presence,
                              @"transit":str,
                              @"status": [NSNumber numberWithInt:Utility.Status],
                              @"driverId" : Utility.userId
                             
                              };
    
    NSData *jsondata = [NSJSONSerialization dataWithJSONObject:message
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"distLat"]) {
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        
        CLLocation *locB = [[CLLocation alloc] initWithLatitude:[[ud objectForKey:@"distLat"] doubleValue] longitude:[[ud objectForKey:@"distLog"] doubleValue]];
        
        CLLocationDistance distance = [locA distanceFromLocation:locB];
        
        //******check the distance has to satisfies the configuration distance********//
        if (distance >= Utility.distanceAccordingStoring) {
            NSMutableArray *latlongsArray = [[NSMutableArray alloc]init];
            
            if ([ud boolForKey:@"noBookings"] && ![ud boolForKey:@"hasInternet"]) {
                NSString *latlongs = [NSString stringWithFormat:@"%f,%f",latitude,longitude];
                if ([ud objectForKey:@"storedLatLongs"]) {
                    [latlongsArray addObjectsFromArray:[ud objectForKey:@"storedLatLongs"]];
                    [latlongsArray addObject:latlongs];
                    [ud setObject:latlongsArray forKey:@"storedLatLongs"];
                }else{
                    [latlongsArray addObject:latlongs];
                    [ud setObject:latlongsArray forKey:@"storedLatLongs"];
                }
                
                NSDictionary *dict = @{
                                       @"latLongs":latlongsArray
                                       };
                
                if ([ud objectForKey:@"documentID"]) {
                    CBLDocument *document = [database existingDocumentWithID:[[NSUserDefaults standardUserDefaults] objectForKey:@"documentID"]]; //Retriving Stored data in document id
                    CBLJSONDict *properties = [document properties];
                    NSMutableDictionary *temp = [properties mutableCopy];
                    [temp setObject:latlongsArray forKey:@"latLongs"];
                    properties = temp;
                    [document putProperties:properties error: &error]; //updating the already exisiting lat longs
                    
                }else{
                    CBLDocument *document = [database createDocument];
                    [document putProperties:dict error: &error];
                    [ud setObject:[document documentID] forKey:@"documentID"]; // storing document id,lat longs r store to this unique id
                    [ud synchronize];
                }
                [ud setBool:false forKey:@"updatedOfflineLatLongs"]; // storing the offline lat longs(no internet)
                [ud synchronize];
            }
            
            ///*********** if the app moves the distance given in configuration********///
            if ([ud boolForKey:@"hasInternet"] && [ud boolForKey:@"updatedOfflineLatLongs"]) {
                NSMutableDictionary *temp = [message mutableCopy];
                [temp setValue:@"1" forKey:@"transit"];
                //                [self updateLocationToServerLatLongs:temp];
            }
            
            //******storing recent lat longs**********
            [ud setObject:[NSString stringWithFormat:@"%f",latitude] forKey:@"distLat"];
            [ud setObject:[NSString stringWithFormat:@"%f",longitude] forKey:@"distLog"];
            [ud synchronize];
            
        }else{
            //******check once whether the couch db lat longs are updated, if yes its starts making location service and also if the app isnt moved much and its doesn't configuration distance *****//
            if ([ud boolForKey:@"hasInternet"] && [ud boolForKey:@"updatedOfflineLatLongs"]) {
                [self updateLocationToServerLatLongs:message];
            }
        }
    }else{
        [ud setObject:[NSString stringWithFormat:@"%f",latitude] forKey:@"distLat"];
        [ud setObject:[NSString stringWithFormat:@"%f",longitude] forKey:@"distLog"];
        [ud synchronize];
    }
    
    
    NSMutableArray * arr = [[NSMutableArray alloc]init];
    arr = [[NSUserDefaults standardUserDefaults] objectForKey:@"SavedStringArray"];
    
    
    NSString * cityString = [[NSUserDefaults standardUserDefaults] stringForKey:@"cityId"];
    
    NSString * channelName = @"onlineDrivers/";
    
    NSData *jsonBodyData = [NSJSONSerialization dataWithJSONObject:message options:kNilOptions error:nil];
    
    NSString* newStr = [NSString stringWithUTF8String:[jsonBodyData bytes]];
    NSLog(@"%@",newStr);

//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:message options:NSJSONWritingPrettyPrinted error:&error];
//
    if(arr.count == 1) {
        NSString * topicChannel = [NSString stringWithFormat:@"%@%@%@%@",channelName,cityString,@"/",arr[0]];
//        [[MQTT sharedInstance] publishDataWithWthData:jsonBodyData onTopic:topicChannel retain:NO withDelivering:MQTTQosLevelAtMostOnce];
        
    }else {
        for (int i = 0; i<arr.count; i++) {
             NSString * topicChannel = [NSString stringWithFormat:@"%@%@%@%@",channelName,cityString,@"/",arr[i]];
//            [[MQTT sharedInstance] publishDataWithWthData:jsonBodyData onTopic:topicChannel retain:NO withDelivering:MQTTQosLevelAtMostOnce];
        }
    }

    
    //***** when internet came back and u was offline before then publish stored couch db lat longs to server******//
    if ([ud boolForKey:@"hasInternet"] && ![ud boolForKey:@"updatedOfflineLatLongs"]) {
        [self updateOfflineLocationToServer];
    }
    
}

///*************updating lat longs to server continously********//
-(void)updateLocationToServerLatLongs:(NSDictionary *)dict{
    self.locationUpdateModel = [LocationTrackerModel new];
    [self.locationUpdateModel locationUpdateWithLocation:dict];
}

///*********updating the offline stored lat longs stored in couch db to server *********//
-(void)updateOfflineLocationToServer {
    NSArray *arrayOfOfllineLatlongs;
    NSError *error;
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"documentID"]) {
        CBLDatabase* database =  [_manager databaseNamed: @"livedayrunner" error: &error];
        CBLDocument *document = [database existingDocumentWithID:[[NSUserDefaults standardUserDefaults] objectForKey:@"documentID"]];
        CBLJSONDict *properties = [document properties];
        arrayOfOfllineLatlongs = properties[@"latLongs"];
        
        NSDictionary *dict = @{@"lt_lg":arrayOfOfllineLatlongs};
        
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"updatedOfflineLatLongs"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        self.locationUpdateModel = [LocationTrackerModel new];
        [self.locationUpdateModel offlineLocationUpdateWithLocation: dict];
        
        //        [NetworkHelper requestPUTWithServiceName:@"master/locationLogs"
        //                                          params:dict
        //                                         success:^(NSDictionary *response) {
        //                                             if (response[@"statusCode"]) {
        //                                                 if ([response[@"statusCode"] integerValue] == 401) {
        //                                                     [self sessionExpired]; // 401: session erpired error
        //                                                 }
        //                                             }else{
        //                                                 [self deletingTheDataFromFromParticularID];
        //
        //                                             }
        //                                         }
        //                                         failure:^(NSError *error) {
        //                                             NSLog(@"failed to get response");
        //                                         }];
        //
        
    }else{
        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"updatedOfflineLatLongs"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
}

///**********deleting the couch db data once its updated to server and document ID***********//
-(void)deletingTheDataFromFromParticularID{
    NSError *error;
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"documentID"]) {
        CBLDatabase* database =  [_manager databaseNamed: @"livedayrunner" error: &error];
        CBLDocument *document = [database existingDocumentWithID:[[NSUserDefaults standardUserDefaults] objectForKey:@"documentID"]];
        NSError *err;
        if([document deleteDocument:&err]){
            if (!err) {
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"documentID"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"storedLatLongs"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
    }
}


///*******when the session expires r admin rejected********//
-(void)sessionExpired{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    SplashViewController *splashVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SplashViewController"]; //or the homeController
    window.rootViewController = splashVC;
}

-(void)startLocationUpdateTimer:(int)timeInterval{
    [customLocationUpdateTimer invalidate];
    if(timeInterval == 0){
        timeInterval = 5;
    }
    
    customLocationUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:self
                                                               selector:@selector(updateLocation)
                                                               userInfo:nil
                                                                repeats:YES];
}

-(void)updateLocation{
    
    [self updateLocationToServer];
    
}

-(void)stopLocationUpdateTimer{
    [customLocationUpdateTimer invalidate];
}

@end

