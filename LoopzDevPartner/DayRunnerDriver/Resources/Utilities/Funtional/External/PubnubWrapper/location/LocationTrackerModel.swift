//
//  LocationTrackerModel.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 28/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxSwift
@objc class LocationTrackerModel: NSObject {
      let disposeBag = DisposeBag()
    @objc func locationUpdate(location: [String: Any])
    {
        
        let locationUpdateApi = LocationUpdateApi()
        locationUpdateApi.updateLocation(latLong: location)
//        offlineLocationUpdate(location: location)
    }
    
    @objc func offlineLocationUpdate(location: [String: Any])
    {
        let locationUpdateApi = LocationUpdateApi()
        locationUpdateApi.updateOfflineLocation(newLocation: location)
        locationUpdateApi.subject_response.subscribe(onNext: { (response) in
            let locationTracker =  LocationTracker.sharedInstance() as! LocationTracker
            locationTracker.deletingTheDataFromFromParticularID()
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)

    }
    
}
