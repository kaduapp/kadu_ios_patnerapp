//
//  LocationTracker.h
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//
@class DistanceCalculation;
@class LocationTrackerModel;
@protocol LocationTrackerDelegates
@required
-(void)didUpdateHeadingTo:(float)heading;
-(void)didUpdateLocationWithLatitude:(float)latitude andLongitude:(float)longitude;
@end

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationShareModel.h"

typedef void(^LocationTrackerDistanceChangeCallback) (float totalDistance, CLLocation *location);
typedef void(^LocationTrackerDistanceChangeCallbackOnOff)(float totalDistance, CLLocation *location);
@interface LocationTracker : NSObject <CLLocationManagerDelegate>
@property (strong,nonatomic) id <LocationTrackerDelegates> delegate;
@property(nonatomic,copy)LocationTrackerDistanceChangeCallback distanceCallback;
@property(nonatomic,copy)LocationTrackerDistanceChangeCallbackOnOff distanceCallbackOnOff;

@property (nonatomic) CLLocationCoordinate2D myLastLocation;
@property (nonatomic) CLLocationAccuracy myLastLocationAccuracy;
@property(nonatomic,strong)CLLocation *lastLocaiton;
@property (strong,nonatomic) NSString * accountStatus;
@property (strong,nonatomic) NSString * authKey;
@property (strong,nonatomic) NSString * device;
@property (strong,nonatomic) NSString * name;
@property (strong,nonatomic) NSString * profilePicURL;
@property (strong,nonatomic) NSNumber * userid;
@property (assign,nonatomic) float distance, distanceOnOff;
@property (strong,nonatomic) LocationShareModel * shareModel;
@property (strong,nonatomic) LocationTrackerModel *locationUpdateModel;
@property (nonatomic) CLLocationCoordinate2D myLocation;
@property (nonatomic) CLLocationAccuracy myLocationAccuracy;
@property (assign, nonatomic) NSInteger statusValue;
@property (assign, nonatomic) NSString* lastLatitude;
@property (assign, nonatomic) NSString* lastLongitude;

+ (CLLocationManager *)sharedLocationManager;

- (void)startLocationTracking;
- (void)stopLocationTracking;
- (void)updateLocationToServer;
+ (id)sharedInstance;

-(void)startLocationUpdateTimer:(int)timeInterval;
-(void)stopLocationUpdateTimer;

-(void)deletingTheDataFromFromParticularID;
//@property (strong,nonatomic) id <LocationTrackerDelegates> delegate;
@property (retain) CLLocationManager *globalLocationManager;

@property (retain) DistanceCalculation* distanceCalulation;

@end
