//
//  Fonts.swift
//  UFly
//
//  Created by 3Embed on 11/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class Fonts: NSObject {

    struct Hind {
        static let Bold             = "Hind-Bold"
        static let Light            = "Hind-Light"
        static let Medium           = "Hind-Medium"
        static let Regular          = "Hind-Regular"
        static let Thin             = "Hind-SemiBold"
    }
    
    struct CircularAir {
        static let Light            = "CircularAirPro-Light"
        static let Book             = "CircularAirPro-Bold"
        static let Bold             = "CircularAirPro-Book"
    }
    struct Roboto {
        static let Bold             = "Roboto-Bold"
        static let Light            = "Roboto-Light"
        static let Medium           = "Roboto-Medium"//500
        static let Regular          = "Roboto-Regular"
        static let Thin             = "Roboto-Thin"
        static let Black            = "Roboto-Black"
    }
    
    
    //////////////////////////////////////////////////////////////////
    class func setPrimaryBold(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: Roboto.Bold, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Roboto.Bold, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Roboto.Bold, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    class func setPrimaryLight(_ sender: Any){
        
        if let element = sender as? UILabel {
            element.font = UIFont(name: Roboto.Light, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Roboto.Light, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Roboto.Light, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    class func setPrimaryMedium(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: Roboto.Medium, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Roboto.Medium, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Roboto.Medium, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    class func setPrimaryRegular(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: Roboto.Regular, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Roboto.Regular, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Roboto.Regular, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    class func setPrimaryBlack(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: Roboto.Black, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Roboto.Black, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Roboto.Black, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    
    class func setPrimaryThin(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: Roboto.Thin, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Roboto.Thin, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Roboto.Thin, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    /////////////////////////////////////////////////////
    class func setSeconderyBold(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: CircularAir.Bold, size: element.font.pointSize)
        }
        if let element = sender as? UITextField {
            element.font = UIFont(name: CircularAir.Bold, size: (element.font?.pointSize)!)
        }
    }
    class func setSeconderyLight(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: CircularAir.Light, size: element.font.pointSize)
        }
        if let element = sender as? UITextField {
            element.font = UIFont(name: CircularAir.Light, size: (element.font?.pointSize)!)
        }
    }
    class func setSeconderyBook(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: CircularAir.Book, size: element.font.pointSize)
        }
        if let element = sender as? UITextField {
            element.font = UIFont(name: CircularAir.Book, size: (element.font?.pointSize)!)
        }
    }
}
