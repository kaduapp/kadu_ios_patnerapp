//
//  MessageNotificationDelegate.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 31/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MQTTClient
import CocoaLumberjack
import FirebaseMessaging
import RxCocoa
import RxSwift

class MQTTDelegate: NSObject, MQTTSessionManagerDelegate {
    
    //RxSwift variable to notify for new message
    let mqttOnDemandManager = MQTTOnDemandAppManager()
    static var mqttMessageHandler = MQTTChatResponseHandler()
     let disposeBag = DisposeBag()
    func handleMessage(_ data: Data!, onTopic topic: String!, retained: Bool) {
        do {
            guard let dataObj = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
            DDLogDebug("Received \(dataObj) on:\(topic) r\(retained)")
            
            if !(dataObj.isEmpty)
            {
                if topic == "message/\(Utility.userId)" {
                    MQTTDelegate.mqttMessageHandler.gotChatResponeFromMqtt(responseData: data, topicChannel: topic)
                    return
                }
                
                if (dataObj["action"] as? Int) == 12 && (dataObj["deviceId"] as? String) != Utility.deviceId {
                    Session.expired()
                    return
                }
                if (dataObj["action"] as? Int) == 10 {
                    
                    // Define identifier
                    let notificationName = Notification.Name("gotNewBooking")
                    
                    // Post notification
                    NotificationCenter.default.post(name: notificationName, object: nil)
                
                }
                if (dataObj["action"] as? Int) == 16 {
                    
                    // Define identifier
                    let notificationName = Notification.Name("gotNewBooking")
                    
                    // Post notification
                    NotificationCenter.default.post(name: notificationName, object: nil)
                    let  locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
                    locationtracker.stopLocationUpdateTimer()
                    return
                }
                
                if (dataObj["action"] as? Int) == 14 || (dataObj["action"] as? Int) == 29 {
                    // Define identifier
                    let notificationName = Notification.Name("gotNewBooking")
                    
                    // Post notification
                    NotificationCenter.default.post(name: notificationName, object: nil)
                    
                }

                
                
//                if (dataObj["action"] as? Int) == 111 {
//                     Messaging.messaging().subscribe(toTopic: "driver_5a0ed20685985b60fa3aa992")
//                    Messaging.messaging().subscribe(toTopic: "driver_5a0ed20685985b60fa3aa992")
//                }
                
                let userDef = UserDefaults.standard
                
                if let currencySymbol = dataObj["currencySymbol"] as? String {
                    userDef.set(currencySymbol, forKey: USER_INFO.CURRENCYSYMBOL)
                }
                
                if let mileageMetric = dataObj["mileageMetric"] as? String {
                    userDef.set(mileageMetric, forKey: USER_INFO.DISTANCE)
                }
                mqttOnDemandManager.gotNewMessage(message: dataObj)
            }
            
        } catch let jsonError {
            DDLogDebug("Error !!!\(jsonError)")
        }
    }
    
    func sessionManager(_ sessionManager: MQTTSessionManager!, didDeliverMessage msgID: UInt16) {
        print("Message delivered")
    }
    
    func messageDelivered(_ session: MQTTSession!, msgID: UInt16, topic: String!, data: Data!, qos: MQTTQosLevel, retainFlag: Bool) {
        DDLogDebug( "\(msgID)Message delivered")
        guard let userID = UserDefaults.standard.value(forKey: USER_INFO.USER_ID) as? String else { return }
        session.persistence.deleteAllFlows(forClientId: userID)
    }
    
    func sessionManager(_ sessionManager: MQTTSessionManager!, didChange newState: MQTTSessionManagerState) {
        switch newState {
        case .connected:
            print("connected")
            let mqtt = MQTT.sharedInstance
            mqtt.isConnected = true
            ///Subscribing Message Channel
            let msgTopic = Utility.driverChannel
            let driverId = Utility.userId
            if msgTopic != "" {
                mqtt.subscribeTopic(withTopicName: msgTopic, withDelivering: .exactlyOnce)
            }
            
            mqtt.subscribeTopic(withTopicName: "message/\(driverId)", withDelivering: .atLeastOnce)
//            mqtt.subscribeTopic(withTopicName: "message/#", withDelivering: .atLeastOnce)
            print("Subscribed to topic \(msgTopic)")
            

        case .closed:
            DDLogDebug("disconnected")
//            if let userID = UserDefaults.standard.value(forKey: USER_INFO.USER_ID) as? String {
//                if MQTT.sharedInstance.isConnected {
//                    if userID.count>1 {
//                    MQTT.sharedInstance.manager.reconnect()
//                    }
//                }
//            }
            MQTT.sharedInstance.isConnected = false
            
        default:
            DDLogDebug("disconnected")
            
        }
    }
}
