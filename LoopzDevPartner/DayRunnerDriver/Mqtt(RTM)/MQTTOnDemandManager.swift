//
//  MQTTOnDemandAppManager.swift
//  LiveM
//
//  Created by Raghavendra V on 07/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import MQTTClient
import Foundation

enum RTMMessage: Int  {
    case NewBooking = 11
    case CancelBooking = 41
    case Logout = 12
    case Offline = 16
    case Error = 0
    
    init(rawValue: Int)
    {
        switch (rawValue) {
        case 11:
            self = .NewBooking
            break
        case 41:
            self = .CancelBooking
            break
        default:
            self = .Error
            break
        }
    }
}

class MQTTOnDemandAppManager {
    
    func gotNewMessage(message: [String: Any])
    {
        if let bookingData = message["bookingData"] as? [String:Any] {
            
            if let aStatus = bookingData["action"] as? Int
            {
                let enumRTMMessage: RTMMessage = RTMMessage.init(rawValue: aStatus)
                switch (enumRTMMessage)
                {
                case .NewBooking:
                    MQTT.sharedInstance.mqttNewMessage.onNext((bookingData, .NewBooking))
                    break
                case .CancelBooking:
                    let Vc:NewBookingPopup = NewBookingPopup.instance
                                           Vc.hide()
                    break
                default:
                    break
                }
            }
        }
    }
}

