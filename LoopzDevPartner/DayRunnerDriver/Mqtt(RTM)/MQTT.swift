//
//  MQTTModule.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 11/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MQTTClient
import Foundation
import CocoaLumberjack
import RxSwift

class MQTT : NSObject{
   
    struct Constants {
        /* Live & Dev MQTT Credentials */
        fileprivate static let host = "54.210.20.56"
        fileprivate  static let port:Int = 2052
        static let userName = "kaduMqtt"
        static let password = "kzHnvv5H8hwxPYhp"
    }
    
    var userID: String? {
        get {
            return UserDefaults.standard.value(forKey: USER_INFO.USER_ID) as? String
        }
    }
    
    /// Shared instance object for gettting the singleton object
   @objc static let sharedInstance = MQTT()
    
    ///This flag will tell you that you are connected or not.
    var isConnected : Bool = false
    
    ///current session object will going to store in this.
    var manager : MQTTSessionManager!
    
    ///Used for running the task in the background.
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    
    //RxSwift variable to notify for new message
    var mqttNewMessage = PublishSubject<([String: Any], RTMMessage)>()
    
    /// MQTT delegate object, its going to store the objects of the delegate receiver class.
    let mqttMessageDelegate = MQTTDelegate()
    
    /// Used for creating the initial connection.
    func createConnection() {
        
        /// Observer for app coming in foreground.
        NotificationCenter.default.setObserver(self, selector: #selector(MQTT.reinstateBackgroundTask), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        registerBackgroundTask()
        
        ///creating connection with the proper client ID.
        if let userId = self.userID {
            self.connect(withClientId: userId)
        }
    }
    
    
    /// Used for disconnecting the MQTT Connection
    func disconnectMQTTConnection() {
        if self.isConnected {
            self.isConnected = false
            manager.disconnect(disconnectHandler: nil)
        }
    }
    
    
    /// Used for connecting with MQTT Connection
    func connect() {
        if let userId = self.userID {
            self.connect(withClientId: userId)
        }
    }
    
    /// Used for subscribing the channel
    ///
    /// - Parameters:
    ///   - topic: name of the current topic (It should contain the name of the topic with saperators)
    ///
    /// eg- Message/UserName
    ///   - Delivering: Type of QOS // can be 0,1 or 2.
    fileprivate func subscribe(topic : String, withDelivering Delivering : MQTTQosLevel) {
        if (manager != nil) {
            if (manager.subscriptions != nil) {
                var subscribeDict = manager.subscriptions!
                if (subscribeDict[topic] == nil) {
                    subscribeDict[topic] = Delivering.rawValue as NSNumber
                }
                self.manager.subscriptions = subscribeDict
            } else {
                let subcription = [topic : Delivering.rawValue as NSNumber]
                self.manager.subscriptions = subcription
            }
            self.manager.connect(toLast: { (error) in
                if let error = error{
                    print("error ",error.localizedDescription)
                }
            })
        }
    }
    
    /// Used for reinstate the background task
    @objc func reinstateBackgroundTask() {
        if (backgroundTask == UIBackgroundTaskIdentifier.invalid) {
            registerBackgroundTask()
        }
    }
    
    ///Here I am registering for the background task.
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskIdentifier.invalid)
    }
    

    ///Before background task ending this method is going to be called.
    func endBackgroundTask() {
        DDLogDebug("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskIdentifier.invalid
    }
    
    /// Used for subscribing the channel
    ///
    /// - Parameter channelName: current channel which you want to subscribe.
    func subscribeTopic(withTopicName topicName : String,withDelivering delivering:MQTTQosLevel ) {
        let topicToSubscribe = topicName
        self.subscribe(topic: "\(topicToSubscribe)", withDelivering: delivering)
    }
    
    /// Used for Unsubscribing the channel
    ///
    /// - Parameter channelName: current channel which you want to Unsubscribing.
    func unsubscribeTopic(topic : String) {
        var unsubscribeDict = manager.subscriptions
        if unsubscribeDict?[topic] != nil {
            unsubscribeDict?.removeValue(forKey:topic)
        }
        self.manager.subscriptions = unsubscribeDict
        self.manager.connect(toLast: { (error) in
            if let error = error{
                print("error ",error.localizedDescription)
            }
        })
    }
    
    /// Used for pubishing the data in between channels.
    ///
    /// - Parameters:
    ///   - jsonData: Data in JSON format.
    ///   - channel: current channel name to publish the data to.
    ///   - messageID: current message ID (this ID should be unique)
    ///   - Delivering: Type of QOS // can be 0,1 or 2.
    ///   - retain: true if you wanted to retain the messages or False if you don't
    ///   - completion: This will going to return MQTTSessionCompletionBlock.
    
    func publishData(wthData jsonData: Data, onTopic topic : String, retain : Bool, withDelivering delivering : MQTTQosLevel) {
        if (self.manager != nil) {
            
            manager.send(jsonData, topic: topic, qos: delivering, retain: retain)
        }
        else {
            guard let userID =  self.userID else { return }
            if (userID.count>1) {
                connect(withClientId: userID)
            }
        }
    }
    
    /// Used for connecting with the server.
    ///
    /// - Parameter clientId: current Client ID.
    func connect(withClientId clientId :String) {
        if (self.manager == nil) {
            self.manager = MQTTSessionManager()
            let host = Constants.host
            let port: Int = Constants.port
            //            manager.connect(to: host, port: port, tls: false, keepalive: 45, clean: false, auth: false, user: "" /*Constants.userName*/, pass: "" /*Constants.password*/, will: false, willTopic: nil, willMsg: nil, willQos: .atMostOnce, willRetainFlag: false, withClientId: "LoopzDevDriver_\(clientId)\(Utility.deviceId)", securityPolicy: nil, certificates: nil, protocolLevel: .version311, connectHandler: { (error) in
            //                if let error = error{
            //                    print("error ",error.localizedDescription)
            //                }
            //            })
            manager.delegate = mqttMessageDelegate
            manager.connect(to: host, port: port, tls: false, keepalive: 45, clean: false, auth: true, user: Constants.userName, pass: Constants.password, will: false, willTopic: nil, willMsg: nil, willQos: .atMostOnce, willRetainFlag: false, withClientId: "trineoDriver_\(clientId)\(Utility.deviceId)", securityPolicy: nil, certificates: nil, protocolLevel: .version311, connectHandler: { (error) in
                if let error = error{
                    print("error ",error.localizedDescription)
                }
            })
        } else {
            self.manager.connect(toLast: { (error) in
                if let error = error{
                    print("error ",error.localizedDescription)
                }
            })
        }
    }

}

extension NotificationCenter {
    func setObserver(_ observer: AnyObject, selector: Selector, name: NSNotification.Name, object: AnyObject?) {
        NotificationCenter.default.removeObserver(observer, name: name, object: object)
        NotificationCenter.default.addObserver(observer, selector: selector, name: name, object: object)
    }
}
