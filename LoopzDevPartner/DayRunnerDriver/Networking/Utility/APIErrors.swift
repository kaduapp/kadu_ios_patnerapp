//
//  APICalls.swift
//  UFly
//
//  Created by 3Embed on 27/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire

class APIErrors: NSObject {
    static let disposeBag = DisposeBag()
    enum ErrorCode: Int {
        case Accepted               = 202
        case success                = 200
        case orderCanceled          = 201
        case badRequest             = 400
        case Unauthorized           = 401
        case Required               = 402
        case Forbidden              = 403
        case NotFound               = 404
        case MethodNotAllowed       = 405
        case NotAcceptable          = 406
        case PreconditionFailed     = 412
        case RequestEntityTooLarge  = 413
        case TooManyAttemt          = 429
        case ExpiredToken           = 477
        case InvalidToken           = 499
        case internalServerError    = 500
        case jwtExpired         = 440
        case bookingRejected        = 303
        case bookingAlreadyUpdated  = 302
        
        init(rawValue: Int) {
            
            switch (rawValue) {
            case 200:
                self = .success
                break
            case 440:
                self = .jwtExpired
                break
            case 498:
                Session.expired()
                self = .Unauthorized
                break
            case 201:
                self = .orderCanceled
                break
            case 202:
                self = .Accepted
            default:
                 self = .badRequest
                 break
            }
        }
    }
    
    enum ErrorFlag: Int{
        case success                = 0
        case error                    = 1
    }
    
    //Refresh Token
//    class func refreshToken() {
//        let header = Utility.getHeader()
//        RxAlamofire.requestJSON(.get, APIEndTails.RefreshToken, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
//            Helper.hidePI()
//            let bodyIn = body as! [String:Any]
//            let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
//            if errNum == .success {
//                let newToken = bodyIn[APIResponceParams.Data] as! String
//                Utility.saveSession(token: newToken)
//            }else{
//                APICalls.basicParsing(data:bodyIn,status:head.statusCode)
//            }
//        }, onError: { (Error) in
//        }).disposed(by: APICalls.disposeBag)
//    }
    
    class func basicParsing(data:[String:Any],status: Int) {
        Helper.hidePI()
        let errNum = ErrorCode(rawValue: status)
        switch errNum {
        case .success:
            break
//        case .ExpiredToken:
//            let temp = data[APIResponceParams.Data] as! [String:Any]
//            Utility.saveSession(token: temp[APIResponceParams.SessionToken] as! String)
//            APICalls.refreshToken()
//            break
        case .InvalidToken:
            break
        default:
//            Helper.showAlert(message: data[APIResponceParams.Message] as! String, head: StringConstants.Error, type: 1)
            break
        }
    }
    
    class func getAOTHHeader() -> [String: String] {
        
        var sessionToken = String()
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
            sessionToken = Utility.sessionCheck
        }else{
            sessionToken = String(format: "%@", Utility.sessionToken)

        }
        print("authorization token.....................................................",sessionToken)
        
        return ["authorization": (sessionToken), "language": "es"] as [String: String]
    }
    
}
