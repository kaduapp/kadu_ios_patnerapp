//
//  APIEndTails.swift
//  UFly
//
//  Created by 3Embed on 27/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class APIEndTails: NSObject {
    
  
    static let Zendesk                                  = API.BASE_URL + "zendesk/"
    static let Ticket                                   = Zendesk + "ticket"
    static let Comment                                  = Ticket + "/comments"
    static let HistoryTicket                            = Ticket + "/history/"
    static let SingleTicketDetails                      = Zendesk + "user/ticket/"
    
    static let BaseUrl                                  = API.BASE_URL + "driver/"    
    static let Customer                                 = "customer/"
    static let Business                                 = "business/"
    static let Products                                 = "products/"
    
    static let WalletDetail                             = BaseUrl + "walletDetail"
    static let RechargeWallet                           = BaseUrl + "rechargeWallet"
    static let WalletTransaction                        = BaseUrl + "walletTransaction"
    static let GuestLogin                               = BaseUrl + "guest/signIn"
    static let Address                                  = BaseUrl + "address"
    static let Cart                                     = BaseUrl + "cart"
    static let Card                                     = BaseUrl + "card"
    static let Order                                    = BaseUrl + "order"
    static let Configure                                = BaseUrl + "config"
    static let VersionAppStore                          = "http://itunes.apple.com/lookup?bundleId=\(Utility.BundleId)"
    
 }
