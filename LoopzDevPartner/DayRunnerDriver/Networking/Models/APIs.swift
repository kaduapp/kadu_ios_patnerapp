//
//  APIs.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 21/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class APIs {
    
    let disposebag = DisposeBag()
    let subject_response = PublishSubject<[String:Any]>()
    
    func logoutApi()
    {
        let apiFullName = API.BASE_URL + API.METHOD.LOGOUT
        RxAlamofire.requestJSON(.patch, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            guard (bodyIn["message"] as? String) != nil else {
                self.subject_response.onNext(bodyIn)
                return
            }
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }  else if(errNum == .jwtExpired) {
                UserDefaults.standard.set(bodyIn["data"], forKey: USER_INFO.SESSION_TOKEN)
                let apis = APIs()
                apis.refreshToken()
                _ = apis.subject_response.subscribe(onNext: { (response) in
                    self.logoutApi()
                }, onError: { (error) in
                    Helper.hidePI()
                }) {
                    
                }
            }
            else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
            }, onError: { (Error) in
//                Helper.alertVC(errMSG: "Internal server error.")
                self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    func getNearestStores(ApiName:String)
    {
        
        let apiFullName = API.BASE_URL + API.METHOD.NEARESTSTORES + ApiName
        RxAlamofire.requestJSON(.get,
                                apiFullName,
                                parameters: nil,
                                encoding: JSONEncoding.default,
                                headers: APIErrors.getAOTHHeader())
            .debug()
            .subscribe(onNext: {(head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: { (Error) in
            //                Helper.alertVC(errMSG: "Internal server error.")
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    
    func getLanguages()
    {
        let apiFullName = API.Language_URL
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: {(head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: { (Error) in
            //                Helper.alertVC(errMSG: "Internal server error.")
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func getCancelReasons()
    {
        let apiFullName = API.BASE_URL + API.METHOD.cancelReasonsNew
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: {(head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: { (Error) in
            //                Helper.alertVC(errMSG: "Internal server error.")
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func getSearchItems(storeId:String,index:String,limit:String,search:String) {
        let apiFullName =  API.BASE_URL + "productsSearchByStoreId/" + storeId + "/" + index + "/" + limit + "/" + search
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: {(head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: { (Error) in
            //                Helper.alertVC(errMSG: "Internal server error.")
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)

    }
    
    func getAssignedTrips()
    {
        let apiFullName = API.BASE_URL + API.METHOD.ASSIGNEDTRIPS
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: {(head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
            }, onError: { (Error) in
//                Helper.alertVC(errMSG: "Internal server error.")
                self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func getAppConfigurations()
    {
        let apiFullName = API.BASE_URL + API.METHOD.CONFIG
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }
            else if(errNum == .jwtExpired) {
                UserDefaults.standard.set(bodyIn["data"], forKey: USER_INFO.SESSION_TOKEN)
                let apis = APIs()
                apis.refreshToken()
                _ = apis.subject_response.subscribe(onNext: { (response) in
                    self.getAppConfigurations()
                }, onError: { (error) in
                    //                    Helper.alertVC(errMSG: "Internal server error.")
                    Helper.hidePI()
                }) {
                    
                }
            }
            else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
            }, onError: {[weak self] (Error) in
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func refreshToken()
    {
      
        let apiFullName = API.BASE_URL + "driver/refreshToken"
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: {[weak self] (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                UserDefaults.standard.set(bodyIn["data"], forKey: USER_INFO.SESSION_TOKEN)
                self?.subject_response.onNext(bodyIn)
            }
            else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: {[weak self] (Error) in
//            Helper.alertVC(errMSG: Error.localizedDescription)
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
}


