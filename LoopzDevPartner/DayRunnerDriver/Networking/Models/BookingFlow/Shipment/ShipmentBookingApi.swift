//
//  ShipmentBookingApi.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 27/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import RxAlamofire
import Alamofire

class ShipmentBookingApi: APIs {
    
    func updateStatus(shipmentModel: BookingModel)
    {
        let ud = UserDefaults.standard
        var params: [String: Any]!
        if (ud.double(forKey: "currentLat") != nil) {
            
            if shipmentModel.udpateStatusCurrentStatus == 11 || shipmentModel.udpateStatusCurrentStatus == 13 {
                params =  ["status": shipmentModel.updateStatusStatus,
                           "bookingId":shipmentModel.udpateStatusBID as Any,
//                           "distance":shipmentModel.udpateStatusDistance as Any,
                           "latitude":shipmentModel.udpateStatusLatitude,
                           "longitude":shipmentModel.updateStatusLongitude]
                
            }else{
                params =  ["status": shipmentModel.updateStatusStatus,
                           "bookingId":shipmentModel.udpateStatusBID as Any,
                           "latitude" :shipmentModel.udpateStatusLatitude,
                           "longitude": shipmentModel.updateStatusLongitude]
            }
        }else{
            
//            let bookingParamsModel: BookingModel!
            if shipmentModel.udpateStatusCurrentStatus == 11 || shipmentModel.udpateStatusCurrentStatus == 13 {
                params =  ["status": shipmentModel.updateStatusStatus,
                           "bookingId":shipmentModel.udpateStatusBID as Any,
//                           "distance":shipmentModel.udpateStatusDistance as Any,
                           "latitude":shipmentModel.udpateStatusLatitude,
                           "longitude":shipmentModel.updateStatusLongitude]
                
            }else{
                params =  ["status": shipmentModel.updateStatusStatus,
                           "bookingId":shipmentModel.udpateStatusBID as Any,
                           "latitude" :shipmentModel.udpateStatusLatitude,
                           "longitude": shipmentModel.updateStatusLongitude]
            }
        }
        
        let apiFullName = API.BASE_URL + API.METHOD.UPDATEBOOKINGSTATUS
        RxAlamofire.requestJSON(.patch, apiFullName, parameters: params, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
            
            
        }, onError: { (Error) in
//            Helper.alertVC(errMSG: Error.localizedDescription)
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func completeBooking(shipmentModel: InvoiceModel)
    {
        let dict : [String : Any] =  [
            "status"         : shipmentModel.updateInvoiceStatus,
            "bookingId"     :shipmentModel.updateInvoiceBookingID,
            "signatureUrl"   : shipmentModel.updateInvoiceSignatureURL,
            "documents"      :shipmentModel.updateInvoiceDocumentImage,
//            "tollFee"        :shipmentModel.udpateInvoiceTollFee,
//            "ent_handlingFee"    :shipmentModel.updateInvoiceHandlingFee,
            "receiverName"   :shipmentModel.updateInvoiceReceiverName,
            "receiverPhone"    :shipmentModel.updateInvoiceReceiverPhone,
            "rating"            :shipmentModel.updateInvoiceRating,
            "latitude"                    :shipmentModel.updateInvoiceLatitude,
            "longitude"                   :shipmentModel.updateInvoiceLongitude]
        
        let apiFullName = API.BASE_URL + API.METHOD.UPDATEBOOKINGSTATUS
        RxAlamofire.requestJSON(.put, apiFullName, parameters: dict, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
            
            
        }, onError: { (Error) in
//            Helper.alertVC(errMSG: Error.localizedDescription)
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
        
    }
    
    func makeServiceCallToUpdateDeliveryOrder(shipmentModel: Shipment) {
        
        let params = ["items" : shipmentModel.updateOrderDetails,
                      "latitude": shipmentModel.updateOrderLatitude as! Double,
                      "longitude": shipmentModel.updateOrderLongitude as! Double,
                      "orderId": shipmentModel.updateOrderId,
                      "deliveryCharge": 0.00] as [String : Any]
        
        print(params)
        
        let apiFullName = API.BASE_URL + API.METHOD.HISTORYSERVICE
        RxAlamofire.requestJSON(.patch , apiFullName, parameters: params, encoding:JSONEncoding.default, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
//                let dict = ["errFlag" : 1]
                self.subject_response.onNext(bodyIn)
            }else{
                if let message = bodyIn["message"] as? String{
                    Helper.alertVC(errMSG: message)
                }else{
                    Helper.alertVC(errMSG: "Some error occured")
                }
            }
            
            
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    
    
    
    func makeServiceCallToUpdateOrder(shipmentModel: Shipment) {
        
        let emptyJsonData = try! JSONSerialization.data(withJSONObject: [:])
        let emptyJsonString = NSString(data: emptyJsonData, encoding: String.Encoding.utf8.rawValue)
        print(emptyJsonString)
        
        let jsonData = try! JSONSerialization.data(withJSONObject: shipmentModel.items)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
        
        let jsonData1 = try! JSONSerialization.data(withJSONObject: shipmentModel.newUpdatedOrders)
        let jsonString1 = NSString(data: jsonData1, encoding: String.Encoding.utf8.rawValue)
        
        var params: [String: Any]!
        
        if(shipmentModel.updateType == "3") {
            params = ["items" :jsonString!,
                      "newItems" :emptyJsonString!,
                      "updateType" : shipmentModel.updateType,
                      "extraNote" : "sdafsafsaffas",
                      "ipAddress" : "dsfsafas",
                      "latitude": shipmentModel.updateOrderLatitude as! Double,
                      "longitude": shipmentModel.updateOrderLongitude as! Double,
                      "orderId": shipmentModel.updateOrderId,
                      "deliveryCharge": 0.00] as [String : Any]
        }else if(shipmentModel.updateType == "1") {
            params = ["items" :jsonString!,
                      "newItems" :emptyJsonString!,
                      "updateType" : shipmentModel.updateType,
                      "extraNote" : "sdafsafsaffas",
                      "ipAddress" : "dsfsafas",
                      "latitude": shipmentModel.updateOrderLatitude as! Double,
                      "longitude": shipmentModel.updateOrderLongitude as! Double,
                      "orderId": shipmentModel.updateOrderId,
                      "deliveryCharge": 0.00] as [String : Any]
        }else {
            params = ["items" :jsonString!,
                          "newItems" : jsonString1!,
                          "updateType" : shipmentModel.updateType,
                          "extraNote" : "sdafsafsaffas",
                          "ipAddress" : "dsfsafas",
                          "latitude": shipmentModel.updateOrderLatitude as! Double,
                          "longitude": shipmentModel.updateOrderLongitude as! Double,
                          "orderId": shipmentModel.updateOrderId,
                          "deliveryCharge": 0.00] as [String : Any]
        }
        
        
        print(params)
        
        let apiFullName = API.BASE_URL + API.METHOD.UPDATEORDER
        RxAlamofire.requestJSON(.patch , apiFullName, parameters: params, encoding:JSONEncoding.default, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                //                let dict = ["errFlag" : 1]
                self.subject_response.onNext(bodyIn)
            }else{
                if let message = bodyIn["message"] as? String{
                    Helper.alertVC(errMSG: message)
                }else{
                    Helper.alertVC(errMSG: "Some error occured")
                }
            }
            
            
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func makeServiceCallToCancelOrder(shipmentModel: Shipment) {
        
        let params = ["ipAddress" : shipmentModel.ipAddress,
                      "latitude": shipmentModel.updateOrderLatitude!,
                      "longitude": shipmentModel.updateOrderLongitude!,
        "orderId": shipmentModel.updateOrderId] as [String: Any] 
        
        print(params)
        
        let apiFullName = API.BASE_URL + API.METHOD.HISTORYSERVICE
        RxAlamofire.requestJSON(.put , apiFullName, parameters: params, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .orderCanceled {
                let dict = ["errFlag" : 1]
                self.subject_response.onNext(dict)
            }else{
                if let message = bodyIn["message"] as? String{
                    Helper.alertVC(errMSG: message)
                }else{
                    Helper.alertVC(errMSG: "Some error occured")
                }
            }

            
            
        }, onError: { (Error) in
//            Helper.alertVC(errMSG: Error.localizedDescription)
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }

    
    func makeServiceCallToUpdateDeliveryStatus(shipmentModel: Shipment){
        let delivertSheduleType:Int = UserDefaults.standard.integer(forKey: "driverScheduleType")
         var params : [String:Any]!
        if delivertSheduleType == 1 {
            
            if(shipmentModel.updateOrderStatus == 27){
                
                params   = [ "status" : shipmentModel.updateOrderStatus,
                             "orderId" : shipmentModel.updateOrderId,
                             "lat" : shipmentModel.updateOrderLatitude,
                             "long" : shipmentModel.updateOrderLongitude,
                             "signatureUrl" : shipmentModel.updateOrderSignature,
                             "rating": shipmentModel.updateOrderRating,
                             "storeId" : shipmentModel.storeId,
                             "weight" : shipmentModel.weight
                ]
                
            }else if(shipmentModel.updateOrderStatus == 11){
                
                params = [ "status" : shipmentModel.updateOrderStatus,
                           "orderId" : shipmentModel.updateOrderId,
                           "lat" : shipmentModel.updateOrderLatitude,
                           "long" : shipmentModel.updateOrderLongitude,
                           "signatureUrl" : shipmentModel.updateOrderSignature,
                           "rating": shipmentModel.updateOrderRating,
                           "slotId" : shipmentModel.slotId,
                           "receiverName": " ",
                           "receiverPhone": " "
                ]
                
            }else {
                
                params = [ "status" : shipmentModel.updateOrderStatus,
                           "orderId" : shipmentModel.updateOrderId,
                           "lat" : shipmentModel.updateOrderLatitude,
                           "long" : shipmentModel.updateOrderLongitude,
                           "signatureUrl" : shipmentModel.updateOrderSignature,
                           "rating": shipmentModel.updateOrderRating,
                           "receiverName": " ",
                           "receiverPhone": " "
                ]
                
            }

        }else {
           
            if(shipmentModel.updateOrderStatus == 27){
                
                params   = [ "status" : shipmentModel.updateOrderStatus,
                             "orderId" : shipmentModel.updateOrderId,
                             "lat" : shipmentModel.updateOrderLatitude,
                             "long" : shipmentModel.updateOrderLongitude,
                             "signatureUrl" : shipmentModel.updateOrderSignature,
                             "rating": shipmentModel.updateOrderRating,
                             "storeId" : shipmentModel.storeId,
                             "weight" : shipmentModel.weight
                ]
                
            }else if(shipmentModel.updateOrderStatus == 11){
                
                params = [ "status" : shipmentModel.updateOrderStatus,
                           "orderId" : shipmentModel.updateOrderId,
                           "lat" : shipmentModel.updateOrderLatitude,
                           "long" : shipmentModel.updateOrderLongitude,
                           "signatureUrl" : shipmentModel.updateOrderSignature,
                           "rating": shipmentModel.updateOrderRating,
                           "receiverName": " ",
                           "receiverPhone": " "
                ]
                
            }else {
                
                params = [ "status" : shipmentModel.updateOrderStatus,
                           "orderId" : shipmentModel.updateOrderId,
                           "lat" : shipmentModel.updateOrderLatitude,
                           "long" : shipmentModel.updateOrderLongitude,
                           "signatureUrl" : shipmentModel.updateOrderSignature,
                           "rating": shipmentModel.updateOrderRating,
                           "receiverName": " ",
                           "receiverPhone": " "
                ]
                
            }

        }
        
  
        print(params)
        
        let apiFullName = API.DISPATCHER_URL + API.METHOD.UPDATEBOOKINGSTATUS
        RxAlamofire.requestJSON(.post, apiFullName, parameters: params, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                let dict = ["errFlag" : 1]
                self.subject_response.onNext(dict)
            }else{
                if let message = bodyIn["message"] as? String{
                    Helper.alertVC(errMSG: message)
                }else{
                    Helper.alertVC(errMSG: "Some error occured")
                }
            }
            
            
        }, onError: { (Error) in
//            Helper.alertVC(errMSG: Error.localizedDescription)
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
        
    }
    
}
