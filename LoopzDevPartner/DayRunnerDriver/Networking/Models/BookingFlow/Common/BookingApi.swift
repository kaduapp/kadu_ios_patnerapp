 //
//  NewBookingApi.swift
//  KarruPro
//
//  Created by Rahul Sharma on 23/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxAlamofire
import Alamofire
import RxSwift
class BookingApi: APIs {
      let disposeBag = DisposeBag()
    func acknowledgeBooking(bookingId: Any){

        let params:[String: Any] = [
            "orderId": bookingId as! NSNumber
//            "name" : "Yogesh"
        ]
        print(params)
        let apiFullName = API.DISPATCHER_URL + API.METHOD.ACKNOWLEDGEBK
        RxAlamofire.requestJSON(.post, apiFullName, parameters: params, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
//                self.apiResponse.onNext(ApiResponse.init(status: true, response: bodyIn))
            }else{
//                Helper.alertVC(errMSG: bodyIn["message"] as! String)
//                self.apiResponse.onNext(ApiResponse.init(status: false, response: bodyIn))
            }
            
            
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    
    
    func respondToBooking(data bookingData: NewbookingModel)
    {
        let params : [String : Any] =  [
            "status": bookingData.status,
            "orderId" :bookingData.bookingID as? Int ?? 0,
            "lat": bookingData.latitude!,
            "long": bookingData.longitude!]
        
        print(params)
        let apiFullName =  API.DISPATCHER_URL + API.METHOD.RESPONDAPPT

        RxAlamofire.requestJSON(.post, apiFullName, parameters: params, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue:head.statusCode)
            if errNum == .success {
                let dict:[String: Any] = ["errFlag": 0]
                self.subject_response.onNext(dict)
            }else if(errNum == .jwtExpired){
                UserDefaults.standard.set(bodyIn["data"], forKey: USER_INFO.SESSION_TOKEN)
                let apis = APIs()
                apis.refreshToken()
                apis.subject_response.subscribe(onNext: { (response) in
                    self.respondToBooking(data: bookingData)
                }, onError: { (error) in
                    Helper.hidePI()
                }, onCompleted: {
                    
                }) {
                    
                }.disposed(by: self.disposeBag)
            }
            else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
            
            
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func getCancellationReason()
    {
        let apiFullName = API.BASE_URL + API.METHOD.CANCELREASONS + "0"
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            
            let errNum: APIErrors.ErrorFlag? = APIErrors.ErrorFlag(rawValue: bodyIn["errFlag"] as! Int)!
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["errMsg"] as! String)
            }
            
            
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func cancelBooking(cancelModel: cancelModel)
    {
        
        let params : [String : Any] =  ["ent_booking_id": cancelModel.bookingID,
                                        "ent_status":cancelModel.status,
                                        "ent_reason":cancelModel.reason]

        
        let apiFullName = API.BASE_URL + API.METHOD.CANCELBOOKING
        RxAlamofire.requestJSON(.put, apiFullName, parameters: params, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            
            let errNum: APIErrors.ErrorFlag? = APIErrors.ErrorFlag(rawValue: bodyIn["errFlag"] as! Int)!
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["errMsg"] as! String)
            }
            
            
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    
    
}
