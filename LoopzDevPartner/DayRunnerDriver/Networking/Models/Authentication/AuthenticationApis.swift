//
//  AuthenticationApis.swift
//  KarruPro
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class AuthenticationApis: APIs {
      let disposeBag = DisposeBag()
//    let subject_response = PublishSubject<[String:Any]>()
    //Api for SignIn
    func signInApi(signInModel model: AuthenticationModel)
    {
        let params : [String : Any] =  [
            "countryCode"      : model.countryCode,
            "mobile"           : model.userName,
            "password"         : model.password,
            "deviceId"         : model.deviceID,
            "pushToken"        : model.pushToken ,
            "appVersion"       : model.appVersion,
            "deviceMake"       : model.devMake,
            "deviceModel"      : model.devModel,
            "deviceType"       : model.devType,
            "deviceTime"       : model.deviceTime
        ]
        print(params)
        let apiFullName = API.BASE_URL + API.METHOD.LOGIN
        
        RxAlamofire.requestJSON(.post, apiFullName, parameters: params , headers: APIErrors.getAOTHHeader()).debug().subscribe(onNext: {(head, body) in
            let bodyIn = body as![String:Any]
            
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode = APIErrors.ErrorCode(rawValue: head.statusCode)
            print(errNum)
            if errNum == .success {
                
                let bodyData = bodyIn["data"] as! [String: Any]
                self.subject_response.onNext(bodyData)
            }else{
                
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: {[weak self] (Error) in
//            Helper.hidePI()
//            Helper.alertVC(errMSG: "Internal server error.")
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    //Select the vehicleID
    func selectVehicle(selectVehicle model: AuthenticationModel)
    {
        let params : [String : Any] =  ["workplaceId"   : model.vehicleWorkplaceID]
//                                        "vehicleTypeId" :model.vehicleID]
        // "goodTypes"      :""/*model.vehicleGoodType*/]
        print(params as NSDictionary)
        let apiFullName = API.BASE_URL + API.METHOD.VEHICLEDEFAULT
        
        RxAlamofire.requestJSON(.post, apiFullName, parameters: params, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: {(head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                if let bodyData = bodyIn["data"] as? [String: Any]
                {
                    self.subject_response.onNext(bodyData)
                }
                else{
                    self.subject_response.onNext(bodyIn)
                }
            }
            else if(errNum == .jwtExpired){
                UserDefaults.standard.set(bodyIn["data"], forKey: USER_INFO.SESSION_TOKEN)
                let apis = APIs()
                apis.refreshToken()
                _ = apis.subject_response.subscribe(onNext: { (response) in
                    self.selectVehicle(selectVehicle: model)
                }, onError: { (error) in
                    Helper.hidePI()
                }) {
                    
                }.disposed(by: self.disposeBag)
            }
            else{
                
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
                
            }
        }, onError: {[weak self] (Error) in
//            Helper.alertVC(errMSG: "Internal server error.")
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    //Verify Referal Code
    func verifyRefferalCode(referalCode model: AuthenticationModel)
    {
        let params : [String : Any] =  ["code": model.refferalCode,
                                        "type": model.deviceID,
                                        "lat" : model.latitude,
                                        "long": model.longitude]
        print(params)
        let apiFullName = API.BASE_URL + API.METHOD.REFERRALCODE
        
        RxAlamofire.requestJSON(.post, apiFullName, parameters: params, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            if(bodyIn["statusCode"] as! Int == 200)
            {
                let errNum: APIErrors.ErrorFlag? = APIErrors.ErrorFlag(rawValue: bodyIn["errFlag"] as! Int)!
                if errNum == .success {
                    if let bodyData = bodyIn["data"] as? [String: Any]
                    {
                        self.subject_response.onNext(bodyData)
                    }
                    else{
                        self.subject_response.onNext(bodyIn)
                    }
                }else{
                    
                    Helper.alertVC(errMSG: bodyIn["errMsg"] as! String)
                    
                }
            }
        }, onError: { (Error) in
//            Helper.alertVC(errMSG: "Internal server error.")
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    //Verify phone/Email
    func verifyPhoneOrEmail(phoneOrEmail model: AuthenticationModel)
    {
        let params : [String : Any]!
        if(model.validateType == "2")
        {
            params  = [
                "email"         : model.verifal,
                "verifyType"    : model.validateType]
        }
        else
        {
            params  = [
                "countryCode" : model.countryCode,
                "mobile"      : model.verifal,
                "verifyType"  : model.validateType]
        }
        print(params)
        let apiFullName = API.BASE_URL + API.METHOD.EMAILPHONEVALIDATE
        
        RxAlamofire.requestJSON(.post, apiFullName, parameters: params, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: {  (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                let dict: [String: Any] = ["errFlag": head.statusCode]
                self.subject_response.onNext(dict)
                
            }else{
                
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
                let dict: [String: Any] = ["errFlag": head.statusCode]
               self.subject_response.onNext(dict)
            }
            
        }, onError: {[weak self] (Error) in
//            Helper.alertVC(errMSG: "Internal server error.")
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    //Get OTP
    func getOtp(otp model: AuthenticationModel)
    {
        let params : [String : Any] = ["mobile"   : model.fullMobileNo,
                                       "countryCode" : model.countryCode]
        
        print(params)
        let apiFullName = API.BASE_URL + API.METHOD.SignupGetOTP
        
        RxAlamofire.requestJSON(.post, apiFullName, parameters: params, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                if let bodyData = bodyIn["message"] as? String
                {
                    let flag: [String: Any] = ["1": bodyData]
                    self.subject_response.onNext(flag)
                }
                else{
                    self.subject_response.onNext(bodyIn)
                }
            }else{
                
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
                if let bodyData = bodyIn["data"] as? [String: Any]
                {
                    self.subject_response.onNext(bodyData)
                }
                else{
                    self.subject_response.onNext(bodyIn)
                }
            }
            
        }, onError: {[weak self] (Error) in
//            Helper.alertVC(errMSG: "Internal server error.")
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    //Verify OTP
    func verifyOtp(otp model: AuthenticationModel)
    {
        let params : [String : Any] =  ["mobile"   : model.fullMobileNo,
                                        "code"     : model.code,
                                        //                                        "processType":model.processType,
            "countryCode":model.countryCode]
        
        print(params)
        let apiFullName = API.BASE_URL + API.METHOD.verifyOTP
        
        RxAlamofire.requestJSON(.post, apiFullName, parameters: params, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                let dict = ["errFlag": head.statusCode]
                self.subject_response.onNext(dict)
            }else{
                
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
                let dict = ["errFlag": head.statusCode]
                self.subject_response.onNext(dict)
            }
            
        }, onError: { [weak self] (Error) in
//            Helper.alertVC(errMSG: "Internal server error.")
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    //    signUP APi
    func signUP(signUP model: AuthenticationModel)
    {
        let params = [     "firstName"             : model.firstName,
                           "lastName"              : model.lastName,
                           "email"                 : model.email,
                           "password"              : model.password,
                           "mobile"                : model.mobileNo,
                           "countryCode"           : model.countryCode,
                           "zipCode"               : model.zipCode,
                           "latitude"              : model.latitude,
                           "longitude"             : model.longitude,
                           "profilePic"            : model.profilePic,
                           "deviceId"              : model.deviceID,
//                           "plateNo"               : model.plateNo,
//                           "type"                  : model.vehicleID,
//                           "vehicleMake"           : model.make,
//                           "vehicleModel"          : model.model,
//                           "vehicleImage"          : model.vehicleImage,
                           "operator"              : "" ,
                           "driverLicense"         : model.driverLicense,
                           "accountType"           : "1",
                           "deviceType"            : "1",
                           "cityId"                 :model.cityId,
                           "cityName"               :model.cityName,
                           "driverLicenseNumber"   : model.refferalCode,
//                           "insurancePhoto"        : model.insurancePhoto,
//                           "carriagePermit"        : model.carrierPermit,
//                           "regCert"               : model.regCert,
                           "pushToken"             : model.pushToken ,
                           "appVersion"            : model.appVersion,
                           "deviceMake"            : model.devMake,
                           "deviceModel"           : model.devModel,
                           "driverLicenseExpiry"   : model.licenseExp,
                           "dateOfBirth"           : model.dob,
                           "zones"                 : model.selectedZones
            ] as [String : Any]
        
        
        print(params)
        let apiFullName = API.BASE_URL + API.METHOD.SIGNUP
        
        RxAlamofire.requestJSON(.post, apiFullName, parameters: params, encoding: JSONEncoding.default ,headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: {  (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                let dict = ["errFlag": head.statusCode]
                self.subject_response.onNext(dict)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
            
        }, onError: { [weak self] (Error) in
//            Helper.alertVC(errMSG: "Internal server error.")
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func getOperatorDetails()
    {
        let apiFullName = API.BASE_URL + API.METHOD.getZones
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func getZonesDetails(cityId: String)
    {
        let apiFullName = API.BASE_URL + API.METHOD.getCityZones + cityId
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func getVehicleMakeModelData()
    {
        let apiFullName = API.BASE_URL + API.METHOD.makeModel
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: {  (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: { [weak self] (Error) in
//            Helper.alertVC(errMSG: "Internal server error.")
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func getVehicleMake()
    {
        let apiFullName = API.BASE_URL + API.METHOD.makeModel
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: {(head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: {[weak self] (Error) in
//            Helper.alertVC(errMSG: "Internal server error.")
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func resendOTP(mobileNumber: VerifyModel)
    {
        let dict : [String : Any] =  [
            "ent_email_mobile":mobileNumber.mobileNumber,
            "userType":mobileNumber.userType,
            "ent_type":mobileNumber.entType
        ]
        
        let apiFullName = API.BASE_URL + API.METHOD.FORGOTPASSWORD
        RxAlamofire.requestJSON(.post, apiFullName, parameters: dict, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorFlag? = APIErrors.ErrorFlag(rawValue: bodyIn["errFlag"] as! Int)!
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["errMsg"] as! String)
            }
        }, onError: { (Error) in
//            Helper.alertVC(errMSG: "Internal server error.")
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
        
    }
    
    func getCity()
    {
        let apiFullName = API.BASE_URL + API.METHOD.GETCITY
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorFlag? = APIErrors.ErrorFlag(rawValue: bodyIn["errFlag"] as! Int)!
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["errMsg"] as! String)
            }
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func updatePassword(password: SetNewPassword)
    {
        let params : [String : Any] =  ["code"          : password.secureNumber,
                                        "password"      : password.password,
                                        "countryCode"   : password.countryCode,
                                        "mobile"        : password.mobileNumber]
        
        let apiFullName = API.BASE_URL + API.METHOD.UPDATEPASSWORD
        RxAlamofire.requestJSON(.patch, apiFullName, parameters: params, headers: APIErrors.getAOTHHeader()).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func getVehicletype(serviceName: String)
    {
        
        let apiFullName = API.BASE_URL + serviceName
        
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: {(head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: {[weak self] (Error) in
//            Helper.alertVC(errMSG: "Internal server error.")
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
}
