//
//  LocationUpdateApi.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 28/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import RxAlamofire
import RxSwift
class LocationUpdateApi: APIs {
      let disposeBag = DisposeBag()
    func updateLocation(latLong location: [String:Any])
    {
        
        print(location)
        let head = APIErrors.getAOTHHeader()
        let apiFullName = API.DISPATCHER_URL + API.METHOD.LOCATION
        RxAlamofire.requestJSON(.patch, apiFullName, parameters: location, headers: ["language": "0",
                                                                                     "authorization": head["authorization"]!] )
            .debug()
            .subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                
                let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
                if errNum == .success {
                    self.subject_response.onNext(bodyIn)
                } else if(errNum == .jwtExpired) {
                    UserDefaults.standard.set(bodyIn["data"], forKey: USER_INFO.SESSION_TOKEN)
                    let apis = APIs()
                    apis.refreshToken()
                    _ = apis.subject_response.subscribe(onNext: { (response) in
                        self.updateLocation(latLong: location)
                    }, onError: { (error) in
                        Helper.hidePI()
                    }) {
                        
                    }.disposed(by: self.disposeBag)
                }
                else{
                    Helper.alertVC(errMSG: bodyIn["message"] as! String)
                }
            }, onError: {[weak self] (Error) in
                self?.subject_response.onError(Error)
            }).disposed(by: disposebag)
    }
    
    func updateOfflineLocation(newLocation location: [String:Any])
    {
        let apiFullName = API.BASE_URL + API.METHOD.OFFLINELATLONGS
        RxAlamofire.requestJSON(.put, apiFullName, parameters: location, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            } else if(errNum == .jwtExpired) {
                UserDefaults.standard.set(bodyIn["data"], forKey: USER_INFO.SESSION_TOKEN)
                let apis = APIs()
                apis.refreshToken()
                _ = apis.subject_response.subscribe(onNext: { (response) in
                    self.updateOfflineLocation(newLocation: location)
                }, onError: { (error) in
                    Helper.hidePI()
                }) {
                    
                }.disposed(by: self.disposeBag)
            }
            else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: {[weak self] (Error) in
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
}
