//
//  HistoryApi.swift
//  KarruPro
//
//  Created by Rahul Sharma on 21/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxAlamofire
import RxSwift
import RxCocoa

class HistoryApi: APIs {
    
    let newSubject_response = PublishSubject<[[String:Any]]>()
    
    func historyApi(params: [String:Any])
    {
        print(params)
        let apiFullName = API.BASE_URL + API.METHOD.HISTORYSERVICE
        RxAlamofire.requestJSON(.post, apiFullName, parameters: params, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as! [String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                if let bodyData = bodyIn["data"]  as? [String: Any] {
                    self.subject_response.onNext( bodyData)
                }
            }else{
                
                if let bodyData = bodyIn  as? [String: Any] {
                    self.subject_response.onNext( bodyData)
                }
                
            }
        }, onError: {[weak self] (Error) in
            //            Helper.alertVC(errMSG: "Internal server error.")
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
}
