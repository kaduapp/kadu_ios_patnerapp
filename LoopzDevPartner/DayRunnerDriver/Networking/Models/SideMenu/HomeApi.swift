//
//  HomeApi.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 23/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxAlamofire
import RxSwift
class HomeApi: APIs {
      let disposeBag = DisposeBag()
    func updateStatus(statusCode status: Home) {
        
        let params : [String : Any] =  [
            "status": status.driverStatus]
        
        let head = APIErrors.getAOTHHeader()
        let apiFullName = API.DISPATCHER_URL + API.METHOD.UPDATEDRIVERSTATUS
        RxAlamofire.requestJSON(.patch, apiFullName, parameters: params, headers: ["language": "0",
                                                                                   "authorization": head["authorization"]!]  ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            } else if(errNum == .jwtExpired) {
                UserDefaults.standard.set(bodyIn["data"], forKey: USER_INFO.SESSION_TOKEN)
                let apis = APIs()
                apis.refreshToken()
                _ = apis.subject_response.subscribe(onNext: { (response) in
                    self.updateStatus(statusCode: status)
                }, onError: { (error) in
                    Helper.hidePI()
                }) {
                    
                }.disposed(by: self.disposeBag)
            }
            else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: {[weak self] (Error) in
//            Helper.alertVC(errMSG: "Internal server error.")
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }

}
