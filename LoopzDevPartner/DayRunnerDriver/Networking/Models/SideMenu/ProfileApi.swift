//
//  ProfileApi.swift
//  KarruPro
//
//  Created by Rahul Sharma on 22/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxAlamofire
import RxSwift
class ProfileApi: APIs {
      let disposeBag = DisposeBag()
    func getProfileData()
    {
        let apiFullName = API.BASE_URL + API.METHOD.PROFILEDATA
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: {(head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            } else if(errNum == .jwtExpired) {
                UserDefaults.standard.set(bodyIn["data"], forKey: USER_INFO.SESSION_TOKEN)
                let apis = APIs()
                apis.refreshToken()
                _ = apis.subject_response.subscribe(onNext: { (response) in
                    self.getProfileData()
                }, onError: { (error) in
                    Helper.hidePI()
                }) {
                    
                    }.disposed(by: self.disposeBag)
            }
            
            else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: {[weak self] (Error) in
//            Helper.alertVC(errMSG: "Internal server error.")
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func changeNumber(num number: profileUpdateModel) {
        let dict : [String : Any] =  [
            "mobile" :number.updatePhoneNumber,
            "countryCode": number.countryCode]
        
        let apiFullName = API.BASE_URL + API.METHOD.UPDATEPROFILE
        RxAlamofire.requestJSON(.patch, apiFullName, parameters: dict, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            } else if(errNum == .jwtExpired) {
                UserDefaults.standard.set(bodyIn["data"], forKey: USER_INFO.SESSION_TOKEN)
                let apis = APIs()
                apis.refreshToken()
                _ = apis.subject_response.subscribe(onNext: { (response) in
                    self.changeNumber(num: number)
                }, onError: { (error) in
                    Helper.hidePI()
                }) {
                    
                }.disposed(by: self.disposeBag)
            }
            
            else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: {[weak self] (Error) in
            Helper.alertVC(errMSG: Error.localizedDescription)
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
        
    }
    
    func createNewPassword(params: ForgotPasswordModel)
    {
        
        let dict : [String : Any] =  [
            "emailOrMobile":params.email,
            "verifyType":params.type,
            "countryCode": params.countryCode
        ]
        
        let apiFullName = API.BASE_URL + API.METHOD.FORGOTPASSWORD
        RxAlamofire.requestJSON(.post, apiFullName, parameters: dict, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success || errNum == .Accepted {
                self.subject_response.onNext(bodyIn)
            }else{
                if let dict = bodyIn["message"] as? [String: Any] {
                    if let errMssg = dict["1"] as? String {
                         Helper.alertVC(errMSG: errMssg)
                    }
                } else {
                    if let message = bodyIn["message"] as? String {
                        Helper.alertVC(errMSG: message)
                    }
                }
            }
        }, onError: {[weak self] (Error) in
            Helper.alertVC(errMSG: Error.localizedDescription)
            self?.subject_response.onError(Error)
        }).disposed(by: self.disposebag)
    }
    
    func updateName(updatedName name: profileUpdateModel)
    {
        let dict : [String : Any] =  [
            "name" :name.updateProfileName]
        
        let apiFullName = API.BASE_URL + API.METHOD.UPDATEPROFILE
        RxAlamofire.requestJSON(.patch, apiFullName, parameters: dict, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            } else if(errNum == .jwtExpired) {
                UserDefaults.standard.set(bodyIn["data"], forKey: USER_INFO.SESSION_TOKEN)
                let apis = APIs()
                apis.refreshToken()
                _ = apis.subject_response.subscribe(onNext: { (response) in
                    self.updateName(updatedName: name)
                }, onError: { (error) in
                    Helper.alertVC(errMSG: error.localizedDescription)
                    Helper.hidePI()
                }) {
                    
                }.disposed(by: self.disposeBag)
            }
            else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: {[weak self] (Error) in
            Helper.alertVC(errMSG: Error.localizedDescription)
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
        
    }
    
    func updateNumber(number: profileUpdateModel)
    {
        let dict : [String : Any] =       ["mobile"   : number.updatePhoneNumber,
                                           "countryCode" : number.countryCode]
        let apiFullName = API.BASE_URL + API.METHOD.SignupGetOTP
        RxAlamofire.requestJSON(.post, apiFullName, parameters: dict, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: {[weak self] (Error) in
            Helper.alertVC(errMSG: Error.localizedDescription)
            self?.subject_response.onError(Error)
        }).disposed(by: self.disposebag)
        
    }
    
    func updateProfilePic(newPic pic: ProfileModel)
    {
        let dict : [String : Any] =  ["profilePic" : pic.profileImageURL,
                                      ]
        let apiFullName = API.BASE_URL + API.METHOD.UPDATEPROFILE
        RxAlamofire.requestJSON(.patch, apiFullName, parameters: dict, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            } else if(errNum == .jwtExpired) {
                UserDefaults.standard.set(bodyIn["data"], forKey: USER_INFO.SESSION_TOKEN)
                let apis = APIs()
                apis.refreshToken()
                _ = apis.subject_response.subscribe(onNext: { (response) in
                    self.updateProfilePic(newPic: pic)
                }, onError: { (error) in
//                    Helper.alertVC(errMSG: error.localizedDescription)
                    Helper.hidePI()
                }) {
                    
                }.disposed(by: self.disposeBag)
            }
            else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: {[weak self] (Error) in
            Helper.alertVC(errMSG: Error.localizedDescription)
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
        
    }
    
    func changePassword(newPassword password: NewPasswordModel)
    {
        let params : [String : Any] =  ["password": password.newPassword,
                                        ]
        let apiFullName = API.BASE_URL + API.METHOD.UPDATEPROFILE
        RxAlamofire.requestJSON(.patch, apiFullName, parameters: params, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: {(head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }
            else if(errNum == .jwtExpired) {
                UserDefaults.standard.set(bodyIn["data"], forKey: USER_INFO.SESSION_TOKEN)
                let apis = APIs()
                apis.refreshToken()
                _ = apis.subject_response.subscribe(onNext: { (response) in
                    self.changePassword(newPassword: password)
                }, onError: { (error) in
                    Helper.hidePI()
                }) {
                    
                }.disposed(by: self.disposeBag)
            }
            else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: {[weak self] (Error) in
//            Helper.alertVC(errMSG: Error.localizedDescription)
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
}
    

