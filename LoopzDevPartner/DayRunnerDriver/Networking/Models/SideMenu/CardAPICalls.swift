//
//  CardAPICalls.swift
//  Rinn
//
//  Created by 3Embed on 06/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxAlamofire
import Alamofire

class CardAPICalls: NSObject {
    
    static let disposeBag = DisposeBag()
    //Add Cart Data
    class func addCard(token:String) ->Observable<Card> {
//        if !AppConstants.Reachable {
////            Helper.showNoInternet()
//            return Observable.create{ observer in
//                return Disposables.create()
//                }.share(replay: 1)
//        }
       // Helper.showPI(string: "")
         Helper.showPI(message: "Loading...")
        let header = APIErrors.getAOTHHeader()
        let url  = APIEndTails.Card
        let emailString:String = UserDefaults.standard.value(forKey: "emailString") as! String
        UserDefaults.standard.synchronize()
        let params = [
            APIRequestParams.CardToken  : token,
            APIRequestParams.Email      : emailString
            ] as [String:Any]
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, url, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    if let bodyIn = body as? [String:Any] {
                        if let bodyData = bodyIn[APIResponceParams.Data] as? [String:Any] {
                             if let cards = bodyData["cards"] as? [ Any] {
                                observer.onNext(Card.init(data: cards.last as! [String : Any]))
                                observer.onCompleted()
                            }
                        }
                    }
                    
                }
                //APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
         return Disposables.create()
        }
    }
    
    
    //get list of card Data
    class func getCard() ->Observable<[Card]> {
//        if !AppConstants.Reachable {
//            Helper.showNoInternet()
//            return Observable.create{ observer in
//                return Disposables.create()
//                }.share(replay: 1)
//        }
//        Helper.showPI(string: "")
         Helper.showPI(message: "Loading...")
        let header =  APIErrors.getAOTHHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, APIEndTails.Card, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    let bodyIn = body as! [String : Any]
                    
                    var arrayCard:[Card] = []
                    if let data = bodyIn[APIResponceParams.Data] as? [String:Any] {
                        if let cards = data["cards"] as? [Any] {
                            for dataItem in cards {
                                arrayCard.append(Card.init(data: dataItem as! [String:Any]))
                            }
                        }
                    }
                    observer.onNext(arrayCard)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }
    }
    
    
    
    //Add Cart Data
    class func deleteCard(card:Card) ->Observable<Bool> {
       
        Helper.showPI(message: "")
        // let header = Utility.getHeader()
        
        
        
        return Observable.create { observer in
            let urlString = APIEndTails.Card
            let json = "{\"cardId\":\"\(card.Id)\"}"
            
            let url1 = URL(string: urlString)!
            let jsonData = json.data(using: .utf8, allowLossyConversion: false)!
            
            var request = URLRequest(url: url1)
            request.httpMethod = HTTPMethod.delete.rawValue
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            request.setValue("en", forHTTPHeaderField: APIRequestParams.Language)
            request.setValue(Utility.sessionToken, forHTTPHeaderField: "authorization")
            request.httpBody = jsonData
            
            Alamofire.request(request).responseJSON {
                (response) in
                print("response code is \(response)")
                  Helper.hidePI()
                
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200:
                        observer.onNext(true)
                        observer.onCompleted()
                    default:
                        print("error with response status: \(status)")
                    }
                }
            }
            
            return Disposables.create()
        }
    }
    
    //Add Cart Data
    class func defaultCard(card:Card) ->Observable<Bool> {
        
//        if !AppConstants.Reachable {
            //            Helper.showNoInternet()
            //            return Observable.create{ observer in
            //                return Disposables.create()
            //                }.share(replay: 1)
            //        }
            //        Helper.showPI(string: "")
            let header = APIErrors.getAOTHHeader()
            let params = [
                APIRequestParams.CardId     : card.Id
            ]
            return Observable.create { observer in
                RxAlamofire.requestJSON(.patch, APIEndTails.Card, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                    Helper.hidePI()
                    let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                    if errNum == .success {
                        observer.onNext(true)
                        observer.onCompleted()
                    }
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }, onError: { (Error) in
                    //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                    Helper.hidePI()
                }).disposed(by: self.disposeBag)
                return Disposables.create()
        }
    }

}
