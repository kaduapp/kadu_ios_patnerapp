//
//  BankingApi.swift
//  KarruPro
//
//  Created by Rahul Sharma on 22/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxAlamofire
class BankingApi: APIs {
    func getBankAccounts()
    {
        
        let apiFullName = API.BASE_URL + API.METHOD.GETBANKSTRIPEDATA
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as! [String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func addBank(bankDetails: AddbankStripeModel)
    {
        
        let bankDetails: [String:Any] = [
            "city"          :bankDetails.addBankCity,
            "line1"         :bankDetails.addBankLine1,
            "postal_code"   :bankDetails.addBankPostalCode,
            "state"         :bankDetails.addBankState,
            "month"         :bankDetails.addBankMonth,
            "day"           :bankDetails.addBankDay,
            "year"          :bankDetails.addBankYear,
            "first_name"    :bankDetails.addBankFirstName,
            "last_name"     :bankDetails.addBankLastName,
            "personal_id_number":bankDetails.addBankPersonalID,
            "ip"            :bankDetails.addBankIP,
            "date"          :bankDetails.addBankDate ,
            "country"       :bankDetails.addBankCountry! as Any,
            "document"      :bankDetails.addBankDocument,
            "email"         :UserDefaults.standard.value(forKey: "emailString")! as Any]
        
        let apiFullName = API.BASE_URL + API.METHOD.ADDBANKSTRIPE
        RxAlamofire.requestJSON(.post, apiFullName, parameters: bankDetails, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                let dict = ["errFlag" : 1]
                self.subject_response.onNext(dict)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
    
    func deleteBankAccount(deleteBank: DeleteBankModel)
    {
        let dict : [String : Any]
        dict  =  ["accountId": deleteBank.bankID!]
        
        let apiFullName = API.BASE_URL + API.METHOD.DELETEBANK
        RxAlamofire.requestJSON(.delete, apiFullName, parameters: dict, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
        
    }
    
    func addAccount(accountDetails: AddBankModel)
    {
        let  bankDetails:[String: Any] = [
            "country"       :accountDetails.addBankAccountCountry! as Any,
            "routing_number":accountDetails.addBankAccountRoutingNumber,
            "account_number":accountDetails.addBankAccountAccountNumber,
            "account_holder_name":accountDetails.addBankAccountAccountHolderName,
            "currency" : UserDefaults.standard.value(forKey: "currency")! as Any,
            "email": "jdjlkj@gmail.com"
        ]
        
        let apiFullName = API.BASE_URL + API.METHOD.ADDBANKAFTERSTRIPE
        RxAlamofire.requestJSON(.post, apiFullName, parameters: bankDetails, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
        
    }
    
    func makeDefaultBankAccount(defaultBank: DefaultBank)
    {
        let dict : [String : Any]
        dict  =  ["accountId": defaultBank.bankID!]
        
        let apiFullName = API.BASE_URL + API.METHOD.DEFAULTBANK
        RxAlamofire.requestJSON(.patch, apiFullName, parameters: dict, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: { (Error) in
            self.subject_response.onError(Error)
        }).disposed(by: disposebag)
        
    }
    
}
