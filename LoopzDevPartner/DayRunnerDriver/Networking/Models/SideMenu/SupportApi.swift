//
//  SupportApi.swift
//  KarruPro
//
//  Created by Rahul Sharma on 21/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxAlamofire
class SupportApi: APIs {
    func supportApi()
    {
        let apiFullName = API.BASE_URL + API.METHOD.SUPPORT  //+ "0/" + "2"
        RxAlamofire.requestJSON(.get, apiFullName, parameters: nil, headers: APIErrors.getAOTHHeader() ).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            
            let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: head.statusCode)
            if errNum == .success {
                self.subject_response.onNext(bodyIn)
            }else{
                Helper.alertVC(errMSG: bodyIn["message"] as! String)
            }
        }, onError: {[weak self] (Error) in
//            Helper.alertVC(errMSG: "Internal server error.")
            self?.subject_response.onError(Error)
        }).disposed(by: disposebag)
    }
}
