//
//  WalletAPI.swift
//  FlagitDriver
//
//  Created by Rahul Sharma on 03/04/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class WalletAPI:APIs {
    
    //    let disposebag = DisposeBag()
    
    //    let getPaymentCardList_Response = PublishSubject<APIResponseModel>()
    //    let addPaymentCard_Response = PublishSubject<APIResponseModel>()
    //    let makePaymentCardDefault_Response = PublishSubject<APIResponseModel>()
    //    let deletePaymentCard_Response = PublishSubject<APIResponseModel>()
    
    
    
//    /// Method to call Get Wallet Details Service API
//    func getWalletDetailsServiceAPICall(){
//
//        let strURL = API.BASE_URL + API.METHOD.WALLET_DETAILS
//
//        Helper.showPI(message: "Loading...".localized)
//
//        RxAlamofire
//            .requestJSON(.get, strURL ,
//                         parameters:nil,
//                         encoding:JSONEncoding.default,
//                         headers: APIErrors.getAOTHHeader())
//            .subscribe(onNext: { (r, json) in
//
//                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
//                if  let dict  = json as? [String:Any]{
//
//                    let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: r.statusCode)
//
//                    if errNum == .success {
////                        self.apiResponse.onNext(APIResponseModel.init(statusCode: errNum?.rawValue, dataResponse: dict))
//                        self.subject_response.onNext(dict)
//                    }else{
////                        self.apiResponse.onNext(APIResponseModel.init(statusCode: errNum?.rawValue, dataResponse: dict))
//                        self.subject_response.onNext(dict)
//                    }
//                }
//
//                Helper.hidePI()
//
//            }, onError: {  (error) in
//
//                print("API Response \(strURL)\nError:\(error.localizedDescription)")
//
//                Helper.hidePI()
//                Helper.alertVC(errMSG: error.localizedDescription)
//
//            }).disposed(by: disposebag)
//
//
//    }
//
    
    
    /// Method to Recharge Wallet Service API
    ///
    /// - Parameter cardTokenId: card token to add
//    func rechargeWalletServiceAPICall(cardId:String,
//                                      amount:String){
//        
//        let strURL = API.BASE_URL + API.METHOD.RECHARGE_WALLET
//        
//        let requestParams: [String: Any] = [
//            
//            "cardId":cardId,
//            "amount":amount
//        ]
//        
//        Helper.showPI(message: "Loading...".localized)
//        
//        RxAlamofire
//            .requestJSON(.post, strURL ,
//                         parameters:requestParams,
//                         encoding:JSONEncoding.default,
//                         headers: APIErrors.getAOTHHeader())
//            .subscribe(onNext: { (r, json) in
//                
//                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
//                if  let dict  = json as? [String:Any]{
//                    
//                    let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: r.statusCode)
//                    
//                    if errNum == .success {
//                        
//                        self.apiResponse.onNext(ApiResponse.init(status: true, response: dict))
//                    }else{
//                        
//                        self.apiResponse.onNext(ApiResponse.init(status: false, response: dict))
//                    }
//                }
//                
//                Helper.hidePI()
//                
//            }, onError: {  (error) in
//                
//                print("API Response \(strURL)\nError:\(error.localizedDescription)")
//                
//                Helper.hidePI()
//                Helper.alertVC(errMSG: error.localizedDescription)
//                
//            }).disposed(by: disposebag)
//        
//        
//    }
    
    
    
    
    /// Method to Get Wallet Transactions service API
    ///
    /// - Parameter cardId: card id to delete
    func getWalletTransactionsServiceAPICall(pageIndex:Int){
        
        let strURL = API.BASE_URL + API.METHOD.WALLET_DETAILS + "/\(pageIndex)"
        
        Helper.showPI(message: "Loading...".localized)
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: APIErrors.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let errNum: APIErrors.ErrorCode? = APIErrors.ErrorCode(rawValue: r.statusCode)
                    
                    if errNum == .success {
                        self.subject_response.onNext(dict)
                    }else{
                        if let message = dict["message"] as? String{
                            Helper.alertVC(errMSG: message)
                        }else{
                            Helper.alertVC(errMSG: "Some error occured")
                        }
                    }
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(errMSG: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
}

