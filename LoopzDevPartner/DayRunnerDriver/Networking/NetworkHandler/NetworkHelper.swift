////
////  NetworkHelper.swift
////  runner
////
////  Created by 3Embed on 15/09/16.
////  Copyright © 2016 3Embed. All rights reserved.
////
//
//import UIKit
//import Alamofire
//import SwiftyJSON
//
//enum ErrorFlagType: Int {
//    case Success = 0
//    case Failure = 1
//}
//
//class NetworkHelper: NSObject {
//
//
//    /// Request PUT
//    ///
//    /// - Parameters:
//    ///   - serviceName: with Method
//    ///   - params: Params
//    ///   - success: Sucess Handler
//    ///   - failure: Failure Handler
//    class func requestPUT(serviceName: String,
//                          params: [String : Any]?,
//                          success: @escaping([String: Any]) -> Void,
//                          failure: @escaping(Error) -> Void) {
//
//        /*
//         if NetworkReachabilityManager()?.isReachable == false {
//         Alert.showErrorMessage(message: "No Network available.")
//         return
//         }
//         */
////        if ReachabilityHelper.shared.isReachable == false {
////            Helper.hidePI()
////            return
////        }
//
//        print("\n\n\(serviceName.capitalized) : \(String(describing: params))\n\n")
//
//        // URL
//        let strURL = API.BASE_URL + serviceName
//
//        let manager = Alamofire.SessionManager.default
//        manager.session.configuration.timeoutIntervalForRequest = 30
//        // Request
//        manager.request(strURL,
//                        method:.put,
//                        parameters: params,
//                        encoding: JSONEncoding.default,
//                        headers: getAOTHHeader()).responseJSON(completionHandler: { response in
//
//                            print("\n\n\(serviceName.capitalized) Response : \(response)\n\n")
//
//                            if response.result.isSuccess {
//                                let resJson = JSON(response.result.value!)
//                                if let dict: [String: Any] = resJson.dictionaryObject {
//                                    success(dict)
//                                }
//                                else {
//                                    success([:])
//                                }
//                            }
//                            else {
//                                Helper.hidePI()
//                                if let error : NSError = response.result.error as NSError? {
//                                    failure(error)
//                                }
//                            }
//                        })
//    }
//
//
//
//    ///Get method
//    class func requestGETURL(method: String,
//                             success:@escaping([String: Any]) -> Void,
//                             failure:@escaping (NSError) -> Void) {
//
//        if NetworkReachabilityManager()?.isReachable == false {
//
//            Helper.hidePI()
//            failure(networkError)
//            return
//        }

        print("\n\n\(serviceName.capitalized) : \(String(describing: params))\n\n")

        // URL
        let strURL = API.BASE_URL + serviceName

        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30
        // Request
        manager.request(strURL,
                        method:.put,
                        parameters: params,
                        encoding: JSONEncoding.default,
                        headers: getAOTHHeader()).responseJSON(completionHandler: { response in

                            print("\n\n\(serviceName.capitalized) Response : \(response)\n\n")

                            if response.result.isSuccess {
                                let resJson = JSON(response.result.value!)
                                if let dict: [String: Any] = resJson.dictionaryObject {
                                    success(dict)
                                }
                                else {
                                    success([:])
                                }
                            }
                            else {
                                Helper.hidePI()
                                if let error : NSError = response.result.error as NSError? {
                                    failure(error)
                                }
                            }
                        })
    }



    ///Get method
    class func requestGETURL(method: String,
                             success:@escaping([String: Any]) -> Void,
                             failure:@escaping (NSError) -> Void) {

        if NetworkReachabilityManager()?.isReachable == false {

            Helper.hidePI()
            failure(networkError)
            return
        }

        var strURL = ""
        if method == API.LiveChat.Licence_url {
            strURL = method
        }else{
            strURL = API.BASE_URL + method
        }


        Alamofire.request(strURL, method:.get, parameters: nil, encoding: JSONEncoding.default, headers: getAOTHHeader()).responseJSON(completionHandler: {
            response in
            print(response)
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                if let dict: [String: Any] = resJson.dictionaryObject {
                    success(dict)
                }
                else {
                    success([:])
                }
            }
            if response.result.isFailure {
                let error : NSError = response.result.error! as NSError
                failure(error)
            }
        })
    }

    /// Request Post
    ///
    /// - Parameters:
    ///   - serviceName: with Method
    ///   - params: Params
    ///   - success: Sucess Handler
    ///   - failure: Failure Handler
    class func requestPOST(serviceName: String,
                           params: [String : Any]?,
                           success: @escaping([String: Any]) -> Void,
                           failure: @escaping(Error) -> Void) {

        // URL
        let strURL = API.BASE_URL + serviceName

        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30
        // Request
        manager.request(strURL,
                        method:.post,
                        parameters: params,
                        encoding: JSONEncoding.default,
                        headers: getAOTHHeader()).responseJSON(completionHandler: { response in

                            print("\n\n\(serviceName.capitalized) Response : \(response)\n\n")

                            if response.result.isSuccess {
                                let resJson = JSON(response.result.value!)
                                if let dict: [String: Any] = resJson.dictionaryObject {
                                    success(dict)
                                }
                                else {
                                    success([:])
                                }
                            }
                            else {
                                Helper.hidePI()
                                if let error : NSError = response.result.error as NSError? {
                                    failure(error)
                                }
                            }
                        })
    }

    /// Request Post
    ///
    /// - Parameters:
    ///   - serviceName: with Method
    ///   - params: Params
    ///   - success: Sucess Handler
    ///   - failure: Failure Handler
    class func requestPOST1(method: String,
                            params: [String : Any]?,
                            completion: @escaping(APIResponse) -> Void) {

        print("\(method.capitalized) : \(String(describing: params))")

        // URL
        let strURL = API.BASE_URL + method

        // Request
        Alamofire.request(strURL,
                          method:.post,
                          parameters: params,
                          encoding: JSONEncoding.default,
                          headers: getAOTHHeader()).responseJSON(completionHandler: { response in

                            print("\n\n\(method.capitalized) Response : \(response)\n")

                            let responseObj: APIResponse = APIResponse()

                            if response.result.isSuccess {
                                let resJson = JSON(response.result.value!)
                                if let dict: [String: Any] = resJson.dictionaryObject {

                                    let flag: Int = dict["errFlag"] as? Int ?? APIResponseFlag.Default.rawValue
                                    responseObj.errKeys = APIKeys(flag: APIResponseFlag(rawValue: flag)!,
                                                                  msg: dict["errMsg"] as? String ?? "",
                                                                  num: dict["errNum"] as? Int ?? 0)

                                    responseObj.data = dict["data"] as? [String : AnyObject] ?? [:]
                                }
                                else {
                                    responseObj.data = [:]
                                }
                                responseObj.status = APIStatus(success: true, error: nil)
                            }
                            else {

                                if let error : NSError = response.result.error as NSError? {
//                                    Alert.showErrorMessage(message: error.localizedDescription)
                                    responseObj.status = APIStatus(success: false, error: error)
                                }
                                else {
                                    responseObj.status = APIStatus(success: false, error: nil)
                                }
                            }
                            completion(responseObj)
                          })
    }

    /// Request Post with Url
    ///
    /// - Parameters:
    ///   - urlString: url
    ///   - success: success Handler
    ///   - failure: Failure Handler
    class func requestPOST(urlString : String!,
                           success:@escaping (JSON) -> Void,
                           failure:@escaping (NSError) -> Void) {


        if NetworkReachabilityManager()?.isReachable == false {

            Helper.hidePI()
            failure(networkError)
            return
        }

        Alamofire.request(urlString!).responseJSON(completionHandler: { response in

            print("\n\nSearch URL : %@\n",urlString)
            print(response)
            print("\n\n")

            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                success(resJson)
            }
            else {
                let error : NSError = response.result.error! as NSError
                failure(error)
            }
        })
    }

    class func requestDELETEURL(method: String,
                                params: [String : Any]?,
                                success:@escaping([String: Any]) -> Void,
                                failure:@escaping (NSError) -> Void) {

        let strURL = API.BASE_URL + method

        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30


        manager.request(strURL,
                        method:.delete,
                        parameters: params,
                        encoding: JSONEncoding.default,
                        headers: getAOTHHeader()).responseJSON(completionHandler: { response in

                            print("\n\n\(method.capitalized) Response : \(response)\n\n")

                            if response.result.isSuccess {
                                let resJson = JSON(response.result.value!)
                                if let dict: [String: Any] = resJson.dictionaryObject {
                                    success(dict)
                                }
                                else {
                                    success([:])
                                }
                            }
                            else {
                                Helper.hidePI()
                                if let error : NSError = response.result.error as NSError? {
                                    failure(error)
                                }
                            }
                        })

    }

    /// Request
    ///
    /// - Parameters:
    ///   - urlString: URL String to be Requested with
    ///   - success: Successfull block with Response in the format
    ///   - failure: Failure block
    class func request(urlString : String!,
                       success: @escaping([String: Any]) -> Void,
                       failure: @escaping(Error) -> Void) {

        if NetworkReachabilityManager()?.isReachable == false {

            Helper.hidePI()
            failure(networkError)
            return
        }

        Alamofire.request(urlString!).responseJSON(completionHandler: { response in

            print("\n\nRequest URL : %@\n",urlString)
            print(response)
            print("\n\n")

            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                if let dict: [String: Any] = resJson.dictionaryObject {
                    success(dict)
                }
                else {
                    success([:])
                }
            }
            else {
                Helper.hidePI()
                if let error : NSError = response.result.error as NSError? {
                    failure(error)
                }
            }
        })
    }

    /// Network Error
    static var networkError: NSError {

        let userInfo: [AnyHashable: Any] = [NSLocalizedDescriptionKey       : NSLocalizedString("No network available.", comment: "No network available."),
                                            NSLocalizedFailureReasonErrorKey: NSLocalizedString("Failed to connect to server.", comment: "Failed to connect to server.")]
        return NSError(domain: "", code: -57, userInfo: userInfo)
    }


    /// Athenticate Header
    ///
    /// - Returns: Dict of Athentication
    class func getAOTHHeader() -> [String: String] {

        var sessionToken = String()
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
            sessionToken = Utility.sessionCheck
        }else{
           sessionToken = String(format: "%@", Utility.sessionToken)
        }

        return ["authorization": (sessionToken), "language": "0"] as [String: String]
    }
}



