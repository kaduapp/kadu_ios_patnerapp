//
//  SignUpAPI.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 16/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit
import RxAlamofire
import Alamofire
import RxSwift
import RxCocoa
import CocoaLumberjack

class ChatAPI:NSObject {
    
    let disposebag = DisposeBag()
    let chat_Response = PublishSubject<APIResponseModel>()
    
    class func getHeader() -> [String:String]{
        let dataUser = APIErrors.getAOTHHeader()
        let header = ["language"             : "en",
                      "authorization"   : dataUser["authorization"]!]
        return header
    }
    
    func getTheChatHist(bookingID: String){
        

        let strURL = "https://api.kadu.app/chatHistory/" + bookingID

        RxAlamofire
            .requestJSON(.get, strURL,headers: ChatAPI.getHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    DDLogVerbose(" API: \(strURL)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                    Helper.hidePI()
                }
            }, onError: {  (error) in
                Helper.hidePI()
                Helper.alertVC(errMSG: error.localizedDescription)

                self.chat_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    
    func postTheMessage(parameters: [String:Any]?){
        let strURL = "https://api.kadu.app/message"

        let auth = APIErrors.getAOTHHeader()
        let header = ["language"             : "en",
                      "authorization"   : auth["authorization"]!]
        RxAlamofire
//            .requestJSON(.post, strURL ,parameters:parameters ,headers: header)
            .requestJSON(.post, strURL, parameters:parameters ,headers: ChatAPI.getHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    DDLogVerbose(" API: \(strURL)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                    
                }
            }, onError: {  (error) in
                Helper.hidePI()
                Helper.alertVC(errMSG: error.localizedDescription)
//                Helper.showAlert(message: error.localizedDescription, head: StringConstants.Error, type: 1)
                self.chat_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    /// parse the chat api response
    ///
    /// - Parameters:
    ///   - statusCode: httpstatuscodes
    ///   - responseDict: response data
    func checkResponse(statusCode:Int,responseDict: [String:Any]){
        
        DDLogVerbose(" Response: \(responseDict)");
        let responseCodes : APICalls.ErrorCode = APICalls.ErrorCode(rawValue: statusCode)!
        Helper.hidePI()
        switch responseCodes {
        case .success:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.chat_Response.onNext(responseModel)
            break
        default:
            APICalls.basicParsing(data: responseDict, status: statusCode)
            break
        }
    }
}

///API Response Model
class APIResponseModel {
    
    var httpStatusCode:Int = 0
    /// Data in DIctionary format
    var data: [String: Any] = [:]
    
    init(statusCode:Int, dataResponse:[String:Any]) {
        httpStatusCode = statusCode
        data = dataResponse
    }
}
