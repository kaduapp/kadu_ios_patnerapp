//
//  AlignmentOfLabel.swift
//  DelivX
//
//  Created by 3EMBED on 21/05/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class AlignmentOfLabel: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setAlignmentForLabel()
    }
    func setAlignmentForLabel(){
         if Utility.getSelectedLanguegeCode() == "ar"
        {
            self.textAlignment = .right
            
        }else
        {
              self.textAlignment = .left
        }
    }
}
