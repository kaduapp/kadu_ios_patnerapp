//
//  LabelAlignmentOpposite.swift
//  DelivX
//
//  Created by 3EMBED on 21/05/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class LabelAlignmentOpposite: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setAlignmentForLabel()
    }
    func setAlignmentForLabel(){
         if Utility.getSelectedLanguegeCode() == "ar"
        {
            self.textAlignment = .left
            
        }else
        {
            self.textAlignment = .right
        }
    }
}
