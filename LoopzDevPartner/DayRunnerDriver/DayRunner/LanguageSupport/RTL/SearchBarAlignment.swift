//
//  SearchBarAlignment.swift
//  DelivX
//
//  Created by 3EMBED on 21/05/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SearchBarAlignment: UISearchBar {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setTextAlignmentForSearchBarField()
    }
    func setTextAlignmentForSearchBarField()  {
         if Utility.getSelectedLanguegeCode() == "ar"
        {
            let textFieldInsideSearchBar:UITextField = self.value(forKey: "searchField") as! UITextField
            textFieldInsideSearchBar.textAlignment = .right
            
            
        }else
        {
           let textFieldInsideSearchBar:UITextField = self.value(forKey: "searchField") as! UITextField
             textFieldInsideSearchBar.textAlignment = .left
        }
    }
}
