//
//  UIButton+RotateButtonClass.swift
//  DelivX
//
//  Created by 3EMBED on 21/05/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class UIButton_RotateButtonClass: UIButton {
    
    override func awakeFromNib() {
        rotateButton()
    }
    
    func rotateButton() {
        
        if Utility.getSelectedLanguegeCode() == "ar" {
            self.transform = CGAffineTransform(scaleX: -1.0,y: 1.0)
        }
    }
    
    
    func buttonTextAlignMent ()
    {
        if Utility.getSelectedLanguegeCode() == "ar" {
            self.contentHorizontalAlignment = .right
        }
    }
    
    
    
    
    func buttonImageInsets(spacing:CGFloat)
    {
        if Utility.getSelectedLanguegeCode() == "ar" {
            self.imageEdgeInsets =  UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -spacing)//UIEdgeInsetsMake(0, 0,0, -spacing);
        }
        else
        {
            self.imageEdgeInsets = UIEdgeInsets(top: 0, left: -spacing, bottom: 0, right: 0)//UIEdgeInsetsMake(0, -spacing,0, 0);
        }
    }
    
}
