//
//  LeftAlignmentImageView.swift
//  Karry
//
//  Created by 3Embed on 14/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class LeftAlignmentImageView: UIImageView {

    override open func awakeFromNib() {
        super.awakeFromNib()
        self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        if Utility.getSelectedLanguegeCode() == "ar" {
            self.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        } else {
            self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
}
