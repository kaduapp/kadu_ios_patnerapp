//
//  LanguageModel.swift
//  Karry
//
//  Created by 3Embed on 17/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

struct LangModel {
    var name: String!
    var code: String!
    var isRTL : Bool!
    init(data:[String:Any])
    {
        if let name = data["lan_name"] as? String{
            self.name = name
        }
        if let code = data["langCode"] as? String {
            self.code = code
        }
        if let isRTL = data["isRTL"] as? NSNumber {
            self.isRTL = isRTL == 1 ? true : false
        }
    }
}

class Languages {
    var langData:[LangModel]!
    func languagesAPI(completion:@escaping(Bool)->()){
        Helper.showPI(message:"Loading..")
        let getLanguage =  APIs()
        getLanguage.getLanguages()
        getLanguage.subject_response.subscribe(onNext: { (response) in
            Helper.hidePI()
            if let data =  response["data"] as? [[String:Any]]{
                self.langData = data.map{
                    LangModel.init(data:$0)
                }
                completion(true)
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }
    }
}
