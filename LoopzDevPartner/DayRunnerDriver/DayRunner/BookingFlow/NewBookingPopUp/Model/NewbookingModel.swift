//
//  NewbookingModel.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift

class NewbookingModel: NSObject {
     let disposeBag = DisposeBag()
    var status: Int = 0
    var bookingID: Any!
    var latitude: Any!
    var longitude: Any!
    
    
    override init()
    {
        super.init()
    }
    
    init(status: String, bookingID: Any, latitude: Any, longitude: Any)
    {
        self.status = Int(status)!
        self.bookingID = bookingID
        self.latitude = latitude
        self.longitude = longitude
    }
    
    //This func make the service call for accepting/rejecting newbooking and responds to it.
    func respondToNewBooking(params:NewbookingModel ,completionHandler:@escaping (Bool) -> ()) {
        let respond =  BookingApi()
        respond.respondToBooking(data: params)
        
        respond.subject_response.subscribe(onNext: { (response) in
            print("Newbooking Accepted response : ",response)
            Helper.hidePI()
            if response.isEmpty == false {
                if (response["statusCode"] != nil){
                    
                    Helper.hidePI()
                    Helper.alertVC(errMSG: response["message"] as! String);
                    
                }else{
                    let flag:Int = response["errFlag"] as! Int
                    if flag == 0{
                        completionHandler(true)
                    }else{
                        completionHandler(false)
                        Helper.alertVC(errMSG: response["errMsg"] as! String);
                    }
                    
                }
                
            } else {
                completionHandler(false)
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
        
    }
}
