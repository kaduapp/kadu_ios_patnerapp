//
//  NewbookingExt.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import AVFoundation

extension NewBookingPopup{
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "taxina-1", withExtension: "wav") else {
            print("error")
            return
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player.play()
            player.numberOfLoops = -1
        } catch let error {
            print(error.localizedDescription)
        }
    }
    

    
    @objc func timerTick(){
        progressVal = progressVal + 1.0
        
        let val:Double = 1/expiryTime
        
        print(val)
        
        self.progressView.progress = ((1.0) - (val * progressVal))
        
        //   if (Float(Double(1/300)) * Float(progressVal)) == countVal  {
        //       if countVal * 10 == proVal  {
        //           proVal = proVal + 1
        countDown = countDown - 1;
        self.progressText.text = "" + nf.string(from: NSNumber(value: self.countDown))!
        //         }
        //          countVal = countVal + 0.1;
        //      }
        
        if countDown <= 0 {
            timerProgress.invalidate()
//             UserDefaults(suiteName: APPGROUP.groupIdentifier)!.set(orderId, forKey: "AckOrder")
           
            hide()
            UserDefaults(suiteName: APPGROUP.groupIdentifier)!.removeObject(forKey:"AckOrder")
            UserDefaults(suiteName: APPGROUP.groupIdentifier)!.synchronize()
            UserDefaults.standard.removeObject(forKey: USER_INFO.ORDERID)
            UserDefaults.standard.synchronize()
        }
    }
}
