//
//  NewBookingPopup.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import AVFoundation

class NewBookingPopup: UIView {
    
    @IBOutlet weak var serviceCallGesture: UITapGestureRecognizer!
    @IBOutlet weak var imageViewDropHeading: UIImageView!
    @IBOutlet weak var imageViewDropTime: UIImageView!
    @IBOutlet weak var labelDropHeading: UILabel!
    @IBOutlet weak var labelDeliveryFee: UILabel!
    @IBOutlet var bid: UILabel!
    var player: AVAudioPlayer?
    @IBOutlet var progressView: CircleProgressView!
    @IBOutlet var pickAddress: UILabel!
    
    @IBOutlet weak var pickupLabel: UILabel!
    @IBOutlet var progressText: UILabel!
    @IBOutlet var dropTime: UILabel!
    //    @IBOutlet var pickTime: UILabel!
    
    @IBOutlet var deliveryFee: UILabel!
    @IBOutlet var dropAddress: UILabel!
    
    @IBOutlet weak var paymentOutlet: UILabel!
    @IBOutlet weak var bookingTypeLabel: UILabel!
    var newbookingModel = NewbookingModel()
    
    @IBOutlet weak var leftToAccept: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    
    @IBOutlet weak var topHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var orderTypeOutlet: UILabel!
    var timerProgress = Timer()
    
    let nf = NumberFormatter()
    var countDown:Int = 0
    var progressVal:Double = 0.0
    var countVal:Float = 0.1
    var proVal:Float = 1.0
    var expiryTime:Double = 0.0
    var distanceStr: String!
    
    @IBOutlet weak var dateAndTimeLabel: UILabel!
    @IBOutlet weak var distOutlet: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet weak var handlers: UILabel!
    @IBOutlet var paymentType: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bookingID: UILabel!
    
    var viewController = UIApplication.shared.keyWindow?.rootViewController
    
    
    var bookingDict = [String:Any]()
    
    private static var share: NewBookingPopup? = nil
    var isShown:Bool = false
    
    override func awakeFromNib() {
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                DispatchQueue.main.async {
                    self.topHeightConstraint.constant = 0
                }
            case 1334:
                DispatchQueue.main.async {
                    self.topHeightConstraint.constant = 0
                }
            case 1920, 2208:
                DispatchQueue.main.async {
                    self.topHeightConstraint.constant = 0
                }
            case 2436:
                DispatchQueue.main.async {
                    self.topHeightConstraint.constant = 24
                }
                
            case 2688:
                DispatchQueue.main.async {
                    self.topHeightConstraint.constant = 24
                }
                
            case 1792:
                DispatchQueue.main.async {
                    self.topHeightConstraint.constant = 24
                }
                
            default:
                print("Unknown")
            }
            distOutlet.text = "Distance".localized
            paymentOutlet.text = "PAYMENT".localized
            orderTypeOutlet.text = "Order Type".localized
            pickupLabel.text = "PICKUP".localized
            labelDropHeading.text = "Drop".localized
            leftToAccept.text = "left to Accept".localized
            labelDeliveryFee.text = "DELIVERY FEE".localized
            dateTimeLabel.text = "TIPS".localized
        }
    }
    
    static var instance: NewBookingPopup {
        
        if (share == nil) {
            share = Bundle(for: self).loadNibNamed("Newbooking",
                                                   owner: nil,
                                                   options: nil)?.first as? NewBookingPopup
        }
        return share!
    }
    
    
    //Handhing the booking data
    func newbookingDic(dict:[String:Any], expTm:Int){
        
        if isShown == true {
            return
        }
        
        if(bookingDict.count > 0)
        {
            return
        }
        
        self.expiryTime = Double(expTm)
        self.playSound()
        
        Helper.shadowView(sender: topView, width:UIScreen.main.bounds.size.width + 30, height:topView.frame.size.height)
        bookingDict =  dict
        var expTime:Int = 0
        self.progressText.text = String(describing: dict["ExpiryTimer"])
        expTime = Int(dict["ExpiryTimer"] as! String)!
        countDown =   expTime
        expiryTime = Double(expTime)
        
        var storeType:NSNumber = 0
        var bookingType:Bool = true
         var slotEnd:NSNumber = 0
         var slotStart:NSNumber = 0
        if let storeTypeN = dict["storeType"] as? NSNumber {
            storeType = storeTypeN
        }
        if let bookingTypeN = dict["isCominigFromStore"] as? Bool {
            bookingType = bookingTypeN
        }
        
        if let slotEndTime = dict["slotEndTime"] as? NSNumber {
            slotEnd = slotEndTime
        }
        
        if let slotStartTime = dict["slotStartTime"] as? NSNumber {
            slotStart = slotStartTime
        }
        
        let date = NSDate(timeIntervalSince1970: TimeInterval(slotStart))
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "hh:mm a"
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        
        let date1 = NSDate(timeIntervalSince1970: TimeInterval(slotEnd))
        let dayTimePeriodFormatter1 = DateFormatter()
        dayTimePeriodFormatter1.dateFormat = "hh:mm a"
        let dateString1 = dayTimePeriodFormatter1.string(from: date1 as Date)
        
        if storeType == 5 && bookingType == false {
            pickupLabel.text = "Pickup Slot :"
            labelDropHeading.text = "Pickup"
            pickAddress.text  = dateString + "-" + dateString1
            bookingTypeLabel.text = "Laundry"
        }else if storeType == 5 && bookingType == true {
             pickAddress.text  = "\(String(describing: dict["storeName"] as! String))\n"+"\(String(describing: dict["pickUpAddress"] as! String))"
            bookingTypeLabel.text = "Laundry"
        }else if(storeType == 7){
            labelDropHeading.text = "Delivery"
            pickAddress.text  = "\(String(describing: dict["storeName"] as! String))\n"+"\(String(describing: dict["pickUpAddress"] as! String))"
            if let storeTypeMsg = dict["storeTypeMsg"] as? String {
                bookingTypeLabel.text = storeTypeMsg
            }
        } else {
            pickAddress.text  = "\(String(describing: dict["storeName"] as! String))\n"+"\(String(describing: dict["pickUpAddress"] as! String))"
            if let storeTypeMsg = dict["storeTypeMsg"] as? String {
                bookingTypeLabel.text = storeTypeMsg
            }
            
        }
            
        
        dropAddress.text  = "\(String(describing: dict["customerName"] as! String))\n"+"\(String(describing: dict["deliveryAddress"] as! String))"
        
        if let amount = bookingDict["deliveryFee"] as? String {
            let deliveryFees = Float(amount)
            deliveryFee.text  = Utility.currencySymbol + String(describing: Helper.clipDigit(value: deliveryFees! , digits: 2))
        } else {
            
//            deliveryFee.text  = Utility.currencySymbol + String(describing: bookingDict["deliveryFee"]!)
            deliveryFee.text  = Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(bookingDict["deliveryFee"] as! NSNumber) , digits: 2))
            
        }
      
        if let drivertip = bookingDict["driverTip"] as? Double {
            let drivertip = Float(drivertip)
            dropTime.text  = Utility.currencySymbol + String(describing: Helper.clipDigit(value: drivertip , digits: 2))
        }
        dateAndTimeLabel.text  = Helper.UTCToLocal(date:dict["deliveryDatetime"]! as! String)
        
        
        //        pickTime.text  = dict["orderDatetime"] as? String
        nf.numberStyle = NumberFormatter.Style.none
        nf.maximumFractionDigits = 2
        if let bookingNum = bookingDict["orderId"] as? NSNumber {
            let string = String(describing: bookingNum)
            bookingID.text = "OID : " + string
        }
        
        timerProgress =  Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerTick), userInfo: nil, repeats: true)
        
        if let serviceType:Int = dict["service_type"] as? Int
        {
            if(serviceType == 2)
            {
                self.deliveryFee.isHidden = true
                self.labelDeliveryFee.isHidden = true
                self.imageViewDropTime.isHidden = true
                //                self.imageViewDropHeading.isHidden = true
                //                self.labelDropHeading.isHidden = true
                self.topView.isHidden = true
            }
        }
        if let distance = bookingDict["distance"] as? NSNumber {
//            distanceStr = String(describing: distance)
            distanceLabel.text = Helper.clipDigit(value: Float(distance) , digits: 2)  + "  " + Utility.DistanceUnits
        } else {
            distanceLabel.text = Helper.clipDigit(value: Float(0.0) , digits: 2)  + "  " + Utility.DistanceUnits
        }
        
        
        //        handlers.text = String(describing:bookingDict["helpers"] as! NSNumber)
        
        let paymentByWallet = dict["payByWallet"] as? Int
        if  let paymentType = dict["paymentType"] as? Int {
            
            
            if paymentByWallet == 0 {
                if paymentType == 1 {
                     self.paymentType.text = "CARD"
                }else if paymentType == 2 {
                    self.paymentType.text = "CASH"
                }else {
                    self.paymentType.text = "WALLET"
                }
            }else {
                if paymentType == 1 {
                     self.paymentType.text = "CARD + WALLET"
                }else if paymentType == 2 {
                    self.paymentType.text = "CASH + WALLET"
                }else {
                    self.paymentType.text = "WALLET"
                }
            }

        }
        
        
        let ud = UserDefaults.standard
        ud.set(false, forKey: "isSound")
        ud.synchronize()
        
        NotificationCenter.default.addObserver(self, selector: #selector(bookingCancelHandling(_:)), name: Notification.Name("cancelledBooking"), object: nil)
    }
    
    
    //MARK: - Cancelled the booking before accepting
    @objc func bookingCancelHandling(_ notification: NSNotification) {
        let bid = "Bid:" + String(describing:notification.userInfo!["bid"]!)
        let msg = notification.userInfo?["msg"] as! String
        Helper.alertVC(errMSG: bid + " " + msg)
        if String(describing:notification.userInfo!["bid"]!)  == String(describing:bookingDict["bid"]!) {
            UserDefaults.standard.removeObject(forKey: "AckOrder")
            UserDefaults.standard.synchronize()
            self.hide()
        }
    }
    
    //MARK: - AcceptBookingAction
    @IBAction func makeTheServiceCall(_ sender: Any) {
        serviceCallGesture.isEnabled = false
        respondingToAppointment(status:"8" )
      
        timerProgress.invalidate()
    }
    
    //MARK: - RejectBookingAction
    @IBAction func rejectButtonAction(_ sender: Any) {
        
        serviceCallGesture.isEnabled = false
        respondingToAppointment(status:"9" )

        timerProgress.invalidate()
    }
    
    
    //MARK: - RespondtoAppointmentApi
    func respondingToAppointment(status:String) {
        let ud = UserDefaults.standard
        Helper.showPI(message:"Loading..")
        let params = NewbookingModel.init(status: status, bookingID: bookingDict["orderId"]!, latitude:ud.double(forKey:"currentLat") , longitude: ud.double(forKey:"currentLog"))
        
        
        
        //        if Utility.OrderId != bookingDict["orderId"] as! Double {
        newbookingModel.respondToNewBooking(params: params, completionHandler:     { success in
            if success{
                if status == "8"{
                    self.serviceCallGesture.isEnabled = true
                    // Define identifier
                    let notificationName = Notification.Name("gotNewBooking")
                    // Post notification
                    NotificationCenter.default.post(name: notificationName, object: nil)
                    ud.set(self.bookingDict["orderId"] as! Double, forKey: USER_INFO.ORDERID)
                }
                self.hide()
                UserDefaults(suiteName: APPGROUP.groupIdentifier)!.removeObject(forKey:"AckOrder")
                UserDefaults(suiteName: APPGROUP.groupIdentifier)!.synchronize()
                UserDefaults.standard.removeObject(forKey: USER_INFO.ORDERID)
                UserDefaults.standard.synchronize()
                UserDefaults.standard.synchronize()
                self.serviceCallGesture.isEnabled = true
            }else{
                UserDefaults(suiteName: APPGROUP.groupIdentifier)!.removeObject(forKey:"AckOrder")
                UserDefaults(suiteName: APPGROUP.groupIdentifier)!.synchronize()
                UserDefaults.standard.removeObject(forKey: USER_INFO.ORDERID)
                UserDefaults.standard.synchronize()
                self.serviceCallGesture.isEnabled = true
                self.hide()
            }
        })
        //        } else {
        //            Helper.hidePI()
        //            self.hide()
        //        }
    }
    
    /// Show Network
    func show() {
        
        if isShown == false {
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
            
            self.alpha = 0.0
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 1.0
            })
        }
    }
    
    /// Hide
    func hide() {
    
        player?.stop()
        timerProgress.invalidate()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "cancelledBooking"), object: nil);
        if isShown == true {
            isShown = false
            
            UIView.animate(withDuration: 0.5,
                           animations: {
                            NewBookingPopup.share = nil
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
        }
    }
}
