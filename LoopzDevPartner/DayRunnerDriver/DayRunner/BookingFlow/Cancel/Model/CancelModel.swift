//
//  CancelModel.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift

enum cancelApiResults {
    case success([String])
    case failure(Bool)
}

class cancelModel: NSObject {
    
    override init()
    {
        super.init()
    }
     let disposeBag = DisposeBag()
    //cancel Booking params
    var bookingID: String!
    var status: String! = "4"
    var reason: String!
    
    init(bookingID: String, status: String, reason: String)
    {
        self.bookingID = bookingID
        self.status = status
        self.reason = reason
    }
    
    func getCancellationReasonsAPI(completionHandler:@escaping (cancelApiResults) -> ()) {
        Helper.showPI(message: "loading..")
        var serviceName = String()
        
        serviceName = "0"
        
        let cancelReason =  BookingApi()
        cancelReason.getCancellationReason()
        
        cancelReason.subject_response.subscribe(onNext: { (response) in
            if response.isEmpty == false {
                completionHandler(.success(response["data"] as! [String]))
            } else {
                completionHandler(.failure(false))
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
    }
    
    
    func cancelBookingWithReason(params:cancelModel,completionHandler:@escaping (Bool) -> ()) {
        
        let cancelBooking =  BookingApi()
        cancelBooking.cancelBooking(cancelModel: params)
        cancelBooking.subject_response.subscribe(onNext: { (response) in
            if response.isEmpty == false {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
        
//        NetworkHelper.requestPUT(serviceName:API.METHOD.CANCELBOOKING,
//                                 params: params,
//                                 success: { (response : [String : Any]) in
//                                    print("UpdateDriverStatus response : ",response)
//                                    Helper.hidePI()
//                                    if response.isEmpty == false {
//                                        completionHandler(true)
//                                        
//                                    } else {
//                                        completionHandler(false)
//                                    }
//                                    
//        }) { (Error) in
//            Helper.hidePI()
//            completionHandler(false)
//            Helper.alertVC(errMSG: Error.localizedDescription)
//        }
    }
}
