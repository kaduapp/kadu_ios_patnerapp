
//  CancelViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 25/04/17.
//  Copyright © 2017 3Embed. All rights reserved.


import UIKit
protocol cancelBookingDelegate:class
{
    func cancelReason()
}

class CancelViewController: UIViewController {
    open weak var delegate: cancelBookingDelegate?
    @IBOutlet var commentsTextView: UITextView!
    @IBOutlet var cancelTableView: UITableView!
    @IBOutlet var ConfirmAction: UIButton!
    let manager = CBLManager.sharedInstance()
    var selectedIndex:Int = -1
    var cancelReasons = [String]()
    var cancelModl = cancelModel()
    
    var bookingID = String()
    override func viewDidLoad() {
        super.viewDidLoad()
     
        
        //MARK: - get cancel reasons @Locate cancelModel
        cancelModl.getCancellationReasonsAPI(completionHandler: { cancelApiResults in
            switch cancelApiResults{
            case .success(let reasons):
                self.cancelReasons = reasons
                self.cancelTableView.reloadData()
                break
            case .failure(let error):
                print(error)
                break
            }
        })
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            // self.moveViewDown()
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
    }
    

    @objc func selectReason(_ sender: UIButton) {
        if selectedIndex == sender.tag % 1000{
            selectedIndex = -1
            ConfirmAction.isSelected = false
        }else{
            selectedIndex = sender.tag % 1000
            ConfirmAction.isSelected = true
        }
        cancelTableView.reloadData()
    }
    
         //MARK: -cancel Booking with reasons @Locate cancelModel
    func cancelBooking(){
        var reason = String()
        
        if selectedIndex == -1 {
            reason = commentsTextView.text
        }else{
            reason = cancelReasons[selectedIndex]
        }
        
        Helper.showPI(message:"Loading..")
        var params = cancelModel.init(bookingID: bookingID, status: "4", reason: reason)
        cancelModl.cancelBookingWithReason(params: params, completionHandler: { success in
            if success {
                self.delegate?.cancelReason()
                self.deletingTheDataFromFromParticularID()
                self.dismiss(animated: true, completion: nil)
            }else{
                
            }
        })
        
    }
    
    //******* Deleting the booking id lat longs from couchDB*****************//
    func deletingTheDataFromFromParticularID(){
        
        let  database  = try! manager.databaseNamed("livedayrunner")
        let document = database.document(withID: (bookingID))
        
        do
        {
            try document?.delete()
        }
        catch
        {
            print("Error when deleting the guest database during migrating guest data : \(error.localizedDescription)")
        }
    }
}

//MARK: - uitextviewDelegates
extension CancelViewController:UITextViewDelegate{
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == cancelViewStrings.writeComment {
            textView.text = ""
        }
        return true
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"{
            self.view.endEditing(true)
            return false
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.characters.count < 1{
            textView.text = cancelViewStrings.writeComment
            ConfirmAction.isSelected = false
        }else{
            ConfirmAction.isSelected = true
        }
    }
}

