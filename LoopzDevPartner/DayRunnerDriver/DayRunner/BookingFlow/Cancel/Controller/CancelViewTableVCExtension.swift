//
//  CancelViewTableVC.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 29/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

///****** tableview datasource*************//
extension CancelViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.cancelReasons.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"cancelBooking") as! CancelTableViewCell!
        if selectedIndex == indexPath.row {
            cell?.cancelReason.isSelected = true
        }else{
            cell?.cancelReason.isSelected = false
        }
        cell?.cancelReason.setTitle(self.cancelReasons[indexPath.row], for: .normal)
        cell?.cancelReason.setTitle(self.cancelReasons[indexPath.row], for: .highlighted)
        cell?.cancelReason.tag = indexPath.row + 1000
        cell?.cancelReason.addTarget(self, action: #selector(self.selectReason), for: .touchUpInside)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50;
    }
}

//MARK:- Tableview delegate methods**************//
extension CancelViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tableView.reloadData()
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        selectedIndex = -1
    }
}

