//
//  CancelViewAction.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 29/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension CancelViewController{
    
    //Confirming the cancellation
    @IBAction func confirmCancellation(_ sender: Any) {
        if selectedIndex == -1 && commentsTextView.text == cancelViewStrings.writeComment{
            self.present(Helper.alertVC(title: "Message".localized, message:cancelViewStrings.cancellation), animated: true, completion: nil)
        }else{
            cancelBooking()
        }
    }
    
    @IBAction func tapgestureAction(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func backToVC(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
