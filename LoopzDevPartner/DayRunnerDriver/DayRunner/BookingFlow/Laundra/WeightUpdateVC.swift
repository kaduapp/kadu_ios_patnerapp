//
//  WeightUpdateVC.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 3/7/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit
import RxSwift

class WeightUpdateVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var topOutlet: NSLayoutConstraint!
    @IBOutlet weak var lbsLabelText: UILabel!
    
    @IBOutlet weak var sameAsbtn: UIButton!
     var isChecked = true
      var shipmentModel: Shipment!
    @IBOutlet weak var weightTF: UITextField!
    
    var keyboardSize = CGSize.zero
    let dismissRxVariable = PublishSubject<Shipment?>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isChecked = true
         self.addDoneButtonOnKeyboard()
        self.topOutlet.constant = UIScreen.main.bounds.size.height - 270
//        sameAsbtn.setImage(UIImage(named: "check_off_icon"), for: .normal)
        self.weightTF.text = "Please enter weight"
        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        //    tokenRefreshed = NO;
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShown), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(LaundraPickingItemViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.weightTF.inputAccessoryView = doneToolbar
    }
    @IBAction func upBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    func doneButtonAction() {
        
        let weightOfLaundry = self.weightTF.text
        print(weightOfLaundry ?? "")
        self.weightTF.resignFirstResponder()
        
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.weightTF.text = ""
        textField.becomeFirstResponder()
    }

    @objc func keyboardWillShown(_ notification: Notification?) {
        // Get the size of the keyboard.
        if let keyboardSize = (notification!.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        {
            self.moveViewUp(self.weightTF, andKeyboardHeight: Float(keyboardSize.height))
        }
        
    }
    /**
     *  Keyboard will be Hidden on Notification
     *
     *  @param notification notification
     */
    
    @objc func keyboardWillBeHidden(_ notification: Notification?) {
        
        if ((notification!.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
    }
    /**
     *  Scroll up when keyboard appears
     */
    
    func moveViewUp(_ textfield: UIView?, andKeyboardHeight keyboardHeight: Float) {
        var textfieldMaxY = CGFloat((textfield?.frame.maxX)!)
        var view: UIView? = textfield?.superview
        while view != self.view.superview {
            textfieldMaxY += (view?.frame.minY)!
            view = view?.superview
        }
        let remainder: Float = Float(UIScreen.main.bounds.height - (CGFloat(textfieldMaxY) + CGFloat(keyboardHeight)))
        if remainder >= 0 {
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                
                self.topOutlet.constant = UIScreen.main.bounds.size.height - 270 - CGFloat(keyboardHeight)
            })
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let oldText = textField.text, let r = Range(range, in: oldText) else {
            return true
        }
        
        let newText = oldText.replacingCharacters(in: r, with: string)
        let isNumeric = newText.isEmpty || (Double(newText) != nil)
        let numberOfDots = newText.components(separatedBy: ".").count - 1
        
        let numberOfDecimalDigits: Int
        if let dotIndex = newText.index(of: ".") {
            numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
        } else {
            numberOfDecimalDigits = 0
        }
        
        return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
    }
    
    /**
     *  Scroll down when keyboard dismiss
     */
    func moveViewDown() {
        self.topOutlet.constant = UIScreen.main.bounds.size.height - 270
    }
    
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirmHeightAction(_ sender: Any) {
        let str = weightTF.text
        if(weightTF.text == "" || str == "Please enter weight") {
            self.present(Helper.alertVC(title: "Message".localized, message:"Please enter the weight"), animated: true, completion: nil)
        } else {
            let bookingId = Double(shipmentModel.bookingId)
            let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 26 , custRating: Float(4.0) , custSignature: " " )
            shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                if success == true {
                    self.shipmentModel.orderStatus = 26
                    self.shipmentModel.weightStr = self.weightTF.text!
                    self.dismiss(animated: true, completion: {
                        self.dismissRxVariable.onNext(self.shipmentModel)

                    })
                }
            })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}
