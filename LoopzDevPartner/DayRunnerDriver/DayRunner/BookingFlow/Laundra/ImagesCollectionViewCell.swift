//
//  ImagesCollectionViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 2/20/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class ImagesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var shirtsImageView: UIImageView!
}
