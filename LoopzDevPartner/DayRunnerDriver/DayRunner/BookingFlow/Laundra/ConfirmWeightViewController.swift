//
//  ConfirmWeightViewController.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 4/10/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit
import RxSwift

class ConfirmWeightViewController: UIViewController {
  
    var weightString:String = "" 
    @IBOutlet weak var weightLbsLabel: UILabel!
    var shipmentModel: Shipment!
    
    let dismissRxVariable = PublishSubject<Shipment?>()
    let changeRxVariable = PublishSubject<Shipment?>()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.weightLbsLabel.text =  weightString + " Kgs"
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func confirmWeight(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.shipmentModel.commingFromHome = true
            self.dismissRxVariable.onNext(self.shipmentModel)
        })
//       self.performSegue(withIdentifier: "selectLaundra", sender:nil)
    }

    @IBAction func upBtnAction(_ sender: Any) {
          self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.changeRxVariable.onNext(self.shipmentModel)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nav =  segue.destination as? UINavigationController {
            if let nextScene: SelectLaundraVC = nav.viewControllers.first as! SelectLaundraVC? {
                nextScene.shipmentModel = shipmentModel
                nextScene.weightValue = Int(weightString)!
                
            }
         
        }
    }
}
