//
//  SelectLaundraVC.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 2/21/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class SelectLaundraVC: UIViewController {
    
    var modelOfReason:[ReasonsModel]?
    var apiModel = Reasons()
    var reason:ReasonsModel!
    var storeid:String = ""
    var shipmentModel: Shipment!
    var weightValue:Int = 0
    var limit:Int=20
    var offset:Int=0
    var isDataLoading:Bool=false
    var isComingFromOnbooking:Bool?
    
    @IBOutlet weak var storesTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        
        let nav = self.navigationController?.navigationBar
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        storesTableView.delegate = self
        Helper.showPI(message: "Loading...")
        isDataLoading = true
        
        let ud = UserDefaults.standard
        let updateOrderLatitude = ud.double(forKey:"currentLat")
        let a:String = String(format:"%.1f", updateOrderLatitude)
        
        let updateOrderLongitude = ud.double(forKey:"currentLog")
        let b:String = String(format:"%.1f", updateOrderLongitude)
        
        let newLat = "/" + a
        let newLong = "/" + b
        
        let offSetLa = "/\(self.offset)"
        let limitLa = "/\(self.limit)"
        
        let newString = "5" + newLat + newLong + offSetLa + limitLa
        apiModel.reasonsAPI(reasonfor: newString, completion: { (success) in
            if success{
                Helper.hidePI()
                self.reason = self.apiModel.reasonData[0]
                self.modelOfReason = self.apiModel.reasonData
                self.storesTableView.reloadData()
            }
        })
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        if(shipmentModel.commingFromHome){
            self.navigationController?.popViewController(animated: true)
        }else {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
        
        //        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func chatBtnAction(_ sender: Any) {
        performSegue(withIdentifier: "toChatIP", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nav =  segue.destination as? UINavigationController
        {
            if let viewController: ChatVC = nav.viewControllers.first as! ChatVC? {
                
                viewController.bookingID = "\(shipmentModel.bookingId!)"
                viewController.custName = shipmentModel.customerName
                viewController.customerID = shipmentModel.customerId
                viewController.custImage = shipmentModel.customerPic
                
            }
            
        }
    }
    
 
}
extension SelectLaundraVC : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if self.modelOfReason != nil  {
            return self.modelOfReason!.count
//            return 10
        } else {
           return 0
        }
//        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"SelectLaundraTC") as! SelectLaundraTC
        cell.storeName.text = self.modelOfReason![indexPath.row].reason
        cell.storeAddress.text = self.modelOfReason![indexPath.row].storeAddress
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let rideBookingController = storyboard.instantiateViewController(withIdentifier: "OnbookingController") as! OnBookingViewController;
//        rideBookingController.status = 2
//        rideBookingController.shipmentModel = self.shipmentModel
//        self.navigationController?.pushViewController(rideBookingController, animated: true)

            storeid = self.modelOfReason![indexPath.row].reasonId
           shipmentModel.pickupStore = self.modelOfReason![indexPath.row].reason
            shipmentModel.pickupAddress = self.modelOfReason![indexPath.row].storeAddress
        

            let bookingId = Double(shipmentModel.bookingId)

            let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 27, storeId: storeid, weight: weightValue,  custRating: Float(4.0), custSignature: " ")

            shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in

                //            self.shipmentStartedButton.isEnabled = true
                if success == true{
                    self.shipmentModel.orderStatus = 27
                    self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 27)
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let rideBookingController = storyboard.instantiateViewController(withIdentifier: "OnbookingController") as! OnBookingViewController;
                    rideBookingController.status = 2
                    rideBookingController.shipmentModel = self.shipmentModel
                    self.navigationController?.pushViewController(rideBookingController, animated: true)

                }

            })

    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = self.modelOfReason!.count - 1
        if !isDataLoading && indexPath.row == lastElement {
                    isDataLoading = true
                    self.offset = self.offset+1
                    self.limit = self.limit+10
                    
                    let ud = UserDefaults.standard
                    let updateOrderLatitude = ud.double(forKey:"currentLat")
                    let a:String = String(format:"%.1f", updateOrderLatitude)
                    
                    let updateOrderLongitude = ud.double(forKey:"currentLog")
                    let b:String = String(format:"%.1f", updateOrderLongitude)
                    
                    let newLat = "/" + a
                    let newLong = "/" + b
                    
                    let offSetLa = "/\(self.offset)"
                    let limitLa = "/\(self.limit)"
                    
                    
                    let newString = "5" + newLat + newLong + offSetLa + limitLa
                    apiModel.reasonsAPI(reasonfor: newString, completion: { (success) in
                        if success{
                            Helper.hidePI()
                            self.reason = self.apiModel.reasonData[0]
                            self.modelOfReason = self.apiModel.reasonData
                            self.storesTableView.reloadData()
                        }
                    })
                    
           
        }
    }
}

