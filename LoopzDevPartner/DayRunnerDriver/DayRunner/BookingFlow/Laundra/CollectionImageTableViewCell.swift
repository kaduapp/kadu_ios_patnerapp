//
//  CollectionImageTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 2/20/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class CollectionImageTableViewCell: UITableViewCell {

    @IBOutlet weak var imageCollectionView: UICollectionView!
//    var taxArray = [String:Any]()
     var imagesArray = [[String:Any]]()
    
    var arrayOf:NSArray = []
    override func awakeFromNib() {
        arrayOf = (["Mask Group 126","Mask Group 127","Mask Group 128"] as NSMutableArray) as! [Any] as NSArray
        super.awakeFromNib()
        // Initialization code
    }
    
    func newOneImage(data:[String:Any]) {
        imagesArray = [data]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension CollectionImageTableViewCell : UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray.count
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCollectionViewCell", for: indexPath as IndexPath) as! ImagesCollectionViewCell
        let imageURL = imagesArray[indexPath.row]["itemImageURL"] as! String
  
        let trimme = imageURL.trimmingCharacters(in: .whitespaces)

        cell.shirtsImageView.kf.setImage(with: URL(string: trimme),
                                      placeholder:UIImage.init(named: "profile_defaultimage"),
                                      options: [.transition(ImageTransition.fade(1))],
                                      progressBlock: { receivedSize, totalSize in
                                        
                                        print(totalSize)
        },
                                      completionHandler: { image, error, cacheType, imageURL in
                                        
                                         print(imageURL)
        })

//        cell.shirtsImageView.image = UIImage(named: arrayOf[indexPath.row] as! String)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.0
        let yourHeight = yourWidth
       return CGSize(width: yourWidth, height: yourHeight)
    }
}
