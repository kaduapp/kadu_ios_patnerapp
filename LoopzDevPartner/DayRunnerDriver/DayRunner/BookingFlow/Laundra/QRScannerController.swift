//
//  QRScannerController.swift
//  QRCodeReader
//
//  Created by Yogesh Makhija on 30/07/2018.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import AVFoundation

protocol QRCodeDelegate {
    func scanCompleted(success: Bool)
}

class QRScannerController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet var messageLabel:UILabel!
    @IBOutlet var topbar: UIView!
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer()
    var qrCodeFrameView:UIView?
    var bookingId = ""
    var delegate: QRCodeDelegate? = nil
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        self.navigationController?.isNavigationBarHidden = false
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubviewToFront(qrCodeFrameView)
        }
        
        
        //  Get the back-facing camera for capturing videos
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
            return
        }
        //
        //        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        
        // Get an instance of the AVCaptureDeviceInput class using the previous device object.
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        // Set the input device on the capture session.
        captureSession = AVCaptureSession()
        
        if (captureSession?.canAddInput(videoInput))! {
            captureSession?.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession?.canAddOutput(metadataOutput))! {
            captureSession?.addOutput(metadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            //            metadataOutput.metadataObjectTypes = [.qr]
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        videoPreviewLayer.frame = view.layer.bounds
        //        videoPreviewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        view.layer.addSublayer(videoPreviewLayer)
        
        // Start video capture.
        captureSession?.startRunning()
        
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0x0777C6)
        if (captureSession?.isRunning == false) {
            captureSession?.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        if (captureSession?.isRunning == true) {
            captureSession?.stopRunning()
        }
    }
    func  captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        captureSession?.stopRunning()
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            messageLabel.text = "No QR code is detected"
            // Move the message label and top bar to the front
            self.view.bringSubviewToFront(messageLabel)
            self.view.bringSubviewToFront(topbar)
            return
        } else {
            if let metadataObject = metadataObjects.first {
                guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
                
                // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
                let barCodeObject = videoPreviewLayer.transformedMetadataObject(for: readableObject)
                qrCodeFrameView?.frame = barCodeObject!.bounds
                
                if readableObject.stringValue != nil {
                    //                    messageLabel.text = readableObject.stringValue
                    if readableObject.stringValue == self.bookingId {
                        
                        view.bringSubviewToFront(qrCodeFrameView!)
                        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                        delegate?.scanCompleted(success: true)
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        let alertController = UIAlertController(title: "Message", message: "You got Store details can you procced further", preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            self.performSegue(withIdentifier: "ToGetStores", sender: nil)
                        }
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                            UIAlertAction in
                            NSLog("Cancel Pressed")
                        }
                        
                        // Add the actions
                        alertController.addAction(okAction)
                        alertController.addAction(cancelAction)
                        
                        // Present the controller
                        self.present(alertController, animated: true, completion: nil)
                        //                        self.dismiss(animated: true, completion: nil)
                    }
                }
                
                //                guard let stringValue = readableObject.stringValue else { return }
                
                //                messageLabel.text = stringValue
                
                // Move the message label, QRCode Frame and top bar to the front
                //                self.view.bringSubview(toFront: messageLabel)
                //                self.view.bringSubview(toFront: topbar)
            }
        }
        
        
    }
    
    //    @objc func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection)
    //
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}

