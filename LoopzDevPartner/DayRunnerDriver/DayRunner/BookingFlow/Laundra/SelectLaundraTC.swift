//
//  SelectLaundraTC.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 2/21/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class SelectLaundraTC: UITableViewCell {

    @IBOutlet weak var storeAddress: UILabel!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var storeName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
