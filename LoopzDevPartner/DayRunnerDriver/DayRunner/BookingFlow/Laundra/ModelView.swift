
import Foundation
import RxSwift

struct ReasonsModel {
    var reason: String!
    var reasonId:String!
    var storeAddress:String!
    init(data:[String:Any])
    {
        
        if let reason = data["storeName"] as? String{
            self.reason = reason
        }
        if let storeName = data["storeAddr"] as? String{
            self.storeAddress = storeName
        }
        if let reasonId = data["storeId"] as? String {
            self.reasonId = reasonId
        }
        
    }
    
}

class Reasons {
    let disposeBag = DisposeBag()
    var reasonData:[ReasonsModel]!
    
    func reasonsAPI(reasonfor:String,completion:@escaping(Bool)->()){
        
//        Helper.showPI(message:"Loading..")
        let getReasons =  APIs()
        
        getReasons.getNearestStores(ApiName:reasonfor)
        
        getReasons.subject_response.subscribe(onNext: { (response) in
            Helper.hidePI()
            //            var data = response
            
            if let data =  response["data"] as? [[String:Any]]{
                self.reasonData = data.map{
                    ReasonsModel.init(data:$0)
                }
                completion(true)
            }
            
            
        }, onError: {
            
            (error) in
            Helper.hidePI()
            
            
        }, onCompleted: {
            
        }).disposed(by: disposeBag)
        
        
    }
}

