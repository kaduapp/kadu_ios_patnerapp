//
//  NewProductVC.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 2/20/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class NewProductVC: UIViewController {
    
    var arrayOfImages:[String:Any] = [:]
    
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var shirtLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var quantity = 0
        if let quant = arrayOfImages["quantity"] as? Int {
            quantity = quant
        }
        let itemName = arrayOfImages["itemName"] as? String
        self.quantityLabel.text = "Quantity : \(quantity)"
        self.shirtLabel.text = itemName
        // Do any additional setup after loading the view.
    }
    

    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation
r
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NewProductVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let imageCell: CollectionImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: CollectionImageTableViewCell.self), for: indexPath) as!
        CollectionImageTableViewCell
        imageCell.newOneImage(data:arrayOfImages)
        DispatchQueue.main.async {
            imageCell.imageCollectionView.reloadData()
        }
        return imageCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
}
