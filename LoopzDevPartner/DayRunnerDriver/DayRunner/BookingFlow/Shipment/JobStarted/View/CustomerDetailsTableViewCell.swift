//
//  CustomerDetailsTableViewCell.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class CustomerDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var heightOfHeader: NSLayoutConstraint!
    @IBOutlet weak var callImage: UIImageView!
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var customerName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
//        customerNameLabel.text = "CUSTOMER NAME".localized
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
