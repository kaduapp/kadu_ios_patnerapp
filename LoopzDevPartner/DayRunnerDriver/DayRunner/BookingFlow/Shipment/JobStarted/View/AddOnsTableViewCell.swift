//
//  AddOnsTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 3/28/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class AddOnsTableViewCell: UITableViewCell {

    @IBOutlet weak var addOntitle: UILabel!
    @IBOutlet weak var priceOfAddOn: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
