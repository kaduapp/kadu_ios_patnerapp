//
//  BreakDownTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 6/8/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class BreakDownTableViewCell: UITableViewCell {

    @IBOutlet weak var paymentBreakDownLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        paymentBreakDownLabel.text = "PAYMENT BREAKDOWN".localized
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
