//
//  JobStartedTotalTableViewCell.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class JobStartedTotalTableViewCell: UITableViewCell {
    @IBOutlet var dividerLabel: UILabel!
    
    @IBOutlet var dividerView: UIView!
    @IBOutlet var invoicePattern: UIImageView!
    @IBOutlet var bottonGrandView: UIView!
    @IBOutlet weak var paymentLabek: UILabel!
    @IBOutlet weak var addNewItemButton: UIButton!
    @IBOutlet weak var grandTotal: UILabel!
    
    @IBOutlet weak var grandtotalLabel: UILabel!
    @IBOutlet weak var paymentMethod: UILabel!
    
    var controller:Bool = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async() {
            self.loadStrings()
        }
       
        // Initialization code
    }

    func loadStrings() {
        if controller == false {
            
        }else {
          //grandtotalLabel.text = "Grand Total".localized
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
