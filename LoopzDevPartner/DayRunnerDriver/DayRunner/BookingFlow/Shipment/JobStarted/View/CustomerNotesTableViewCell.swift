//
//  CustomerNotesTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 8/13/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class CustomerNotesTableViewCell: UITableViewCell {
    @IBOutlet weak var customerNotesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
