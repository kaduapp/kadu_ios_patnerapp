//
//  JobStartedAdressesTableViewCell.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class JobStartedAdressesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var pickupLabel: UILabel!
    @IBOutlet weak var pickUpAddress: UILabel!
    @IBOutlet weak var deliveryAddress: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    
    @IBOutlet weak var customerLabel: UILabel!
    @IBOutlet weak var pickupOutlet: UILabel!
    @IBOutlet weak var paymentlabel: UILabel!
    @IBOutlet weak var storeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var paymentType: UILabel!
    
    var controller:Bool = true
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        DispatchQueue.main.async() {
            self.loadStrings()
        }
        // Initialization code
    }

    func loadStrings() {
        if controller == false {
            print("Laundra type.......")
        }else {
//            storeLabel.text = "STORE".localized
//            deliveryLabel.text = "DELIVERY".localized
//            paymentlabel.text = "PAYMENT".localized
//            pickupOutlet.text = "PICKUP".localized
//            customerLabel.text = "CUSTOMER NAME".localized
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
