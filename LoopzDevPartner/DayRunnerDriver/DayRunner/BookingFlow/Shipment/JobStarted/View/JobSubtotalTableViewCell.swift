//
//  JobSubtotalTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 5/13/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class JobSubtotalTableViewCell: UITableViewCell {
    @IBOutlet weak var subtotalLabel: UILabel!
    
    @IBOutlet weak var subtotalOutLet: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
