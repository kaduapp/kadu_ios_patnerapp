//
//  TaxTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 3/29/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class TaxTableViewCell: UITableViewCell {

    @IBOutlet weak var priceTax: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
