//
//  JobTaxTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 3/29/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class JobTaxTableViewCell: UITableViewCell {

    var taxArray = [[String:Any]]()
    @IBOutlet weak var taxTableView: UITableView!
    
    @IBOutlet weak var taxesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        taxesLabel.text = "Tax".localized
        // Initialization code
    }
    
    func newOneTax(data:[[String:Any]]) {
       taxArray = data
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension JobTaxTableViewCell:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taxArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TaxTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: TaxTableViewCell.self), for: indexPath) as! TaxTableViewCell
      
        
        
        if let taxPrice = taxArray[indexPath.row]["price"] as? Double {
            
            if taxPrice == 0.00 {
                cell.taxLabel.text = "(" + (taxArray[indexPath.row]["taxtName"] as! String) +  String(taxArray[indexPath.row]["taxValue"] as! Int) + "%" + ")"
                cell.priceTax.text =  Utility.currencySymbol + "0.00"
            }
            
            
        }
        
        cell.taxLabel.text = "(" + (taxArray[indexPath.row]["taxtName"] as! String) +  String(taxArray[indexPath.row]["taxValue"] as! Int) + "%" + ")"
        
        if let taxPrice = taxArray[indexPath.row]["price"] as? Double {
            cell.priceTax.text =  Utility.currencySymbol + String(format: "%.2f",taxPrice)
        }else {
            cell.priceTax.text =  Utility.currencySymbol + "0.00"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
}
