//
//  JobStartedItemsTableViewCell.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class JobStartedItemsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var unit: UILabel!
    @IBOutlet weak var qty: UILabel!
    @IBOutlet weak var productAndQuantity: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var priceOfProduct: UILabel!
    @IBOutlet weak var addOns: UILabel!
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var addOnsTableView: UITableView!
    @IBOutlet weak var priceOfAddOn: UILabel!
    @IBOutlet weak var addOnsLabelHide: UILabel!
    
    var addOnGroup = [[String:Any]]()
    
    var controller:Bool = true
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async() {
            self.loadStrings()
        }
        // Initialization code
    }
    
    
    
    func loadStrings() {
        if controller == false {
            
        }else {
//            addOnsLabelHide.text = "Add Ons:".localized
        }
    }
    
    func newOne(data:[[String:Any]]) {
        
        let newAddOnGroups = data.map {
            
            $0["addOnGroup"] as! Array<[String:Any]>
            
        }
        addOnGroup = newAddOnGroups.flatMap({$0})
        
         self.addOnsTableView.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension JobStartedItemsTableViewCell:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addOnGroup.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AddOnsTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddOnsTableViewCell.self), for: indexPath) as! AddOnsTableViewCell
        let imageURL = addOnGroup[indexPath.row]["name"] as? String
        let trimme = imageURL!.trimmingCharacters(in: .whitespaces)
        cell.addOntitle.text = trimme
        cell.priceOfAddOn.text = Utility.currencySymbol + String((addOnGroup[indexPath.row]["price"] as? String)!) + ".00"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
}
