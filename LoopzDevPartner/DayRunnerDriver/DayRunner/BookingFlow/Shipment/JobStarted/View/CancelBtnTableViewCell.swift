//
//  CancelBtnTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 8/14/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class CancelBtnTableViewCell: UITableViewCell {

    @IBOutlet weak var cancelBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
