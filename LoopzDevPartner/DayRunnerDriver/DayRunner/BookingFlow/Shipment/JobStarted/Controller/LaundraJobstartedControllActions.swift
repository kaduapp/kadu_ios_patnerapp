//
//  JobStartedControllerAction.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import JaneSliderControl
extension LaundraJobStartedVC
{
    
    @IBAction func sliderFineshed(_ sender: SliderControl) {
        
        if(shipmentModel.storeType == 5 && !shipmentModel.bookingTypeLaundra){
            startJobButton.isEnabled = false
            let bookingId = Double(shipmentModel.bookingId)
            
            let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 10, custRating: Float(4.0), custSignature: " " )
            
            self.resetSlider()
            shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                self.startJobButton.isEnabled = true
                if success == true{
                    self.shipmentModel.orderStatus = 10
                    self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 10)
                    self.performSegue(withIdentifier: "toOnBookingFromJobStarted", sender: nil)
                  
                }
            })
            
        }else if(shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra){
            startJobButton.isEnabled = false
            let bookingId = Double(shipmentModel.bookingId)
            
            let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 10, custRating: Float(4.0), custSignature: " " )
            
            self.resetSlider()
            shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                self.startJobButton.isEnabled = true
                if success == true{
                    self.shipmentModel.orderStatus = 10
                    self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 10)
                    self.performSegue(withIdentifier: "toOnBookingFromJobStarted", sender: nil)
                    
                }
            })
            
            
            
        }else {
            startJobButton.isEnabled = false
            let bookingId = Double(shipmentModel.bookingId)
            
            let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 10, custRating: Float(4.0), custSignature: " " )
            
            
            shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                self.startJobButton.isEnabled = true
                if success == true{
                    self.shipmentModel.orderStatus = 10
                    self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 10)
                    self.performSegue(withIdentifier: "toOnBookingFromJobStarted", sender: nil)
                }
            })
        }
    
    }
    
    func resetSlider()
    {
        self.leftSlider.reset()
    }
    
    @IBAction func slideCanceled(_ sender: SliderControl) {
        print("Cancelled")
    }
    
    
    @IBAction func startJobAction(_ sender: UIButton) {
        
      
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let nextScene = segue.destination as? OnBookingViewController{
            nextScene.shipmentModel = shipmentModel
            nextScene.status = 1
        } else {
            if segue.identifier == "toChat" {
            let nav = segue.destination as! UINavigationController
            if let viewController: ChatVC = nav.viewControllers.first as! ChatVC? {
                
                viewController.bookingID = "\(shipmentModel.bookingId!)"
                viewController.custName = shipmentModel.customerName
                viewController.customerID = shipmentModel.customerId
                viewController.custImage = shipmentModel.customerPic
 
            }
        }
      }
    }
}
