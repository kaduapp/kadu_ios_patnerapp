//
//  LaundraJobStartedVC.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 4/8/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit
import JaneSliderControl

class LaundraJobStartedVC: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var startJobButton: UIButton!
  
    @IBOutlet weak var leftSlider: SliderControl!
    var latitude:Double  = 0.00
    var longitude:Double = 0.00
    
    var locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
    
    var shipmentModel: Shipment!
    var newOneData = [[String:Any]]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.editNavigationBar()
        let num = shipmentModel.bookingId
        titleLabel.text = "Id: \(num!)"
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
    }
    
    
    @IBAction func actionbuttonBack(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func editNavigationBar()
    {
        self.navigationController?.isNavigationBarHidden = true
        self.title = "Job ID: 10011"
    }
    
    @IBAction func callToCustomer(_ sender: UIButton) {
        
        let dropPhone = "tel://" + (shipmentModel.customerPhone)
        if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
        
    }
    
    @IBAction func callToStore(_ sender: UIButton) {
        let dropPhone = "tel://" + (shipmentModel.storePhone)
        if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
        
    }
    
    @IBAction func chatAction(_ sender: UIButton) {
        performSegue(withIdentifier: "toChat", sender: self)
    }
    
    @IBAction func sliderChanged(_ sender: SliderControl) {
        
    }
    
    
}
