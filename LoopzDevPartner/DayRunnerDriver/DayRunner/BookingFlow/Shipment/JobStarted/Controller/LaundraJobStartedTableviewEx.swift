//
//  JobStartedTableViewEx.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
enum LaundraJobStartedTableViewSections: Int
{
    case CustomerDetails = 0
    case Adresses = 1
    case PickupSlot = 2
    case ItemsHeader = 3
    case Items = 4
    case Total = 5
}


extension LaundraJobStartedVC: UITableViewDataSource, UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let numberOfCells: LaundraJobStartedTableViewSections = LaundraJobStartedTableViewSections.init(rawValue: Int(section))!
        switch numberOfCells {
        case .CustomerDetails:
            return 1
            
        case .Adresses:
            return 1
        case .PickupSlot:
            return 1
            //        case .Weight:
            //            return 1
            
        case .ItemsHeader:
            return 1
            
        case .Items:
            return shipmentModel.productDetails.count
//            return 5
            
        case .Total:
            return 1
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section: LaundraJobStartedTableViewSections = LaundraJobStartedTableViewSections.init(rawValue: Int(indexPath.section))!
        
        switch section {
            
        case .CustomerDetails:
            let cell: CustomerDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: CustomerDetailsTableViewCell.self), for: indexPath) as! CustomerDetailsTableViewCell
            //            cell.headerLabel.text = "Customer Name"
            cell.customerName.text = shipmentModel.customerName
            return cell
            
        case .Adresses:
            let cell: JobStartedAdressesTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedAdressesTableViewCell.self), for: indexPath) as! JobStartedAdressesTableViewCell
            cell.controller = false;
            let address = "\(shipmentModel.pickupStore): \(shipmentModel.pickupAddress)"
            let range = NSMakeRange(shipmentModel.pickupStore.count, (address.count - shipmentModel.pickupStore.count))
            cell.pickUpAddress.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
            if shipmentModel.pickupStore == "" {
                cell.pickUpAddress.text = shipmentModel.deliveryAddress
            }else {
                cell.pickUpAddress.text = "\(shipmentModel.pickupStore): \(shipmentModel.pickupAddress)"
            }
            
            //                cell.deliveryAddress.text = shipmentModel.deliveryAddress
            let paymentType = shipmentModel.paymentType
            if paymentType == 1 {
                cell.paymentType.text = "Card"
            } else if paymentType == 2 {
                cell.paymentType.text = "Cash"
            } else {
                cell.paymentType.text = "Wallet"
            }
            
            return cell
        case .PickupSlot:
            let cell: JobStartedAdressesTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedAdressesTableViewCell.self), for: indexPath) as! JobStartedAdressesTableViewCell
            cell.controller = false;
            let address = "\(shipmentModel.pickupStore): \(shipmentModel.pickupAddress)"
            let range = NSMakeRange(shipmentModel.pickupStore.count, (address.count - shipmentModel.pickupStore.count))
            cell.pickUpAddress.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
            
            let date = NSDate(timeIntervalSince1970: TimeInterval(shipmentModel.slotStartTime))
            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "hh:mm a"
            let dateString = dayTimePeriodFormatter.string(from: date as Date)
            
            let date1 = NSDate(timeIntervalSince1970: TimeInterval(shipmentModel.slotEndTime))
            let dayTimePeriodFormatter1 = DateFormatter()
            dayTimePeriodFormatter1.dateFormat = "hh:mm a"
            let dateString1 = dayTimePeriodFormatter1.string(from: date1 as Date)
            
           
            
            
            if(shipmentModel.storeType == 5 && !shipmentModel.bookingTypeLaundra){
                
                if shipmentModel.pickupStore == "" {
                    cell.pickupLabel.text = "PICKUP SLOT"
                    cell.pickUpAddress.text = dateString + "-" + dateString1
                }
                
            }else if (shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra){
                cell.pickupLabel.text = "DROP LOCATION"
                cell.pickUpAddress.text = "\(shipmentModel.customerName): \(shipmentModel.deliveryAddress)"
            }else {
                cell.pickUpAddress.text = "\(shipmentModel.pickupStore): \(shipmentModel.pickupAddress)"
            }
            
            
            
          
            //                cell.deliveryAddress.text = shipmentModel.deliveryAddress
            let paymentType = shipmentModel.paymentType
            if paymentType == 1 {
                cell.paymentType.text = "Card"
            } else if paymentType == 2 {
                cell.paymentType.text = "Cash"
            } else {
                cell.paymentType.text = "Wallet"
            }
            
            return cell
        case .ItemsHeader:
            let cell: JobStartedItemHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedItemHeaderTableViewCell.self), for: indexPath) as! JobStartedItemHeaderTableViewCell
            cell.controller = false;
            return cell
            
        case .Items:
            let cell: JobStartedItemsTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedItemsTableViewCell.self), for: indexPath) as! JobStartedItemsTableViewCell
            cell.controller = false;
                         let products = shipmentModel.productDetails[indexPath.row]
                            if let itemName = products["itemName"] as? String {
                               
                                var quantity = 0
                                if let quant = products["quantity"] as? Int {
                                    quantity = quant
                                }
            
                                if let itemImageURL = products["itemImageURL"] as? String{
                                    if itemImageURL == ""{
                                        
                                    }else{
                                    let url = URL(string: itemImageURL)
                                    DispatchQueue.global().async { [weak self] in
                                        if let data = try? Data(contentsOf: url!) {
                                            if let image = UIImage(data: data) {
                                                DispatchQueue.main.async {
                                                    cell.itemImageView.image = image
                                                }
                                            }
                                        }
                                    }
                                    }
                                }
                                var allAddOnsArray:String = ""
                                if let addOnsArray = products["addOns"] as? [[String:Any]] {
            
                                    for eachAddOns in addOnsArray {
                                        if let addOnGroup = eachAddOns["addOnGroup"] as? [[String:Any]] {
                                            for eachAddOnGroup in addOnGroup {
                                                if let addOnName = eachAddOnGroup["name"] as? String {
            
                                                    allAddOnsArray = allAddOnsArray + addOnName + ","
                                                }
                                            }
                                        }
                                    }
                                }
                                cell.productAndQuantity.text = itemName + "  X  " + String(quantity)
                            }
            return cell
            
        case .Total:
            let cell: JobStartedTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedTotalTableViewCell.self), for: indexPath) as! JobStartedTotalTableViewCell
            cell.controller = false
            //            cell.grandTotal.text = "Grand Total: " +   Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.grandTotal!) , digits: 2))
            let paymentType = shipmentModel.paymentType
            let payByWallet = shipmentModel.payByWallet
            
            if payByWallet == 0 {
                if paymentType == 1 {
                    cell.paymentMethod.text = "Card"
                }else if paymentType == 2 {
                    cell.paymentMethod.text = "Cash"
                }else {
                    cell.paymentMethod.text = "Wallet"
                }
            }else {
                if paymentType == 1 {
                    cell.paymentMethod.text = "Card + Wallet"
                }else if paymentType == 2 {
                    cell.paymentMethod.text = "Cash + Wallet"
                }else {
                    cell.paymentMethod.text = "Wallet"
                }
            }
//            cell.grandTotal.text = Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.grandTotal!) , digits: 2))
            return cell
            //            "Grand Total: \(Utility.currencySymbol)\(shipmentModel.grandTotal!)"
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        let sections: LaundraJobStartedTableViewSections = LaundraJobStartedTableViewSections.init(rawValue: Int(indexPath.section))!
        switch sections {
        case .CustomerDetails:
            return 65
            
        case .Adresses:
            return 76
            
        case .PickupSlot:
            return 76
            
        case .ItemsHeader:
            return 43
            
        case .Items:
            let products = shipmentModel.productDetails[indexPath.row]
            if let isAddOnAvailable = products["addOnAvailable"] as? Bool {
                if isAddOnAvailable {
                    return 120
                } else {
                    return 93
                }
            }
            return 35
            
        case .Total:
            return 72
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section: LaundraJobStartedTableViewSections = LaundraJobStartedTableViewSections.init(rawValue: Int(indexPath.section))!
        
        if section == .Items {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let rideBookingController = storyboard.instantiateViewController(withIdentifier: "NewProductVC") as! NewProductVC;
            rideBookingController.arrayOfImages = shipmentModel.productDetails[indexPath.row]
            self.present(rideBookingController, animated: true, completion: nil)
        }
    }
}


