//
//  ProductDetails.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 9/25/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import Foundation

struct ProductDetails {
    var itemName:String = ""
    var quantity:Int = 0
    var unitName:String = ""
    var addOns:[[String:Any]] = [[:]]
    var finalPrice:NSNumber = 0
    
    init(data:[String:Any]) {
        if let itemName = data["itemName"]  as? String {
            self.itemName = itemName
        }
        if let quantity = data["quantity"] as? Int {
            self.quantity = quantity
        }
        if let unitName = data["unitName"] as? String {
            self.unitName = unitName
        }
        if let finalPrice = data["finalPrice"] as? NSNumber {
            self.finalPrice = finalPrice
        }
        if let addOns = data["addOns"] as? [[String:Any]] {
           
        }
    }
}
