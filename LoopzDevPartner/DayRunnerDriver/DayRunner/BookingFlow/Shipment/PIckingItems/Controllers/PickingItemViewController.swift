//
//  PickingItemViewController.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import JaneSliderControl

class PickingItemViewController: UIViewController,dismissPopUp {
    func goToHome() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    

    var status: Int!
    
    @IBOutlet weak var youareHereLabel: UILabel!
    @IBOutlet weak var pickingItemTableView: UITableView!
    @IBOutlet weak var currentLocationStatus: UILabel!
    @IBOutlet weak var jobId: UILabel!
    @IBOutlet weak var shipmentStartedButton: UIButton!
     @IBOutlet weak var leftSlider: SliderControl!
    var newOneData = [[String:Any]]()
    var shipmentModel: Shipment!
    var comingFromStore:Bool = true
    var selectedIndex:Int = 0
    var classBObject = SearchBarItemViewController()
    var delivertSheduleType:Int = 0

    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = false
        youareHereLabel.text = "YOU ARE HERE".localized
        jobId.text = "Id: \(shipmentModel.bookingId!)"
        
        delivertSheduleType = UserDefaults.standard.integer(forKey: "driverScheduleType")
        
        if delivertSheduleType == 1 {
            if(status == 2){
                leftSlider.sliderText = "DELIVERED".localized
                //            shipmentStartedButton.setTitle("DELIVERED", for: .normal)
                currentLocationStatus.text = shipmentModel.deliveryAddress
            }else {
                leftSlider.sliderText = "Picked"
                let address = "\(shipmentModel.pickupStore): \(shipmentModel.pickupAddress)"
                let range = NSMakeRange(shipmentModel.pickupStore.count, (address.count - shipmentModel.pickupStore.count))
                currentLocationStatus.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
                
            }
        }else {
            if(status == 2){
                leftSlider.sliderText = "DELIVERED".localized
                //            shipmentStartedButton.setTitle("DELIVERED", for: .normal)
                currentLocationStatus.text = shipmentModel.deliveryAddress
            }else if(status  == 3){
                leftSlider.sliderText = "Picked & Started"
                //            shipmentStartedButton.setTitle("DELIVERED", for: .normal)
                currentLocationStatus.text = shipmentModel.deliveryAddress
            } else {
                leftSlider.sliderText = "Picked & Started".localized
                
                if(shipmentModel.storeType == 7) {
                    let address = "\("") \(shipmentModel.pickupAddress)"
                    let range = NSMakeRange(shipmentModel.pickupStore.count, (address.count - shipmentModel.pickupStore.count))
                    currentLocationStatus.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
                }else {
                    let address = "\(shipmentModel.pickupStore): \(shipmentModel.pickupAddress)"
                    let range = NSMakeRange(shipmentModel.pickupStore.count, (address.count - shipmentModel.pickupStore.count))
                    currentLocationStatus.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
                }
               
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
        self.pickingItemTableView.reloadData()
    }
    
   
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
    }
    
    @IBAction func actionbuttonBack(_ sender: UIButton) {
        if(comingFromStore == false) {
            self.navigationController?.popViewController(animated: true)
        }else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
    @IBAction func callToCustomer(_ sender: UIButton) {
        
        let dropPhone = "tel://" + (shipmentModel.customerPhone)
        if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    @IBAction func chatAction(_ sender: UIButton) {
        performSegue(withIdentifier: "toChatIP", sender: self)
    }
    
}
