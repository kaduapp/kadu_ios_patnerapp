//
//  PickingItemActionExtensionVC.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxSwift
import JaneSliderControl

extension LaundraPickingItemViewController{
    
    @IBAction func sliderFineshed(_ sender: SliderControl) {
        
        //  shipmentStartedButton.isEnabled = false
        if(status == 2)
        {
          
            if shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra {
                let bookingId = Double(shipmentModel.bookingId)
                self.leftSlider.reset()
                let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 12 , custRating: Float(4.0), custSignature: " " )
                
                
                shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                    
                    self.shipmentStartedButton.isEnabled = true
                    if success == true{
                        self.shipmentModel.orderStatus = 12
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let rideBookingController = storyboard.instantiateViewController(withIdentifier: "OnbookingController") as! OnBookingViewController;
                        rideBookingController.status = 2
                        rideBookingController.shipmentModel = self.shipmentModel
                        self.navigationController?.pushViewController(rideBookingController, animated: true)
                    }
                    
                })
            }else {
                let bookingId = Double(shipmentModel.bookingId)
                self.leftSlider.reset()
                let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 14 , custRating: Float(4.0), custSignature: " " )
                
                
                shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                    
                    self.shipmentStartedButton.isEnabled = true
                    if success == true{
                        self.shipmentModel.orderStatus = 14
                        self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 14)
                        self.performSegue(withIdentifier: "toProviderInvoice", sender: self.shipmentModel)
                    }
                    
                })
                
            }
        }else{
            
            if shipmentModel.storeType == 5 && !shipmentModel.bookingTypeLaundra {
                self.leftSlider.reset()
                self.openWeightVC(self.shipmentModel)
            }else if shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra {
                let bookingId = Double(shipmentModel.bookingId)
                self.leftSlider.reset()
                let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 12 , custRating: Float(4.0), custSignature: " " )
                
                
                shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                    
                    self.shipmentStartedButton.isEnabled = true
                    if success == true{
                        self.shipmentModel.orderStatus = 12
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let rideBookingController = storyboard.instantiateViewController(withIdentifier: "OnbookingController") as! OnBookingViewController;
                        rideBookingController.status = 2
                        rideBookingController.shipmentModel = self.shipmentModel
                        self.navigationController?.pushViewController(rideBookingController, animated: true)
                    }
                    
                })
            }else {
                let bookingId = Double(shipmentModel.bookingId)
                self.leftSlider.reset()
                let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 14 , custRating: Float(4.0), custSignature: " " )
                
                
                shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                    
                    self.shipmentStartedButton.isEnabled = true
                    if success == true{
                        self.shipmentModel.orderStatus = 14
                        self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 14)
                        self.performSegue(withIdentifier: "toProviderInvoice", sender: self.shipmentModel)
                    }
                    
                })
            }
            
        }
    }
    
    func openWeightVC(_ shipmentModel : Shipment) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rideBookingController = storyboard.instantiateViewController(withIdentifier: "WeightUpdateVC") as! WeightUpdateVC;
        rideBookingController.shipmentModel = shipmentModel
        rideBookingController.dismissRxVariable.subscribe(onNext: { (shipmentModel) in
            // Write new logic
            if let shipmentModel = shipmentModel {
                self.openConfirmWeightVC(shipmentModel)
            }
        }, onError: { (error) in
            print(error.localizedDescription)
        }
            , onCompleted: {
                print("completed")
        }) {
            print("disposed")
        }
        self.navigationController?.present(rideBookingController, animated: true, completion: nil)
    }
    
    func openConfirmWeightVC(_ shipmentModel: Shipment) {
        self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 26)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rideBookingController = storyboard.instantiateViewController(withIdentifier: "ConfirmWeightViewController") as! ConfirmWeightViewController;
        rideBookingController.shipmentModel = shipmentModel
        rideBookingController.weightString = shipmentModel.weightStr
        rideBookingController.dismissRxVariable.subscribe(onNext: { (shipmentModel) in
            if let shipmentModel = shipmentModel {
                self.openSelectLaundraVC(shipmentModel)
            }
        }, onError: { (error) in
            print(error.localizedDescription)
        }
            , onCompleted: {
                print("completed")
        }) {
            print("disposed")
        }
        rideBookingController.changeRxVariable.subscribe(onNext: { (shipmentModel) in
            if let shipmentModel = shipmentModel {
                self.openWeightVC(shipmentModel)
            }
        }, onError: { (error) in
            print(error.localizedDescription)
        }
            , onCompleted: {
                print("completed")
        }) {
            print("disposed")
        }
        self.navigationController?.present(rideBookingController, animated: true, completion: nil)
    }
    
    func openSelectLaundraVC(_ shipmentModel: Shipment) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let selectLaundraVC = storyboard.instantiateViewController(withIdentifier: "SelectLaundraVC") as! SelectLaundraVC;
        selectLaundraVC.shipmentModel = shipmentModel
        selectLaundraVC.weightValue = Int(shipmentModel.weightStr) ?? 0
        self.navigationController?.pushViewController(selectLaundraVC, animated: true)
    }
    
    func resetSlider() {
        self.leftSlider.reset()
    }
    
    @IBAction func slideCanceled(_ sender: SliderControl) {
        print("Cancelled")
    }
    
    @IBAction func sliderChanged(_ sender: SliderControl) {
        print("Changed")
    }
    
    @IBAction func startJobAction(_ sender: UIButton) {
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nextScene = segue.destination as? ProviderInvoiceViewController {
            nextScene.shipmentModel = shipmentModel
        } else if let nextSc = segue.destination as? SelectLaundraVC {
            nextSc.shipmentModel = shipmentModel
            nextSc.storeid = "\(self.shipmentModel.bookingId!)"
        } else if let nav =  segue.destination as? UINavigationController {
            if let viewController: ChatVC = nav.viewControllers.first as! ChatVC? {
                
                viewController.bookingID = "\(shipmentModel.bookingId!)"
                viewController.custName = shipmentModel.customerName
                viewController.customerID = shipmentModel.customerId
                viewController.custImage = shipmentModel.customerPic
            }
        }
    }
}
