//
//  LaundraPickingItemViewController.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 4/9/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit
import JaneSliderControl
class LaundraPickingItemViewController: UIViewController {
    
    var status: Int!
    
    @IBOutlet weak var weightTF: UITextField!
    @IBOutlet weak var lbsLabel: UILabel!
    @IBOutlet weak var laundryWeightLabel: UILabel!
    @IBOutlet weak var pickingItemTableView: UITableView!
    @IBOutlet weak var currentLocationStatus: UILabel!
    @IBOutlet weak var jobId: UILabel!
    @IBOutlet weak var shipmentStartedButton: UIButton!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var leftSlider: SliderControl!
    
    var shipmentModel: Shipment!
    
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = false
        self.addDoneButtonOnKeyboard()
        jobId.text = "Id: \(shipmentModel.bookingId!)"
        if(status == 2){
            self.weightTF.isHidden = true
            self.laundryWeightLabel.isHidden = true
            self.lbsLabel.isHidden = true
            if shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra {
                customerName.text = shipmentModel.pickupStore
                currentLocationStatus.text = shipmentModel.pickupAddress
            }else {
                currentLocationStatus.text = shipmentModel.deliveryAddress
                
            }
            shipmentStartedButton.setTitle("DELIVERED", for: .normal)
        } else {
            if shipmentModel.storeType == 5 && !shipmentModel.bookingTypeLaundra {
                leftSlider.sliderText = "NEXT"
                shipmentStartedButton.setTitle("NEXT", for: .normal)
                customerName.text = shipmentModel.customerName
                currentLocationStatus.text = shipmentModel.deliveryAddress
            }else if shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra {
                 leftSlider.sliderText = "Picked & Started"
                customerName.text = shipmentModel.pickupStore
                currentLocationStatus.text = shipmentModel.pickupAddress
            }else {
                leftSlider.sliderText = "Picked & Started"
                shipmentStartedButton.setTitle("NEXT", for: .normal)
                customerName.text = shipmentModel.customerName
                    currentLocationStatus.text = shipmentModel.deliveryAddress
             
            }
            
          
            //            let address = "\(shipmentModel.pickupStore): \(shipmentModel.pickupAddress)"
            //            let range = NSMakeRange(shipmentModel.pickupStore.count, (address.count - shipmentModel.pickupStore.count))
            //            currentLocationStatus.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
    }
    
    @IBAction func actionbuttonBack(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(LaundraPickingItemViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.weightTF.inputAccessoryView = doneToolbar
    }
    
   @objc func doneButtonAction() {
        
        let weightOfLaundry = self.weightTF.text
        print(weightOfLaundry ?? "")
        self.weightTF.resignFirstResponder()
        
    }
    @IBAction func callToCustomer(_ sender: UIButton) {
        
        if shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra {
            
            let dropPhone = "tel://" + (shipmentModel.storePhone)
            if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.openURL(url as URL)
            }
        }else {
            
            let dropPhone = "tel://" + (shipmentModel.customerPhone)
            if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.openURL(url as URL)
            }
        }
     
    }
    @IBAction func chatAction(_ sender: UIButton) {
        performSegue(withIdentifier: "toChatIP", sender: self)
    }
    
}



