//
//  PickingItemTableViewEx.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

enum LaundraPickingControllerTableViewSections: Int
{
    case Adresses = 6
    case ItemsHeader = 3
    case Items = 4
    case Total = 7
    case pickupSlot = 2
    case customerName = 0
    case deliveryLocation = 1
    case Weight = 5
}

extension LaundraPickingItemViewController: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print(section)
        
        let numberOfCells: LaundraPickingControllerTableViewSections = LaundraPickingControllerTableViewSections.init(rawValue: Int(section))!
        switch numberOfCells {
            
        case .ItemsHeader:
            return 1
            
        case .Items:
                return shipmentModel.productDetails.count
//            return 5
        case .Total:
            return 1
            
        case .Adresses:
            return 1
            
        case .pickupSlot:
            return 1
        case .customerName:
            return 1
        case .deliveryLocation:
            return 1
        case .Weight:
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print(indexPath.row)
        
        
        let section: LaundraPickingControllerTableViewSections = LaundraPickingControllerTableViewSections.init(rawValue: Int(indexPath.section))!
        
        switch section {
            
            
        case .ItemsHeader:
            let cell: JobStartedItemHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedItemHeaderTableViewCell.self), for: indexPath) as! JobStartedItemHeaderTableViewCell
            return cell
            
        case .Items:
            let cell: JobStartedItemsTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedItemsTableViewCell.self), for: indexPath) as! JobStartedItemsTableViewCell
            let products = shipmentModel.productDetails[indexPath.row]
            if let itemName = products["itemName"] as? String {
                
                var quantity = 0
                if let quant = products["quantity"] as? Int {
                    quantity = quant
                }
                
                if let itemImageURL = products["itemImageURL"] as? String{
                   if itemImageURL == ""{
                        
                    }else{
                    let url = URL(string: itemImageURL)
                    DispatchQueue.global().async { [weak self] in
                        if let data = try? Data(contentsOf: url!) {
                            if let image = UIImage(data: data) {
                                DispatchQueue.main.async {
                                    cell.itemImageView.image = image
                                }
                            }
                        }
                    }
                    }
                }
                var allAddOnsArray:String = ""
                if let addOnsArray = products["addOns"] as? [[String:Any]] {
                    
                    for eachAddOns in addOnsArray {
                        if let addOnGroup = eachAddOns["addOnGroup"] as? [[String:Any]] {
                            for eachAddOnGroup in addOnGroup {
                                if let addOnName = eachAddOnGroup["name"] as? String {
                                    
                                    allAddOnsArray = allAddOnsArray + addOnName + ","
                                }
                            }
                        }
                    }
                }
                cell.productAndQuantity.text = itemName + "  X  " + String(quantity)
            }
            return cell
            
        case .Total:
            let cell: JobStartedTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedTotalTableViewCell.self), for: indexPath) as! JobStartedTotalTableViewCell
          
            let paymentType = shipmentModel.paymentType
            let payByWallet = shipmentModel.payByWallet
            if payByWallet == 0 {
                if paymentType == 1 {
                    cell.paymentLabek.text = "Card"
                }else if paymentType == 2 {
                    cell.paymentLabek.text = "Cash"
                }else {
                    cell.paymentLabek.text = "Wallet"
                }
            }else {
                if paymentType == 1 {
                    cell.paymentLabek.text = "Card + Wallet"
                }else if paymentType == 2 {
                    cell.paymentLabek.text = "Cash + Wallet"
                }else {
                    cell.paymentLabek.text = "Wallet"
                }
            }
//            cell.grandTotal.text = Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.grandTotal!) , digits: 2))
            //            cell.addNewItemButton.layer.borderWidth = 1
            //            cell.addNewItemButton.layer.borderColor = Helper.UIColorFromRGB(rgbValue: 0x5F94D2).cgColor
            return cell
            
        case .Adresses:
            let cell: JobStartedAdressesTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedAdressesTableViewCell.self), for: indexPath) as! JobStartedAdressesTableViewCell
            cell.customerName.text = shipmentModel.customerName
            cell.deliveryAddress.text = shipmentModel.deliveryAddress
            let paymentType = shipmentModel.paymentType
            if paymentType == 1 {
                cell.paymentType.text = "Card"
            } else if paymentType == 2 {
                cell.paymentType.text = "Cash"
            } else {
                cell.paymentType.text = "WALLET"
            }
            return cell
        case .pickupSlot:
            let cell: JobslotTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobslotTableViewCell.self), for: indexPath) as! JobslotTableViewCell
            
            let date = NSDate(timeIntervalSince1970: TimeInterval(shipmentModel.slotStartTime))
            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "hh:mm a"
            let dateString = dayTimePeriodFormatter.string(from: date as Date)
            
            let date1 = NSDate(timeIntervalSince1970: TimeInterval(shipmentModel.slotEndTime))
            let dayTimePeriodFormatter1 = DateFormatter()
            dayTimePeriodFormatter1.dateFormat = "hh:mm a"
            let dateString1 = dayTimePeriodFormatter1.string(from: date1 as Date)
            
            cell.slotLabel.text = dateString + "-" + dateString1
//            if shipmentModel.expressType {
//                cell.expressLabel.isHidden = false;
//            }else{
//                cell.expressLabel.isHidden = true;
//            }
            
            return cell
            
        case .customerName:
               let cell: LaundrCustomerCell = tableView.dequeueReusableCell(withIdentifier: String(describing: LaundrCustomerCell.self), for: indexPath) as! LaundrCustomerCell
               cell.customerName.text = shipmentModel.customerName
            return cell
        case .deliveryLocation:
            let cell: LaundraDeliveryCell = tableView.dequeueReusableCell(withIdentifier: String(describing: LaundraDeliveryCell.self), for: indexPath) as! LaundraDeliveryCell
            cell.deliveryLocation.text = shipmentModel.deliveryAddress
            return cell
        case .Weight:
            let cell: LaundraWeightCell = tableView.dequeueReusableCell(withIdentifier: String(describing: LaundraWeightCell.self), for: indexPath) as! LaundraWeightCell
             if(shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra){
                cell.weightLabel.text = String(shipmentModel.weightPicked) + shipmentModel.weightText + " weight"
            }else {
                cell.weightLabel.text = String(shipmentModel.weight) + shipmentModel.weightText + " weight"
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if shipmentModel.storeType == 5 && !shipmentModel.bookingTypeLaundra{
            let sections: LaundraPickingControllerTableViewSections = LaundraPickingControllerTableViewSections.init(rawValue: Int(indexPath.section))!
            
            switch sections {
                
            case .ItemsHeader:
                return 40
                
            case .Items:
                let products = shipmentModel.productDetails[indexPath.row]
                if let isAddOnAvailable = products["addOnAvailable"] as? Bool {
                    if isAddOnAvailable {
                        return 120
                    } else {
                        return 88
                    }
                }
                return 35
            case .Total:
                if(status == 2){
                    return 88
                }else{
                    return 88
                    //                return 90
                }
                
            case .Adresses:
                return 0
                
            case .pickupSlot:
                if shipmentModel.expressType {
                     return 110
                }else {
                     return 75
                }
               
            case .customerName:
                return 0
            case .deliveryLocation:
                return 0
                
            case .Weight:
                return 0
                
            }
        }else {
            let sections: LaundraPickingControllerTableViewSections = LaundraPickingControllerTableViewSections.init(rawValue: Int(indexPath.section))!
            
            switch sections {
                
            case .ItemsHeader:
                return 40
                
            case .Items:
                let products = shipmentModel.productDetails[indexPath.row]
                if let isAddOnAvailable = products["addOnAvailable"] as? Bool {
                    if isAddOnAvailable {
                        return 120
                    } else {
                        return 88
                    }
                }
                return 35
            case .Total:
                if(status == 2){
                    return 88
                }else{
                    return 88
                    //                return 90
                }
                
            case .Adresses:
                return 0
                
            case .pickupSlot:
                
                if shipmentModel.expressType {
                    return 110
                }else {
                    return 75
                }
            case .customerName:
                return 70
            case .deliveryLocation:
                return 90
                
            case .Weight:
                return 50
                
            }
        }
     
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let section: LaundraPickingControllerTableViewSections = LaundraPickingControllerTableViewSections.init(rawValue: Int(indexPath.section))!
        
        switch section {
            
        case .Items:
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let rideBookingController = storyboard.instantiateViewController(withIdentifier: "NewProductVC") as! NewProductVC;
            rideBookingController.arrayOfImages =  shipmentModel.productDetails[indexPath.row]
            self.present(rideBookingController, animated: true, completion: nil)
            
            //            UpdateQuantityPopup.instance.show()
            //
            //            UpdateQuantityPopup.instance.productDetailsInit(details: shipmentModel, ItemNo: indexPath.row)
            //            UpdateQuantityPopup.instance.delegate = self
            //
            break
            
        default:
            break
            
        }
    }
}

extension LaundraPickingItemViewController: UpdateQuantityDelegate {
    
    func updateOrderDetails(updatedOrder: Shipment) {
        var OrderId: Double!
        if let orderId = updatedOrder.bookingId as? NSNumber {
            OrderId = Double(orderId)
        }
        
        if self.shipmentModel.productDetails.count > 0 {
            
            let params = Shipment.init( items: updatedOrder.productDetails , orderId: OrderId )
            shipmentModel.makeServiceCallToUpdateOrder(withParams: params, completionHanler: { (success,gTotal,subtotal) in
                if success{
                    updatedOrder.grandTotal = gTotal
                    self.shipmentModel = updatedOrder
                    self.pickingItemTableView.reloadData()
                } else{
                    
                }
            })
        } else {
            
            let alertController = UIAlertController(title: "Message".localized, message: "You are about to cancel order, press Okay to cancel Order", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                
                let ipAddress = Utility.getIPAddress()
                let params = Shipment.init(orderId: OrderId, ipAddress: ipAddress)
                self.shipmentModel.makeServiceCallToCancelOrder(shipmentModel: params, completionHanler: { success in
                    if success{
                        //removing the product from array of product details
                        self.shipmentModel = updatedOrder
                        self.navigationController?.popToRootViewController(animated: true)
                    } else{
                        
                    }
                })
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
}

