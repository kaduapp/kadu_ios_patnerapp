//
//  PickingItemTableViewEx.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

enum PickingControllerTableViewSections: Int
{
    case Adresses = 2
    case EstimateValue = 3
    case CustomerBreakDown = 4
    case CustomerNote = 5
    case ItemsHeader = 0
    case Items = 1
    case Total = 9
    case Subtotal = 7
    case BreakDown = 6
    case Tax = 8
    case CancelBtn = 10
}

extension PickingItemViewController: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print(section)
        
        let numberOfCells: PickingControllerTableViewSections = PickingControllerTableViewSections.init(rawValue: Int(section))!
        switch numberOfCells {
            
        case .ItemsHeader:
            return 1
            
        case .Items:
            return shipmentModel.productDetails.count
            
        case .Total:
            return 3
        case .Tax:
            return 1
        case .Subtotal:
            return 4
            
        case .Adresses:
            return 1
        case .BreakDown :
            return 1
            
        case .EstimateValue:
            return 1
        case .CustomerBreakDown:
             return 1
        case .CustomerNote:
             return 1
        case .CancelBtn :
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print(indexPath.row)
        
        
        let section: PickingControllerTableViewSections = PickingControllerTableViewSections.init(rawValue: Int(indexPath.section))!
        
        switch section {
            
            
        case .ItemsHeader:
            let cell: JobStartedItemHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedItemHeaderTableViewCell.self), for: indexPath) as! JobStartedItemHeaderTableViewCell
            if(shipmentModel.storeType == 7) {
                cell.qtyLabel.isHidden = true
                cell.tapOnLabel.text = "PRODUCTS"
                cell.priceLabel.text = " Quantity "
                cell.priceLabel.textAlignment = .center
            }else {
                cell.qtyLabel.isHidden = false
                cell.priceLabel.text = " PRICE ".localized + "(  " + Utility.currencySymbol + "  )"
            }
            
            return cell
            
        case .Items:
            let cell: JobStartedItemsTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedItemsTableViewCell.self), for: indexPath) as! JobStartedItemsTableViewCell
            if(shipmentModel.storeType == 7){
                cell.qty.isHidden = true
            }
            if let products = shipmentModel.productDetails[indexPath.row] as? [String: Any] {
                
                if let itemName = products["itemName"] as? String {
                    var quantity = 0
                    var unit = ""
                    
                    if let quant = products["quantity"] as? Int {
                        quantity = quant
                    }
                    if let unitName = products["unitName"] as? String {
                        unit = unitName
                    }
                    if let itemImageURL = products["itemImageURL"] as? String{
                       if itemImageURL == ""{
                            
                        }else{
                        let url = URL(string: itemImageURL)
                        DispatchQueue.global().async { [weak self] in
                            if let data = try? Data(contentsOf: url!) {
                                if let image = UIImage(data: data) {
                                    DispatchQueue.main.async {
                                        cell.itemImageView.image = image
                                    }
                                }
                            }
                        }
                        }
                    }
                    let addOnsArray = products["addOns"] as? [[String:Any]]
                    if addOnsArray?.count == 0 {
                        cell.addOnsLabelHide.isHidden = true
                    }else {
                        cell.addOnsLabelHide.isHidden = false
                    }
                    newOneData = addOnsArray!
                    cell.newOne(data: addOnsArray!)
                    //                    cell.addOns.text = "\(allAddOnsArray)"
                    cell.productAndQuantity.text = itemName
                    if(shipmentModel.storeType == 7) {
                        cell.priceOfProduct.text = "\(quantity)"
                         cell.priceOfProduct.textAlignment = .center
                    }
                    cell.qty.text =  "\(quantity)"
//                    cell.unit.text = "Unit : \(unit)"
                    
                }
                
                if let itemPrice = products["finalPrice"] as? NSNumber {
                    if(shipmentModel.storeType == 7) {
                        
                    }else {
                       cell.priceOfProduct.text = Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(itemPrice) , digits: 2))
                    }
                    
                }
            }
            if(shipmentModel.storeType == 7) {
                
            }else {
                cell.editBtn.addTarget(self, action: #selector(self.selectReason), for: .touchUpInside)
                cell.editBtn.tag = indexPath.row
            }
            return cell
        case .Tax:
            let cell: JobTaxTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobTaxTableViewCell.self), for: indexPath) as! JobTaxTableViewCell
            cell.newOneTax(data:shipmentModel.exculsiveTax)
            cell.taxTableView.reloadData()
            return cell
        case .Subtotal:
            
            switch indexPath.row {
            case 0:
                let cell: JobSubtotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobSubtotalTableViewCell.self), for: indexPath) as! JobSubtotalTableViewCell
                cell.subtotalOutLet.text = "Sub Total".localized
                cell.subtotalLabel.text =  Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.subTotal!) , digits: 2))
                return cell
            case 1:
                let cell: JobSubtotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobSubtotalTableViewCell.self), for: indexPath) as! JobSubtotalTableViewCell
                cell.subtotalOutLet.text = "Delivery Charges".localized
                cell.subtotalLabel.text =  Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.deliveryCharge!) , digits: 2))
                return cell
                
            case 2:
                let cell: JobSubtotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobSubtotalTableViewCell.self), for: indexPath) as! JobSubtotalTableViewCell
                cell.subtotalOutLet.text = "Discount".localized
                cell.subtotalLabel.text =  Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.discount!) , digits: 2))
                return cell
                
                
                
            default:
                let cell: JobSubtotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobSubtotalTableViewCell.self), for: indexPath) as! JobSubtotalTableViewCell
                cell.subtotalOutLet.text = "Tips".localized
                cell.subtotalLabel.text =  Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.drivertip!) , digits: 2))
                return cell
            }
            
            
        case .Total:
            
            switch indexPath.row {
            case 0:
                let cell: JobStartedTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedTotalTableViewCell.self), for: indexPath) as! JobStartedTotalTableViewCell
                cell.dividerView.isHidden = false
                cell.dividerLabel.isHidden = true
                cell.grandtotalLabel.text = "Wallet :".localized
                let walletAmount:Double = shipmentModel.grandTotal - shipmentModel.cashCollect
                cell.grandTotal.text =  Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(walletAmount) , digits: 2))
                 cell.invoicePattern.isHidden = true
                return cell
            case 1:
                let cell: JobStartedTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedTotalTableViewCell.self), for: indexPath) as! JobStartedTotalTableViewCell
                cell.dividerView.isHidden = true
                cell.dividerLabel.isHidden = true
                cell.grandtotalLabel.text = "Cash :".localized
                cell.grandTotal.text =  Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.cashCollect) , digits: 2))
                  cell.invoicePattern.isHidden = true
                return cell
                
            default:
                let cell: JobStartedTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedTotalTableViewCell.self), for: indexPath) as! JobStartedTotalTableViewCell
                cell.dividerView.isHidden = true
                cell.dividerLabel.isHidden = true
                cell.grandtotalLabel.text = "Grand Total :"
                cell.grandTotal.text = " " +   Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.grandTotal!) , digits: 2))
                 cell.invoicePattern.isHidden = false
                return cell
            }
            
        case .Adresses:
            let cell: JobStartedAdressesTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedAdressesTableViewCell.self), for: indexPath) as! JobStartedAdressesTableViewCell
            cell.customerName.text = shipmentModel.customerName
            cell.deliveryAddress.text = shipmentModel.deliveryAddress
            let paymentType = shipmentModel.paymentType
            let payByWallet = shipmentModel.payByWallet
            if payByWallet == 0 {
                if paymentType == 1 {
                    cell.paymentType.text = "Card".localized
                }else if paymentType == 2 {
                    cell.paymentType.text = "Cash".localized
                }else {
                    cell.paymentType.text = "Wallet".localized
                }
            }else {
                if paymentType == 1 {
                    cell.paymentType.text = "Card".localized + "Wallet".localized
                }else if paymentType == 2 {
                    cell.paymentType.text = "Cash".localized + "Wallet".localized
                }else {
                    cell.paymentType.text = "Wallet".localized
                }
            }
            return cell
        case .BreakDown:
            let cell: BreakDownTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: BreakDownTableViewCell.self), for: indexPath) as! BreakDownTableViewCell
            return cell
            
        case .EstimateValue:
            let cell: CustomerDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: CustomerDetailsTableViewCell.self), for: indexPath) as! CustomerDetailsTableViewCell
            cell.customerNameLabel.text = "ESTIMATE PACKAGE VALUE"
            cell.customerName.text = Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.estimatedPackageValue) , digits: 2))
            cell.callImage.isHidden = true
            return cell
        case .CustomerBreakDown:
            let cell: BreakDownTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: BreakDownTableViewCell.self), for: indexPath) as! BreakDownTableViewCell
            cell.paymentBreakDownLabel.text = "CUSTOMER NOTES"
            cell.paymentBreakDownLabel.font = UIFont(name:"ClanPro-NarrNews", size: 12.0)
            return cell
        case .CustomerNote:
            let cell: CustomerNotesTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: CustomerNotesTableViewCell.self), for: indexPath) as! CustomerNotesTableViewCell
            cell.customerNotesLabel.text = shipmentModel.extraNote
            return cell
        case .CancelBtn:
            let cell: CancelBtnTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: CancelBtnTableViewCell.self), for: indexPath) as! CancelBtnTableViewCell
            cell.cancelBtn.addTarget(self, action: #selector(self.selectReason1), for: .touchUpInside)
            cell.cancelBtn.tag = indexPath.row
            return cell
        }
        
    }
    @objc func selectReason1(_ sender: UIButton) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "CancelNewViewController") as! CancelNewViewController
        secondViewController.shipment = shipmentModel
        secondViewController.delegate = self
        self.navigationController?.present(secondViewController, animated: true, completion: nil)
        
    }
    
    @objc func selectReason(_ sender: UIButton) {
        let delivertSheduleType:Int = UserDefaults.standard.integer(forKey: "driverScheduleType")
        
        if delivertSheduleType == 1 {
             selectedIndex = sender.tag
             performSegue(withIdentifier: "goToEditItem", sender:nil)
        }else {
            let selectedIndex = sender.tag
            UpdateQuantityPopup.instance.show()
            
            UpdateQuantityPopup.instance.productDetailsInit(details: shipmentModel, ItemNo: selectedIndex)
            UpdateQuantityPopup.instance.delegate = self
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let sections: PickingControllerTableViewSections = PickingControllerTableViewSections.init(rawValue: Int(indexPath.section))!
        
        switch sections {
            
        case .ItemsHeader:
            return 55
   
        case .Items:
            let products = shipmentModel.productDetails[indexPath.row]
            if let isAddOnAvailable = products["addOnAvailable"] as? Bool {
                var addOnGroup = [[String:Any]]()
                let addOnsArray = products["addOns"] as? [[String:Any]]
                let newAddOnGroups = newOneData.map {
                    
                    $0["addOnGroup"] as! Array<[String:AnyObject]>
                    
                }
                
                addOnGroup = newAddOnGroups.flatMap({$0})
                
                if isAddOnAvailable {
                    return CGFloat(30 * addOnGroup.count) + 80
                } else {
                    if addOnsArray?.count == 0 {
                        if(shipmentModel.storeType == 7) {
                            return 50
                        }else {
                             return UITableView.automaticDimension
                        }
                    }else {
                        return 80
                    }
                }
            }
            return UITableView.automaticDimension
        case .Total:
            switch indexPath.row {
            case 0,1:
                return 35
            default:
                return 44
            }
        case .Tax :
            if shipmentModel.exculsiveTax.count == 0 {
                return 0
            }
            return 88
        case .Subtotal :
            if(shipmentModel.storeType == 7) {
                if( indexPath.row == 0 ) {
                    return 0
                }
            }
            return 30
            
        case .Adresses:
            return 180
        case .BreakDown:
            return 44
            
        case .EstimateValue:
            if(shipmentModel.storeType == 7){
                return 60
            }
            return 0
        case .CustomerNote:
            if(shipmentModel.storeType == 7){
                return UITableView.automaticDimension
            }
            return 0
        case .CustomerBreakDown :
            if(shipmentModel.storeType == 7){
                return 44
            }
            return 0
        case .CancelBtn:
            if(shipmentModel.storeType == 7){
                
                    if(status == 2){
                        return 0
                    
                }else {
                     return 65
                }
                
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let section: PickingControllerTableViewSections = PickingControllerTableViewSections.init(rawValue: Int(indexPath.section))!
        
        switch section {
            
        case .Items:
            
            break
            
        default:
            break
            
        }
    }
}

extension PickingItemViewController: UpdateQuantityDelegate {
    
    func updateOrderDetails(updatedOrder: Shipment) {
        var OrderId: Double!
        if let orderId = updatedOrder.bookingId as? NSNumber {
            OrderId = Double(orderId)
        }
        
        if self.shipmentModel.productDetails.count > 0 {
          
//            let loadedCart = UserDefaults.standard.array(forKey: "myCart") as? [[String: Any]]
     
            let params = Shipment.init( items: shipmentModel.productDetails , orderId: OrderId )
            shipmentModel.makeServiceCallToUpdateOrder(withParams: params, completionHanler: { (success,gTotal,subtotal) in
                if success {
                    updatedOrder.grandTotal = gTotal
                    updatedOrder.subTotal = subtotal
                    self.shipmentModel = updatedOrder
                    self.pickingItemTableView.reloadData()
                } else {
                    
                }
            })
        } else {
            
            let alertController = UIAlertController(title: "Message".localized, message: "You are about to cancel order, press Okay to cancel Order", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                
                let ipAddress = Utility.getIPAddress()
                let params = Shipment.init(orderId: OrderId, ipAddress: ipAddress)
                self.shipmentModel.makeServiceCallToCancelOrder(shipmentModel: params, completionHanler: { success in
                    if success{
                        //removing the product from array of product details
                        self.shipmentModel = updatedOrder
                        self.navigationController?.popToRootViewController(animated: true)
                    } else{
                        
                    }
                })
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
}

