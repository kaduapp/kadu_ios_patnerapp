//
//  PickingItemActionExtensionVC.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import JaneSliderControl

extension PickingItemViewController{
    
    @IBAction func sliderFineshed(_ sender: SliderControl) {
        shipmentStartedButton.isEnabled = false
        if(status == 2)
        {
            let bookingId = Double(shipmentModel.bookingId)
            
            let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 14 , custRating: Float(4.0), custSignature: " " )
            
            resetSlider()
            shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                
                self.shipmentStartedButton.isEnabled = true
                if success == true{
                    self.shipmentModel.orderStatus = 14
                    self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 14)
                    self.performSegue(withIdentifier: "toProviderInvoice", sender: self.shipmentModel)
                }
                
            })
        }else if(status == 3){
            
            let bookingId = Double(shipmentModel.bookingId)
            
            let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 12 , custRating: Float(4.0) , custSignature: " " )
            resetSlider()
            shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                
                self.shipmentStartedButton.isEnabled = true
                if success == true{
                    self.shipmentModel.orderStatus = 12
                    self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 12)
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let rideBookingController = storyboard.instantiateViewController(withIdentifier: "OnbookingController") as! OnBookingViewController;
                    rideBookingController.status = 2
                    rideBookingController.shipmentModel = self.shipmentModel
                    self.navigationController?.pushViewController(rideBookingController, animated: true)
                    
                }
                
            })
        }else{
            
            let bookingId = Double(shipmentModel.bookingId)
            
            let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 12 , custRating: Float(4.0) , custSignature: " " )
            resetSlider()
            shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                
                self.shipmentStartedButton.isEnabled = true
                if success == true{
                    
                    let delivertSheduleType:Int = UserDefaults.standard.integer(forKey: "driverScheduleType")
                    
                    if delivertSheduleType == 1 {
                        self.shipmentModel.orderStatus = 12
                        self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 12)
                        self.navigationController?.popViewController(animated: true)
                        
                    }else {
                        self.shipmentModel.orderStatus = 12
                        self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 12)
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let rideBookingController = storyboard.instantiateViewController(withIdentifier: "OnbookingController") as! OnBookingViewController;
                        rideBookingController.status = 2
                        rideBookingController.shipmentModel = self.shipmentModel
                        self.navigationController?.pushViewController(rideBookingController, animated: true)
                    }
                    
                   
                    
                }
                
            })
        }
    }
    func resetSlider()
    {
        self.leftSlider.reset()
    }
    
    @IBAction func slideCanceled(_ sender: SliderControl) {
        print("Cancelled")
    }
    
    
    @IBAction func sliderChanged(_ sender: SliderControl) {
        print("Changed")
    }
//
//    @IBAction func ShipmentStatusAction(_ sender: UIButton) {
//
//
//    }
//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nextScene = segue.destination as? ProviderInvoiceViewController
        {
            nextScene.shipmentModel = shipmentModel
        }else if let nextScene = segue.destination as? EditItemviewViewController
        {
            nextScene.shipmentModel = shipmentModel.productDetails[selectedIndex]
            nextScene.shipment = shipmentModel
            nextScene.selectedIndex = selectedIndex
        }
        
        else if let nav =  segue.destination as? UINavigationController
        {
            if let viewController: ChatVC = nav.viewControllers.first as! ChatVC? {
                
                viewController.bookingID = "\(shipmentModel.bookingId!)"
                viewController.custName = shipmentModel.customerName
                viewController.customerID = shipmentModel.customerId
                viewController.custImage = shipmentModel.customerPic
                
            }
            
            
        }
        
    }
    
}
