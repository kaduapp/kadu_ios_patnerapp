
//
//  UpdateQuantityPopup.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 04/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

protocol UpdateQuantityDelegate {
    func updateOrderDetails(updatedOrder: Shipment)
}

class UpdateQuantityPopup: UIView {
    
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productSubtitle: UILabel!
    @IBOutlet weak var productQuantityFeild: UITextField!
    @IBOutlet weak var perQuantityPrice: UILabel!
    private static var share: UpdateQuantityPopup? = nil
    var isShown:Bool = false
    var shipmentModel: Shipment!
    var index: Int = 0
    var delegate: UpdateQuantityDelegate?
    
    static var instance: UpdateQuantityPopup {
        
        if (share == nil) {
            share = Bundle(for: self).loadNibNamed("EditProductPopUP",
                                                   owner: nil,
                                                   options: nil)?.first as? UpdateQuantityPopup
        }
        return share!
    }
    
    /// Show Network
    func show() {
        
        if isShown == false {
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
            
            self.alpha = 0.0
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 1.0
            })
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    /// Hide
    func hide() {
        
        if isShown == true {
            isShown = false
            
            UIView.animate(withDuration: 0.5,
                           animations: {
                            UpdateQuantityPopup.share = nil
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
        }
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    //MARK: - update order details to server
    func updateProductDetails() {
        
//        var loadedCart = UserDefaults.standard.array(forKey: "myCart") as? [[String: Any]]
//        var seletedDict = loadedCart![index] as [String:Any]
//
//        seletedDict.updateValue("\(self.productQuantityFeild.text!)",forKey: "quantity")
//        loadedCart?.remove(at: index)
//        loadedCart?.insert(seletedDict, at: index)
//        UserDefaults.standard.set(loadedCart, forKey: "myCart")
//        print(loadedCart)
            self.delegate?.updateOrderDetails(updatedOrder: self.shipmentModel)
            self.hide()
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        {
            self .moveViewUp(activeView: productQuantityFeild, keyboardHeight: keyboardSize.height)
        }
    }
    
    func moveViewUp(activeView: UIView, keyboardHeight: CGFloat) {
        
        
    }
    
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveViewDown() {
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        
        })
    }
    
    //initialize the popup with product details
    func productDetailsInit(details: Shipment, ItemNo: Int) {
        
    
        self.shipmentModel = details
        self.index = ItemNo
        
//        let loadedCart = UserDefaults.standard.array(forKey: "myCart") as? [[String: Any]]
//
        
        if shipmentModel.productDetails.count == 0 || shipmentModel.productDetails == nil {
            
        }else {
            
            if let products = shipmentModel.productDetails[index] as? [String: Any] {
                
                if let itemName = products["itemName"] as? String {
                    productTitle.text = itemName
                }
                
                if let quant = products["quantity"] as? Int {
                    productQuantityFeild.text =  "\(quant)"
                }
                
                if let itemPrice = products["finalPrice"] as? NSNumber {
                    perQuantityPrice.text  = "x " + Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(itemPrice) , digits: 2))
                }
                
            }
        }
    }
    
    //Func called when only one product is available and deleted, to cancel the order
    func cancelOrder(){
        
        self.shipmentModel.productDetails.remove(at: self.index)
        self.delegate?.updateOrderDetails(updatedOrder: self.shipmentModel)
        self.hide()
    }
    
    @IBAction func updateOrderToServer(_ sender: UIButton) {
        
        
        
        
        var products:[String:Any] = [:]
        
        if shipmentModel.productDetails.count == 0 {
            
        }else {
            products = shipmentModel.productDetails[index]
        }
        let quantity = productQuantityFeild.text
        
        
        
        if quantity!.length > 0 {
            
            
            let newValue = products["quantity"] as! Int
            let enterValue = Int(productQuantityFeild.text!)
            
            if(newValue <= enterValue!) {
                
                
                 self.hide()
                let alertController = UIAlertController(title: "Message".localized, message: "You can't return same number and grater than quantity.", preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    
                
                }
                alertController.addAction(okAction)
                
               let presentView =  Helper.finalController()
                presentView.present(alertController, animated: true, completion: nil)
                
            }else {
                
                products["quantity"] = Int(quantity!)
                
                
                
                //Increasing/decrising the grandtotal locally
                if let newQuant = products["quantity"] as? Int {
                    
                    if shipmentModel.productDetails.count == 0 {
        
                    }else {
                        if let oldQuant = (shipmentModel.productDetails[index])["quantity"] as? Int {
                            
                            if newQuant == 0 {
                                
                                self.cancelOrder()
                            } else {
                                shipmentModel.productDetails[index] = products
                            }
                        }
                    }
                    
                    
                }
                 updateProductDetails()
            }
            
        }
        
       
    }
    
    
    
    @IBAction func deleteProduct(_ sender: UIButton) {
        
        UpdateQuantityPopup.share?.hide()
        
    }
//    {
//
//        var products:[String:Any] = [:]
//
//        if shipmentModel.productDetails.count == 0 {
//
//        }else {
//             products = shipmentModel.productDetails[index]
//        }
//        let produductsCount = shipmentModel.productDetails.count
//
//        if produductsCount == 1 {
//
//            self.cancelOrder()
//        } else {
//
//            if shipmentModel.productDetails.count == 0 {
//
//            }else {
//                if let oldQuant = (shipmentModel.productDetails[index])["quantity"] as? Int {
//                    if let itemPrice = products["finalPrice"] as? Double {
//                        if let grandTotal = shipmentModel.grandTotal as? Double {
//                            shipmentModel.grandTotal = grandTotal - Double(Double(oldQuant) * itemPrice)
//
//                        }
//                    }
//
//                }
//                 shipmentModel.productDetails.remove(at: index)
//            }
//            //reducing the grand total locally for the product i.e. deleted.
//
//            //removing the product from array of product details
//
//
//            //updating order to server
//            updateProductDetails()
//        }
//    }
    
    
    @IBAction func cancelOrderUpdate(_ sender: UIButton) {
        UpdateQuantityPopup.share?.hide()
    }
    
    @IBAction func gestureAction(_ sender: UITapGestureRecognizer) {
        self.endEditing(true)
    }
}
