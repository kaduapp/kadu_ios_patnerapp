//
//  LaundraDeliveryCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 4/29/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class LaundraDeliveryCell: UITableViewCell {

    @IBOutlet weak var deliveryLocation: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
