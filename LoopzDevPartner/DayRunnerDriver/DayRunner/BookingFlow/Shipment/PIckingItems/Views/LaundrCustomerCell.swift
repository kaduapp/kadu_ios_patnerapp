//
//  LaundrCustomerCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 4/29/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class LaundrCustomerCell: UITableViewCell {

    @IBOutlet weak var customerName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
