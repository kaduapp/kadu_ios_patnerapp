//
//  InvoiceTableViewController.swift
//  GoGoCargoDriver
//
//  Created by Vengababu Maparthi on 24/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation


extension InvoiceViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension InvoiceViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if servicesConfirmed == false {
            return 4;
        }else{
            return 6
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionType : ICSectionType = ICSectionType(rawValue: section)!
        
        switch sectionType {
            
        case .ICBillDetails:
            if invoiceDetails.appliedFee == 1{
                return 3
            }else{
                return 8
            }
            
        case .ICAddServices :
            return 2
            
        case .ICGrandTot :
            return 1
            
        case .ICrecepientData :
            
            if servicesConfirmed == false {
                return 1;
            }else{
                return 3
            }
        case .ICDocPhotos :
            return 3
            
        case .ICRatingCust :
            return 1
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionType : ICSectionType = ICSectionType(rawValue: indexPath.section)!
        let sectionCell : ICDividerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "divider") as! ICDividerTableViewCell
        
        let header: PBHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "title") as! PBHeaderTableViewCell
        
        switch sectionType {
        case .ICBillDetails:
            let cell: ICBillDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "billDetails") as! ICBillDetailsTableViewCell
            cell.selectionStyle = .none
            let rowType : ICRowType = ICRowType(rawValue: indexPath.row)!
            switch rowType {
            case .ICRowDefault:
                sectionCell.selectionStyle = .none
                return sectionCell
            case .ICRowSeperator:
                header.updateHeaderField(header:InvoiceStrings.billDetails)
                if bookingDict!.paidByWhom == 1 {
                    header.paidByReciever.text = "COLLECT CASH BY RECEIVER"  // if cash pay by receiver
                }
                return header
            
            case .ICRowSecond:
                if invoiceDetails.appliedFee == 1{
                    cell.updatingTheFields(keyString: InvoiceStrings.discount, valueString: Utility.currencySymbol + " " + invoiceDetails.disc)
                }else{
                    cell.updatingTheFields(keyString: InvoiceStrings.baseFee, valueString: Utility.currencySymbol + " " + invoiceDetails.baseFare)
                }
                return cell
                
            case .ICRowThird:
                
                cell.updatingTheFields(keyString: InvoiceStrings.distFee, valueString: Utility.currencySymbol + " " + invoiceDetails.distFare)
                
                return cell
            case .ICRowFourth:
                
                cell.updatingTheFields(keyString: InvoiceStrings.timeFee, valueString: Utility.currencySymbol + " " + invoiceDetails.timeFare)
                
                return cell
            case .ICRowFifth:
                cell.updatingTheFields(keyString: InvoiceStrings.discount, valueString: Utility.currencySymbol + " " + invoiceDetails.disc)
                
                return cell
            case .ICRowSixth:
                
                cell.updatingTheFields(keyString: InvoiceStrings.waitFee, valueString: Utility.currencySymbol + " " +  invoiceDetails.waitFee)
                
                return cell
            default:
                let subTtoal: SubtotalTVCell = tableView.dequeueReusableCell(withIdentifier: "subtotal") as! SubtotalTVCell
                
                subTtoal.updatingTheSubtotal(subtot:Utility.currencySymbol + " " + invoiceDetails.subTotal)
                
                return subTtoal
            }
            
        case .ICAddServices:
            let cell :ICServicesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "services") as! ICServicesTableViewCell
            cell.selectionStyle = .none
            switch indexPath.row {
            case 0:
                cell.serviceNameTF.text = InvoiceStrings.tollFee
                cell.serviceAmount.tag = 1
                
                if tollFee.isEmpty ||  tollFee == "0.00" {
                    cell.serviceAmount.placeholder = Utility.currencySymbol + " " + "0.00"
                }else{
                    cell.serviceAmount.text = Utility.currencySymbol + " " + tollFee
                }
                return cell
                
            default:
                cell.serviceNameTF.text = InvoiceStrings.handlingFee
                cell.serviceAmount.tag = 2
                
                if handlingFee.isEmpty ||  handlingFee == "0.00" {
                    cell.serviceAmount.placeholder = Utility.currencySymbol + " " + "0.00"
                }else{
                    cell.serviceAmount.text = Utility.currencySymbol + " " + handlingFee
                }
                return cell
            }
            
        case .ICGrandTot:
            
            let cell :ICGrandTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "grandTotal") as! ICGrandTotalTableViewCell
            if (tollFee.isEmpty ||  tollFee == "0.00") && (handlingFee.isEmpty ||  handlingFee == "0.00")  {
                cell.grandTotal.text = Utility.currencySymbol + " " + invoiceDetails.subTotal
            }else{
                let a:Float = Float(invoiceDetails.subTotal)!
                let b:Float = Float(tollFee)!
                let c:Float = Float(handlingFee)!
                let tot:Float = a + b + c
                cell.grandTotal.text = Utility.currencySymbol + " " + String(describing:tot)
            }
            
            cell.selectionStyle = .none
            return cell
            
        case .ICrecepientData:
            
            let rowType : ICRowType = ICRowType(rawValue: indexPath.row)!
            switch rowType {
            case .ICRowSecond:
                sectionCell.selectionStyle = .none
                return sectionCell
            case .ICRowDefault:
                if servicesConfirmed == false {
                    let cell: ICConfirmTableViewCell = tableView.dequeueReusableCell(withIdentifier: "confirm") as! ICConfirmTableViewCell
                    return cell
                }else{
                    header.updateHeaderField(header:InvoiceStrings.deliveredTo)
                    return header
                }
                
            default:
                let cell: ICReceiverTableViewCell = tableView.dequeueReusableCell(withIdentifier: "receiverDetails") as! ICReceiverTableViewCell
                if receiverNumber.isEmpty {
                    cell.phoneNumber.text = bookingDict?.dropPhone
                }else
                {
                    cell.phoneNumber.text = receiverNumber
                }
                if nameReceiver.isEmpty {
                    cell.receiverName.text = bookingDict?.dropCustName
                }else
                {
                    cell.receiverName.text = nameReceiver
                }
                
                cell.selectionStyle = .none
                return cell
            }
            
        case .ICDocPhotos:
            let rowType : ICRowType = ICRowType(rawValue: indexPath.row)!
            switch rowType {
            case .ICRowSecond:
                sectionCell.selectionStyle = .none
                return sectionCell
            case .ICRowDefault:
                header.updateHeaderField(header:InvoiceStrings.documents)
                return header
            default:
                let cell: ICAddPhotosTableViewCell = tableView.dequeueReusableCell(withIdentifier: "addPhotos") as! ICAddPhotosTableViewCell
                cell.reloadCollectionViewData(arrayOFImages:InvoiceViewController.myImages)
                cell.selectionStyle = .none
                return cell
            }
            
        case .ICRatingCust:
            
            let cell :ICRatingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "rating") as! ICRatingTableViewCell
            cell.selectionStyle = .none
            cell.updateRatingView()
            cell.ratingView.delegate = self
            return cell
        }
    }
}
