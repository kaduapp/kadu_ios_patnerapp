//
//  InvoiceTextFieldVC.swift
//  GoGoCargoDriver
//
//  Created by Vengababu Maparthi on 24/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation


extension InvoiceViewController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //**** if service confirmed not able to edit those fields
        if servicesConfirmed == true {
            if textField.tag == 1 || textField.tag == 2 {
                return false
            }else{
                return true
            }
        }else{
            switch textField.tag {
            case 1:
                textField.text = ""
                break
            case 2:
                textField.text = ""
                break
            default:
                break
            }
            return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
    }
    
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {  //delegate method
        switch textField.tag {
        case 1:
            if !(textField.text?.isEmpty)!{
                tollFee = textField.text!
            }
            break
        case 2:
            if !(textField.text?.isEmpty)!{
                handlingFee = textField.text!
            }
            break
        case 22:
            nameReceiver = textField.text!
            break
        case 33:
            receiverNumber =  textField.text!
            break
        default:
            self.view.endEditing(true)
        }
        self.invoiceTableView.reloadData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        switch textField.tag {
        case 1:
            textField.text = ""
            if !(textField.text?.isEmpty)!{
                tollFee =  textField.text!
            }else{
                tollFee = "0.00"
            }
            break
        case 2:
            textField.text = ""
            if !(textField.text?.isEmpty)!{
                handlingFee =  textField.text!
            }else{
                handlingFee = "0.00"
            }
            break
        case 22:
            nameReceiver = textField.text!
            break
        case 33:
            receiverNumber = textField.text!
            break
        default:
            self.view.endEditing(true)
        }
        self.view.endEditing(true)
        self.invoiceTableView.reloadData()
        return true
    }
}

