//
//  InvoiceButtonActionVC.swift
//  GoGoCargoDriver
//
//  Created by Vengababu Maparthi on 24/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension InvoiceViewController{
    
    
    //****** submmitting invocie action
    @IBAction func submitTheInvoice(_ sender: Any) {
        self.updateTheJobCompletionStatus()
        
    }
    
    //***** add gesture to view to hide keyboard
    @IBAction func gestureAction(_ sender: Any) {
        view.endEditing(true)
    }
    
    //****** way to live chat if any help
    @IBAction func needHelp(_ sender: Any) {
        self.performSegue(withIdentifier: "fromInvoice", sender:  nil)
    }
    
    //**** back to home vc action
    @IBAction func backToRootVC(_ sender: Any) {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    //**** add document photos action
    @IBAction func takeDocPhotos(_ sender: Any) {
        selectImage()
    }
    
    
    /*****************************************************************/
    //MARK: - UIImage Picker
    /*****************************************************************/
    
    func selectImage() {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: "Please Select Image".localized,
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel".localized,
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: "Gallery".localized,
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: "Camera".localized,
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //MARK: - Choose gallery for image
    func chooseFromPhotoGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    //MARK: - Choose camera for image
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.camera;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
    //********* confirming the entered services fees********//
    @IBAction func confirmServiceFee(_ sender: Any) {
        self.servicesConfirmed = true
        invoiceTableView.reloadData()
    }
    
    
    //*** update complete booking status to server***// 10: Completions job and submitting invoice
    func updateTheJobCompletionStatus(){
        let ud = UserDefaults.standard
        if ud.bool(forKey: "signatureMade") {
            self.uploadSignatureAndDocimgToAmazon()
            if bookingDict!.paidByWhom == 1 {
                CashCollection.instance.grandAmount = self.totalbookingAmount
                CashCollection.instance.show()
                CashCollection.instance.delegate = self
            }else{
                self.updateBookingStatus(status: 10)
            }
            
        }else{
            Helper.alertVC(errMSG: InvoiceStrings.signature)
        }
    }
    
    //******uploading the signature and job photos(if any) to amazon
    func uploadSignatureAndDocimgToAmazon(){
        temp = [UploadImage]()
        var imagesCount:Int = 0
        for images in InvoiceViewController.myImages {
            imagesCount =  imagesCount + 1
            var url = String()
            url = AMAZONUPLOAD.DOCUMENTS + String(describing:bookingDict!.bookingId) + "_" + String(imagesCount) + ".png"
            temp.append(UploadImage.init(image: images, path: url))
        }
        
        let indexPath = IndexPath(row: 1, section: ICSectionType.ICrecepientData.rawValue)
        if let cell: ICReceiverTableViewCell = invoiceTableView.cellForRow(at: indexPath) as? ICReceiverTableViewCell {
            var url = String()
            url = AMAZONUPLOAD.SIGNATURE + String(describing:bookingDict!.bookingId) + ".png"
            let image = self.image(from: cell.signatureView)
            temp.append(UploadImage.init(image: image, path: url))
        }
        
        let upMoadel = UploadImageModel.shared
        upMoadel.uploadImages = temp
        upMoadel.start()
        
    }
    
    //*****Convert the view to image*****//
    func image(from view: SignatureView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    
    //MARK: - update booking status API
    func updateBookingStatus(status:Any) {
        var strUrl = String()
        
        var imagesCount:Int = 0
        
        for _ in InvoiceViewController.myImages {
            imagesCount =  imagesCount + 1
            strUrl = Utility.amazonUrl + AMAZONUPLOAD.DOCUMENTS + String(describing:bookingDict!.bookingId) + "_" + String(imagesCount) + ".png"
            if self.documentImages.isEmpty{
                self.documentImages = strUrl
            }else{
                self.documentImages = self.documentImages + "," + strUrl
            }
        }
        let signatureURL = Utility.amazonUrl + AMAZONUPLOAD.SIGNATURE + String(describing:bookingDict!.bookingId) + ".png"
        
        let indexPath = IndexPath(row: 1, section: ICSectionType.ICrecepientData.rawValue)
        let cell: ICReceiverTableViewCell = invoiceTableView.cellForRow(at: indexPath) as! ICReceiverTableViewCell
        
        
        if tollFee.isEmpty {
            tollFee = "0"
        }
        
        Helper.showPI(message:"Loading..")
        
        
        let shipmentComplete = InvoiceModel.init(updateInvoice: status, updateInvoiceBookingID: bookingDict!.bookingId, updateInvoiceSignatureURL: signatureURL, updateInvoiceDocumentImage: documentImages, udpateInvoiceTollFee: tollFee, updateInvoiceHandlingFee: handlingFee, updateInvoiceReceiverName: cell.receiverName.text!, updateInvoiceReceiverPhone: cell.phoneNumber.text!, updateInvoiceRating: String(ratingBycust), updateInvoiceLatitude: String(lastLat), updateInvoiceLongitude: String(lastLat))
        
        invoiceMdl.updateBookingStatus(params:shipmentComplete, completionHandler: { success in //@locate InvoiceModel
            if success{
                self.deletingTheDataFromFromParticularID() // @locate InvoiceviewController
                _ = self.navigationController?.popToRootViewController(animated: true)
            }else{
                
            }
        })
    }
}

extension InvoiceViewController:CashCollectionDelegate{
    func totalCashCollected() {
        self.updateBookingStatus(status: 10)
    }
}
