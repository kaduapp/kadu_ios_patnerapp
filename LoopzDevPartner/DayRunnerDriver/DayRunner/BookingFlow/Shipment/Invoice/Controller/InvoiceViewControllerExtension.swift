
//
//  InvoiceViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 24/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import FloatRatingView

enum ICSectionType : Int {
    case ICBillDetails   = 0
    case ICAddServices   = 1
    case ICGrandTot      = 2
    case ICrecepientData = 3
    case ICDocPhotos     = 4
    case ICRatingCust    = 5
}

enum ICRowType : Int {
    case ICRowDefault   = 0
    case ICRowSeperator = 1
    case ICRowSecond    = 2
    case ICRowThird     = 3
    case ICRowFourth    = 4
    case ICRowFifth     = 5
    case ICRowSixth     = 6
    case ICRowSeventh   = 7
}



class InvoiceViewController: UIViewController ,UINavigationControllerDelegate {
    
    @IBOutlet var invoiceTableView: UITableView!
    var noOfServices: Int = 0
    static var myImages = [UIImage]()
    var SelectedIndex:Int = 0
    let amazonWrapper:AmazonWrapper = AmazonWrapper.sharedInstance()
    var documentImages = String()
    var ratingBycust:Float = 0
    

    
    var receiverNumber = String()
    var nameReceiver = String()
    var tollFee = "0.00"
    var handlingFee = "0.00"
    
    let manager = CBLManager.sharedInstance()
    let location = LocationManager.sharedInstance
    var locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
    
    var temp = [UploadImage]()
    var servicesConfirmed = false
    
    var lastLat:Double  = 0.00
    var lastLog:Double = 0.00
    
    @IBOutlet var bookingID: UILabel!
    var bookingDict:Home?
    var invoiceDetails = BookingModel()
    var invoiceMdl = InvoiceModel()
    var totalbookingAmount = String()
    
    @IBOutlet weak var submitButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        updateViewAndPubnubStr()
        if bookingDict!.paidByWhom == 1 {// payment should be taken from reciever
            submitButton.setTitle("COLLECT CASH", for: .normal)  // if cash pay by receiver
        }
        // Do any additional setup after loading the view.
    }
    
    //Making initial ratingBycust default 4, assigning the bid to bookingID label
    func updateViewAndPubnubStr(){
        InvoiceViewController.myImages = [UIImage]()
        invoiceTableView.estimatedRowHeight = 10
        invoiceTableView.rowHeight = UITableView.automaticDimension
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
        ratingBycust = 4.0
        bookingID.text = "BID:" + String(describing:bookingDict!.bookingId)
        let ud = UserDefaults.standard
        ud.set(false, forKey:"signatureMade")
        ud.synchronize()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
//MARK: - Deleting the couch db of particular booking id if any
    func deletingTheDataFromFromParticularID(){
        let  database  = try! manager.databaseNamed("livedayrunner")
        let document = database.document(withID: String(describing:bookingDict!.bookingId))
        
        do
        {
            try document?.delete()
        }
        catch
        {
            print("Error when deleting the guest database during migrating guest data : \(error.localizedDescription)")
        }
    }

}

//MARK: - uiiimage picker delegate
extension InvoiceViewController : UIImagePickerControllerDelegate{
      func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
          if let image = info[.originalImage] as? UIImage{
        InvoiceViewController.myImages.append(Helper.resizeImage(image: image, newWidth: 200)!)
        invoiceTableView.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    }
}

//MARK: - Rating view delegate

extension InvoiceViewController: FloatRatingViewDelegate{
    /**
     Returns the rating value when touch events end
     */
    public func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        ratingBycust = rating
    }
}


