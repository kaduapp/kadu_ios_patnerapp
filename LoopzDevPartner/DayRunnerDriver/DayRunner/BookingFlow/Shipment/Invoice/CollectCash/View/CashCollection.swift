//
//  CashCollection.swift
//  RunnerDev
//
//  Created by Vengababu Maparthi on 12/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
protocol CashCollectionDelegate:class
{
    func totalCashCollected()
}


class CashCollection: UIView {
    
    open weak var delegate: CashCollectionDelegate?

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var grandTotal: UILabel!
    @IBOutlet weak var collectedAmount: UITextField!
    var grandAmount = String()
    var isShown:Bool = false
    private static var share: CashCollection? = nil
    
    @IBOutlet weak var instructionLabel: UILabel!
    
    static var instance: CashCollection {
        
        if (share == nil) {
            share = Bundle(for: self).loadNibNamed("CashCollect",
                                                   owner: nil,
                                                   options: nil)?.first as? CashCollection
        }
        return share!
    }
    
    
    @IBAction func VerifiesTheAmountAction(_ sender: Any) {
        if grandAmount == collectedAmount.text{
            delegate?.totalCashCollected()
            self.hide()
        }else{
         self.instructionLabel.isHidden = false
           self.endEditing(true)
        }
    }
    
    
    
    @IBAction func gestureAction(_ sender: Any) {
        self.hide()
        self.endEditing(true)
    }
    
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            self .moveViewUp(activeView: collectedAmount, keyboardHeight: keyboardSize.height)
        }
    }
    
    
    /// Show Network
    func show() {
        self.instructionLabel.isHidden = true
        collectedAmount.text = ""
        grandTotal.text = Utility.currencySymbol + grandAmount
        if isShown == false {
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
            
            self.alpha = 0.0
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 1.0
            })
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    /// Hide
    func hide() {
        if isShown == true {
            isShown = false
            
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
        }
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    func moveViewUp(activeView: UIView, keyboardHeight: CGFloat) {
        
        
    }
    
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveViewDown() {
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        
        })
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.instructionLabel.isHidden = true
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        self.endEditing(true)
        return true
    }
}
