//
//  ICRatingTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 24/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import FloatRatingView

class ICRatingTableViewCell: UITableViewCell {

    @IBOutlet var ratingView: FloatRatingView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateRatingView(){
       ratingView.fullImage = UIImage.init(named: "invoice_green_star_icon")
       ratingView.emptyImage = UIImage.init(named: "invoice_grey_star_icon")
       ratingView.maxRating = 5
       ratingView.minRating = 1
       ratingView.rating = 4
       ratingView.editable = true
       ratingView.halfRatings = true
       ratingView.floatRatings = true
    }
}
