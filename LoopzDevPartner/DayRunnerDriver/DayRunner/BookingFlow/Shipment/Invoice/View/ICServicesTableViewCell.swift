//
//  ICServicesTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 24/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ICServicesTableViewCell: UITableViewCell {

    @IBOutlet var labelOverTF: UILabel!
    @IBOutlet var serviceAmount: UITextField!
    @IBOutlet var serviceNameTF: UITextField!
    @IBOutlet var removeService: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        labelOverTF.layer.borderWidth = 1.3;
        labelOverTF.layer.borderColor = Helper .UIColorFromRGB(rgbValue: 0xE5E6E7).cgColor
        // Initialization code7
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
