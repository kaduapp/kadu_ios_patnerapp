//
//  InvoiceModel.swift
//  GoGoCargoDriver
//
//  Created by Vengababu Maparthi on 24/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift

//Invoice model class
class InvoiceModel:NSObject {

    var updateInvoiceStatus: Any!
    var updateInvoiceBookingID: Any!
    var updateInvoiceSignatureURL: String!
    var updateInvoiceDocumentImage: String!
    var udpateInvoiceTollFee: String!
    var updateInvoiceHandlingFee: String!
    var updateInvoiceReceiverName: String!
    var updateInvoiceReceiverPhone: String!
    var updateInvoiceRating: String!
    var updateInvoiceLatitude: String!
    var updateInvoiceLongitude: String!
    var disposeBag = DisposeBag()
    override init()
    {
        super.init()
    }
    
    init(updateInvoice updateInvoiceStatus: Any, updateInvoiceBookingID: Any, updateInvoiceSignatureURL: String, updateInvoiceDocumentImage: String, udpateInvoiceTollFee: String, updateInvoiceHandlingFee: String, updateInvoiceReceiverName: String, updateInvoiceReceiverPhone: String, updateInvoiceRating: String, updateInvoiceLatitude: String, updateInvoiceLongitude: String)
    {
        self.updateInvoiceStatus = updateInvoiceStatus
        self.updateInvoiceBookingID = updateInvoiceBookingID
        self.updateInvoiceSignatureURL = updateInvoiceSignatureURL
        self.updateInvoiceDocumentImage = updateInvoiceDocumentImage
        self.udpateInvoiceTollFee = udpateInvoiceTollFee
        self.updateInvoiceHandlingFee = updateInvoiceHandlingFee
        self.updateInvoiceReceiverName = updateInvoiceReceiverName
        self.updateInvoiceReceiverPhone = updateInvoiceReceiverPhone
        self.updateInvoiceRating = updateInvoiceRating
        self.updateInvoiceLatitude = updateInvoiceLatitude
        self.updateInvoiceLongitude = updateInvoiceLongitude
    }
    
    //MARK: - updatebooking status API 10:Completing booking
    func updateBookingStatus(params:InvoiceModel ,completionHandler:@escaping (Bool) -> ())  {
        let bookingComplete =  ShipmentBookingApi()
        bookingComplete.completeBooking(shipmentModel: params)
        bookingComplete.subject_response.subscribe(onNext: { (response) in
            if response.isEmpty == false {
                let flag:Int = response["errFlag"] as! Int
                
                if flag == 0{
                    completionHandler(true)
                }else{
                    completionHandler(false)
                    Helper.hidePI()
                    Helper.alertVC(errMSG: response["errMsg"] as! String)
                }
            } else {
            }

        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
    }
}
