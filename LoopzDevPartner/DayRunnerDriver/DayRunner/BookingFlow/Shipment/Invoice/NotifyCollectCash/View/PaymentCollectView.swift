//
//  PaymentCollectView.swift
//  RunnerDev
//
//  Created by Vengababu Maparthi on 12/09/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class PaymentCollectView: UIView {
    
    private static var share: PaymentCollectView? = nil
    var isShown:Bool = false

    static var instance: PaymentCollectView {
        
        if (share == nil) {
            share = Bundle(for: self).loadNibNamed("PaymentCollect",
                                                   owner: nil,
                                                   options: nil)?.first as? PaymentCollectView
        }
        return share!
    }


    @IBAction func okAction(_ sender: Any) {
        self.hide()
    }
    
    /// Show Network
    func show() {

        if isShown == false {
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
            
            self.alpha = 0.0
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 1.0
            })
        }
        
        
    }
    
    /// Hide
    func hide() {
        if isShown == true {
            isShown = false
            
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
        }
 
    }
    
}
