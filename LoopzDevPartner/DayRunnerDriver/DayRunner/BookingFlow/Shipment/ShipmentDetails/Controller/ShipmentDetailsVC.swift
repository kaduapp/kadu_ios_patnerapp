//
//  ShipmentDetailsVC.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 24/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

enum SDSectionType : Int {
    case SDCustDetails = 0
    case SDAddress = 1
    case SDRecepient = 2
    case SDDetails = 3
}

enum SDRowType : Int {
    case SDRowDefault = 0
    case SDRowSeperator = 1
}


class ShipmentDetailsVC: UIViewController {
  
    @IBOutlet var shipmentDetailsTV: UITableView!
    
    var SelectedIndex:Int = 0
    var bookingDict:Home?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        shipmentDetailsTV.estimatedRowHeight = 10
        shipmentDetailsTV.rowHeight = UITableView.automaticDimension
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        shipmentDetailsTV.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backToVC(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
     func showTheImageViewPop(gallery: INSPhotosViewController){
        self.present(gallery, animated: true, completion: nil)
    }
}

extension ShipmentDetailsVC : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionType : SDSectionType = SDSectionType(rawValue: section)!
        
        switch sectionType {
            
        case .SDCustDetails:
            return 2
            
        case .SDAddress :
            return 2
            
        case .SDRecepient :
            return 2
            
        case .SDDetails :
            return 1
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionType : SDSectionType = SDSectionType(rawValue: indexPath.section)!
        let sectionCell : SDDividerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "divider") as! SDDividerTableViewCell
        
        let rowType : SDRowType = SDRowType(rawValue: indexPath.row)!
            
            switch sectionType {
            case .SDCustDetails:
                switch rowType {
                case .SDRowSeperator:
                    return sectionCell
                default:
                    let cell: ShipmentDetailsHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "header") as! ShipmentDetailsHeaderTableViewCell
                    cell.bookingID.text = String(describing:bookingDict?.bookingId)
                    cell.amount.text = Utility.currencySymbol + String((bookingDict?.bookingAmt)!) 
                    return cell
                }
                
            case .SDAddress:
                
                switch rowType {
                case .SDRowSeperator:
                    return sectionCell
                    
                default:
                    let cell :SDAddressTableViewCell = tableView.dequeueReusableCell(withIdentifier: "address") as! SDAddressTableViewCell
                    cell.pickAddress.text = bookingDict?.pickAddress
                    cell.dropAddress.text = bookingDict?.dropAddress
                    cell.pickTime.text = Helper.changeDateFormatForHome((bookingDict?.bookingDateNtime)!)
                    cell.dropTime.text = Helper.changeDateFormatForHome((bookingDict?.timeLeftToPick)!)
                    return cell
                }
                
            case .SDRecepient:
                
                switch rowType {
                case .SDRowSeperator:
                    return sectionCell
                    
                default:
                    let cell :SDSenderReceiverTableViewCell = tableView.dequeueReusableCell(withIdentifier: "recepientDetails") as! SDSenderReceiverTableViewCell
                    
                    cell.custName.text = bookingDict?.dropCustName
                    cell.custPhone.text = bookingDict?.pickPhone
                    cell.receiverName.text = bookingDict?.pickCustName
                    cell.receiverPhone.text = bookingDict?.dropPhone
                    
                    return cell
                }
                
            case .SDDetails:
                
                switch rowType {
                    
                case .SDRowSeperator:
                    return sectionCell
                    
                default:
                    let cell :SDTableViewCell = tableView.dequeueReusableCell(withIdentifier: "shipmentDetails") as! SDTableViewCell
                    cell.goodType.text = bookingDict?.goodType
//                    if bookingDict?.quantity == ""{
//                        cell.quantity.text = shipmentDetails.quant
//                    }else{
//                        cell.quantity.text = bookingDict?.quantity
//                    }
                    if bookingDict?.notes == "" {
                        cell.notes.text = shipmentDetails.noNotes
                    }else{
                        cell.notes.text = bookingDict?.notes
                    }
                    cell.delegate = self
                   cell.updatedTheJobImages(photo:(bookingDict?.shipImage)!)
                    return cell
                }
            }
    }
}

extension ShipmentDetailsVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension ShipmentDetailsVC : showTheImagePopupDelegate{
    func showImageGalley(gallery: INSPhotosViewController) {
        self.present(gallery, animated: true, completion: nil)
    }
}
