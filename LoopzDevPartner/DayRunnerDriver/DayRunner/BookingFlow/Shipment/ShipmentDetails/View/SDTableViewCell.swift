//
//  SDTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 24/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

protocol showTheImagePopupDelegate:class
{
    func showImageGalley(gallery: INSPhotosViewController)
}


class SDTableViewCell: UITableViewCell {
    open weak var delegate: showTheImagePopupDelegate?
    
    @IBOutlet var noPhotosLabel: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var notes: UILabel!
    @IBOutlet var quantity: UILabel!
    @IBOutlet var goodType: UILabel!
    var jobPhoto = [String]()
    var useCustomOverlay = false
    var photos: [INSPhotoViewable] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updatedTheJobImages(photo:[String]){
   
        let indexPathForFirstRow = IndexPath(row: 0, section: 0)
        collectionView.selectItem(at: indexPathForFirstRow, animated: false, scrollPosition: [])
        collectionView?.deselectItem(at: indexPathForFirstRow, animated: false)
        
        if photo.count == 0 {
            noPhotosLabel.isHidden = false
        }else{
            noPhotosLabel.isHidden = true
        }
        
        for dict in photo {
            photos.append(INSPhoto(imageURL: URL(string: dict), thumbnailImageURL: URL(string:dict)))
        }
        //  jobPhoto = photo
        collectionView.reloadData()
    }
}


// MARK: - CollectionView datasource method
extension SDTableViewCell:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shipments", for: indexPath as IndexPath) as! ShipmentCollectionViewCell
        
        cell.populateWithPhoto(photos[(indexPath as NSIndexPath).row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if photos.count == 0{
            return 0
        }else{
            return photos.count
        }
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}


// MARK: - CollectionView delegate method
extension SDTableViewCell:UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ShipmentCollectionViewCell
        let currentPhoto = photos[(indexPath as NSIndexPath).row]
        let galleryPreview = INSPhotosViewController(photos: photos, initialPhoto: currentPhoto, referenceView: cell)
        if useCustomOverlay {
            galleryPreview.overlayView = CustomOverlayView(frame: CGRect.zero)
        }
        
        galleryPreview.referenceViewForPhotoWhenDismissingHandler = { [weak self] photo in
            if let index = self?.photos.index(where: {$0 === photo}) {
                let indexPath = IndexPath(item: index, section: 0)
                return collectionView.cellForItem(at: indexPath) as? ShipmentCollectionViewCell
            }
            return nil
        }
        
        self.delegate?.showImageGalley(gallery: galleryPreview)
        
    }
    
}



