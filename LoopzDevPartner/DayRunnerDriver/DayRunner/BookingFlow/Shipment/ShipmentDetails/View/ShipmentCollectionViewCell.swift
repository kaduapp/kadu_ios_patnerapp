//
//  ShipmentCollectionViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 02/06/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ShipmentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var acitivityIndicator: UIActivityIndicatorView!
    @IBOutlet var image: UIImageView!
    
    func populateWithPhoto(_ photo: INSPhotoViewable) {
        photo.loadThumbnailImageWithCompletionHandler { [weak photo] (image, error) in
            if let image = image {
                if let photo = photo as? INSPhoto {
                    photo.thumbnailImage = image
                }
                self.image.image = image
            }
        }
    }
}
