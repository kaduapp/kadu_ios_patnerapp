//
//  ProviderTVExViewController.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import FloatRatingView

enum ProviderInvoiceTableViewSection: Int {
    case Amount = 0
    case Signature = 1
    case Rating = 2
    case LaundraAmount = 3
    case PickupAddress = 4
    case HeaderItem = 5
    case LaundraDetails = 6
    case WeightDetails = 7
}

extension ProviderInvoiceViewController: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows: ProviderInvoiceTableViewSection = ProviderInvoiceTableViewSection.init(rawValue: section)!
        switch numberOfRows {
            
        case .Amount:
            return 2
            
        case .Signature:
            return 2
            
        case .Rating:
            return 1
            
        case .LaundraAmount:
            return 1
            
        case .PickupAddress :
            return 1
            
        case .HeaderItem :
            return 1
            
        case .LaundraDetails :
             let delivertSheduleType:Int = UserDefaults.standard.integer(forKey: "driverScheduleType")
             if(delivertSheduleType == 1) {
                return 1
             }
             return shipmentModel.productDetails.count
            
        case .WeightDetails :
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section: ProviderInvoiceTableViewSection = ProviderInvoiceTableViewSection.init(rawValue: indexPath.section)!
        switch section {
            
        case .Amount:
            switch indexPath.row{
            case 0:
                let cell: ProviderInvoiceAmountTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProviderInvoiceAmountTableViewCell.self), for: indexPath) as! ProviderInvoiceAmountTableViewCell
                let wallet:Double = shipmentModel.grandTotal - shipmentModel.cashCollect
                cell.walletAmountLabel.text = "Wallet : " + Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(wallet) , digits: 2))
                 cell.cashAmountlabel.text = "Cash : " + Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.cashCollect) , digits: 2))
                cell.Totalamount.text = Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.grandTotal!) , digits: 2))
                return cell
            case 1:
                let cell: ICDividerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "divider", for: indexPath) as! ICDividerTableViewCell
                
                cell.upperShadowView.isHidden = true
                return cell

            default:
                return tableView.dequeueReusableCell(withIdentifier: "divider", for: indexPath)
            }
            
        case .Signature:
            switch indexPath.row{
            case 0:
                let cell: ProviderInvoiceSignatureTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProviderInvoiceSignatureTableViewCell.self), for: indexPath) as! ProviderInvoiceSignatureTableViewCell
                return cell
                
            default:
                return tableView.dequeueReusableCell(withIdentifier: "divider", for: indexPath)
            }
            
        case .Rating:
            let cell: ProviderInvoiceRatingTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProviderInvoiceRatingTableViewCell.self), for: indexPath) as! ProviderInvoiceRatingTableViewCell
            cell.selectionStyle = .none
            cell.updateRatingView()
            cell.ratingView.delegate = self 
            return cell
            
            
            
        case .LaundraAmount:
            
            let cell: AmountTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: AmountTableViewCell.self), for: indexPath) as! AmountTableViewCell
            let num = shipmentModel.bookingId
            cell.bookingId.text = "Id: \(num!)"
            let storeType:Int = UserDefaults.standard.integer(forKey: "storeAccountType")
            if storeType == 2 {
                cell.timeLabel.text = shipmentModel.bookingTime
            }else {
                cell.timeLabel.text = shipmentModel.bookingTime
            }
            var feeValue = 0
            if let fee = shipmentModel.deliverySplits["deliveryPriceFromCustomerToLaundromat"] as? NSNumber {
                feeValue = Int(fee)
            }
            
            cell.amountLabel.text = Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(feeValue) , digits: 2))
            
            let paymentType = shipmentModel.paymentType
            let payByWallet = shipmentModel.payByWallet
            
            if payByWallet == 0 {
                if paymentType == 1 {
                    cell.paymentLabel.text = "Card"
                }else if paymentType == 2 {
                    cell.paymentLabel.text = "Cash"
                }else {
                    cell.paymentLabel.text = "Wallet"
                }
            }else {
                if paymentType == 1 {
                    cell.paymentLabel.text = "Card + Wallet"
                }else if paymentType == 2 {
                    cell.paymentLabel.text = "Cash + Wallet"
                }else {
                    cell.paymentLabel.text = "Wallet"
                }
            }
            return cell
            
        case .PickupAddress:
            let cell: PickupDeliveryDetailsCell = tableView.dequeueReusableCell(withIdentifier: String(describing: PickupDeliveryDetailsCell.self), for: indexPath) as! PickupDeliveryDetailsCell
            cell.customerName.text = shipmentModel.customerName
            cell.storeName.text = shipmentModel.pickupStore
            cell.customerAddress.text = shipmentModel.deliveryAddress
            cell.storeAddress.text = shipmentModel.pickupAddress
            return cell
            
        case .HeaderItem:
            let cell: HearderTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: HearderTableViewCell.self), for: indexPath) as! HearderTableViewCell
            return cell
            
        case .LaundraDetails:
            let cell: DetailsLaundraTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: DetailsLaundraTableViewCell.self), for: indexPath) as! DetailsLaundraTableViewCell
            let products = shipmentModel.productDetails[indexPath.row]
            if let itemName = products["itemName"] as? String {
                
                var quantity = 0
                if let quant = products["quantity"] as? Int {
                    quantity = quant
                }
                
                var allAddOnsArray:String = ""
                if let addOnsArray = products["addOns"] as? [[String:Any]] {
                    
                    for eachAddOns in addOnsArray {
                        if let addOnGroup = eachAddOns["addOnGroup"] as? [[String:Any]] {
                            for eachAddOnGroup in addOnGroup {
                                if let addOnName = eachAddOnGroup["name"] as? String {
                                    
                                    allAddOnsArray = allAddOnsArray + addOnName + ","
                                }
                            }
                        }
                    }
                }
                cell.productQuantity.text = itemName + " x " + String(quantity)
            }
            return cell
            
            
        case .WeightDetails:
            let cell: weightLbTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: weightLbTableViewCell.self), for: indexPath) as! weightLbTableViewCell
            cell.weightLabel.text = shipmentModel.weightStr + " Kgs" + " weight"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if shipmentModel.storeType == 5 && !shipmentModel.bookingTypeLaundra{
            
            let numberOfRows: ProviderInvoiceTableViewSection = ProviderInvoiceTableViewSection.init(rawValue: indexPath.section)!
            switch numberOfRows {
                
            case .Amount:
                switch indexPath.row {
                case 0:
                    return 0
                default:
                    return 0
                }
                
            case .Signature:
                switch indexPath.row {
                case 0:
                    return 0
                default:
                    return 0
                }
                
            case .Rating:
                return 0
            case .LaundraAmount:
                return 170
            case .PickupAddress:
                return 220
            case .HeaderItem:
                return 55
            case .LaundraDetails:
                return 30
            case .WeightDetails:
                return 45
            }
        }else {
            let numberOfRows: ProviderInvoiceTableViewSection = ProviderInvoiceTableViewSection.init(rawValue: indexPath.section)!
            switch numberOfRows {
                
            case .Amount:
                switch indexPath.row {
                case 0:
                    return 180
                default:
                    return 15
                }
                
            case .Signature:
                switch indexPath.row {
                case 0:
                    return 151
                default:
                    return 15
                }
                
            case .Rating:
                return 116
            case .LaundraAmount:
                return 0
            case .PickupAddress:
                return 0
            case .HeaderItem:
                return 0
            case .LaundraDetails:
                return 0
            case .WeightDetails:
                return 0
            }
        }
    
   
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section: ProviderInvoiceTableViewSection = ProviderInvoiceTableViewSection.init(rawValue: Int(indexPath.section))!
        
        if section == .LaundraDetails {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let rideBookingController = storyboard.instantiateViewController(withIdentifier: "NewProductVC") as! NewProductVC;
            rideBookingController.arrayOfImages = shipmentModel.productDetails[indexPath.row]
            self.present(rideBookingController, animated: true, completion: nil)
        }
    }
}


extension ProviderInvoiceViewController: FloatRatingViewDelegate {
    /**
     Returns the rating value when touch events end
     */
     func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        self.ratingBycust = rating
    }
}

