//
//  ProviderInvoiceViewController.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import JaneSliderControl
class ProviderInvoiceViewController: UIViewController {

    @IBOutlet weak var submitInvoiceButton: UIButton!
    
    @IBOutlet weak var deliveryCompletedLabel: UILabel!
    @IBOutlet weak var jobId: UILabel!
      @IBOutlet weak var leftSlider: SliderControl!
    @IBOutlet weak var invoiceTableView: UITableView!
    
    var ratingBycust:Float = 0
    var shipmentModel: Shipment!
    var temp = [UploadImage]()
    static var myImages = [UIImage]()
    
    
    override func viewDidLoad() {
        deliveryCompletedLabel.text = "DELIVERY".localized
        leftSlider.sliderText = "Done".localized
        if shipmentModel.storeType == 5 && !shipmentModel.bookingTypeLaundra {
            leftSlider.sliderText = "Done".localized
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let ud = UserDefaults.standard
        ud.set(false, forKey: "signatureMade")
        let num = shipmentModel.bookingId
        self.ratingBycust = 4.0 //giving a default value
        jobId.text = "Id: \(num!)"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
    }
    
}



