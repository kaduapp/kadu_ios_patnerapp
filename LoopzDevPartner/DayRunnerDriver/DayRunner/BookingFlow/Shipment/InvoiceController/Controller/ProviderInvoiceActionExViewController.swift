//
//  ProviderInvoiceActionExViewController.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 19/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import UIKit
import JaneSliderControl

extension ProviderInvoiceViewController{
    
    @IBAction func sliderFineshed(_ sender: SliderControl) {
        
        if shipmentModel.storeType == 5 && !shipmentModel.bookingTypeLaundra {
            
            performSegue(withIdentifier: "UnwindSegue", sender: self)
            
        }else if shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra {
            
            
            submitInvoiceButton.isEnabled = false
            self.updateTheJobCompletionStatus()
            
        }else {
            
            
            submitInvoiceButton.isEnabled = false
            self.updateTheJobCompletionStatus()
        }
    }
    
    
    func resetSlider()
    {
        self.leftSlider.reset()
    }
    
    @IBAction func slideCanceled(_ sender: SliderControl) {
        print("Cancelled")
    }
    
    
    @IBAction func sliderChanged(_ sender: SliderControl) {
        print("Changed")
    }
    
//    @IBAction func actionButtonSubmitInvoice(_ sender: UIButton) {
//
//    }
    
    //*** update complete booking status to server***// 15: Completions job and submitting invoice
    func updateTheJobCompletionStatus(){
        let ud = UserDefaults.standard
        if ud.bool(forKey: "signatureMade") {
            self.uploadSignatureAndDocimgToAmazon()
            //            if bookingDict!.paidByWhom == 1 {
            ////                CashCollection.instance.grandAmount = self.totalbookingAmount
            //                CashCollection.instance.show()
            ////                CashCollection.instance.delegate = self
            //            }else{
            self.updateBookingStatus()
            //            }
            
        }else{
            submitInvoiceButton.isEnabled = true
            Helper.alertVC(errMSG: InvoiceStrings.signature)
            resetSlider()
        }
    }
    
    //******uploading the signature and job photos(if any) to amazon
    func uploadSignatureAndDocimgToAmazon(){
        temp = [UploadImage]()
        
        let indexPath = IndexPath(row: 0, section: ProviderInvoiceTableViewSection.Signature.rawValue)
        if let cell: ProviderInvoiceSignatureTableViewCell = invoiceTableView.cellForRow(at: indexPath) as? ProviderInvoiceSignatureTableViewCell {
            var url = String()
            url = AMAZONUPLOAD.SIGNATURE + "\(shipmentModel.bookingId!)" + ".png"
            let image = self.image(from: cell.signatureView)
            temp.append(UploadImage.init(image: image, path: url))
        }
        
        let upMoadel = UploadImageModel.shared
        upMoadel.uploadImages = temp
        upMoadel.start()
        
    }
    
    //*****Convert the view to image*****//
    func image(from view: SignatureView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    
    //MARK: - update booking status API
    func updateBookingStatus() {
        //        var strUrl = String()
        //
        //        var imagesCount:Int = 0
        
        //        for _ in InvoiceViewController.myImages {
        //            imagesCount =  imagesCount + 1
        //            strUrl = Utility.amazonUrl + AMAZONUPLOAD.DOCUMENTS +  "\(shipmentModel.bookingId)" + "_" + String(imagesCount) + ".png"
        //            if self.documentImages.isEmpty{
        //                self.documentImages = strUrl
        //            }else{
        //                self.documentImages = self.documentImages + "," + strUrl
        //            }
        //        }
        let signatureURL = Utility.amazonUrl + AMAZONUPLOAD.SIGNATURE + "\(shipmentModel.bookingId!)" + ".png";
        
        //        let indexPath = IndexPath(row: 1, section: ICSectionType.ICrecepientData.rawValue)
        //        let cell: ICReceiverTableViewCell = invoiceTableView.cellForRow(at: indexPath) as! ICReceiverTableViewCell
        //
        
        //        if tollFee.isEmpty {
        //            tollFee = "0"
        //        }
        
        Helper.showPI(message:"Loading..")
        
        
        //        let shipmentComplete = InvoiceModel.init(updateInvoice: status, updateInvoiceBookingID: bookingDict!.bookingId, updateInvoiceSignatureURL: signatureURL, updateInvoiceDocumentImage: documentImages, udpateInvoiceTollFee: tollFee, updateInvoiceHandlingFee: handlingFee, updateInvoiceReceiverName: cell.receiverName.text!, updateInvoiceReceiverPhone: cell.phoneNumber.text!, updateInvoiceRating: String(ratingBycust), updateInvoiceLatitude: String(lastLat), updateInvoiceLongitude: String(lastLat))
        
        let bookingId = Double(shipmentModel.bookingId)
        
        let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 15, custRating: ratingBycust, custSignature: signatureURL)
        resetSlider()
        shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
            
            self.submitInvoiceButton.isEnabled = true
            if success == true{
                self.shipmentModel.orderStatus = 15
                self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 15)
                self.navigationController?.popToRootViewController(animated: true)
            }
            
        })
        
        self.submitInvoiceButton.isEnabled = true
        
    }
}
