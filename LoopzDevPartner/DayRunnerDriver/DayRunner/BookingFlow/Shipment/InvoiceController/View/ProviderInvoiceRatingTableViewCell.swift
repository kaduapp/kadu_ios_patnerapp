//
//  ProviderInvoiceRatingTableViewCell.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import FloatRatingView

class ProviderInvoiceRatingTableViewCell: UITableViewCell {

    @IBOutlet weak var rateCustomerLabel: UILabel!
    @IBOutlet var ratingView: FloatRatingView!
    override func awakeFromNib() {
        super.awakeFromNib()
        rateCustomerLabel.text = "Rate the Customer".localized
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateRatingView(){
        ratingView.fullImage = UIImage.init(named: "invoice_green_star_icon")
        ratingView.emptyImage = UIImage.init(named: "invoice_grey_star_icon")
        ratingView.maxRating = 5
        ratingView.minRating = 1
        ratingView.rating = 4
        ratingView.editable = true
        ratingView.halfRatings = false
        ratingView.floatRatings = false
    }

}
