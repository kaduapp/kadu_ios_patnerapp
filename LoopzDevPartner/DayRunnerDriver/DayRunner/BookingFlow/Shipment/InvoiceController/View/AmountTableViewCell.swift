//
//  AmountTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 4/12/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class AmountTableViewCell: UITableViewCell {
    @IBOutlet weak var paymentLabel: UILabel!
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var bookingId: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
