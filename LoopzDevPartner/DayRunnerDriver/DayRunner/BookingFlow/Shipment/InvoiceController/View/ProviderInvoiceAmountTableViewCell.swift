//
//  ProviderInvoiceAmountTableViewCell.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ProviderInvoiceAmountTableViewCell: UITableViewCell {

    @IBOutlet var cashAmountlabel: UILabel!
    @IBOutlet var walletAmountLabel: UILabel!
    @IBOutlet weak var Totalamount: UILabel!
    
    @IBOutlet weak var billingAmountLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        billingAmountLabel.text = "BILLABLE AMOUNT".localized
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
