//
//  ProviderInvoiceSignatureTableViewCell.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 02/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ProviderInvoiceSignatureTableViewCell: UITableViewCell {
    @IBOutlet weak var signatureView: SignatureView!
    var isSignatureMade = false
    
    @IBOutlet weak var signatureLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        signatureView.delegate = self
        signatureLabel.text = "SIGNATURE".localized
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func actionButtonClearSignatureView(_ sender: UIButton) {
        signatureView.erase()
        let ud = UserDefaults.standard
        ud.set(false, forKey: "signatureMade")
        ud.synchronize()
        isSignatureMade = false
    }
    
}

extension ProviderInvoiceSignatureTableViewCell :SignatureViewDelegate{
    public func signatureMade() {
        let ud = UserDefaults.standard
        ud.set(true, forKey: "signatureMade")
        ud.synchronize()
    }
}
