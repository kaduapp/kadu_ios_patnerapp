//
//  DetailsLaundraTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 4/12/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class DetailsLaundraTableViewCell: UITableViewCell {
    @IBOutlet weak var productQuantity: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
