//
//  LiveChatViewController.swift
//  CargoDriver
//
//  Created by Vengababu Maparthi on 28/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import WebKit

class LiveChatViewController: UIViewController {

    @IBOutlet weak var chatView: WKWebView!
    var activityIndicator = UIActivityIndicatorView()
var liveChatMdl = liveModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.chatView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        self.requestUrls()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToVc(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func requestUrls(){
        
        //@locate livechatmodel
        liveChatMdl.getLiveChatAPi(completionHandler: { liveChatResult in
            switch liveChatResult{
            case .success(let urlRequest):
                self.chatView.load(urlRequest)
                break
            case .failure(let error):
                self.activityIndicator.stopAnimating()
                print(error)
                break
            }
        })

    }
    
}

//MARK: - Delegate of webview
    //MARK: - Delegate of webview

    extension LiveChatViewController : WKNavigationDelegate {
        
        
        func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            activityIndicator.stopAnimating()
        }
        
        func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
              activityIndicator.stopAnimating()
        }
        
    }
