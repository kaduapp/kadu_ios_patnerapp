//
//  LiveChatModel.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

enum liveChatResult {
    case success(URLRequest)
    case failure(Bool)
}

class liveModel: NSObject {
    
    func getLiveChatAPi(completionHandler:@escaping (liveChatResult) -> ()) {
//        NetworkHelper.requestGETURL(method: API.LiveChat.Licence_url,
//                                    success: { (response) in
//                                        Helper.hidePI()
//                                        if response.isEmpty == false {
//                                            
//                                            let url = URL(string: self.prepareUrl(response["chat_url"] as! String))
//                                            let request = URLRequest(url: url!)
//                                            completionHandler(.success(request))
//                                      
//                                        } else {
//                                            completionHandler(.failure(false))
//                                        }
//                                        
//        })
//        { (Error) in
//            Helper.alertVC(errMSG: Error.localizedDescription)
//        }
    }
    
    func prepareUrl(_ url: String) -> String {
        var string: String = "https://\(url)"
        string = string.replacingOccurrences(of: "{%license%}", with: API.LiveChat.Licence)
        string = string.replacingOccurrences(of: "{%group%}", with: "GogoCargo")
        return string
    }
    
    
}
