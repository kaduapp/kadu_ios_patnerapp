//
//  Shipment.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 09/12/2017.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxSwift

enum BookingStatus : Int {
    case onTheWayToPickup = 8
    case arrivedAtPickup = 10
    case deliveryStarted = 11
    case reachedDropLoc = 12
    case jobCompleted = 13
    case finished = 14
    case submit = 15
    case laundryPickup = 26
    case laundraMart = 27
    case jobDoneByDriver = 28
}

class Shipment: NSObject {
    
    //Customer Details
    var orderStatus = 0
    var updateType = "0"
    var customerName = ""
    var cashCollect = 0.00
    var dateOfSlot = Date()
    var storeId = ""
    var pickupStore = ""
    var pickupAddress = ""
    var bidID = 0
   
    var deliveryCustomer = ""
    var deliveryAddress = ""
    var paymentMethod = ""
    var paymentType = 0
    var payByWallet = 0
    var bookingId: NSNumber!
    var custChn = ""
    var customerPhone = ""
    var storePhone = ""
    var customerLatLng = ""
    var weight = 0
    var storeLatLng = ""
    var bookingTime = ""
    var statusMessage = ""
    var storeType = 0
    var slotEndTime = 0
    var estimatedPackageValue = 0.00
    var extraNote = ""
    var slotStartTime = 0
    var expressType:Bool = true
    var storeMessage = ""
    var bookingTypeLaundra:Bool = true
    var bookingTypemsg = ""
    var bookingDateNtime = ""
    var timeLeftToPick = ""
    var customerId = ""
    var customerPic = ""
    var weightPicked = 0
    var weightText = ""
    var weightStr = ""
    var laundraStoreName = ""
    var laundraAddress = ""
    var slotId = ""
    
    var commingFromHome:Bool = true
     var multiArray = [(key : Int , value : [Shipment] )]()
    
    //Product Details
    var productDetails: [[String:Any]]!
    var exculsiveTax: [[String:Any]] = []
    var deliverySplits:[String:Any] = [:]
    
    var itemPickedTime: String!
    
    //Bill Details
    var grandTotal: Double!
    var subTotal: Double!
    var drivertip: Double!
    var deliveryCharge:Double!
    var discount:Double!
    let apiResponse = PublishSubject<Bool>()
    var disposebag = DisposeBag()
    
    var locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
    
    var updateOrderStatus = 0
    var updateOrderBookingID: NSNumber!
    var updateOrderLongitude: Any!
    var updateOrderLatitude: Any!
    var updateOrderDetails: [[String: Any]] = []
    var newUpdatedOrders: [String: Any] = [:]
    var items:[String:Any] = [:]
    var updateOrderId = 0.00
    var ipAddress = ""
    
    override init() {
        super.init()
    }
  
    init(items: [[String: Any]], orderId: Double){
        
        self.updateOrderDetails = items
        let ud = UserDefaults.standard
        self.updateOrderLatitude = ud.double(forKey:"currentLat")
        self.updateOrderLongitude = ud.double(forKey:"currentLog")
        self.updateOrderId = orderId
    }
    
    init(oldItems: [String: Any],newItems: [String: Any],orderId: Double,updateType:String) {
        
        self.items = oldItems
        self.newUpdatedOrders = newItems
        let ud = UserDefaults.standard
        self.updateOrderLatitude = ud.double(forKey:"currentLat")
        self.updateOrderLongitude = ud.double(forKey:"currentLog")
        self.updateOrderId = orderId
       self.updateType = updateType
    }
    
    
    var updateOrderSignature = ""
    var updateOrderRating = Float(4.0)
    
    
    init(orderId: Double, orderStatus: Int , storeId : String, weight : Int,custRating: Float, custSignature: String) {
        self.updateOrderStatus = orderStatus
        let ud = UserDefaults.standard
        self.updateOrderLatitude = ud.double(forKey:"currentLat")
        self.updateOrderLongitude = ud.double(forKey:"currentLog")
        self.updateOrderId = orderId
        self.storeId = storeId
        self.updateOrderSignature = custSignature
        self.updateOrderRating = custRating
        self.weight = weight
    }
    
    init(orderId: Double, orderStatus: Int, custRating: Float, custSignature: String ) {
        self.updateOrderStatus = orderStatus
        let ud = UserDefaults.standard
        self.updateOrderLatitude = ud.double(forKey:"currentLat")
        self.updateOrderLongitude = ud.double(forKey:"currentLog")
        self.updateOrderId = orderId
        self.updateOrderSignature = custSignature
        self.updateOrderRating = custRating
    }
    
    
    
    init(orderId: Double, slotID: String, orderStatus: Int, custRating: Float, custSignature: String ) {
        self.updateOrderStatus = orderStatus
        let ud = UserDefaults.standard
        self.updateOrderLatitude = ud.double(forKey:"currentLat")
        self.updateOrderLongitude = ud.double(forKey:"currentLog")
        self.updateOrderId = orderId
        self.updateOrderSignature = custSignature
        self.updateOrderRating = custRating
        self.slotId = slotID
    }
    
    
    func makeServiceCallToUpdateStatus(shipmentModel: Shipment, completionHanler:@escaping(Bool) -> ()){
        
        Helper.showPI(message:"Loading..")
        let shipmentBookingApi = ShipmentBookingApi()
        shipmentBookingApi.makeServiceCallToUpdateDeliveryStatus(shipmentModel: shipmentModel)
        _ = shipmentBookingApi.subject_response.subscribe(onNext: { (response) in
            Helper.hidePI()
            if let errFlag = response["errFlag"] as? Int  {
                if(errFlag == 1){
//                    shipmentModel.statusMessage = response["statusmessage"] as! String
                    completionHanler(true)
                }
            }
        }, onError: { (error) in
        }, onCompleted: {
        }) {
            }.disposed(by: disposebag)
    }
    
    func makeServiceCallToUpdateOrder(withParams params: Shipment, completionHanler:@escaping(Bool, Double, Double) -> ()){
        Helper.showPI(message:"Loading..")
        let shipmentBookingApi = ShipmentBookingApi()
        shipmentBookingApi.makeServiceCallToUpdateDeliveryOrder(shipmentModel: params)
        _ = shipmentBookingApi.subject_response.subscribe(onNext: { (response) in
            Helper.hidePI()
            if let data =  response["data"] as? [String:Any] {
                
                //                self.updateOrderDetails(responseData: data)
                var newTotal = 0.0
                if let total = data["totalAmount"] as? Double {
                    newTotal = total
                }
                
                var subtotal = 0.0
                if let stotal = data["subTotalAmount"] as? Double {
                    subtotal = stotal
                }
                
                completionHanler(true,newTotal,subtotal)
            }
            
            //            if let errFlag = response["errFlag"] as? Int  {
            //                if(errFlag == 1){
            //                    completionHanler(true)
            //                }
            //            }
        }, onError: { (error) in
        }, onCompleted: {
        }) {
            }.disposed(by: disposebag)
    }
    
    
    
    func makeServiceCallUpdateOrder(withParams params: Shipment, completionHanler:@escaping(Bool, Double, Double) -> ()){
        Helper.showPI(message:"Loading..")
        let shipmentBookingApi = ShipmentBookingApi()
        shipmentBookingApi.makeServiceCallToUpdateOrder(shipmentModel: params)
        _ = shipmentBookingApi.subject_response.subscribe(onNext: { (response) in
            Helper.hidePI()
            if let data =  response["data"] as? [String:Any] {
                
                self.productDetails = data["Items"] as? [[String: Any]]
                
                //                self.updateOrderDetails(responseData: data)
                var newTotal = 0.0
                if let total = data["totalAmount"] as? Double {
                    newTotal = total
                }
                
                var subtotal = 0.0
                if let stotal = data["subTotalAmount"] as? Double {
                    subtotal = stotal
                }
                
                completionHanler(true,newTotal,subtotal)
            }
            
            //            if let errFlag = response["errFlag"] as? Int  {
            //                if(errFlag == 1){
            //                    completionHanler(true)
            //                }
            //            }
        }, onError: { (error) in
        }, onCompleted: {
        }) {
            }.disposed(by: disposebag)
    }
    
    func updateOrderDetails(responseData:[String: Any])  {
        //        if let total = responseData["totalAmount"] as? Double {
        //            self.grandTotal = total
        //        }
    }
    
    init(orderId: Double, ipAddress: String  ) {
        let ud = UserDefaults.standard
        self.updateOrderLatitude = ud.double(forKey:"currentLat")
        self.updateOrderLongitude = ud.double(forKey:"currentLog")
        self.updateOrderId = orderId
        self.ipAddress = ipAddress
    }
    
    func makeServiceCallToCancelOrder(shipmentModel: Shipment, completionHanler:@escaping(Bool) -> ()) {
        Helper.showPI(message:"Loading..")
        let shipmentBookingApi = ShipmentBookingApi()
        shipmentBookingApi.makeServiceCallToCancelOrder(shipmentModel: shipmentModel)
        _ = shipmentBookingApi.subject_response.subscribe(onNext: { (response) in
            Helper.hidePI()
            if let errFlag = response["errFlag"] as? Int  {
                if(errFlag == 1){
                    completionHanler(true)
                }
            }
        }, onError: { (error) in
        }, onCompleted: {
        }) {
            }.disposed(by: disposebag)
    }
    
    
    func updateTheBookingsString(bookingID:NSNumber,status:Int){
        let ud = UserDefaults.standard
        
        let onGoingBids:[String] = Utility.onGoingBid.components(separatedBy: ",")
        
        var bidSum = String()
        
        for dict  in onGoingBids {
            
            let bookingData = dict.components(separatedBy: "|")
            
            
            if NSNumber.aws_number(from: bookingData[0]) == bookingID{  //String(describing:bookingDict!.bookingId) {
                if bidSum.isEmpty {
                    bidSum = bookingData[0] + "|" + bookingData[1] + "|" + String(describing:status)
                    
                }else{
                    bidSum = bidSum + "," + bookingData[0] + "|" + bookingData[1] + "|" + String(describing:status)
                }
            }else{
                if bidSum.isEmpty {
                    bidSum = bookingData[0] + "|" + bookingData[1] + "|" + bookingData[2]
                    
                }else{
                    bidSum = bidSum + "," + bookingData[0] + "|" + bookingData[1] + "|" + bookingData[2]
                }
            }
        }
        
        ud.set(bidSum, forKey: USER_INFO.SELBID)
        ud.synchronize()
    }
    
}

