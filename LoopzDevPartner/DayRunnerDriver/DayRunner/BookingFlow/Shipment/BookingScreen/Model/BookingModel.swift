//
//  BookingModel.swift
//  DayRunnerDriverDev
//
//  Created by Vengababu Maparthi on 12/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

//
//  BookingModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 18/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxSwift
protocol onBookingDelegate {
    func bookingDelegate(data: [BookingModel])
}


class BookingModel:NSObject{
    
    override init()
    {
        super.init()
    }
    var BVCDelegate: onBookingDelegate! = nil
    
    var  invoiceData    = [BookingModel]()
    
    var disc            = ""
    var distFare        = ""
    var timeFare        = ""
    var grandTotal      = ""
    var waitFee         = ""
    var baseFare        = ""
    var subTotal        = ""
    var appliedFee      = 0
    
    var distanceTravelled = ""
    var masEarning = ""
    var durationTravlled = ""
    var tollFee = ""
    var handling = ""
    var total = ""
    var marEarning = ""
    var appCom = ""
     let disposeBag = DisposeBag()
    //Update booking status variables
    var udpateStatusCurrentStatus: Int!


    var udpateStatusDistance: String!
    
    
    var updateStatusStatus: Int!
    var udpateStatusBID: String!
    var udpateStatusLatitude: String!
    var updateStatusLongitude: String!
    
    init(updateStatusStatus: Int!, udpateStatusBID: String!, udpateStatusLatitude: String!, updateStatusLongitude: String!) {
        
        self.updateStatusStatus = updateStatusStatus
        self.udpateStatusBID = udpateStatusBID
        self.udpateStatusLatitude = udpateStatusLatitude
        self.updateStatusLongitude = updateStatusLongitude
    }
    
    init(updateStatus udpateStatusCurrentStatus: Int!, updateStatusStatus: Int!, udpateStatusBID: String!, udpateStatusLatitude: String!, updateStatusLongitude: String!)
    {
        self.udpateStatusCurrentStatus = udpateStatusCurrentStatus
        self.updateStatusStatus = updateStatusStatus
        self.udpateStatusBID = udpateStatusBID
        self.udpateStatusLatitude = udpateStatusLatitude
        self.updateStatusLongitude = updateStatusLongitude
    }
    
    init(updateStatus udpateStatusCurrentStatus: Int!, updateStatusStatus: Int!, udpateStatusBID: String!, udpateStatusDistance: String, udpateStatusLatitude: String!, updateStatusLongitude: String!)
    {
        self.udpateStatusCurrentStatus = udpateStatusCurrentStatus
        self.updateStatusStatus = updateStatusStatus
        self.udpateStatusBID = udpateStatusBID
        self.udpateStatusDistance = udpateStatusDistance
        self.udpateStatusLatitude = udpateStatusLatitude
        self.updateStatusLongitude = updateStatusLongitude
    }
    
    //MARK: - update the booking status API
    func updateBookingStatus(dict: BookingModel , status:Int){
        Helper.showPI(message:"Loading..")
        
                let updateShipmentStatus =  ShipmentBookingApi()
                updateShipmentStatus.updateStatus(shipmentModel: dict)
                updateShipmentStatus.subject_response.subscribe(onNext: { (response) in
                    if response.isEmpty == false {
                        
//                        if (response["statusCode"] != nil){
//                            let statCode:Int = response["statusCode"] as! Int
//                            if statCode == 401 {
//                                Helper.hidePI()
//                                Session.expired()
//                                Helper.alertVC(errMSG: "Your Session has beed expired")
//                            }else
//                            {
//                                Helper.hidePI()
//                                Helper.alertVC(errMSG: "bad request or  internal server error")
//                            }
//                        }else{
                        
//                            let flag:Int = response["errFlag"] as! Int
//                            if flag == 1{
//                                Helper.alertVC(errMSG: response["errMsg"] as! String)
//                            }else{
                                if status == 14 {
///                                    self.storingInvoiceData(dict: (response["data"] as? [String: Any])!)
                                }
                                else{
///                                    if (self.BVCDelegate != nil) {
///                                        self.BVCDelegate.bookingDelegate(data: self.invoiceData)
///                                    }
                                }
///                                self.updateTheBookingsString(bookingID: Int(dict.udpateStatusBID)!, status: status)
//                            }
//                        }
                    }
                }, onError: { (error) in
                    Helper.hidePI()
                }, onCompleted: {
        
                }) {
        
                }.disposed(by: disposeBag)
        
    }
    

    func updateTheBookingsString(bookingID:Int,status:Int){
        let ud = UserDefaults.standard
        
        let onGoingBids:[String] = Utility.onGoingBid.components(separatedBy: ",")
        
        var bidSum = String()
        
        for dict  in onGoingBids {
            
            let bookingData = dict.components(separatedBy: "|")
            
            if Int(bookingData[0]) == bookingID{  //String(describing:bookingDict!.bookingId) {
                if bidSum.isEmpty {
                    bidSum = bookingData[0] + "|" + bookingData[1] + "|" + String(describing:status)
                    
                }else{
                    bidSum = bidSum + "," + bookingData[0] + "|" + bookingData[1] + "|" + String(describing:status)
                }
            }else{
                if bidSum.isEmpty {
                    bidSum = bookingData[0] + "|" + bookingData[1] + "|" + bookingData[2]
                    
                }else{
                    bidSum = bidSum + "," + bookingData[0] + "|" + bookingData[1] + "|" + bookingData[2]
                }
            }
        }
        
        ud.set(bidSum, forKey: USER_INFO.SELBID)
        ud.synchronize()
    }
    
    //MARK: - Storing the invoice data Status = 16
    func storingInvoiceData(dict: [String:Any]) {
        
        let model = BookingModel()
        
        let invoice       = dict["invoice"]      as! [String:Any]
        
        if  let distFare      = invoice["distFare"]  as? String {
            model.distFare = distFare
        }
        
        if let timeFare      = invoice["timeFare"]  as? String {
            model.timeFare = timeFare
        }
        
        if  let waitFare      = invoice["watingFee"] as? String {
            model.waitFee    = waitFare
        }
        
        if  let grandTot      = invoice["total"]     as? String {
            model.grandTotal = grandTot
        }
        
        if  let discount      = invoice["Discount"]  as? String {
            model.disc = discount
        }
        
        if  let baseAmt       = invoice["baseFare"]  as? String {
            model.baseFare = baseAmt
        }
        
        if  let appliedAmt    = invoice["appliedAmount"] as? NSNumber {
            model.appliedFee = Int(appliedAmt)
        }
        
        if  let subTot        = invoice["subtotal"]  as? String {
            model.subTotal = subTot
        }
        
        invoiceData.append(model)
        
        if (BVCDelegate != nil) {
            BVCDelegate.bookingDelegate(data: invoiceData)
        }
    }
    
}


