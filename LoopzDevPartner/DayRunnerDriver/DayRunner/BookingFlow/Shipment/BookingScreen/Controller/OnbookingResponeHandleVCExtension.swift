//
//  OnbookingResponeHandleVC.swift
//  GoGoCargoDriver
//
//  Created by Vengababu Maparthi on 23/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension OnBookingViewController:onBookingDelegate{
    func bookingDelegate(data: [BookingModel]) {
        switch self.status{
        case 7:
            break
        case 8:
            break
        case 9:
            if bookingDict!.paidByWhom == 1 {
                PaymentCollectView.instance.show() // if cash pay by receiver
            }
            break
        case 16:
            invoiceData = data
            self.status = 16 // for deleting database
            self.performSegue(withIdentifier: "toSignatureVC", sender: nil)
            break
        default:
            break
            
        }
    }
}

