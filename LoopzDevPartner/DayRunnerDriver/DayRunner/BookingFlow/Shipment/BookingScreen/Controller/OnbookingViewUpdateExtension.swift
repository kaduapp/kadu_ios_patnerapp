//
//  OnbookingViewUpdate.swift
//  GoGoCargoDriver
//
//  Created by Vengababu Maparthi on 23/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import GoogleMaps
import JaneSliderControl

extension OnBookingViewController{
    //MARK: - set the delegate to map and location
    func initiateMap() {
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        locationManager.startUpdatingLocation()
    }
    
    //MARK: - Get current location on map
    func getCurrentLocationPostion(){
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitude) ,longitude: CLLocationDegrees(longitude) , zoom: 16)
        self.mapView.animate(to: camera)
    }
    
    
    
    //MARK: - update the driver status images and address according the
    func updateCustomerInfoStatus(){
        
        self.loadingTime.invalidate()
        let sectionType : BookingStatus = BookingStatus(rawValue : status)!
        switch sectionType {
        case .arrivedAtPickup:
            if(shipmentModel.storeType == 5 && !shipmentModel.bookingTypeLaundra){
                if shipmentModel.statusMessage == "Not Picked" {
                    titleLabel.text = "Driver enroute to customer location."
                }else {
                    titleLabel.text = shipmentModel.statusMessage//"On the way to pickup".localized
                }
                addressImage.image = #imageLiteral(resourceName: "home_pickup_icon")
                let address = "\(shipmentModel.customerName): \(shipmentModel.pickupAddress)"
                let range = NSMakeRange(shipmentModel.customerName.count, (address.count - shipmentModel.customerName.count))
                
                bookingAddress.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
                distanceTitleLabel.text = "Distance".localized
                timeLeftTitleLabel.text = "Time".localized
                distanceLabel.text = ""
                timeLeft.text = ""
                showTheLoadingNUnloadingTime()
                
                break
            }else {
                
                if shipmentModel.statusMessage == "Picked" {
                    titleLabel.text = "Driver enroute to store location."//"On the way to pickup".localized
                }else {
                    titleLabel.text = shipmentModel.statusMessage//"On the way to pickup".localized
                }
                addressImage.image = #imageLiteral(resourceName: "home_pickup_icon")
                
                if(shipmentModel.storeType == 7) {
                    let address = "\("Pickup "): \(shipmentModel.pickupAddress)"
                    let range = NSMakeRange(shipmentModel.pickupStore.count, (address.count - shipmentModel.pickupStore.count))
                    
                    bookingAddress.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
                }else {
                    let address = "\(shipmentModel.pickupStore): \(shipmentModel.pickupAddress)"
                    let range = NSMakeRange(shipmentModel.pickupStore.count, (address.count - shipmentModel.pickupStore.count))
                    
                    bookingAddress.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
                }
               
                distanceTitleLabel.text = "Distance".localized
                timeLeftTitleLabel.text = "Time".localized
                distanceLabel.text = ""
                timeLeft.text = ""
                showTheLoadingNUnloadingTime()
                
                break
            }
        case .laundryPickup:
            titleLabel.text = shipmentModel.statusMessage//"On the way to pickup".localized
            addressImage.image = #imageLiteral(resourceName: "home_pickup_icon")
            let address = "\(shipmentModel.customerName): \(shipmentModel.pickupAddress)"
            let range = NSMakeRange(shipmentModel.customerName.count, (address.count - shipmentModel.customerName.count))
            
            bookingAddress.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
            distanceTitleLabel.text = "Distance".localized
            timeLeftTitleLabel.text = "Time".localized
            distanceLabel.text = ""
            timeLeft.text = ""
            showTheLoadingNUnloadingTime()
            
            break
        case .laundraMart:
            if shipmentModel.statusMessage == "Driver enroute to customer location." || shipmentModel.statusMessage == "Driver Arrived & Picked."  {
                titleLabel.text = "Driver Arrived to laundromat."
            }else {
                titleLabel.text = shipmentModel.statusMessage//"On the way to pickup".localized
            }
            
            //            titleLabel.text = shipmentModel.statusMessage//"On the way to pickup".localized
            addressImage.image = #imageLiteral(resourceName: "home_pickup_icon")
            let address = "\(shipmentModel.pickupStore): \(shipmentModel.pickupAddress)"
            let range = NSMakeRange(shipmentModel.pickupStore.count, (address.count - shipmentModel.pickupStore.count))
            
            bookingAddress.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
            distanceTitleLabel.text = "Distance".localized
            timeLeftTitleLabel.text = "Time".localized
            distanceLabel.text = ""
            timeLeft.text = ""
            showTheLoadingNUnloadingTime()
            
            break
        case .reachedDropLoc:
            if shipmentModel.statusMessage == "Picked" {
                titleLabel.text = "Driver enroute to delivery location."//"On the way to pickup".localized
            }else {
                titleLabel.text = shipmentModel.statusMessage//"On the way to pickup".localized
            }
            //            titleLabel.text = shipmentModel.statusMessage//"On the way to delivery".localized
            addressImage.image = #imageLiteral(resourceName: "home_dropoff_icon")
            let address = "\(shipmentModel.customerName): \(shipmentModel.deliveryAddress)"
            let range = NSMakeRange(shipmentModel.customerName.count, (address.count - shipmentModel.customerName.count))
            bookingAddress.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
            distanceTitleLabel.text = "Distance".localized
            timeLeftTitleLabel.text = "Time".localized
            distanceLabel.text = ""
            timeLeft.text = ""
            showTheLoadingNUnloadingTime()
            break
        case .jobCompleted:
            if shipmentModel.statusMessage == "Picked" {
                titleLabel.text = "Driver enroute to delivery location."//"On the way to pickup".localized
            }else {
                titleLabel.text = shipmentModel.statusMessage//"On the way to pickup".localized
            }
            //            titleLabel.text = shipmentModel.statusMessage//"On the way to delivery".localized
            addressImage.image = #imageLiteral(resourceName: "home_dropoff_icon")
            let address = "\(shipmentModel.customerName): \(shipmentModel.deliveryAddress)"
            let range = NSMakeRange(shipmentModel.customerName.count, (address.count - shipmentModel.customerName.count))
            bookingAddress.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
            distanceTitleLabel.text = "Distance".localized
            timeLeftTitleLabel.text = "Time".localized
            distanceLabel.text = ""
            timeLeft.text = ""
            showTheLoadingNUnloadingTime()
            break
        default:
            break
        }
    }
}

