//
//  OnbookingCoreDataNDistCal.swift
//  GoGoCargoDriver
//
//  Created by Vengababu Maparthi on 23/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension OnBookingViewController{
    
    
    //MARK: -  Shows the loading waiting time and unloading waiting time
    func showTheLoadingNUnloadingTime(){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        dateFormatter.locale = Locale.init(identifier: "en_IN")
        TimeZone.ReferenceType.default = TimeZone(abbreviation: "GMT")!
        dateFormatter.timeZone = TimeZone.ReferenceType.default
        let startTime = dateFormatter.string(from: Date())

        if status == 10{
            
            
            if shipmentModel.bookingTime == nil || shipmentModel.bookingTime == "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
//                dateFormatter.locale = Locale.init(identifier: "en_IN")
//                TimeZone.ReferenceType.default = TimeZone(abbreviation: "GMT")!
//                dateFormatter.timeZone = TimeZone.ReferenceType.default
                let tempTime = dateFormatter.string(from: Date())
                shipmentModel.bookingTime = tempTime
            }
            
            

            
            let timeDiff = Helper.getDifferenceForTwoTimes(currentDate: startTime, previousDate: (shipmentModel.bookingTime) )
            
            self.hours = timeDiff.hour
            self.minutes = timeDiff.minute
            self.seconds = timeDiff.second
            //            self.loadingTime.invalidate()
            self.loadingTime = Timer.scheduledTimer(timeInterval: 1.0
                , target: self, selector: #selector(self.showTheLoadingUnloadingTimer), userInfo: nil, repeats: true)
        }else {
            if shipmentModel.itemPickedTime == nil || shipmentModel.itemPickedTime == ""{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
//                dateFormatter.locale = Locale.init(identifier: "en_IN")
//                TimeZone.ReferenceType.default = TimeZone(abbreviation: "GMT")!
//                dateFormatter.timeZone = TimeZone.ReferenceType.default
                let tempTime = dateFormatter.string(from: Date())
                shipmentModel.itemPickedTime = tempTime
            }

            let timeDiff = Helper.getDifferenceForTwoTimes(currentDate: startTime, previousDate: (shipmentModel.itemPickedTime)! )
            
            self.hours = timeDiff.hour
            self.minutes = timeDiff.minute
            self.seconds = timeDiff.second
            //            self.loadingTime.invalidate()
            self.loadingTime = Timer.scheduledTimer(timeInterval: 1.0
                , target: self, selector: #selector(self.showTheLoadingUnloadingTimer), userInfo: nil, repeats: true)
        }
        
        
    }
    
    //MARK: -updates loading time, unloading time
    @objc func showTheLoadingUnloadingTimer(){
        
        self.seconds += 1
        if self.seconds == 60 {
            self.minutes += 1
            self.seconds = 0
        }
        if self.minutes == 60 {
            hours += 1;
            self.minutes = 0;
        }
        
        let ud = UserDefaults.standard
        let proper  = [     "lat" : ud.double(forKey:"currentLat"),
                            "long" : ud.double(forKey:"currentLog"),
                            "dist":0] as [String : Any]
        let  database  = try! manager.databaseNamed("livedayrunner")
        
        if status == 15 || status == 12 {
            timeLeft.text = String(format:"%0d:%0d:%0d",self.hours,self.minutes,self.seconds)
            self.updatetheDistanceTravlled()
            
            
            //            if let documents = database.existingDocument(withID:("(shipmentModel.bookingId)")){
            //                var properties = documents.properties
            //                properties?["dist"] = 0.0
            //                properties?["lat"] = ud.double(forKey:"currentLat") as Any
            //                properties?["long"] = ud.double(forKey:"currentLog") as Any
            //                try! documents.putProperties(properties!)
            //            }else{
            //                let document = database.document(withID: ("\(shipmentModel.bookingId)"))
            //                try! document?.putProperties(proper)
            //            }
        }else{
            timeLeft.text = String(format:"%0d:%0d:%0d",self.hours,self.minutes,self.seconds)
            self.updatetheDistanceTravlled()
        }
    }
    
    //MARK: - Distance calculation
    //****** distance calculation for each booking and maintains couch db for storing distance***//
    func updatetheDistanceTravlled(){
        let ud = UserDefaults.standard
        if (ud .value(forKey: "currentLat") != nil) {
            
            let onGoingBids:[String] = Utility.onGoingBid.components(separatedBy: ",")
            
            let  database  = try! manager.databaseNamed("livedayrunner")
            
            let proper:[String : Any]  = [ "lat" : ud.double(forKey:"currentLat"),
                                           "long" : ud.double(forKey:"currentLog"),
                                           "dist":0]
            if onGoingBids.count == 0 {
                self.loadingTime.invalidate()
            }
            for dict  in onGoingBids {
                
                let bookingData = dict.components(separatedBy: "|")
                
                if bookingData.count == 1 {
                    return
                }
                
                if (bookingData[2] == "10") || (bookingData[2] == "12"){
                    
                    if let documents = database.existingDocument(withID:(bookingData[0])){
                        var properties = documents.properties
                        
                        let coordinate₀ = CLLocation(latitude: CLLocationDegrees(ud.double(forKey:"currentLat")), longitude: CLLocationDegrees(ud.double(forKey:"currentLog")))
                        var lat:Double = 0.00
                        if let latit = properties!["lat"] as? Double{
                            lat = latit
                            
                        }
                        var log:Double = 0.00
                        if let logit = properties!["long"] as? Double{
                            log = logit
                            
                        }
                        //let log:Double = Double(properties!["long"] as! String)!
                        let coordinate₁ = CLLocation(latitude: CLLocationDegrees(lat), longitude:  CLLocationDegrees(log))
                        let distanceInMeters = coordinate₁.distance(from: coordinate₀)
                        var latitute:Double  = 0.00
                        var logintute:Double = 0.00
                        if let dropLatLng = UserDefaults.standard.string(forKey: "dropLatLng"){
                            var lng = dropLatLng.components(separatedBy: ",")
                            latitute =  Double(lng[0])!
                            logintute = Double(lng[1])!
                            
                        }
                        let coordinate2 = CLLocation(latitude: latitute, longitude: logintute)
                        var distance = coordinate2.distance(from: coordinate₀)
                        distanceLabel.text = String(format:"%.2f Kms",(distance)/1000)
                        
                       // if distanceInMeters > 40 {
                        //var distanceLocal:Double = 0.0
//                            if Utility.DistanceUnits == "Km" {
//                                distanceLocal = (distanceInMeters)/1000 + Double((properties?["dist"] as?  NSNumber)!)
//                                if bookingData[0] == "\(shipmentModel.bookingId!)" {
//                                    distanceLabel.text = String(format:"%.2f Kms",(distanceInMeters)/1000 + Double((properties?["dist"] as?  NSNumber)!))
//                                    travelledDistance = distanceLocal;
//                                }
//
//                            }else{
//                                distanceLocal = (distanceInMeters)/1609 + Double((properties?["dist"] as?  NSNumber)!)
//                                let bookingId = "\(shipmentModel.bookingId!)"
//                                if bookingData[0] == bookingId  {
//                                    distanceLabel.text = String(format:"%.2f Miles",(distanceInMeters)/1609 + Double((properties?["dist"] as?  NSNumber)!))
//                                    travelledDistance = distanceLocal;
//                                }
//
//                            }
//
//                            properties?["dist"] = distanceLocal
//                            properties?["lat"] = ud.double(forKey:"currentLat")
//                            properties?["long"] = ud.double(forKey:"currentLog")
//                            try! documents.putProperties(properties!)
//                        }else{
//                            let bookingId = "\(shipmentModel.bookingId!)"
//                            if bookingData[0] == bookingId {
//                                if Utility.DistanceUnits == "Km" {
//                                    distanceLabel.text = String(format:"%.2f Kms",Double((properties?["dist"] as?  NSNumber)!))
//                                    travelledDistance = Double((properties?["dist"] as?  NSNumber)!);
//                                } else {
//                                    distanceLabel.text = String(format:"%.2f Miles",Double((properties?["dist"] as?  NSNumber)!))
//                                    travelledDistance = Double((properties?["dist"] as?  NSNumber)!);
//                                }
//                            }
                            
                        //}
                    }
                    else{
                        let document = database.document(withID: (bookingData[0]))
                        try! document?.putProperties(proper)
                        
                    }
                }
            }
        }
    }
}
