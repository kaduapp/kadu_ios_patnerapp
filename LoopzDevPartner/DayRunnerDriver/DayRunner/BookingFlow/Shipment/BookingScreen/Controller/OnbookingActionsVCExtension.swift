//
//  OnbookingActionsVC.swift
//  GoGoCargoDriver
//
//  Created by Vengababu Maparthi on 23/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import JaneSliderControl

extension OnBookingViewController{
    
    
    
    
    @IBAction func jobDetailsAction(_ sender: UIButton) {
        
        if shipmentModel.storeType == 5 {
             performSegue(withIdentifier: "laundraShipmentDetails", sender: self)
        }else {
             performSegue(withIdentifier: "shipmentDetails", sender: self)
        }
        
       
    }
    
    
    @IBAction func sliderFineshed(_ sender: SliderControl) {
        
        deliveryStatusButton.isEnabled = false
        if(status == 12){
            
            if shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra {
                let bookingId = Double(shipmentModel.bookingId)
                
                let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 14 , custRating: Float(4.0), custSignature: " " )
                
                
                shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                    
                    if success == true{
                        self.shipmentModel.orderStatus = 14
                        self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 14)
                        self.performSegue(withIdentifier: "goToProvideInvoice", sender: self.shipmentModel)
                    }
                    
                })
                
            }else {
                
                let bookingId = Double(shipmentModel.bookingId)
                
                let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 13, custRating: Float(4.0), custSignature: " " )
                
                
                shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                    
                    self.deliveryStatusButton.isEnabled = true
                    if success == true{
                        self.loadingTime.invalidate()
                        self.shipmentModel.orderStatus = 13
                        self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 13)
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let rideBookingController = storyboard.instantiateViewController(withIdentifier: "pickingController") as! PickingItemViewController;
                        rideBookingController.status = 2
                        rideBookingController.shipmentModel = self.shipmentModel
                        self.navigationController?.pushViewController(rideBookingController, animated: true)
                    }
                })
            }
            
        }else if(status == 27){
            let bookingId = Double(shipmentModel.bookingId)
            
            let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 28 , custRating: Float(4.0), custSignature: " " )
            
            
            shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                
                if success == true{
                    self.shipmentModel.orderStatus = 28
                    self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 28)
                    self.performSegue(withIdentifier: "goToProvideInvoice", sender: self.shipmentModel)
                }
                
            })
        }else if(status == 14){
            let bookingId = Double(shipmentModel.bookingId)
            
            let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 15 , custRating: Float(4.0), custSignature: " " )
            
            
            shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                
                if success == true{
                    self.shipmentModel.orderStatus = 15
                    self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 15)
                    self.performSegue(withIdentifier: "goToProvideInvoice", sender: self.shipmentModel)
                }
                
            })
            
            
            
        }else {
            if(shipmentModel.storeType == 5 && !shipmentModel.bookingTypeLaundra){
                let bookingId = Double(shipmentModel.bookingId)
                
                let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 26, custRating: Float(4.0), custSignature: " " )
                
                
                shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                    
                    self.deliveryStatusButton.isEnabled = true
                    if success == true{
                        self.loadingTime.invalidate()
                        self.shipmentModel.orderStatus = 26
                        self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 26)
                        
                        if self.shipmentModel.storeType == 5 && !self.shipmentModel.bookingTypeLaundra {
                            self.performSegue(withIdentifier: "laundraToAddItems", sender: self.shipmentModel)
                        }else {
                            self.performSegue(withIdentifier: "toAddItems", sender: self.shipmentModel)
                        }
                        
                    }
                    
                })
            }else if shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra {
                
                let bookingId = Double(shipmentModel.bookingId)
                
                let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 11 , custRating: Float(4.0), custSignature: " " )
                
                
                shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                    
                    self.deliveryStatusButton.isEnabled = true
                    if success == true{
                        self.loadingTime.invalidate()
                        self.shipmentModel.orderStatus = 11
                        self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 11)
                        
                        if self.shipmentModel.storeType == 5 && self.shipmentModel.bookingTypeLaundra {
                            self.performSegue(withIdentifier: "laundraToAddItems", sender: self.shipmentModel)
                        }else {
                            self.performSegue(withIdentifier: "toAddItems", sender: self.shipmentModel)
                        }
                        
                    }
                    
                })
                
            } else {
                let bookingId = Double(shipmentModel.bookingId)
                
                let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, orderStatus: 11 , custRating: Float(4.0), custSignature: " " )
                
                
                shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
                    
                    self.deliveryStatusButton.isEnabled = true
                    if success == true{
                        self.loadingTime.invalidate()
                        self.shipmentModel.orderStatus = 11
                        self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 11)
                        
                        if self.shipmentModel.storeType == 5 {
                            self.performSegue(withIdentifier: "laundraToAddItems", sender: self.shipmentModel)
                        }else {
                            self.performSegue(withIdentifier: "toAddItems", sender: self.shipmentModel)
                        }
                        
                    }
                    
                })
            }
            
        }
    }
    
    
    
    func resetSlider()
    {
        self.leftSlider.reset()
    }
    
    @IBAction func slideCanceled(_ sender: SliderControl) {
        print("Cancelled")
    }
    
    
    @IBAction func sliderChanged(_ sender: SliderControl) {
        print("Changed")
    }
    
    
//    @IBAction func deliveryStatusAction(_ sender: UIButton)
    
    //MARK: - BackToHome view controller
    @IBAction func backToHomeVC(_ sender: Any) {
        let sectionType : BookingStatus = BookingStatus(rawValue : status)!
        if sectionType.rawValue == 17 {
            navigationController?.popToRootViewController(animated: true)
//            performSegue(withIdentifier: "goToHomeWind", sender: self)
        }else {
               navigationController?.popToRootViewController(animated: true)
        }
        
     
        
        
        
    }
    
    //MARK: - Customer and drop cust call
    @IBAction func callToCustomer(_ sender: Any) {
        
        
        
        if status <= 11 {
            
            if shipmentModel.storePhone == "" {
                Helper.alertVC(errMSG: "No store phonenumber is available")
            }else {
                
                let dropPhone = "tel://" + (shipmentModel.storePhone)
                if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
                    UIApplication.shared.openURL(url as URL)
                }
            }
        }else{
            if shipmentModel.customerPhone == "" {
                Helper.alertVC(errMSG: "No customer phonenumber is available")
            }else {
                let dropPhone = "tel://" + (shipmentModel.customerPhone)
                if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
                    UIApplication.shared.openURL(url as URL)
                }
            }
        }
    }
    
    
    //MARK: - get current location icon action
    @IBAction func getCurrentLocation(_ sender: Any) {
        getCurrentLocationPostion() //@Locate OnBookingViewUpdate
    }
    
    //MARK: - Navigate using google maps
    @IBAction func wayToGoogleMaps(_ sender: Any) {
        let lat: Double
        let log: Double
        if status <= 11 {
            
            let latLongs = shipmentModel.storeLatLng
            
            let name = latLongs.components(separatedBy: ",")
            
            lat = Double(name[0])!
            log = Double(name[1])!
            
        }else{
            let latLongs = shipmentModel.customerLatLng
            
            let name = latLongs.components(separatedBy: ",")
            
            lat = Double(name[0])!
            log = Double(name[1])!
            
        }
        MapNavigation.navgigateTogoogleMaps(latit: lat, logit: log) //@Locate MapNavigation
    }
    
}
