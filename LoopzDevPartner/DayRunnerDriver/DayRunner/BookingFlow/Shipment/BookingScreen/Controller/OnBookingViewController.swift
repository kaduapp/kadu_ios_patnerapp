//
//  OnBookingViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 15/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import JaneSliderControl
import RxSwift
//10: When the driver is on the way to pickup location
//11: when the driver arrived to pickup location
//12: when the driver started the trip after loading
//13: when the driver reached the drop location
//14: when the vehicle is unloaded




class OnBookingViewController: UIViewController {
    let locationManager = CLLocationManager()
    var location        = CLLocation()
    
    @IBOutlet weak var changeStore: UIButton!
    
    @IBOutlet weak var callOutlet: UIButton!
    var shipmentModel: Shipment!
    let disposebag = DisposeBag()
    let model   = Home.init() //Home model
    @IBOutlet var googleMaps: UIButton!
    @IBOutlet var getCurrentLocation: UIButton!
    
    @IBOutlet weak var detailsoutlet: UIButton!
    @IBOutlet weak var leftSlider: SliderControl!
    
    @IBOutlet var bookingAddress: UILabel!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet weak var deliveryStatusButton: UIButton!
    @IBOutlet var titleLabel: UILabel!

    @IBOutlet var distanceTitleLabel: UILabel!
    @IBOutlet var timeLeftTitleLabel: UILabel!
    @IBOutlet var timeLeft: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var addressImage: UIImageView!
    let source       = GMSMarker()
    let destination  = GMSMarker()
    var movingMarker = GMSMarker()
    
    var bookingMod = BookingModel()
    var invoiceData = [BookingModel]()
    
    var locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker

    
    
    let manager = CBLManager.sharedInstance()
    
    var status:Int        = 0
    var SelectedIndex:Int = 0
    var hours:Int         = 0
    var minutes:Int       = 0
    var seconds:Int       = 0
    var latitude:Double  = LocationManager.sharedInstance.latitude
    var longitude:Double = LocationManager.sharedInstance.longitude
    var travelledDistance:Double = 0.00
    
    var loadingTime     = Timer()
    
    var bookingDict:Home?
    var didFindMyLocation = false
    var isPathPlotted     = false
    @IBAction func actionbuttonBack(_ sender: UIButton) {
        
        let delivertSheduleType:Int = UserDefaults.standard.integer(forKey: "driverScheduleType")
        
        if delivertSheduleType == 1 {
            self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!,animated:true)
           
        }else {
             self.navigationController?.popToRootViewController(animated: true)
        }
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.callOutlet.setTitle("Call".localized, for: .normal)
        self.detailsoutlet.setTitle( "Details".localized, for: .normal)
        status = (shipmentModel?.orderStatus)!
        if status == 27 {
            self.callOutlet.isHidden = true
        }else {
            self.changeStore.isHidden = true
        }
        self.navigationController?.isNavigationBarHidden = false
        let ud = UserDefaults.standard
        latitude = ud.double(forKey:"currentLat")
        longitude = ud.double(forKey:"currentLog")
        self.updateViewAndMapCameraInitialise()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.makeTheHomeServiceCall()
//         updateCustomerInfoStatus()
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
        NotificationCenter.default.addObserver(self, selector: #selector(bookingCancelHandling(_:)), name: Notification.Name("cancelledBooking"), object: nil)
    }
//    func makeTheHomeServiceCall(){
//        model.assignedBookingAPI() //@locate Home model class
//         updateCustomerInfoStatus()
//    }
    override func viewDidAppear(_ animated: Bool) {
//        model.assignedBookingAPI() //@locate Home model class
        updateCustomerInfoStatus()
    }
    
    @IBAction func changeStore(_ sender: Any) {
        
         performSegue(withIdentifier: "goToStoreList", sender: self)
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "cancelledBooking"), object: nil);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Notifies when the booking has been cancelled*******//
 @objc func bookingCancelHandling(_ notification: NSNotification) {
        let bid = "Bid:" + String(describing:notification.userInfo!["bid"]!)
        let msg = notification.userInfo?["msg"] as! String
        Helper.alertVC(errMSG: bid + " " + msg)
        if String(describing:notification.userInfo!["bid"]!)  == String(describing:bookingDict!.bookingId) {
            _ = navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
    func updateViewAndMapCameraInitialise(){
        initiateMap()
        self.mapView.bringSubviewToFront(self.googleMaps)
        self.mapView.bringSubviewToFront(self.getCurrentLocation)
        
        

            if(status == 12){
                let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitude) ,longitude: CLLocationDegrees(longitude) , zoom: 16)
                self.mapView.animate(to: camera)
                leftSlider.sliderText = "ARRIVED".localized
//                deliveryStatusButton.setTitle("ARRIVED", for: .normal)
                
            }else if(status == 27){
                let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitude) ,longitude: CLLocationDegrees(longitude) , zoom: 16)
                self.mapView.animate(to: camera)
                leftSlider.sliderText = "JOB COMPLETED".localized
            }else{
                let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitude) ,longitude: CLLocationDegrees(longitude) , zoom: 16)
                self.mapView.animate(to: camera)
                 leftSlider.sliderText = "ARRIVED".localized
//                deliveryStatusButton.setTitle("ARRIVED", for: .normal)
                
            }

    }
    ///************ segue identifier method****************//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {

        if let nextScene = segue.destination as? PickingItemViewController {
            nextScene.status = 1
            nextScene.shipmentModel = shipmentModel
        }else if let nextScene = segue.destination as? LaundraPickingItemViewController {
            nextScene.status = 1
            nextScene.shipmentModel = shipmentModel
        }else if let nextScene = segue.destination as? ProviderInvoiceViewController {
            nextScene.shipmentModel = shipmentModel
        }
        else if let nav =  segue.destination as? UINavigationController
        {
            if let nextScene = nav.viewControllers.first as? JobDetailsController?{
                nextScene?.shipmentModel = shipmentModel
            }else if let nextScene = nav.viewControllers.first as? LaundraJobDetailsController?{
                nextScene?.shipmentModel = shipmentModel
            }else if let nextScene = nav.viewControllers.first as? SelectLaundraVC {
                nextScene.shipmentModel = shipmentModel
                nextScene.shipmentModel.commingFromHome = false
            }
            else if let viewController: ChatVC = nav.viewControllers.first as? ChatVC {
                
                viewController.bookingID = "\(shipmentModel.bookingId!)"
                viewController.custName = shipmentModel.customerName
                viewController.customerID = shipmentModel.customerId
                viewController.custImage = shipmentModel.customerPic
                
            }
            
            
        }
        
    }
    
    
    @IBAction func chatAction(_ sender: UIButton) {
        performSegue(withIdentifier: "toChatOB", sender: self)
    }
    
}

//MARK: - CllocationDelegates
extension OnBookingViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse
        {
            locationManager.startUpdatingLocation()
            mapView.isMyLocationEnabled = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
              didFindMyLocation = false
        latitude = coord.latitude
        longitude = coord.longitude
        UserDefaults.standard.set(latitude, forKey:"currentLat")
        UserDefaults.standard.set(longitude, forKey:"currentLog")
        
        var destinationLocation = CLLocation()
        destinationLocation = CLLocation(latitude: coord.latitude,  longitude: coord.longitude)
        updateLocationoordinates(coordinates: destinationLocation.coordinate)
        //@Locate onbookingUpdateMarkerPostionVC
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        let newDirection = newHeading.trueHeading
        self.movingMarker.rotation = newDirection
    }
}

//MARK: - GmsMapviewDelegates
extension OnBookingViewController: GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
        let point:CGPoint =  self.mapView.center
        self.mapView.projection.coordinate(for: point)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool)
    {
        print("willMove gesture")
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        print("changes the camera postion")
    }
}

//MARK: - CancelDelegate
extension OnBookingViewController:cancelBookingDelegate{
    func cancelReason() {
        _ = navigationController?.popToRootViewController(animated: true)
    }
}

