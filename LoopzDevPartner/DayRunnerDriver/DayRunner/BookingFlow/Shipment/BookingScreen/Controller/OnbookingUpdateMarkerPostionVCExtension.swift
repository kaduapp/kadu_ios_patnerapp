//
//  OnbookingUpdateMarkerPostionVC.swift
//  GoGoCargoDriver
//
//  Created by Vengababu Maparthi on 23/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import GoogleMaps

extension OnBookingViewController{
    //MARK: - update the marker on map and changes the path according to the lat longs
    func updateLocationoordinates(coordinates:CLLocationCoordinate2D) {
        if movingMarker.icon == nil
        {
            movingMarker = GMSMarker()
            movingMarker.position = coordinates
            let image = #imageLiteral(resourceName: "live_tracking_van_icon-ipad")
            movingMarker.icon = image
            movingMarker.map = self.mapView
            movingMarker.appearAnimation = .pop
        }
        else
        {
            
           let angle = GMSGeometryHeading(CLLocationCoordinate2D.init(latitude: CLLocationDegrees(UserDefaults.standard.double(forKey:"currentLat")), longitude: CLLocationDegrees(UserDefaults.standard.double(forKey:"currentLog"))), CLLocationCoordinate2D.init(latitude: CLLocationDegrees(coordinates.latitude), longitude: CLLocationDegrees(coordinates.longitude)))
            CATransaction.begin()
            CATransaction.setAnimationDuration(1.0)
            movingMarker.position =  coordinates
            movingMarker.rotation = angle
            CATransaction.commit()
        }
        
        var lat = Double()
        var lon = Double()
        if(ConstantUI.forUI == true){
            
        }else{
            if status == 7 || status == 8 {
                let latLongs = bookingDict?.pickLatlog
                let name = latLongs?.components(separatedBy: ",")
                lat = Double(name![0])!
                lon = Double(name![1])!
                
                if !isPathPlotted {
                    self .setMarkers(sourceLatitude: coordinates.latitude, sourceLongitude: coordinates.longitude, destinationLatitude: lat, destinationLongitude: lon)
                    isPathPlotted = true
                }
                
            }else{
                let latLongs = bookingDict?.dropLatlog
                let name = latLongs?.components(separatedBy: ",")
                lat = Double(name![0])!
                lon = Double(name![1])!
                
                if isPathPlotted {
                    self .setMarkers(sourceLatitude: coordinates.latitude, sourceLongitude: coordinates.longitude, destinationLatitude: lat, destinationLongitude: lon)
                    isPathPlotted = false
                }
            }
        }
    }
    
    //MARK: - initiate the source adn drop location
    func setMarkers(sourceLatitude: Double,
                    sourceLongitude: Double,
                    destinationLatitude: Double,
                    destinationLongitude: Double) {
        
        setDestination(latitude: destinationLatitude,
                       longitude: destinationLongitude)
        
        addPolyline(sourceLatitude: sourceLatitude,
                    sourceLongitude: sourceLongitude,
                    destinationLatitude: destinationLatitude,
                    destinationLongitude: destinationLongitude)
    }
    
    
    //MARK: - Draws the route from source to destination
    private func addPolyline(sourceLatitude: Double,
                             sourceLongitude: Double,
                             destinationLatitude: Double,
                             destinationLongitude: Double) {
        
        let origin = "\(sourceLatitude),\(sourceLongitude)"
        let destination = "\(destinationLatitude),\(destinationLongitude)"
        let API_KEY = SERVER_CONSTANTS.serverKey // // Get the Key/Server from Google Developer Console
        
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(API_KEY)"
        
        // Call Service
        //        NetworkHelper.requestPOST(urlString: url,
        //                                  success: { (response) in
        //
        //                                    if response.isEmpty == false {
        //
        //                                        if let routes: [AnyObject] = response["routes"].arrayObject as [AnyObject]? {
        //
        //                                            for route in routes
        //                                            {
        //                                                let dict: [String: AnyObject] = route as! [String : AnyObject]
        //
        //                                                let routeOverviewPolyline: [String: AnyObject] = dict["overview_polyline"] as! [String : AnyObject]
        //                                                let points: String = routeOverviewPolyline["points"] as! String
        //                                                let path = GMSPath.init(fromEncodedPath: points)
        //                                                let polyline = GMSPolyline.init(path: path)
        //
        //                                                polyline.strokeColor = UIColor.black.withAlphaComponent(0.6)
        //                                                polyline.geodesic = true
        //                                                polyline.strokeWidth = 5.0
        //
        //
        //                                            }
        //                                        }
        //                                    }
        //                                    else {
        //                                    }
        //        },
        //                                  failure: { (Error) in
        //
        //        })
        //        setBounds()
    }
    
    /// Set Bounds                      (Call 4)
    /// - Fit Two marker in limited area
    private func setBounds() {
        
        let bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: movingMarker.position,
                                                              coordinate: destination.position)
        mapView.animate(with: GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 64 + 10, left: 10, bottom: 10, right: 10)))
    }
    
    /// Set Destination Marker          (Call 2)
    ///
    /// - Parameters:
    ///   - latitude: Destination Latitude
    ///   - longitude: Destination Longitude
    private func setDestination(latitude: Double, longitude: Double) {
        
        destination.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        destination.snippet = "Destination"
        destination.map = mapView
        if status == 7 || status == 8 {
            destination.icon =  #imageLiteral(resourceName: "pick_marker")
        }else{
            destination.icon = #imageLiteral(resourceName: "drop_marker")
        }
    }
    
}
