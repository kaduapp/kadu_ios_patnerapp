//
//  PickupDeliveryDetailsCell.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 08/12/2017.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class PickupDeliveryDetailsCell: UITableViewCell {

    @IBOutlet weak var storeAddress: UILabel!
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var customerAddress: UILabel!
    @IBOutlet weak var customerName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
