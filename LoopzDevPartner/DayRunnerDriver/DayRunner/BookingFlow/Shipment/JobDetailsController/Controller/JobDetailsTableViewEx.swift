//
//  JobDetailsTableViewEx.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 08/12/2017.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

enum JobDetailsTableViewSections: Int
{
    case CustomerDetails = 0
    case Adresses = 1
    case ItemsHeader = 6
    case Items = 7
    case BreakDown = 8
    case Subtotal = 9
    case Tax = 10
    case Total = 11
    case Payment = 2
    case EstimatedValue = 3
    case CustomerNote = 5
    case CustomerBreakDown = 4
}

extension JobDetailsController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 12
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print(section)
        
        let numberOfCells: JobDetailsTableViewSections = JobDetailsTableViewSections.init(rawValue: Int(section))!
        switch numberOfCells {
        case .CustomerDetails:
            return 1
        case .Adresses:
            return 1
        case .ItemsHeader:
            return 1
        case .Items:
            return shipmentModel.productDetails.count
        case .Total:
            return 3
        case .Tax:
            return 1
        case .Subtotal:
            return 4
        case .Payment:
            return 1
        case .BreakDown:
            return  1
        case .CustomerNote:
            return 1
        case .CustomerBreakDown :
            return 1
        case .EstimatedValue :
            return 1
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.row)
        
        
        let section: JobDetailsTableViewSections = JobDetailsTableViewSections.init(rawValue: Int(indexPath.section))!
        
        switch section {
            
        case .CustomerDetails:
            let cell: CustomerDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: CustomerDetailsTableViewCell.self), for: indexPath) as! CustomerDetailsTableViewCell
            cell.customerName.text = shipmentModel.customerName
            return cell
            
        case .Adresses:
            let cell: JobStartedAdressesTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedAdressesTableViewCell.self), for: indexPath) as! JobStartedAdressesTableViewCell
            if(shipmentModel.storeType == 7){
                let address = "\("") \(shipmentModel.pickupAddress)"
                let range = NSMakeRange(shipmentModel.pickupStore.count, (address.count - shipmentModel.pickupStore.count))
                cell.pickUpAddress.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
                cell.deliveryAddress.text = shipmentModel.deliveryAddress
            }else {
                let address = "\(shipmentModel.pickupStore): \(shipmentModel.pickupAddress)"
                let range = NSMakeRange(shipmentModel.pickupStore.count, (address.count - shipmentModel.pickupStore.count))
                cell.pickUpAddress.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
                cell.deliveryAddress.text = shipmentModel.deliveryAddress
            }
            return cell
            
        case .ItemsHeader:
            let cell: JobStartedItemHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedItemHeaderTableViewCell.self), for: indexPath) as! JobStartedItemHeaderTableViewCell
            if(shipmentModel.storeType == 7){
                cell.qtyLabel.isHidden = true
                cell.priceLabel.text = "Quantity"
            }else {
                cell.priceLabel.text = " PRICE ".localized + "(  " + Utility.currencySymbol + "  )"
            }
            return cell
            
        case .Items:
            let cell: JobStartedItemsTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedItemsTableViewCell.self), for: indexPath) as! JobStartedItemsTableViewCell
            if let products = shipmentModel.productDetails[indexPath.row] as? [String: Any]{
                
                if let itemName = products["itemName"] as? String {
                    var quantity = 0
                    var unit = ""
                    if let quant = products["quantity"] as? Int {
                        quantity = quant
                    }
                    if let unitName = products["unitName"] as? String {
                        unit = unitName
                    }
                    if let itemImageURL = products["itemImageURL"] as? String{
                        if itemImageURL == ""{
                            
                        }else{
                        let url = URL(string: itemImageURL)
                        DispatchQueue.global().async { [weak self] in
                            if let data = try? Data(contentsOf: url!) {
                                if let image = UIImage(data: data) {
                                    DispatchQueue.main.async {
                                        cell.itemImageView.image = image
                                    }
                                }
                            }
                        }
                        }
                    }
                    let addOnsArray = products["addOns"] as? [[String:Any]]
                    //                    newAddArray = (products["addOns"] as? [[String:Any]])!
                    
                    if addOnsArray?.count == 0 {
                        cell.addOnsLabelHide.isHidden = true
                    }else {
                         cell.addOnsLabelHide.isHidden = false
                    }
                    newOneData = addOnsArray!
                    cell.newOne(data: addOnsArray!)
                    
                    cell.productAndQuantity.text = itemName
                    if(shipmentModel.storeType == 7) {
                        cell.priceOfProduct.text =  "\(quantity)"
                        cell.priceOfProduct.textAlignment = .center
                        cell.qty.isHidden = true
                    }
                    cell.qty.text =  "\(quantity)"
//                    cell.unit.text = "\(unit)"
                    
                }
                
                if let itemPrice = products["finalPrice"] as? NSNumber {
                    if(shipmentModel.storeType == 7) {
                        
                    }else {
                        cell.priceOfProduct.text = Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(itemPrice) , digits: 2))
                    }
                }
            }
            return cell
        case .Tax:
            let cell: JobTaxTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobTaxTableViewCell.self), for: indexPath) as! JobTaxTableViewCell
            cell.newOneTax(data:shipmentModel.exculsiveTax)
            cell.taxTableView.reloadData()
            return cell
        case .Subtotal:
            
            switch indexPath.row {
            case 0:
                let cell: JobSubtotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobSubtotalTableViewCell.self), for: indexPath) as! JobSubtotalTableViewCell
                 cell.subtotalOutLet.text = "Sub Total".localized
                cell.subtotalLabel.text =  Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.subTotal!) , digits: 2))
                return cell
            case 1:
                let cell: JobSubtotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobSubtotalTableViewCell.self), for: indexPath) as! JobSubtotalTableViewCell
                cell.subtotalOutLet.text = "Delivery Charges".localized
                cell.subtotalLabel.text =  Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.deliveryCharge!) , digits: 2))
                return cell
                
            case 2:
                let cell: JobSubtotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobSubtotalTableViewCell.self), for: indexPath) as! JobSubtotalTableViewCell
                cell.subtotalOutLet.text = "Discount".localized
                cell.subtotalLabel.text =  Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.discount!) , digits: 2))
                return cell
                
                
            default:
                let cell: JobSubtotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobSubtotalTableViewCell.self), for: indexPath) as! JobSubtotalTableViewCell
                cell.subtotalOutLet.text = "Tips".localized
                cell.subtotalLabel.text =  Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.drivertip!) , digits: 2))
                return cell
            }
            
            
        case .Total:
        
            switch indexPath.row {
            case 0:
                let cell: JobStartedTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedTotalTableViewCell.self), for: indexPath) as! JobStartedTotalTableViewCell
                cell.dividerLabel.isHidden = false
                cell.bottonGrandView.isHidden = true
                cell.grandtotalLabel.text = "Wallet :".localized
                let walletAmount:Double = shipmentModel.grandTotal - shipmentModel.cashCollect
                cell.grandTotal.text =  Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(walletAmount) , digits: 2))
                return cell
            case 1:
                let cell: JobStartedTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedTotalTableViewCell.self), for: indexPath) as! JobStartedTotalTableViewCell
                cell.bottonGrandView.isHidden = true
                cell.dividerLabel.isHidden = true
                cell.grandtotalLabel.text = "Cash :".localized
                cell.grandTotal.text =  Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.cashCollect) , digits: 2))
                return cell
                
            default:
                let cell: JobStartedTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedTotalTableViewCell.self), for: indexPath) as! JobStartedTotalTableViewCell
                cell.dividerLabel.isHidden = true
                cell.bottonGrandView.isHidden = false
                cell.grandtotalLabel.text = "Grand Total :"
                cell.grandTotal.text = " " +   Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.grandTotal!) , digits: 2))
                return cell
            }
            
        case .Payment:
            let cell: PaymentTypeCell = tableView.dequeueReusableCell(withIdentifier: String(describing: PaymentTypeCell.self), for: indexPath) as! PaymentTypeCell
            
            let paymentType = shipmentModel.paymentType
            let payByWallet = shipmentModel.payByWallet
            if payByWallet == 0 {
                if paymentType == 1 {
                    cell.paymentType.text = "Card".localized
                }else if paymentType == 2 {
                    cell.paymentType.text = "Cash".localized
                }else {
                    cell.paymentType.text = "Wallet".localized
                }
            }else {
                if paymentType == 1 {
                    cell.paymentType.text = "Card".localized + "Wallet".localized
                }else if paymentType == 2 {
                    cell.paymentType.text = "Cash".localized + "Wallet".localized
                }else {
                    cell.paymentType.text = "Wallet".localized
                }
            }
            
            return cell
        case .BreakDown:
            let cell: BreakDownTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: BreakDownTableViewCell.self), for: indexPath) as! BreakDownTableViewCell
            return cell
            
        case .EstimatedValue:
            let cell: CustomerDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: CustomerDetailsTableViewCell.self), for: indexPath) as! CustomerDetailsTableViewCell
            cell.customerNameLabel.text = "ESTIMATE PACKAGE VALUE"
            cell.customerName.text = Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.estimatedPackageValue) , digits: 2))
            return cell
        case .CustomerBreakDown:
            let cell: BreakDownTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: BreakDownTableViewCell.self), for: indexPath) as! BreakDownTableViewCell
            
            cell.paymentBreakDownLabel.text = "CUSTOMER NOTES"
            cell.paymentBreakDownLabel.font = UIFont(name:"ClanPro-NarrNews", size: 12.0)
            return cell
        case .CustomerNote :
            let cell: CustomerNotesTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: CustomerNotesTableViewCell.self), for: indexPath) as! CustomerNotesTableViewCell
            cell.customerNotesLabel.text = shipmentModel.extraNote
            return cell
        }
        
    }
    
}

extension JobDetailsController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sections: JobDetailsTableViewSections = JobDetailsTableViewSections.init(rawValue: Int(indexPath.section))!
        switch sections {
        case .CustomerDetails:
            return 60
            
        case .Adresses:
            return 140
            
        case .ItemsHeader:
            return 57
            
        case .Items:
            let products = shipmentModel.productDetails[indexPath.row]
            if let isAddOnAvailable = products["addOnAvailable"] as? Bool {
                
                var addOnGroup = [[String:Any]]()
                 let addOnsArray = products["addOns"] as? [[String:Any]]
                let newAddOnGroups = newOneData.map {
                    
                    $0["addOnGroup"] as! Array<[String:AnyObject]>
                    
                }
                
                addOnGroup = newAddOnGroups.flatMap({$0})
                
                if isAddOnAvailable {
                    return CGFloat(30 * addOnGroup.count) + 128
                } else {
                    if addOnsArray?.count == 0 {
                        return UITableView.automaticDimension
                    }else {
                        return 80
                    }
                }
            }
            return UITableView.automaticDimension
        case .Total:
            switch indexPath.row {
            case 0,1:
                return 30
            default:
                return 44
            }
            
        case .Payment:
            return 46
        case .Tax:
            if shipmentModel.exculsiveTax.count == 0 {
                return 0
            }
            return 88
            
        case .Subtotal:
            if(shipmentModel.storeType == 7) {
                if( indexPath.row == 0 ) {
                    return 0
                }
            }
            return 30
        case .BreakDown:
            return 44
        case .EstimatedValue:
            if(shipmentModel.storeType == 7){
                return 60
            }
            return 0
        case .CustomerNote:
            if(shipmentModel.storeType == 7){
                return UITableView.automaticDimension
            }
            return 0
        case .CustomerBreakDown :
            if(shipmentModel.storeType == 7){
                return 44
            }
            return 0
        }
    }
    
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //
    //    }
}
