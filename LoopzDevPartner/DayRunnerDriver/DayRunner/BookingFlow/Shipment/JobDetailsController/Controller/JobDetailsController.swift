//
//  JobDetailsController.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 08/12/2017.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class JobDetailsController: UIViewController {
    
    @IBOutlet weak var jobdetailsLabel: UILabel!
    @IBOutlet weak var jobId: UILabel!
    var shipmentModel: Shipment!
    var newOneData = [[String:Any]]()
    var newAddArray = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        let num = shipmentModel.bookingId
        jobId.text = "Id: \(num!)"
        jobdetailsLabel.text = "Job Details".localized
        for shipments in (shipmentModel.productDetails)!  {
            
            print(shipments)
            
        }
//        print(newAddArray.count);

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func chatAction(_ sender: UIButton) {
        performSegue(withIdentifier: "toChatJD", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if let nav =  segue.destination as? UINavigationController
        {
            if let viewController: ChatVC = nav.viewControllers.first as! ChatVC? {
                
                viewController.bookingID = "\(shipmentModel.bookingId!)"
                viewController.custName = shipmentModel.customerName
                viewController.customerID = shipmentModel.customerId
                viewController.custImage = shipmentModel.customerPic
                
            }
            
            
        }
        
    }

    
}
