//
//  LaundraJobDetailsController.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 4/9/19.
//  Copyright © 2019 3Embed. All rights reserved.
//
import UIKit

class LaundraJobDetailsController: UIViewController {
    
    @IBOutlet weak var jobId: UILabel!
    var shipmentModel: Shipment!
    var newOneData = [[String:Any]]()
    var newAddArray = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        let num = shipmentModel.bookingId
        jobId.text = "Id: \(num!)"
        
        for shipments in (shipmentModel.productDetails)!  {
            
            print(shipments)
            
        }
        //        print(newAddArray.count);
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func chatAction(_ sender: UIButton) {
        performSegue(withIdentifier: "toChatJD", sender: self)
    }
    
    @IBAction func dismissAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func callToCustomer(_ sender: UIButton) {
        
        let dropPhone = "tel://" + (shipmentModel.customerPhone)
        if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nav =  segue.destination as? UINavigationController
        {
            if let viewController: ChatVC = nav.viewControllers.first as! ChatVC? {
                
                viewController.bookingID = "\(shipmentModel.bookingId!)"
                viewController.custName = shipmentModel.customerName
                viewController.customerID = shipmentModel.customerId
                viewController.custImage = shipmentModel.customerPic
                
            }
            
            
        }
        
    }
    
    
}
