//
//  JobDetailsTableViewEx.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 08/12/2017.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
enum LaundraJobDetailsTableViewSections: Int
{
    case CustomerDetails = 3
    case Adresses = 1
    case AddressSlot = 2
    case LaundraName = 0
    case DeliveryLocation = 4
    case ItemsHeader = 5
    case Items = 6
    case Weight = 7
    case Payment = 8
}

extension LaundraJobDetailsController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print(section)
        
        let numberOfCells: LaundraJobDetailsTableViewSections = LaundraJobDetailsTableViewSections.init(rawValue: Int(section))!
        switch numberOfCells {
        case .CustomerDetails:
            return 1
            
        case .Adresses:
            return 1
            
        case .AddressSlot:
            return 1
            
        case .ItemsHeader:
            return 1
            
        case .Items:
            return shipmentModel.productDetails.count
//            return 5
            
        case .Weight:
            return 1
        case .LaundraName :
            return 1
            
        case .DeliveryLocation :
            return 1
            
        case .Payment:
            return 1
            
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.row)
        
        
        let section: LaundraJobDetailsTableViewSections = LaundraJobDetailsTableViewSections.init(rawValue: Int(indexPath.section))!
        
        switch section {
            
        case .CustomerDetails:
            let cell: CustomerDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: CustomerDetailsTableViewCell.self), for: indexPath) as! CustomerDetailsTableViewCell
            cell.customerName.text = shipmentModel.customerName
            return cell
            
        case .Adresses:
            let cell: JobStartedAdressesTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedAdressesTableViewCell.self), for: indexPath) as! JobStartedAdressesTableViewCell
            cell.controller = false
            cell.addressLabel.text = "PICKUP LOCATION"
//
//            if shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra {
//
//            }
//
//
            if shipmentModel.pickupStore == "" && shipmentModel.pickupAddress == "" {
                cell.pickUpAddress.text = "PICKUP LOCATION"
            }else {
                
                if shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra {
                    
                    let address = "\(shipmentModel.pickupStore): \(shipmentModel.pickupAddress)"
                    let range = NSMakeRange(shipmentModel.pickupStore.count, (address.count - shipmentModel.pickupStore.count))
                    cell.pickUpAddress.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
                    
                } else  {
                    let address = "\(self.shipmentModel.customerName): \(shipmentModel.deliveryAddress)"
                    let range = NSMakeRange(self.shipmentModel.customerName.count, (address.count - shipmentModel.customerName.count))
                    cell.pickUpAddress.attributedText = Helper.attributedString(from: address, nonBoldRange: range)
                }
                
            }
            
            
            //            cell.deliveryAddress.text = shipmentModel.deliveryAddress
            return cell
        case .AddressSlot:
            let cell: JobStartedAdressesTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedAdressesTableViewCell.self), for: indexPath) as! JobStartedAdressesTableViewCell
            cell.controller = false;
            if shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra {
                 cell.addressLabel.text = "DELIVERY SLOT"
            }else {
                 cell.addressLabel.text = "PICKUP SLOT"
            }
           
            
            let date = NSDate(timeIntervalSince1970: TimeInterval(shipmentModel.slotStartTime))
            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "hh:mm a"
            let dateString = dayTimePeriodFormatter.string(from: date as Date)
            
            let date1 = NSDate(timeIntervalSince1970: TimeInterval(shipmentModel.slotEndTime))
            let dayTimePeriodFormatter1 = DateFormatter()
            dayTimePeriodFormatter1.dateFormat = "hh:mm a"
            let dateString1 = dayTimePeriodFormatter1.string(from: date1 as Date)
            
            
            
            if shipmentModel.pickupStore == "" && shipmentModel.pickupAddress == "" {
                cell.pickUpAddress.text = "PICKUP SLOT"
            }else {
               
                cell.pickUpAddress.text = dateString + "-" + dateString1
            }
            
            
            //            cell.deliveryAddress.text = shipmentModel.deliveryAddress
            return cell
        case .ItemsHeader:
            let cell: JobStartedItemHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedItemHeaderTableViewCell.self), for: indexPath) as! JobStartedItemHeaderTableViewCell
            cell.controller = false
            return cell
            
        case .Items:
            let cell: JobStartedItemsTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedItemsTableViewCell.self), for: indexPath) as! JobStartedItemsTableViewCell
            cell.controller = false
            let products = shipmentModel.productDetails[indexPath.row]
            if let itemName = products["itemName"] as? String {
                
                var quantity = 0
                if let quant = products["quantity"] as? Int {
                    quantity = quant
                }
                
                if let itemImageURL = products["itemImageURL"] as? String{
                    if itemImageURL == ""{
                        
                    }else{
                    let url = URL(string: itemImageURL)
                    DispatchQueue.global().async { [weak self] in
                        if let data = try? Data(contentsOf: url!) {
                            if let image = UIImage(data: data) {
                                DispatchQueue.main.async {
                                    cell.itemImageView.image = image
                                }
                            }
                        }
                    }
                    }
                }
                
                var allAddOnsArray:String = ""
                if let addOnsArray = products["addOns"] as? [[String:Any]] {
                    
                    for eachAddOns in addOnsArray {
                        if let addOnGroup = eachAddOns["addOnGroup"] as? [[String:Any]] {
                            for eachAddOnGroup in addOnGroup {
                                if let addOnName = eachAddOnGroup["name"] as? String {
                                    
                                    allAddOnsArray = allAddOnsArray + addOnName + ","
                                }
                            }
                        }
                    }
                }
                cell.productAndQuantity.text = itemName + "  X  " + String(quantity)
            }

            return cell
            
//        case .Total:
//            let cell: JobStartedTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: JobStartedTotalTableViewCell.self), for: indexPath) as! JobStartedTotalTableViewCell
//            //            cell.grandTotal.text = "Grand Total: " +   Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.grandTotal!) , digits: 2))
//            cell.grandTotal.text = Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(shipmentModel.grandTotal!) , digits: 2))
//            return cell
            
        case .Weight:
            let rightCell: WeightLaundraCell = tableView.dequeueReusableCell(withIdentifier: String(describing: WeightLaundraCell.self), for: indexPath) as! WeightLaundraCell
            if (shipmentModel.orderStatus == 27) {
                rightCell.weightLaundra.text = String(shipmentModel.weight) + shipmentModel.weightText + " weight"
            }else if(shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra){
                  rightCell.weightLaundra.text = String(shipmentModel.weightPicked) + shipmentModel.weightText + " weight"
            }else {
                  rightCell.weightLaundra.text = String(shipmentModel.weight) + shipmentModel.weightText + " weight"
            }
          
    
            return rightCell
            
        case .LaundraName:
            let cell: LaundramartCell = tableView.dequeueReusableCell(withIdentifier: String(describing: LaundramartCell.self), for: indexPath) as! LaundramartCell
            cell.laundraname.text = shipmentModel.pickupStore
            return cell
            
        case .DeliveryLocation:
            let cell: DropLaundraCell = tableView.dequeueReusableCell(withIdentifier: String(describing: DropLaundraCell.self), for: indexPath) as! DropLaundraCell
            cell.deliveryDrop.text = shipmentModel.deliveryAddress
            return cell
            
        case .Payment:
            let cell: PaymentTypeCell = tableView.dequeueReusableCell(withIdentifier: String(describing: PaymentTypeCell.self), for: indexPath) as! PaymentTypeCell
            cell.controller = false
            cell.headerLabel.text = "PAYMENT METHOD"
            let paymentType = shipmentModel.paymentType
            if paymentType == 1 {
                cell.paymentType.text = "Card"
            } else if paymentType == 2 {
                cell.paymentType.text = "Cash"
            } else {
                cell.paymentType.text = "WALLET"
            }
            
            return cell
            
            
        default:
            return UITableViewCell()
        }
        
    }
    
}

extension LaundraJobDetailsController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sections: LaundraJobDetailsTableViewSections = LaundraJobDetailsTableViewSections.init(rawValue: Int(indexPath.section))!
        
        if (shipmentModel.orderStatus == 27) {
            switch sections {
                
            case .CustomerDetails:
                return 67
                
            case .Adresses:
                return 82
                
            case .AddressSlot:
                if shipmentModel.expressType {
                    return 110
                }else {
                    return 82
                }
            
            case .ItemsHeader:
                return 35
                
            case .Items:
                let products = shipmentModel.productDetails[indexPath.row]
                if let isAddOnAvailable = products["addOnAvailable"] as? Bool {
                    if isAddOnAvailable {
                        return 120
                    } else {
                        return 88
                    }
                }
                return 62
                
            case .Payment:
                return 85
                
            case .Weight:
                return 60
            case .LaundraName:
                return 60
            case .DeliveryLocation:
                return 110
            }
        }else if(shipmentModel.storeType == 5 && shipmentModel.bookingTypeLaundra){
            switch sections {
                
            case .CustomerDetails:
                return 67
                
            case .Adresses:
                return 83
                
            case .AddressSlot:
                
                if shipmentModel.expressType {
                     return 110
                }else {
                     return 82
                }
               
                
                
            case .ItemsHeader:
                return 35
                
            case .Items:
                let products = shipmentModel.productDetails[indexPath.row]
                if let isAddOnAvailable = products["addOnAvailable"] as? Bool {
                    if isAddOnAvailable {
                        return 120
                    } else {
                        return 88
                    }
                }
                return 35
                
            case .Payment:
                return 85
                
            case .Weight:
                return 60
            case .LaundraName:
                return 60
            case .DeliveryLocation:
                return 110
            }
            
        }else {
            switch sections {
            case .CustomerDetails:
                return 67
                
            case .Adresses:
                return 82
                
            case .AddressSlot:
                if shipmentModel.expressType {
                        return 110
                }else {
                        return 82
                }
            case .ItemsHeader:
                return 35
                
            case .Items:
                let products = shipmentModel.productDetails[indexPath.row]
                if let isAddOnAvailable = products["addOnAvailable"] as? Bool {
                    if isAddOnAvailable {
                        return 120
                    } else {
                        return 88
                    }
                }
                return 35
                
            case .Payment:
                return 85
                
            case .Weight:
                return 0
            case .LaundraName:
                return 0
            case .DeliveryLocation:
                return 0
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section: LaundraJobDetailsTableViewSections = LaundraJobDetailsTableViewSections.init(rawValue: Int(indexPath.section))!
        
        if section == .Items {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let rideBookingController = storyboard.instantiateViewController(withIdentifier: "NewProductVC") as! NewProductVC;
            rideBookingController.arrayOfImages = shipmentModel.productDetails[indexPath.row]
            self.present(rideBookingController, animated: true, completion: nil)
        }
    }
}

