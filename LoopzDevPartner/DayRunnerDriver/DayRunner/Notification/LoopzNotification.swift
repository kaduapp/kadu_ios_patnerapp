//
//  LoopzNotification.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 17/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import UserNotifications

class LoopzNotification: NSObject {
    
    /// Shared instance object for gettting the singleton object
    static let sharedInstance = LoopzNotification()
    let notificationDelegate = LoopzNotificationDelegate()
    
    
     /// Used for asking permission from user.
    ///
    /// - Parameter completion: Return the granted access in boolean form.
     func requestPermissionsWithCompletionHandler(completion: ((Bool) -> (Void))? ) {
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]) {[weak self] (granted, error) in
            
            guard error == nil else {
                
                completion?(false)
                return
            }
            
            if granted {
                // If permission is granted then it will be going to add the categories. And delegates
                
                UNUserNotificationCenter.current().delegate = self?.notificationDelegate
                self?.setNotificationCategories()
                
            }
            
            completion?(granted)
        }
    }
    
     func setNotificationCategories() {
        
        /// Options is used for showing the type of action on the notification.
        let acceptAction = UNNotificationAction(identifier: "accept", title: "Accept", options: [.foreground])
        let rejectAction = UNNotificationAction(identifier: "reject", title: "Reject", options: [.destructive])

        let localCat =  UNNotificationCategory(identifier: "booking", actions: [acceptAction,rejectAction], intentIdentifiers: [], options: [])
        
        UNUserNotificationCenter.current().setNotificationCategories([localCat])
        
    }

}


