//
//  MQTTNotificationDelegate.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 29/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UserNotifications
import CocoaLumberjack

class LoopzNotificationDelegate: NSObject, UNUserNotificationCenterDelegate {
    
    var bookingDict = [String:Any]()
    var newbookingModel = NewbookingModel()
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let state: UIApplication.State = UIApplication.shared.applicationState
        if state == .background || state == .inactive {
            
            // Play sound and show alert to the user
            completionHandler([.alert,.sound,.badge])
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let bookings = response.notification.request.content.userInfo
        print(bookings)
        let data = Helper.convertToDictionary(text: bookings[AnyHashable("data")] as? String ?? "")
        
        var bookingsDict = [String: Any]()
        bookingsDict = data!
  
        if let dict = data?["bookingData"] as? [String: Any]  {
            
            bookingsDict = dict
            
            
            let userDef = UserDefaults.standard
            
            if let currencySymbol = dict["currencySymbol"] as? String {
                userDef.set(currencySymbol, forKey: USER_INFO.CURRENCYSYMBOL)
            }
            
            if let mileageMetric = dict["mileageMetric"] as? String {
                userDef.set(mileageMetric, forKey: USER_INFO.DISTANCE)
            }
            self.bookingDict = dict
        }
        
        if let action = bookings["action"] as? String {
            
            if action == "1" {
                
                //12 - session out
                if let controllerNav = UIStoryboard(name: "ChatUI", bundle: nil).instantiateViewController(withIdentifier: "FirstNav") as? UINavigationController {
                    let controller = controllerNav.viewControllers.first as! ChatVC
                    controller.bookingID = String(describing:bookingsDict["bid"]!)
                    
                    if let name =  bookingsDict["name"] as? String{
                        controller.custName = name
                    }
                    
                    if let custImage  = bookingsDict["profilePic"] as? String{
                        controller.custImage = custImage
                    }
                    
                    if let custID  = bookingsDict["fromID"] as? String{
                        controller.customerID = custID
                    }
                    self.getController().1.present(controllerNav, animated: true, completion: nil)
                }
            }
            
        }
        
        
        // Determine the user action
        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            DDLogDebug("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            DDLogDebug("Default")
        case "Snooze":
            DDLogDebug("Snooze")
        case "Delete":
            DDLogDebug("Delete")
        case "accept":
            respondingToAppointment(status:"8")
            break
        case  "reject":
            respondingToAppointment(status:"9")
            break
            
        default:
            DDLogDebug("Unknown action")
        }
        completionHandler()
    }
    
    
    func getController() -> (Bool,UIViewController) {
        let vc = Helper.finalController()
        if (vc is ChatVC) {
            return (true,vc)
        }
        return (false,vc)
    }
    
    //MARK: - RespondtoAppointmentApi
    func respondingToAppointment(status:String) {
        let ud = UserDefaults.standard
        Helper.showPI(message:"Loading..")
        let params = NewbookingModel.init(status: status, bookingID: bookingDict["orderId"]!, latitude:ud.double(forKey:"currentLat") , longitude: ud.double(forKey:"currentLog"))
        
        
        
        if Utility.OrderId != bookingDict["orderId"] as! Double {
            newbookingModel.respondToNewBooking(params: params, completionHandler: { success in
                if success{
                    if status == "8"{
                        // Define identifier
                        let notificationName = Notification.Name("gotNewBooking")
                    
                        // Post notification
                        NotificationCenter.default.post(name: notificationName, object: nil)
                    
                        NewBookingPopup.instance.hide()
                        ud.set(self.bookingDict["orderId"] as! Double, forKey: USER_INFO.ORDERID)
                    }else{
                        NewBookingPopup.instance.hide()
                    }
                
                }
            })
        } else {
            Helper.hidePI()
        }
    }
    
}


