//
//  WalletHomeVC.swift
//  Rinn
//
//  Created by 3 Embed on 22/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class WalletHomeVC: UIViewController {
    
    @IBOutlet weak var balanceHead: UILabel!
    @IBOutlet weak var balance: UILabel!
    @IBOutlet weak var separatorBalance: UIView!
    @IBOutlet weak var lightLimit: UILabel!
    @IBOutlet weak var hardLimit: UILabel!
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var headerSeparator: UIView!
    @IBOutlet weak var listHead: UILabel!
    @IBOutlet weak var footerHead: UILabel!
    @IBOutlet weak var moneyView: UIView!
   
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var moneyTF: UITextField!
    @IBOutlet weak var moneySeparator: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomButton: UIButton!
    @IBOutlet weak var bottomButtonView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var softColorView: UIView!
    @IBOutlet weak var softText: UILabel!
    @IBOutlet weak var hardColorView: UIView!
    @IBOutlet weak var hardText: UILabel!
    @IBOutlet weak var voucherButton: UIButton!
    
    let disposeBag = DisposeBag()
    //Add Money
    var paymentMethord:Int = 0
    var isComingCheckout = false
    var delegate: PaymentVCDelegate? = nil
    var cardArray   = [Card]()
    let responseData = PublishSubject<Card>()
    var amount:Float = 0
    var addedAmount = ""
    
    var amountIn:Float = 0
    var selectedCard = 0
    static let addMoneyResp = PublishSubject<Bool>()
    var navView = NavigationView().shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        voucherButton.isHidden = true
        mainTableView.backgroundColor = UIColor.white
        self.amount = 200.0
        validateUI()
        setBalance()
//        self.navigationController?.navigationItem.title = "Wallet"
        self.navigationController?.navigationBar.tintColor = UIColor.white
        // Do any additional setup after loading the view.
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: "Wallet".localized)
            navView.tintColor = UIColor.white
        }
    }
    
    @IBAction func slideActionBtn(_ sender: Any) {
        if Utility.getSelectedLanguegeCode() == "ar" {
            slideMenuController()?.toggleRight()
            
        }else {
            slideMenuController()?.toggleLeft()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       addObserver()
        WalletAPICalls.getBalance().subscribe(onNext: { [weak self]data in
            self?.amount = Utility.getWallet().Amount
            self?.setBalance()
        }).disposed(by: disposeBag)
        CardAPICalls.getCard().subscribe(onNext: { [weak self]data in
            self?.cardArray = data
            self?.mainTableView.reloadData()
        }).disposed(by: disposeBag)
    }
    
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
             /// remove Observer
    func removeObserver() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
            //Mark:- Keyboard ANimation
    @objc func keyboardWillShow(_ notification: NSNotification){
        // Do something here
        
        let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as! NSNumber
        
        self.view.setNeedsLayout()
        
        UIView.animate(withDuration: TimeInterval(truncating: duration), delay: 0, options: [UIView.AnimationOptions(rawValue: UInt(truncating: curve))], animations: {
            
            let info = notification.userInfo!
            let inputViewFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let screenSize: CGRect = UIScreen.main.bounds
            var frame = self.view.frame
            frame.size.height = screenSize.height - inputViewFrame.size.height
            self.view.frame = frame
            
            
            self.view.layoutIfNeeded()
        }, completion: nil)
        
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification){
        let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as! NSNumber
        
        self.view.setNeedsLayout()
        
        UIView.animate(withDuration: TimeInterval(truncating: duration), delay: 0, options: [UIView.AnimationOptions(rawValue: UInt(truncating: curve))], animations: {
            let screenSize: CGRect = UIScreen.main.bounds
            var frame = self.view.frame
            frame.size.height = screenSize.height
            self.view.frame = frame
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
        removeObserver()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction  func addAction(_ sender: Any){
        if cardArray.count == 0 {
            Helper.showAlert(message: "No Cards Available", head: "Error", type: 1)
            return
        }
        if let amountIn = moneyTF.text?.floatValue {
            if amountIn > 0 {
                
                Helper.showAlertReturn(message: "Wallet Confirmation " + String(describing: Helper.clipDigit(value: amountIn, digits: 2)) + " ?", head: "Confirm", type: "Confirm", closeHide: false, responce: CommonAlertView.ResponceType.WalletAmount)
            }
        }
        
        textFieldDidEndEditing(moneyTF)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.title = ""
    }
    
    @IBAction func VoucherAction(_ sender: UIButton) {

        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "VoucherVC") as! VoucherVC
        viewController.voucherVM.VoucherVM_responseWallet.subscribe(onNext: { [weak self]success in
            if success{
                WalletAPICalls.getBalance().subscribe(onNext: { [weak self]data in
                    self?.amount = Utility.getWallet().Amount
                    self?.setBalance()
                }).disposed(by: self!.disposeBag)
            }
        }).disposed(by: disposeBag)
        viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(viewController, animated:true, completion: nil)
    }
    @IBAction func historyAction(_ sender: Any) {
        self.performSegue(withIdentifier: String(describing: "recentTransitions"), sender: self)
    }
}
extension String {
    var floatValue: Float? {
        if self.length == 0 {
            return 0
        }
        return Float(self)
    }
}

