//
//  VoucherVC.swift
//  Rinn
//
//  Created by 3Embed on 11/09/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift

class VoucherVC: UIViewController {
    
    
    var dispose = DisposeBag()
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var separator: UIView!
    
    let voucherVM = VoucherVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        self.didGetResponse()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       removeObserver()
    }
    
    //MARK: - UIButton Action
    @IBAction func closeAction(_ sender: Any) {
      closeVC()
    }
    @IBAction func confirmAction(_ sender: Any) {
        let cell = mainTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! VoucherTVC
        if let text = cell.textField.text{
            if text.sorted().count > 0{
                voucherVM.voucherCode = cell.textField.text!
                voucherVM.redeemVoucher()
            }else{
               Helper.showAlert(message: "Voucher code Missing", head: "Message", type: 1)
            }
        }
    }
    
    
        /// Observer for keyboard
        func addObserver() {
            NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name:UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        }
         /// remove Observer
        func removeObserver() {
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        }
    
        //Mark:- Keyboard ANimation
        @objc func keyboardWillShow(_ notification: NSNotification){
            // Do something here
    
            let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber
            let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as! NSNumber
    
            self.view.setNeedsLayout()
    
            UIView.animate(withDuration: TimeInterval(truncating: duration), delay: 0, options: [UIView.AnimationOptions(rawValue: UInt(truncating: curve))], animations: {
    
                let info = notification.userInfo!
                let inputViewFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
                let screenSize: CGRect = UIScreen.main.bounds
                var frame = self.view.frame
                frame.size.height = screenSize.height - inputViewFrame.size.height
                self.view.frame = frame
    
    
                self.view.layoutIfNeeded()
            }, completion: nil)
    
        }
    
        @objc func keyboardWillHide(_ notification: NSNotification){
            let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber
            let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as! NSNumber
    
            self.view.setNeedsLayout()
    
            UIView.animate(withDuration: TimeInterval(truncating: duration), delay: 0, options: [UIView.AnimationOptions(rawValue: UInt(truncating: curve))], animations: {
                let screenSize: CGRect = UIScreen.main.bounds
                var frame = self.view.frame
                frame.size.height = screenSize.height
                self.view.frame = frame
                self.view.layoutIfNeeded()
            }, completion: nil)
    
    //
    //        // Do something here
    //        UIView.animate(withDuration: 0.5, animations: {() -> Void in
    //            let screenSize: CGRect = UIScreen.main.bounds
    //            var frame = self.view.frame
    //            frame.size.height = screenSize.height
    //            self.view.frame = frame
    //        })
        }
}
