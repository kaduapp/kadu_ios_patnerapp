//
//  VoucherUIE.swift
//  Rinn
//
//  Created by 3Embed on 11/09/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
extension VoucherVC:UITextFieldDelegate {
    
    /// initial ViewSetup
    func setUI() {
        
        //Done Button
        titleLabel.textColor = UIColor.white
        titleView.backgroundColor = Colors.baseColor
        Helper.setUiElementBorderWithCorner(element: titleView, radius: 10, borderWidth: 0, color: UIColor.clear)
        Helper.setShadow(sender: self.view)
        
        //Title
        separator.backgroundColor = Colors.baseColor
        Fonts.setPrimaryMedium(titleLabel)
        titleLabel.text = "VoucherCode"
        Helper.setButton(button: confirmButton, view: confirmView, primaryColour: UIColor.white, seconderyColor: Colors.baseColor, shadow: true)
        Helper.setButtonTitle(normal: "Redeem", highlighted: "Redeem", selected: "Redeem", button: confirmButton)
    }
    
    func closeVC(){
        self.voucherVM.VoucherVM_responseWallet.onNext(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func didGetResponse(){
        voucherVM.VoucherVM_response.subscribe(onNext: { [weak self]success in
            self?.view.endEditing(true)
           Helper.showAlert(message: "success", head: "Message", type: 1)
            if success.0{
                Helper.finalController().dismiss(animated: true, completion: {
                  self?.closeVC()
                })
            }
        }).disposed(by: dispose)
    }
}
