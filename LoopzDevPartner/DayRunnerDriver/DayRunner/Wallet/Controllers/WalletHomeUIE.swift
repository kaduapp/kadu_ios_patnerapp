//
//  QuickCardUIE.swift
//  Rinn
//
//  Created by 3 Embed on 22/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

import UIKit

extension WalletHomeVC {
    
    func validateUI() {
        Helper.setButton(button: bottomButton, view: bottomButtonView, primaryColour: Colors.baseColor, seconderyColor: UIColor.white, shadow: false)
        balanceHead.textColor = UIColor.black
        //Saldo actual
        balanceHead.text = "Current Balance".localized
        Fonts.setPrimaryMedium(balanceHead)
        balance.textColor = Colors.baseColor
        Fonts.setPrimaryBold(balance)
        separatorBalance.backgroundColor = Helper.getUIColor(color:"DFE4EF")
        moneySeparator.backgroundColor = Helper.getUIColor(color:"1A1945")
        historyButton.tintColor = Colors.baseColor
        Fonts.setPrimaryMedium(historyButton)
        //Transacciones Recientes
        historyButton.setTitle("Recent Transactions".localized, for: .normal)
        hardLimit.textColor = Helper.getUIColor(color:"A3A3A3")
        lightLimit.textColor = Helper.getUIColor(color:"A3A3A3")
//        Fonts.setPrimaryRegular(hardLimit)
//        Fonts.setPrimaryRegular(lightLimit)
        headerSeparator.backgroundColor = Helper.getUIColor(color:"DFE4EF")
        Helper.setUiElementBorderWithCorner(element: headerSeparator, radius: 0, borderWidth: 1, color: UIColor.lightGray)
        
        //Pagar con tarjeta
        listHead.text = "Pay Using Card".localized
        Fonts.setPrimaryBold(listHead)
        listHead.textColor = Colors.baseColor
        footerHead.text = "Add Money".localized
        Fonts.setPrimaryBold(footerHead)
        footerHead.textColor = Colors.baseColor
        hardColorView.backgroundColor = UIColor.red
        softColorView.backgroundColor = UIColor.blue
        hardText.textColor = Helper.getUIColor(color:"A3A3A3")
        softText.textColor = Helper.getUIColor(color:"A3A3A3")
        Helper.updateText(text: "Soft Limit".localized + ": " + "Maximum allowed cash limit is about to reached.".localized, subText: "Soft Limit".localized + ": ", softText, color: Helper.getUIColor(color:"7B8091"), link: "")
        Helper.updateText(text: "Hard Limit".localized + ": " + "Cash limit hit and no more cash booking is allowed.".localized, subText: "Hard Limit".localized + ": ", hardText, color: Helper.getUIColor(color:"7B8091"), link: "")
        Helper.setUiElementBorderWithCorner(element: bottomView, radius: 0, borderWidth: 1, color: Helper.getUIColor(color:"DFE4EF"))
        Fonts.setPrimaryMedium(moneyTF)
        Fonts.setPrimaryMedium( currencyLabel)
         moneyTF.textColor = Helper.getUIColor(color:"1A1945")//Black
         currencyLabel.textColor = Helper.getUIColor(color:"1A1945")//Black

       
        moneyTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        textFieldDidChange(moneyTF)
        
//        Fonts.setPrimaryRegular(voucherButton)
        voucherButton.setTitleColor(Colors.baseColor, for: UIControl.State.normal)

//        Helper.setButtonTitle(normal: "HaveVoucher", highlighted: StringConstants.HaveVoucher(), selected: StringConstants.HaveVoucher(), button: voucherButton)
        Helper.setButtonTitle(normal: "ADD MONEY".localized, highlighted: "ADD MONEY".localized, selected: "ADD MONEY".localized, button: bottomButton)
                CommonAlertView.AlertPopupResponse.subscribe(onNext: { data in
            if data == CommonAlertView.ResponceType.WalletAmount {
                self.updateTheWallet()
            }
        }).disposed(by: disposeBag)
    }
    
    func updateTheWallet(){
        
        if moneyTF.text?.floatValue != nil {
            amountIn = (moneyTF.text?.floatValue)!
        }
        if let amount = moneyTF.text?.floatValue {
            if amount > 0{
                amountIn = amount
                WalletAPICalls.updateBalance(data: cardArray[selectedCard], amount: amountIn).subscribe(onNext: { [weak self]data in
                    WalletAPICalls.getBalance().subscribe(onNext: { [weak self]data in
                        self?.amount = Utility.getWallet().Amount
                        self?.setBalance()
                        Helper.showAlert(message: "Wallet Recharged " + Utility.currencySymbol + String(describing: Helper.clipDigit(value: amount, digits: 2)) + "Balance", head: "Message", type: 1)
                        self?.moneyTF.text = ""
                        self?.moneyTF.resignFirstResponder()
                        self?.textFieldDidChange((self?.moneyTF)!)
                    }).disposed(by: (self?.disposeBag)!)
                }).disposed(by: self.disposeBag)
            }
        }
    }
    
    func setBalance() {
        let wallet = Utility.getWallet()
        balance.text =  Utility.currencySymbol + String(describing: Helper.clipDigit(value: amount, digits: 2))
        hardLimit.text = "Hard Limit".localized + ": " + Utility.currencySymbol + String(describing: Helper.clipDigit(value: wallet.HardLimit, digits: 2))
        lightLimit.text = "Soft Limit".localized + ": " + Utility.currencySymbol + String(describing: Helper.clipDigit(value: wallet.SoftLimit, digits: 2))
        Helper.updateText(text: hardLimit.text!, subText: Utility.currencySymbol +  String(describing: Helper.clipDigit(value: wallet.HardLimit, digits: 2)), hardLimit, color: Helper.getUIColor(color:"D95656"), link: "")
        Helper.updateText(text: lightLimit.text!, subText: Utility.currencySymbol +  String(describing: Helper.clipDigit(value: wallet.SoftLimit, digits: 2)), lightLimit, color: Helper.getUIColor(color:"4F86EB"), link: "")
        moneyTF.placeholder = "Amount".localized //Cantidad
        currencyLabel.text = Utility.currencySymbol
    }
    
    
}

extension WalletHomeVC: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        Helper.addDoneButtonOnTextField(tf: textField, vc: self.view)
        if textField.text?.sorted().count != 0 {
            Helper.setButton(button: bottomButton, view: bottomButtonView, primaryColour:  Colors.baseColor, seconderyColor:  UIColor.white, shadow: true)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    @objc func textFieldDidChange(_ textField : UITextField) {
        
        if textField.text?.sorted().count != 0 {
           Helper.setButton(button: bottomButton, view: bottomButtonView, primaryColour:  Colors.baseColor, seconderyColor:  UIColor.white, shadow: true)
        }
        else {
           Helper.setButton(button: bottomButton, view: bottomButtonView, primaryColour:  Helper.getUIColor(color:"DFE2E7"), seconderyColor:  UIColor.white, shadow: true)
        }
        
    }
}
