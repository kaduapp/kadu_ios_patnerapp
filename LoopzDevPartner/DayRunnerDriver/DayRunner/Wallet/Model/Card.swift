//
//  Card.swift
//  UFly
//
//  Created by 3Embed on 29/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class Card: NSObject {
    
    var Last4               = ""
    var Id                  = ""
    var Brand               = ""
    var Default             = 0
    var ExpMonth            = ""
    var ExpYear             = ""
    var Name                = ""
    var Funding             = ""
    
   
    init(data:[String:Any]) {
        if let titleTemp = data[APIResponceParams.Last4] as? String{
            Last4                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CardId] as? String{
            Id             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Brand] as? String{
            Brand                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.DefaultCard] as? Int{
            Default             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.DefaultCardWallet] as? Int{
            Default             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ExpYear] as? String{
            ExpYear                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ExpMonth] as? String{
            ExpMonth             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ExpYear1] as? Float{
            ExpYear                         = String(Int(titleTemp))
//            ExpYear = Helper.getDateString(value: Helper.getStringToDate(value: ExpYear, format: DateFormat.Year, zone: false)!, format: DateFormat.Short, zone: false)
        }
        if let titleTemp = data[APIResponceParams.ExpMonth1] as? Float{
            ExpMonth             = String(Int(titleTemp))
        }
        if let titleTemp = data[APIResponceParams.Name] as? String{
            Name                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Funding] as? String{
            Funding                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CardToken] as? String{
            Id                         = titleTemp
        }
    }

}
