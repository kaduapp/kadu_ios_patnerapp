//
//  Wallet.swift
//  Rinn
//
//  Created by 3Embed on 14/08/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

struct Wallet {
    var Amount:Float = 0
    var HardLimit:Float = 0
    var SoftLimit:Float = 0
    init(data:[String:Any]) {
        if let temp = data["walletBalance"] as? String {
            Amount = Float(temp)!
        }
        if let temp = data["walletBalance"] as? Float {
            Amount = temp
        }
        if let temp = data["walletBalance"] as? Double {
            Amount = Float(temp)
        }
        if let temp = data["walletSoftLimit"] as? String {
            SoftLimit = Float(temp)!
        }
        if let temp = data["walletSoftLimit"] as? Float {
            SoftLimit = temp
        }
        if let temp = data["walletSoftLimit"] as? Double {
            SoftLimit = Float(temp)
        }
        if let temp = data["walletHardLimit"] as? String {
            HardLimit = Float(temp)!
        }
        if let temp = data["walletHardLimit"] as? Float {
            HardLimit = temp
        }
        if let temp = data["walletHardLimit"] as? Double {
            HardLimit = Float(temp)
        }
    }
}
