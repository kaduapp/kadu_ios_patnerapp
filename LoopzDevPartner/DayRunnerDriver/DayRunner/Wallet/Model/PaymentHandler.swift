//
//  PaymentHandler.swift
//  Rinn
//
//  Created by Rahul Sharma on 01/02/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation

class PaymentHandler {

  var isPayByWallet = true
  var isPayByCard = false
  var isPayByCash = false
  var dontReloadCash = false
  var dontReloadCard = false
  var selectedIndexPath = IndexPath.init(row: -1, section: -1)

    
    var isPayCashWallet :Bool{
       
        if isPayByCash && isPayByWallet{
            
            return true
        }
         return false
    }
    var isPayCardWallet :Bool{
        if isPayByCard && isPayByWallet{
            
            return true
        }
        return false
    }
    
    var paymentType : Int{
        
        if isPayByCard{
            return 1
            
        }
        else if isPayByCash{
            
            return 2
        }
        else{
            
            return 0
        }
    }
  
}
