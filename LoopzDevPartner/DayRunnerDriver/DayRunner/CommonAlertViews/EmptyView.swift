//
//  EmptyView.swift
//  Rinn
//
//  Created by 3Embed on 02/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class EmptyView: UIView {

    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var emptyButton: UIButton!
    
    var obj: EmptyView? = nil
    var shared: EmptyView {
        if obj == nil {
            obj = UINib(nibName: "CommonAlertView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[5] as? EmptyView
            obj?.frame = UIScreen.main.bounds
           // Fonts.setPrimaryItalics(obj?.emptyLabel as Any)
            Fonts.setPrimaryMedium(obj?.emptyButton as Any)
            //obj?.emptyLabel.textColor = Colors.SecoundPrimaryText
            obj?.emptyLabel.numberOfLines = 0
            Helper.setButton(button: (obj?.emptyButton)!, view: (obj?.buttonView)!, primaryColour: UIColor.clear, seconderyColor:Colors.baseColor , shadow: false)
        }
        return obj!
    }
    
    func setData(image:UIImage, title: String, buttonTitle: String,buttonHide:Bool) {
        emptyLabel.text = title
        emptyImage.image = image
        emptyButton.setTitle(buttonTitle, for: .normal)
        buttonView.isHidden = buttonHide
    }
    
    
}
