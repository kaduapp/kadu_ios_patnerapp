//
//  searchItem.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 7/23/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import Foundation

struct searchItem {
    var storeId: String!
    var parentProductId: String!
    var name : String!
    var productId: String!
    var units:[[String:Any]]!
    var currentQuantity = 0
   
    init(data:[String:Any])
    {
        
        if let storeId = data["storeId"] as? String{
            self.storeId = storeId
        }
        if let units = data["units"] as? [[String:Any]] {
            
            self.units = units
        }
        if let parentProductId = data["parentProductId"] as? String {
            self.parentProductId = parentProductId
        }
        if let productName = data["productName"] as? String {
            self.name = productName
        }
        if let productId = data["productId"] as? String {
            self.productId = productId
        }
        
    }
    
}

class searchItems {
    var searchItemData:[searchItem]!
    
    func getSearchItems(storeId:String,index:String,limit:String,search:String, completion:@escaping(Bool)->()){
        Helper.showPI(message:"Loading..")
        let getLanguage =  APIs()
        getLanguage.getSearchItems(storeId: storeId, index: index, limit: limit, search: search)
        
        getLanguage.subject_response.subscribe(onNext: { (response) in
            Helper.hidePI()
            if let data =  response["data"] as? [[String:Any]]{
                self.searchItemData = data.map{
                    searchItem.init(data:$0)
                }
                completion(true)
            }
            
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }
        
        
    }
}
