//
//  AppDelegate.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 12/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import GoogleMaps
import UserNotifications
import AWSS3
import AWSCore
import AVFoundation
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import RxSwift
import Fabric
import Crashlytics
import Stripe
import CocoaLumberjack

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var bookingDict = [String : Any]()
    var backgroundUpdateTask: UIBackgroundTaskIdentifier!
    var player: AVAudioPlayer?
    var countDown:Int = 0
    var ringToneCount:Int = 0
    var newBookingMDL = NewBookingData()
    var ringToneTimer       = Timer()
    let disposeBag = DisposeBag()
//    let mqtt = MQTT.sharedInstance
    
    var viewController = UIApplication.shared.keyWindow?.rootViewController
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        OneSkyOTAPlugin.provideAPIKey("J8oMSeMEMBHVzrJQXvaJNZhSgGyEOYYo", apiSecret:"6BI4cU0aVIYdj8UXrFWtWsZIeryPxiAt", projectID: "174757")
        OneSkyOTAPlugin.setLanguage(Utility.getSelectedLanguegeCode())
        if Utility.getSelectedLanguegeCode() == "en" {
            OneSkyOTAPlugin.setLanguage("en")
        }else {
            OneSkyOTAPlugin.setLanguage("es")
        }
        OneSkyOTAPlugin.checkForUpdate()
        Stripe.setDefaultPublishableKey(AppConstants.StripeKey)
        DispatchQueue.global().async {
            do {
                let update = try Helper.isUpdateAvailable()
                if update == true {
//                    APICalls.appVersion()
                    
                    if Utility.VersionMandate {
                        let refreshAlert = UIAlertController(title: "Alert", message: "A New Version (\(Utility.AppVersion)), please update application.", preferredStyle: UIAlertController.Style.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "UPDATE", style: .default, handler: { (action: UIAlertAction!) in
                            UIApplication.shared.open(NSURL(string: "https://itunes.apple.com/us/app/grocer-partner/id1386109504?ls=1&mt=8")! as URL, options: [:], completionHandler: nil)
                        }))
                        
                        let vc = Helper.finalController
                        vc().present(refreshAlert, animated: true, completion: nil)
                        
                    } else {
                        
                        let refreshAlert = UIAlertController(title: "Alert", message: "A New Version (\(Utility.AppVersion)), please update application.", preferredStyle: UIAlertController.Style.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "UPDATE", style: .default, handler: { (action: UIAlertAction!) in
                            UIApplication.shared.open(NSURL(string: "https://itunes.apple.com/us/app/delivx-partner/id1386109504?ls=1&mt=8")! as URL, options: [:], completionHandler: nil)
                        }))
                        
                        refreshAlert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: { (action: UIAlertAction!) in
                            refreshAlert.dismiss(animated: true, completion: nil)
                        }))
                        
                        let vc = Helper.finalController
                        vc().present(refreshAlert, animated: true, completion: nil)
                        
                    }
                }
            } catch {
                print(error)
            }
        }
        
        
        let pre = Locale.preferredLanguages[0]
        
        if Utility.getSelectedLanguegeCode() == "ar" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
        }else{
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
        }
        
        let location = LocationManager.sharedInstance
        location.startUpdatingLocation()

        let locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
        locationtracker.startLocationTracking()
        
        UserDefaults(suiteName: APPGROUP.groupIdentifier)!.set(true, forKey: "isAppActive")
        
        setupNavigation()
        GMSServices.provideAPIKey(SERVER_CONSTANTS.googleMapsApiKey)
        
        let amazonWrapper:AmazonWrapper = AmazonWrapper.sharedInstance()
        
        amazonWrapper.setConfigurationWithRegion(AWSRegionType.USEast1, accessKey: AmazonAccessKey, secretKey: AmazonSecretKey)
        
        self.setupNotification(application: application)
 
        Fabric.with([Crashlytics.self])
        
        UIDevice.current.isBatteryMonitoringEnabled = true
        self.listenForRealTimeMessageUpdate()
        
        self.setAppConfig()
        self.setUpDebugLog()
        return true
    }
    
    func application(application: UIApplication, willFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
    GMSServices.provideAPIKey(SERVER_CONSTANTS.googleMapsApiKey)

        return true
    }
    
    //In app delegate
    let alertWindow: UIWindow = {
        let win = UIWindow(frame: UIScreen.main.bounds)
        win.windowLevel = UIWindow.Level.alert + 1
        return win
    }()
    
    func setUpDebugLog() {
        
        DDLog.add(DDTTYLogger.sharedInstance) // TTY = Xcode console
        DDLog.add(DDASLLogger.sharedInstance) // ASL = Apple System Logs
        
        let fileLogger: DDFileLogger = DDFileLogger() // File Logger
        fileLogger.rollingFrequency = TimeInterval(60*60*24)  // 24 hours
        fileLogger.logFileManager.maximumNumberOfLogFiles = 7
        DDLog.add(fileLogger)
        
        DDLogDebug("this is from debuglog")
        DDLogInfo("this info")
        DDLogWarn("this is warning")
    }
    
    // [START connect_to_fcm]
    func connectToFcm() {
        Messaging.messaging().subscribe(toTopic: "/all") { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    
    
    //MARK: - Setup Notification
    func setupNotification(application: UIApplication) {
        // iOS 10 support
        if #available(iOS 10,iOSApplicationExtension 10.0, *) {
            
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
//            application.registerForRemoteNotifications()
        }else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(tokenRefreshNotification(notification:)),
                                               name: NSNotification.Name.InstanceIDTokenRefresh,
                                               object: nil)
        //FCM Push
        FirebaseApp.configure()

    }
    
    //App Config
    func setAppConfig() {
        APICalls.configure().subscribe(onNext: { [weak self]success in
            
            STPPaymentConfiguration.shared().publishableKey = AppConstants.StripeKey
            
            //self?.initMap()
        }).disposed(by: APICalls.disposeBag)
    }
    
    
    // [START refresh_token]
    @objc func tokenRefreshNotification(notification: Notification) {
        if let refreshedToken = Messaging.messaging().fcmToken {
            print("InstanceID token: \(refreshedToken)")
            let defaults = UserDefaults.standard
            defaults.set(refreshedToken , forKey: USER_INFO.PUSH_TOKEN)
        }
        connectToFcm()
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void){
        
        completionHandler([.sound, .alert, .badge])
        
        
    }
    

    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        handleNotification(dict: response.notification.request.content.userInfo as! [String : Any])
         }

    func handleRemoteNotification(application:UIApplication,unserInfo:[String:Any]) {
     
        print("it'working fine.....")
        
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        // With swizzling disabled you must set the APNs token here.
//        InstanceID.instanceID().setAPNSToken(deviceToken, type: InstanceIDAPNSTokenType.sandbox)
//        InstanceID.instanceID().setAPNSToken(deviceToken, type: InstanceIDAPNSTokenType.prod)
        
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
//        completionHandler(.newData)
        
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        //return application(app, open: url,
        let stripeHandled = Stripe.handleURLCallback(with: url)
        if (stripeHandled) {
            return true
        } else {
            return true
        }
    }
    
    
    // This method handles opening universal link URLs (e.g., "https://example.com/stripe_ios_callback")
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if let url = userActivity.webpageURL {
                let stripeHandled = Stripe.handleURLCallback(with: url)
                if (stripeHandled) {
                    return true
                } else {
                    // This was not a stripe url – do whatever url handling your app
                    // normally does, if any.
                }
            }
        }
        return false
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(.newData)
        let bookings = Helper.convertToDictionary(text: userInfo[AnyHashable("data")] as? String ?? "")
        if bookings != nil {
            print(bookings!)

            var bookingsDict = [String: Any]()
            
            
            if let dict = bookings!["bookingData"] as? [String: Any] {
                bookingsDict = dict
                let userDef = UserDefaults.standard
                
                if let currencySymbol = bookingsDict["currencySymbol"] as? String {
                    userDef.set(currencySymbol, forKey: USER_INFO.CURRENCYSYMBOL)
                }
                
                if let mileageMetric = bookingsDict["mileageMetric"] as? String {
                    userDef.set(mileageMetric, forKey: USER_INFO.DISTANCE)
                }
                
                
                if let action = bookingsDict["action"] as? Int {
                    
                    if action == 12,(bookingsDict["deviceId"] as? String) != Utility.deviceId {
                        Session.expired()
                        return
                    } else if action == 10 || action == 14 || action == 29 {
                        // Define identifier
                        let notificationName = Notification.Name("gotNewBooking")
                        
                        // Post notification
                        NotificationCenter.default.post(name: notificationName, object: nil)
                    } else if action == 16 {
                        
                        let notificationName = Notification.Name("gotNewBooking")
                        
                        // Post notification
                        NotificationCenter.default.post(name: notificationName, object: nil)
                        let  locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
                        locationtracker.stopLocationUpdateTimer()
                        return
                    } else if action == 1 {
                        
                       
                        return
                    }else {
                        
                        if let orderId = dict["orderId"] as? Int {
                            
                            if let checkOrderId = UserDefaults(suiteName: APPGROUP.groupIdentifier)!.object(forKey:"AckOrder") as? Int {
                                
                                if checkOrderId != orderId {
                                    
                                    UserDefaults(suiteName: APPGROUP.groupIdentifier)!.set(orderId, forKey: "AckOrder")
//                                    UserDefaults.standard.set(orderId, forKey: "AckOrder")
                                    self.realTimeNewMessage(bookingDict:dict)
                                }
                                
                            } else {
                                UserDefaults(suiteName: APPGROUP.groupIdentifier)!.set(orderId, forKey: "AckOrder")
//                                UserDefaults.standard.set(orderId, forKey: "AckOrder")
                                self.realTimeNewMessage(bookingDict:dict)
                            }
                        }
                    }
                }
                
            }
            
        }

        
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        
    }
    
    func handleNotification(dict:[String: Any]) {
        
      print("vhjsabdvas ....... working")
        
     }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        NotificationCenter.default.addObserver(self, selector: #selector(networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        center.removeAllDeliveredNotifications()
        
        Reach().monitorReachabilityChanges()
        
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
            //            newBookingMDL.getConfigData()
        }else{
            MQTT.sharedInstance.createConnection()
            newBookingMDL.getConfigData()
        }
        let pre = Locale.preferredLanguages[0]
        
        if Utility.getSelectedLanguegeCode() == "ar" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
        }else{
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
        }
        
        if UserDefaults(suiteName: APPGROUP.groupIdentifier)!.object(forKey:"bookingData") != nil{
            if let dict = UserDefaults(suiteName: APPGROUP.groupIdentifier)!.object(forKey:"bookingData") as? [String:Any]{
                if let newDict = dict["bookingData"] as? [String:Any] {
                    if let action = newDict["action"] as? Int, action == 11 {
                        
                        let ud = UserDefaults.standard
                        let currentTimeStamp = Date().timeIntervalSince1970
                        let bookingTimeStamp = UserDefaults(suiteName:  APPGROUP.groupIdentifier)!.integer(forKey: "timeStamp")
                        let differenceTimestamp = Int(currentTimeStamp) - bookingTimeStamp
                        
                        if differenceTimestamp < Int(newDict["ExpiryTimer"] as! String)!{
                            if (ud.value(forKey: "lastBid") != nil) {
                                let bid = newDict["orderId"] as? NSNumber
                                if ud.value(forKey: "lastBid") as? NSNumber == bid{
                                    return
                                }
                                else{
                                    ud.setValue(dict["orderId"],forKey:"lastBid")
                                    ud.synchronize()
                                }
                            }else{
                                ud.setValue(dict["orderId"],forKey:"lastBid")
                                ud.synchronize()
                            }
                            
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
                                        self.openNewBookingVC(dict: newDict, expiryTime: differenceTimestamp)
                                    }

                            UserDefaults(suiteName: APPGROUP.groupIdentifier)!.removeObject(forKey:"bookingData")
                        }else{
                            
                        }
                    }
                }
            }
        }
        
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        //  let userInfo = (notification as NSNotification).userInfo
        
        let ud = UserDefaults.standard
        
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            ud.set(false, forKey: "hasInternet")
            
            ud.synchronize()
            ReachabilityView.instance.show()
            
        case .online(.wwan):
            ud.set(true, forKey: "hasInternet")
            ud.synchronize()
            ReachabilityView.instance.hide()
            if MQTT.sharedInstance.isConnected == false && Utility.sessionToken != USER_INFO.SESSION_TOKEN {
                MQTT.sharedInstance.createConnection()
            }
            
            
        case .online(.wiFi):
            ud.set(true, forKey: "hasInternet")
            ReachabilityView.instance.hide()
            ud.synchronize()
            if MQTT.sharedInstance.isConnected == false && Utility.sessionToken != USER_INFO.SESSION_TOKEN {
                MQTT.sharedInstance.createConnection()
            }
        }
    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        UserDefaults(suiteName: APPGROUP.groupIdentifier)!.set(false, forKey: "isAppActive")
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    
    // MARK: - Core Data stack
    
    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "DayRunnerDriver")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if #available(iOS 10.0, *) {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        }
        else {
            // Fallback on earlier versions
            saveContextForBelowiOS10()
        }
    }
    
    
    
    // MARK: - Core Data stack For XCode7 and iOS Version lesser than 10.0
    
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.demo.imageList.DBDemo_Swift_Xcode7" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "DayRunnerDriver", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("DayRunnerDriver.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support for iOS Version Lesser than 10
    
    func saveContextForBelowiOS10 () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    //set pubnub delegate to appdelegate
    //    func intiatingThePubnubFromHome(){
    //        let pub = VNHPubNubWrapper.sharedInstance() as! VNHPubNubWrapper
    //        pub.delegate = self
    //    }
    
    
    //MARK:- Shows New booking popup
    func openNewBookingVC(dict:[String:Any], expiryTime: Int) {
        
        //        if let bookingData = dict["bookingData"] as? [String:Any] {
        if Utility.OrderId != dict["orderId"] as! Double {
            DispatchQueue.main.async {
                //            var expTime:Int = 0
                //            expTime = Int(dict["ExpiryTimer"] as! String)!
//                self.player?.stop()
                
                let Vc:NewBookingPopup = NewBookingPopup.instance
                //            if (Vc.bookingDict == nil){
                //
                //            }
                //            else{
                //                if(Vc.bookingDict["bid"] as! UInt64 == dict["bid"] as! UInt64!)
                //                {
                //                    return
                //                }
                //            }
                
                if(expiryTime == 0)
                {
                    Vc.newbookingDic(dict: dict,expTm:Int(dict["ExpiryTimer"] as! String)!)
                  //  Vc.expiryTime = Double(Int(dict["ExpiryTimer"] as! String)!)
                }else
                {
                      Vc.newbookingDic(dict: dict,expTm:expiryTime)
                 //   Vc.expiryTime = Double(expiryTime)
                }
                Vc.show()
            }
        }
        //        }
        
    }

    
    //Play sound when the app in background
    func playSound() {
        player?.stop()
        ringToneCount = 0
        guard let url = Bundle.main.url(forResource: "taxina-1", withExtension: "wav") else {
            print("error")
            return
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player.play()
            
            
            player.numberOfLoops = Int(countDown/28) + 1
        } catch let error {
            print(error.localizedDescription)
        }
        
        self.ringToneTimer = Timer.scheduledTimer(timeInterval:1,
                                                  target: self,
                                                  selector: #selector(updateRingToneCount),
                                                  userInfo: nil,
                                                  repeats: true)
    }
    
    //Stops playing sound when the countdown over
    @objc func updateRingToneCount(){
        if ringToneCount >= countDown{
            self.ringToneTimer.invalidate()
            player?.stop()
        }else{
            ringToneCount = ringToneCount + 1
        }
    }
    
    //Handling the pubnub data after acknowledging to server
    func handlingTheBookingAfterAcknowledging(dict:[String : Any]){
        //        let state: UIApplicationState = UIApplication.shared.applicationState
        let ud = UserDefaults.standard
        ud.set(Helper.currentDateTime, forKey: "currentbookingDateNtime")
        //        if state == .background || state == .inactive {
        //            UIApplication.shared.applicationIconBadgeNumber = 0
        //            self.playSound()
        //
        //            if #available(iOS 10.0, *) {
        //                bookingDict   = dict
        //                countDown     =   Int(bookingDict["ExpiryTimer"] as! String)!
        //                let content   = UNMutableNotificationContent()
        //                content.title = "New booking"
        //                content.body  = "Congratulations you got New Booking"
        //                content.categoryIdentifier = "NewBooking"
        //                content.badge = 1
        //                let trigger   = UNTimeIntervalNotificationTrigger(timeInterval:1, repeats:false)
        ////                let request   = UNNotificationRequest(identifier: "NEWBOOKING", content: content, trigger: trigger)
        ////                UNUserNotificationCenter.current().add(request, withCompletionHandler:nil)
        //            }else{
        //                bookingDict   = dict
        //                countDown     =   Int(bookingDict["ExpiryTimer"] as! String)!
        //                let date      = Date(timeIntervalSinceNow:1)
        //                let notification = UILocalNotification()
        //                notification.fireDate = date
        //                notification.alertTitle = "New booking"
        //                notification.alertBody  = "Congratulations you got New Booking"
        //                notification.repeatInterval = NSCalendar.Unit.hour
        ////                UIApplication.shared.scheduleLocalNotification(notification)
        //            }
        ////            self.openNewBookingVC(dict: dict, expiryTime: 0 )
        //        }else{
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
            self.openNewBookingVC(dict: dict, expiryTime: 0 )
        }
        
       
        //        }
    }
    
    func listenForRealTimeMessageUpdate()
    {
        MQTT.sharedInstance.mqttNewMessage.subscribe(onNext: { (response, messageType) in
            switch (messageType)
            {
            case .NewBooking:
                
                if let orderId = response["orderId"] as? Int {
                    
                    if let checkOrderId = UserDefaults(suiteName: APPGROUP.groupIdentifier)!.object(forKey:"AckOrder") as? Int {
                        
                        if checkOrderId != orderId {
                            
                            UserDefaults(suiteName: APPGROUP.groupIdentifier)!.set(orderId, forKey: "AckOrder")
                            self.realTimeNewMessage(bookingDict:response)
                        }
                        
                    } else {
                        UserDefaults(suiteName: APPGROUP.groupIdentifier)!.set(orderId, forKey: "AckOrder")
                        self.realTimeNewMessage(bookingDict:response)
                    }
                }

                self.realTimeNewMessage(bookingDict: response)
                break
            case .Logout:
                Session.expired()
                break
                
            default:
                break
            }
        }, onError: { (error) in
            
        }, onCompleted: {
            
        }).disposed(by: disposeBag)
       
    }
    
    
    
    func realTimeNewMessage(bookingDict: [String : Any]) {
        print(bookingDict)
        if let aStatus = bookingDict["action"] as? Int
        {
            switch aStatus {
            case 49:
                break
            default:
                
                let bookingApi = BookingApi()
                //                self.handlingTheBookingAfterAcknowledging(dict: bookingDict)
                
                bookingApi.acknowledgeBooking(bookingId: bookingDict["orderId"] as Any)
                bookingApi.subject_response.subscribe(onNext: { (response) in
                    if(response.count > 0)
                    {
                        //                        UserDefaults.standard.set(false, forKey: "publishLocation")
                        self.handlingTheBookingAfterAcknowledging(dict: bookingDict)
                        
                    }else{
                        //                        Helper.alertVC(errMSG: response.data["message"] as! String)
                    }
                }, onError: { (error) in
                    Helper.hidePI()
                }, onCompleted: {
                    
                }).disposed(by: disposeBag)
            }
        }
        
    }
    
    //MARK: - Setup Navigation Bar
    func setupNavigation() {

        UINavigationBar.appearance().tintColor = Helper.getUIColor(color: Colors.AppBaseColor)
        UINavigationBar.appearance().barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)

    }
    

}

