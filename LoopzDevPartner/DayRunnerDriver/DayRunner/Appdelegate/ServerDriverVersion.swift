 //

//  RunnerLive
//
//  Created by Vengababu Maparthi on 30/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import AWSS3
import FirebaseMessaging
import RxSwift
protocol NewBookingDelegate{
    
    func pubnubBookingData(bookingDict:[String:Any])
}


class NewBookingData:NSObject{
    var newBookingDelegate: NewBookingDelegate! = nil
    let disposebag = DisposeBag()
    
    func getConfigData(){
        
        let configData =  APIs()
        configData.getAppConfigurations()
        configData.subject_response.subscribe(onNext: { (response) in
            Helper.hidePI()
            if response.isEmpty == false {
                if (response["errFlag"] != nil){
                    let statCode:Int = response["errNum"] as! Int
                    if statCode == 499 {
                        Helper.hidePI()
                        Helper.alertVC(errMSG: "Your Session has beed expired")
                        Session.expired()
                    }
                }else{
                    let dict = response["data"] as? [String:Any]
                    
                    self.handlingTheCongigData(response: dict!)
                }
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposebag)

        
//        NetworkHelper.requestGETURL(method: API.METHOD.CONFIG,
//                                    success: { (response) in
//                                        Helper.hidePI()
//                                        if response.isEmpty == false {
//                                            if (response["statusCode"] != nil){
//                                                let statCode:Int = response["statusCode"] as! Int
//                                                if statCode == 401 {
//                                                    Helper.hidePI()
//                                                    Helper.alertVC(errMSG: "Your Session has beed expired")
//                                                    Session.expired()
//                                                }
//                                            }else{
//                                                let dict = response["data"] as? [String:Any]
//
//                                                self.handlingTheCongigData(response: dict!)
//                                            }
//                                        } else {
//
//                                        }
//        })
//        { (Error) in
//
//        }
    }
    
    //MARK: - parsing the configuration data
    func handlingTheCongigData(response:[String:Any]) {
        let ud = UserDefaults.standard
//        ud.set(response["currencySymbol"] as! String, forKey: USER_INFO.CURRENCYSYMBOL)
//        ud.set(response["mileage_metric"] as! String, forKey: USER_INFO.DISTANCE)
        ud.set(String(describing:response["presenceTime"]!), forKey: USER_INFO.PRESENCE)
        ud.set(response["driverApiInterval"] as! Int, forKey: USER_INFO.APIINTERVAL)
        ud.set(response["tripStartedInterval"] as! Int, forKey: USER_INFO.BOOKAPIINTERVAL)
        ud.set(response["DistanceForLogingLatLongs"] as! Int, forKey: USER_INFO.DISTANCESTORINGDATA)

        if let appversions = response["appVersions"] as? [[String:Any]] {
            
            for versions in appversions {
                if let type = versions["type"] as? Int {
                    
                    if type == 21 {
                        if let version = versions["latestVersion"] as? String {
                            
                            ud.set(version, forKey: USER_INFO.APPVERSION)
                        }
                    }
                }
            }
        }
        
//        if let pushTopicArray: [Any] = response["allDriverspushTopics"] as? [Any]
//        {
//            ud.set(pushTopicArray, forKey: "pushTopicArray")
//            for var x in 0..<pushTopicArray.count
//            {
//                print("Push Topic \(x + 1): \(pushTopicArray[x])")
//                if let pushTopicName = pushTopicArray[x] as? String {
//                     Messaging.messaging().subscribe(toTopic: pushTopicName)
//                }
//            }
//        }
        

        
        if let versionDict = response["appVersions"]  as? [String:Any]{
            guard !((versionDict["ios_driver"] as! String).isEmpty) else {
                return
            }
            ud.set(versionDict["ios_driver"] as! String, forKey: USER_INFO.ServerDriverVersion)
        }
        ud.synchronize()
    }
    
    
    func acknowledgingTheBooking(dict:[String:Any]) {
        
        Helper.showPI(message:"loading..")
        let params:[String:Any] = ["ent_booking_id": String(describing:dict["bid"]!),
                                   "serverTime":dict["serverTime"]! as Any,
                                   "PingId":dict["PingId"]! as Any]
        
//        NetworkHelper.requestPOST(serviceName:API.METHOD.ACKNOWLEDGEBK,
//                                  params: params,
//                                  success: { (response : [String : Any]) in
//                                    print("signup response : ",response)
//                                    Helper.hidePI()
//                                    if response.isEmpty == false {
//                                        let flag:Int = response["errFlag"] as! Int
//                                        if flag == 1{
//                                            Helper.alertVC(errMSG: response["errMsg"] as! String)
//                                        }else{
////
//                                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
////                                            if (self.newBookingDelegate != nil) {
//                                                appDelegate.realTimeNewMessage(bookingDict: dict)
////                                            }
//                                        }
//                                    } else {
//
//                                    }
//        }) { (Error) in
//            Helper.hidePI()
//        }
    }
    
    
    //MARK:-  handling pubnub  Booking data
    func handlingThePubnubResponse(pubDict:[String : Any])  {
        
        let statusPubnub:Int = pubDict["action"] as! Int
        let ud = UserDefaults.standard
        switch statusPubnub {
        case 11:
            if (ud.value(forKey: "lastBid") != nil) {
                let bid = pubDict["bid"] as? NSNumber
                if ud.value(forKey: "lastBid") as? NSNumber == bid{
                    return
                }
                else{
                    ud.setValue(pubDict["bid"],forKey:"lastBid")
                    ud.synchronize()
                }
            }else{
                ud.setValue(pubDict["bid"],forKey:"lastBid")
                ud.synchronize()
            }
            acknowledgingTheBooking(dict: pubDict)
            break
        case 3:
            // Post notification
            let imageDataDict:[String: Any] = pubDict
            NotificationCenter.default.post(name: Notification.Name("cancelledBooking"), object: nil, userInfo: imageDataDict)
            break
        default:
            break
        }
    }
    
    func handleMqttData(mqttDict:[String : Any])
    {
        guard let _:Int = mqttDict["action"] as? Int  else {
            return
        }
        self.handlingThePubnubResponse(pubDict: mqttDict)
    }
}

