//
//  QuickCardTableCell.swift
//  Rinn
//
//  Created by 3 Embed on 22/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class QuickCardTableCell: UITableViewCell {
    
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var amountTF: HoshiTextField!
    @IBOutlet weak var amountSeparator: UIView!
    
    @IBOutlet weak var money1Btn: UIButton!
    @IBOutlet weak var money2Btn: UIButton!
    @IBOutlet weak var money3Btn: UIButton!
    @IBOutlet weak var addMoneyBtn: UIButton!
    @IBOutlet weak var addMoneyBtnView: UIView!
    
    var tempMoney =  " "
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
//        amountTF.Semantic()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func initView(){
        
        Fonts.setPrimaryMedium(addMoneyBtn)
        Fonts.setPrimaryRegular(balanceLabel)
        
        Fonts.setPrimaryRegular(money1Btn)
        Fonts.setPrimaryRegular(money2Btn)
        Fonts.setPrimaryRegular(money3Btn)
        
//        Helper.setUiElementBorderWithCorner(element: money1Btn, radius: 2, borderWidth: 1, color:  Colors.ScreenBackground)
//        Helper.setUiElementBorderWithCorner(element: money2Btn, radius: 2, borderWidth: 1, color:  Colors.ScreenBackground)
//        Helper.setUiElementBorderWithCorner(element: money3Btn, radius: 2, borderWidth: 1, color:  Colors.ScreenBackground)
//
//        money1Btn.setTitleColor( Colors.PrimaryText, for: .normal)
//        money2Btn.setTitleColor( Colors.PrimaryText, for: .normal)
//        money3Btn.setTitleColor( Colors.PrimaryText, for: .normal)
//
//        amount.textColor =  Colors.PrimaryText
//        balanceLabel.textColor =  Colors.PrimaryText
//        amountTF.textColor =  Colors.PrimaryText
//
//        money1Btn.setTitle("+ "+Helper.setCurrency(displayAmount: Float(money.addMoney[0]), price: Float(money.addMoney[0])).string, for: .normal)
//        money2Btn.setTitle("+ "+Helper.setCurrency(displayAmount: Float(money.addMoney[1]), price: Float(money.addMoney[1])).string, for: .normal)
//        money3Btn.setTitle("+ "+Helper.setCurrency(displayAmount: Float(money.addMoney[2]), price: Float(money.addMoney[2])).string, for: .normal)
//        amountTF.text = String(money.addMoney[0])
//
//        Helper.setButtonTitle(normal:StringConstants.AddMoney(), highlighted: StringConstants.AddMoney(), selected: StringConstants.AddMoney(), button: addMoneyBtn)
//
//        Helper.setButton(button: addMoneyBtn, view: addMoneyBtnView, primaryColour:  Colors.AppBaseColor, seconderyColor:  Colors.SecondBaseColor, shadow: true)
//        amountTF.placeholder =  StringConstants.Amount() + "(\( Utility.getCurrency().1))"
        
    }
    
    func updateMoney(amount: Float){
//        tempMoney =   Helper.setCurrency(displayAmount: amount, price: amount).string
        self.amount.text = "tempMoney"
    }
    
    @IBAction func moneyBtnAction(_ sender: UIButton) {
        amountTF.text = "ttt"
            
//            String(money.addMoney[sender.tag - 200])
//        Helper.setButton(button: addMoneyBtn, view: addMoneyBtnView, primaryColour:  Colors.AppBaseColor, seconderyColor:  Colors.SecondBaseColor, shadow: true)
    }
    
    @IBAction func addMoneyAction(_ sender: Any) {
        
    }
}

