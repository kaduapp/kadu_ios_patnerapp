//
//  PaymentTableCell.swift
//  UFly
//
//  Created by Rahul Sharma on 25/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class PaymentTableCell: UITableViewCell {
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var cardIcon: UIImageView!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var cellseperator: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var discriptionLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var payUsingQC: UIButton!
    @IBOutlet weak var addMoney: UIButton!
    @IBOutlet weak var payByQCView: UIView!
    @IBOutlet weak var addMoneyView: UIView!
    @IBOutlet weak var cardLabelView: UIView!
    @IBOutlet weak var payBtnLeading: NSLayoutConstraint!
    @IBOutlet weak var payBtnWidth: NSLayoutConstraint!
    //Add New Card
    var cartDiscount:Float     = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    /// initial view setup
    func initialSetup(){
        //deleteButton.setImage(UIImage(), for: .normal)
        Fonts.setPrimaryRegular(cardNumberLabel)
        cardNumberLabel.textColor =  Helper.getUIColor(color:"1A1945")//Black
        cellseperator.backgroundColor =  Helper.getUIColor(color:"DFE4EF")
        // Helper.setShadow(sender: cardView)
        cellseperator.isHidden = false
        if addMoney != nil {
            Fonts.setPrimaryMedium(addMoney)
            Fonts.setPrimaryMedium(payUsingQC)
            Fonts.setPrimaryMedium(amountLabel)
            Fonts.setPrimaryMedium(discriptionLabel)
            
        }
    }
    
    /// updates the payment cell
    ///
    /// - Parameters:
    ///   - data: it is of typeCard
    ///   - show: if true - hides the separator , false - shows the separator
    func updatePaymentCell(data:Card){
        deleteButton.tintColor =  Colors.baseColor
        deleteButton.isHidden = false
        deleteButton.setImage(#imageLiteral(resourceName: "Check_off"), for: .normal)
        cardNumberLabel.text = UIConstants.UIElement.Card + data.Last4
        cardIcon.image = Helper.cardImage(with: data.Brand.lowercased())
        if data.Default == 1 {
            deleteButton.setImage(#imageLiteral(resourceName: "CheckOn"), for: .normal)
            cardNumberLabel.textColor =  Colors.baseColor
        }else{
            cardNumberLabel.textColor =   Helper.getUIColor(color:"1A1945")//Black
            deleteButton.setImage(UIImage(), for: .normal)
        }
        //cellseperator.isHidden = show
    }
    
    
    func setSelected(data:Card,show:Bool,id:String){
        
        //        if data.Id == id {
        //            deleteButton.isSelected = true
        //        }
        //        else {
        //            deleteButton.isSelected = false
        //        }
        cardNumberLabel.text = UIConstants.UIElement.Card + data.Last4
        deleteButton.setTitle("", for: .normal)
        cardIcon.image = Helper.cardImage(with: data.Brand.lowercased())
        //  cellseperator.isHidden = show
        deleteButton.isHidden = false
    }
    
    func setSelectedCard(data:Card, id:String){
        
        deleteButton.isHidden = false
        deleteButton.setTitle("", for: .normal)
        if data.Id == id{
            cardNumberLabel.textColor =  Colors.baseColor
            deleteButton.tintColor =  Colors.baseColor
            deleteButton.setImage(#imageLiteral(resourceName: "CheckOn"), for: .normal)
            
        }
        else {
            cardNumberLabel.textColor =  Helper.getUIColor(color:"1A1945")//Black
            deleteButton.setImage(#imageLiteral(resourceName: "Check_off"), for: .normal)
            deleteButton.tintColor =  Helper.getUIColor(color:"7B8091")
        }
    }
    
    
    
    //********************  for payment options ************************
    
    
    //update wallet section
    func updateWalletCell(handler:PaymentHandler,show: Bool){
        
        let amount = Utility.getWallet().Amount
//        let totlaPrice =  AppConstants.TotalCartPriceWithTax + AppConstants.TotalDeliveryPrice - self.cartDiscount
        let totlaPrice = 20
        
        if (show && (Int(amount) >= totlaPrice)){
            handler.isPayByCash = false
            handler.isPayByCard = false
        }
        
        
        var showPayByWallet = show
        if handler.paymentType == 0{
            showPayByWallet = true
        }
        else if ((Int(amount) >= totlaPrice)  &&  !(handler.isPayByCash || handler.isPayByCard)) {
            showPayByWallet = true
            handler.isPayByCash = false
            handler.isPayByCard = false
        }
        
        
        
        if Int(amount) >= totlaPrice{
            DispatchQueue.main.async() { // or you can use
                self.payBtnLeading.constant = 10
                self.payBtnWidth.constant = self.cardLabelView.frame.size.width/2 - 10
                self.addMoneyView.isHidden = false
                self.payByQCView.isHidden = false
            }
        }
        else{
            
            DispatchQueue.main.async() {
                self.payBtnLeading.constant = 0
                self.payBtnWidth.constant = 0
                self.addMoneyView.isHidden = false
                self.payByQCView.isHidden = true
            }
        }
        let tempMoney = Utility.currencySymbol +  String(describing: Helper.clipDigit(value: amount, digits: 2))
        amountLabel.text = String(tempMoney)
        cardIcon.image = #imageLiteral(resourceName: "Wallet")
        discriptionLabel.textColor =  Colors.baseColor
        cellseperator.isHidden = false
        cardNumberLabel.text =  "Wallet"
        
        if (showPayByWallet && !(handler.isPayByCash || handler.isPayByCard)){
            handler.dontReloadCash = false
            handler.dontReloadCard = false
            
            addMoney.setTitle("Add Money", for: .normal)
            payUsingQC.setTitle("Pay", for: .normal)
            discriptionLabel.text = "\n\n\n"
            Helper.setButton(button: payUsingQC,view:payByQCView, primaryColour:  Colors.baseColor, seconderyColor: UIColor.white,shadow: true)
            Helper.setButton(button: addMoney,view:addMoneyView, primaryColour:  UIColor.white, seconderyColor: Colors.baseColor,shadow: true)
            
        }else {
            if handler.isPayByCash{
                handler.dontReloadCash = true
            }
            if handler.isPayByCard{
                handler.dontReloadCard = true
            }
            
            if ((handler.isPayByCash || handler.isPayByCard) && Int(amount) >= totlaPrice){
                handler.isPayByWallet = false
            }
            
            addMoney.setTitle("", for: .normal)
            payUsingQC.setTitle("", for: .normal)
            discriptionLabel.text = ""
        }
        
        
        if handler.isPayByWallet{
            deleteButton.tintColor =  Colors.baseColor
            deleteButton.setImage(#imageLiteral(resourceName: "CheckOn"), for: .normal)
        }
        else{
            deleteButton.setImage(#imageLiteral(resourceName: "Check_off"), for: .normal)
            deleteButton.tintColor =   Helper.getUIColor(color:"7B8091")//Light Secound Head
            discriptionLabel.text = ""
        }
        
    }
    
    
    /// updates Credit or debit card section
    ///
    /// - Parameters:
    ///   - data: is of type Card
    ///   - show: is of type Bool true - means set selected false - set unselected
    ///   - id: selected Card Id
    func updateCreditCardCell(data:Card,show:Bool,handler:PaymentHandler,index:IndexPath){
        
        deleteButton.isHidden = false
        amountLabel.text = ""
        deleteButton.setTitle("", for: .normal)
        discriptionLabel.text = ""
        cardNumberLabel.text = UIConstants.UIElement.Card + " " + data.Last4
        cardIcon.image = Helper.cardImage(with: data.Brand.lowercased())
        cellseperator.isHidden = false
        addMoneyView.isHidden = true
        payByQCView.isHidden = false
        
        DispatchQueue.main.async() {
            self.payBtnLeading.constant = 0
            self.payBtnWidth.constant = self.cardLabelView.frame.size.width
        }
        
        if ((show || handler.dontReloadCard) && (handler.selectedIndexPath == index)) {
            if handler.dontReloadCard{
                handler.dontReloadCard = false
            }
            cardNumberLabel.textColor =  Colors.baseColor
            discriptionLabel.text = "\n\n\n"
            payUsingQC.setTitle("Pay", for: .normal)
            
            Helper.setButton(button: payUsingQC,view:payByQCView, primaryColour:  Colors.baseColor, seconderyColor: UIColor.white,shadow: true)
            
        }else {
            addMoneyView.isHidden = true
            discriptionLabel.text = ""
            cardNumberLabel.textColor =  Helper.getUIColor(color:"1A1945")//Black
        }
        
        if handler.isPayByCard && (handler.selectedIndexPath == index) {
            
            deleteButton.tintColor =  Colors.baseColor
            deleteButton.setImage(#imageLiteral(resourceName: "CheckOn"), for: .normal)
        }
        else{
            
            deleteButton.tintColor = Helper.getUIColor(color:"7B8091")//Light Secound Head
            deleteButton.setImage(#imageLiteral(resourceName: "Check_off"), for: .normal)
            discriptionLabel.text = ""
        }
        
    }
    
    
    /// updates add new card section
    func updateAddNewCard(){
        if amountLabel != nil {
            amountLabel.text = ""
            discriptionLabel.text = ""// StringConstants.AddNewCardText()
            discriptionLabel.textColor = Helper.getUIColor(color:"7B8091")//Light Secound Head
            addMoneyView.isHidden = true
            payByQCView.isHidden = true
        }
        deleteButton.setTitle("", for: .normal)
        cellseperator.isHidden = false
        deleteButton.isHidden = true
        cardIcon.image = #imageLiteral(resourceName: "Add_new1")
        cardNumberLabel.text = "Add New Card".localized
        cardNumberLabel.textColor =  Colors.baseColor
    }
    
    
    /// updates the cash cell
    ///
    /// - Parameter show: is of type Bool true - set selected false - set unselected
    func updateCash(show: Bool,handler:PaymentHandler){
        deleteButton.isHidden = false
        amountLabel.text = ""
        cellseperator.isHidden = false
        cardNumberLabel.text = "Cash"
        cardIcon.image = #imageLiteral(resourceName: "Wallet")
        deleteButton.setTitle("", for: .normal)
        
        // discriptionLabel.text = StringConstants.KeepChange
        
        discriptionLabel.textColor =  Helper.getUIColor(color:"7B8091")//Light Secound Head
        addMoneyView.isHidden = true
        payByQCView.isHidden = false
        DispatchQueue.main.async() {
            self.payBtnLeading.constant = 0
            self.payBtnWidth.constant = self.cardLabelView.frame.size.width
        }
        
        if show || handler.dontReloadCash {
            
            if handler.dontReloadCash{
                handler.dontReloadCash = false
            }
            payUsingQC.setTitle("Pay", for: .normal)
            Helper.setButton(button: payUsingQC,view:payByQCView, primaryColour:  Colors.baseColor, seconderyColor: UIColor.white,shadow: true)
            discriptionLabel.text = "\n\n\n"
        }
        else {
            discriptionLabel.text = ""
            addMoneyView.isHidden = true
        }
        
        if handler.isPayByCash {
            deleteButton.tintColor =  Colors.baseColor
            deleteButton.setImage(#imageLiteral(resourceName: "CheckOn"), for: .normal)
        }
        else{
            deleteButton.setImage(#imageLiteral(resourceName: "Check_off"), for: .normal)
            deleteButton.tintColor = Helper.getUIColor(color:"7B8091")//Light Secound Head
            discriptionLabel.text = ""
        }
        
    }
    
    
    func updateiDeal(show: Bool){
        deleteButton.isHidden = false
        amountLabel.text = ""
        cellseperator.isHidden = false
        cardNumberLabel.text = "iDeal"
        cardIcon.image = #imageLiteral(resourceName: "Wallet")
        deleteButton.setTitle("", for: .normal)
        discriptionLabel.textColor =  Helper.getUIColor(color:"7B8091")//Light Secound Head
        addMoneyView.isHidden = true
        payByQCView.isHidden = false
        DispatchQueue.main.async() {
            self.payBtnLeading.constant = 0
            self.payBtnWidth.constant = self.cardLabelView.frame.size.width
        }
        if show {
            discriptionLabel.text = "\n\n\n"
            deleteButton.tintColor =  Colors.baseColor
            deleteButton.setImage(#imageLiteral(resourceName: "CheckOn"), for: .normal)
            
            payUsingQC.setTitle("Pay", for: .normal)
            
            Helper.setButton(button: payUsingQC,view:payByQCView, primaryColour:  Colors.baseColor, seconderyColor: UIColor.white,shadow: true)
        }
        else {
            discriptionLabel.text = ""
            deleteButton.setImage(#imageLiteral(resourceName: "Check_off"), for: .normal)
            addMoneyView.isHidden = true
            deleteButton.tintColor =  Helper.getUIColor(color:"7B8091")//Light Secound Head
        }
        
    }
    
    
    
    
    /// updates the quick card cell when selected
    ///
    /// - Parameter isSelectd: is of type bool true - set selected false means set unselected
    func selectQuickCard(isSelectd: Bool){
        deleteButton.isHidden = false
        if isSelectd {
            deleteButton.tintColor =  Colors.baseColor
            deleteButton.setImage(#imageLiteral(resourceName: "CheckOn"), for: .normal)
            addMoney.setTitle("Add Money", for: .normal)
            payUsingQC.setTitle("Pay", for: .normal)
            discriptionLabel.text = "\n\n\n"
            addMoneyView.isHidden = false
            
            Helper.setButton(button: payUsingQC,view:payByQCView, primaryColour:  Colors.baseColor, seconderyColor: UIColor.white,shadow: true)
            Helper.setButton(button: addMoney,view:addMoneyView, primaryColour:  UIColor.white, seconderyColor: Colors.baseColor,shadow: true)
            
        }else {
            addMoney.setTitle("", for: .normal)
            payUsingQC.setTitle("", for: .normal)
            deleteButton.setImage(#imageLiteral(resourceName: "Check_off"), for: .normal)
            deleteButton.tintColor =  Helper.getUIColor(color:"7B8091")//Light Secound Head
            discriptionLabel.text = ""
        }
    }
    
}
