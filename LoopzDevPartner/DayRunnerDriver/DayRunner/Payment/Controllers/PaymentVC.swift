//
//  PaymentVC.swift
//  UFly
//
//  Created by Rahul Sharma on 25/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

protocol PaymentVCDelegate {
    func didSelectPaymentMethod(cardNumber: Card)
}

class PaymentVC: UIViewController {
    
    @IBOutlet weak var paymentTableView:UITableView!
    @IBOutlet weak var addNewCardBtn: UIButton!
    @IBOutlet weak var addNewCardView: UIView!
    
    @IBOutlet var noCardsLabel: UILabel!
    
    @IBOutlet weak var navSeparatorHt: NSLayoutConstraint!
    @IBOutlet weak var navSeparator: UIView!
    
    let disposeBag = DisposeBag()
    let btn = UIButton(type: .custom)
//    var navView = NavigationView().shared
    
    var loaded = false
    var paymentMethod:Int = 0
    var isComingCheckout = false
    var delegate: PaymentVCDelegate? = nil
    var cardArray   = [Card]()
    var amt:Float = 0
    var selectedCard = 0
  //  var emptyView = EmptyView().shared
    let responseData = PublishSubject<Card>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noCardsLabel.isHidden = true
        //setEmptyScreen()
        //  rightBarButton()
        initialSetup()
        self.title = "Cards".localized
        UINavigationBar.appearance().tintColor = UIColor.white
        var nav = self.navigationController?.navigationBar
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if paymentMethod == 3 {
            btn.isHidden = false
        }else {
            btn.isHidden = true
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
    }
    
    @IBAction func slideBtnAction(_ sender: Any) {
        if Utility.getSelectedLanguegeCode() == "ar" {
            slideMenuController()?.toggleRight()
            
        }else {
            slideMenuController()?.toggleLeft()
        }
    }
    
    
    /// initial view setup
    func initialSetup() {
        noCardsLabel.text = "No cards available".localized
        addNewCardBtn.isHidden = false
        if self.navigationController != nil{
//            self.navigationItem.titleView = navView
//            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
        }
        Helper.setButton(button: addNewCardBtn,view:addNewCardView, primaryColour:  Helper.getUIColor(color: Colors.AppBaseColor), seconderyColor: UIColor.white,shadow: true)
        Helper.setButtonTitle(normal: "Add New Card".localized, highlighted: "Add New Card".localized, selected: "Add New Card".localized, button: addNewCardBtn )
        Fonts.setPrimaryMedium(addNewCardBtn)
        navSeparator.backgroundColor =  Colors.baseColor
        if paymentMethod == 3 {
//            navView.setData(title: StringConstants.Cards())
            self.paymentTableView.reloadData()
            setUpVM()
            AddNewCardVM().getCard()
        }else{
            //addNewCardBtn.isHidden = true
//            navView.setData(title: StringConstants.Cards())
            
            self.paymentTableView.reloadData()
            setUpVM()
            AddNewCardVM().getCard()
        }
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
        self.paymentTableView.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xF5F5F5)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addNewCardAction(_ sender: Any) {
        //        self.title = ""
        performSegue(withIdentifier: "paymenToAddNewCard", sender: self)
    }
    
    
    func addMoney(){
        if cardArray.count == 0 {
           // Helper.showAlert(message: StringConstants.NoCardsAvailable(), head: StringConstants.Error(), type: 1)
            return
        }
        WalletAPICalls.updateBalance(data: cardArray[selectedCard], amount: amt).subscribe(onNext: { [weak self]data in
            self?.navigationController?.popViewController(animated: true)
        }).disposed(by: self.disposeBag)
    }
    
    
    //Setup Left bar button
    func rightBarButton() {
        btn.setTitle("Pay", for: .normal)
        btn.setTitleColor( UIColor.red, for: .normal)
        btn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        Fonts.setPrimaryMedium(btn)
        btn.addTarget(self, action: #selector(payAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btn)
        self.navigationItem.setRightBarButton(item, animated: true)
    }
    
    
    @objc func payAction(){
        addMoney()
    }
    
    
//
//    func setEmptyScreen() {
//        emptyView.setData(image: #imageLiteral(resourceName: "login_cross_icon_off"), title: "Empty card", buttonTitle: StringConstants.StartShopping(),buttonHide: true)
//        emptyView.emptyButton.addTarget(self, action:#selector(StartShopping) , for: UIControl.Event.touchUpInside)
//    }
    
    @objc func StartShopping() {
//        Helper.changetTabTo(index: AppConstants.Tabs.Home)
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension PaymentVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.paymentTableView {
            if self.navigationController != nil {
                if scrollView.contentOffset.y > 0 {
                    if (self.navigationController != nil) {
                        Helper.addShadowToNavigationBar(controller: self.navigationController!)
                    }
                }else{
                    if (self.navigationController != nil) {
                        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
                    }
                }
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.SegueIds.AllCardsToDetails {
            let paymentDetailsViewController: PaymentDetailsViewController =  segue.destination as! PaymentDetailsViewController
            paymentDetailsViewController.cardData = sender as? Card
        }
    }
}

//Extnsion For Subscription
extension PaymentVC {
    func setUpVM() {
        AddNewCardVM.addNewCard_response.subscribe(onNext: { (success) in
            self.loaded = true
            self.cardArray = AddNewCardVM.arrayOfCard
            self.paymentTableView.reloadData()
        }, onError: { (error) in
            
        }, onCompleted: {
            
        }).disposed(by: CardAPICalls.disposeBag)
        
        APICalls.LogoutInfoTo.subscribe(onNext: { success in
            if success {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }).disposed(by: APICalls.disposeBag)
    }
}

