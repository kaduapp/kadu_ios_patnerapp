//
//  SelectVehicleVC.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SelectVehicleVC: UIViewController {
    
    var window: UIWindow?
    var selectedIndex:Int = -1
    @IBOutlet var ConfirmAction: UIButton!
    var vehicles = [[String : Any]]()
    var mapIcon: String!
    var vehiclemodl = SelectVehicle()
    
    var vehiclesData = [Signin]()
    
    var token = String()
    
    @IBOutlet var vehicleTypeTableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter .default .addObserver(self, selector: Selector("callBack"), name: UIApplication.willTerminateNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter .default .removeObserver(self)
    }
    
    func callBack()  {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: USER_INFO.SESSION_TOKEN)
        defaults.synchronize()
    }
    override func viewDidLoad() {
        self.window = UIApplication.shared.keyWindow
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
}
