//
//  SelectVehicleActionVC.swift
//  GoGoCargoDriver
//
//  Created by Vengababu Maparthi on 23/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import AlamofireImage
import Alamofire

extension SelectVehicleVC{
    
    @IBAction func LogoutAction(_ sender: Any) {
        vehiclemodl.logoutFromApp(completionHandler: { Success in
            if Success{
                self.dismiss(animated: true, completion: nil)
            }else{
                
            }
        })
        
    }
    
    @IBAction func ConfirmVehicle(_ sender: Any) {
        verifyingTheSelectedVehicle()
    }
    
    
    //MARK: - Verifies selected vehicle Api
    func verifyingTheSelectedVehicle() {
        if selectedIndex == -1 {
            self.present(Helper.alertVC(title: "Message".localized, message:selectVehicle.vehicleID), animated: true, completion: nil)
        }else{
            
            let selectedVehicleInfo = AuthenticationModel.init(vehicleWorkplaceID: vehiclesData[selectedIndex].iD, vehicleID: vehiclesData[selectedIndex].vehicleID, vehicleGoodType: vehiclesData[selectedIndex].goodType)

            vehiclemodl.makeVehicleDefault(params: selectedVehicleInfo, completionHandler: { success in
                if success{
                    UserDefaults.standard.set(self.vehiclesData[self.selectedIndex].vehicleID, forKey: USER_INFO.VEHTYPEID)
                    UserDefaults(suiteName: APPGROUP.groupIdentifier)?.set(self.vehiclesData[self.selectedIndex].vehicleID, forKey: "VEHTYPEID")
                    self.moveTohomeVC()
                    let defaults = UserDefaults.standard
                    defaults.set(self.token, forKey: USER_INFO.SESSION_TOKEN)
                    LocksmithWrapper.addObjectToKeychain(data:USER_INFO.SESSION_TOKEN , value: self.token)
//                    LocksmithWrapper.deleteData()
                    defaults.synchronize()
                }else{
                    
                }
            })
        }
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func moveTohomeVC() {
        dismiss(animated: true, completion: nil)
        // create viewController code...
//        let mapURL = URL(string: mapIcon)
//        let data = Data(content)
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: homeVC)
        
        //  UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
        
        leftViewController.homeVC = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = homeVC
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
        // self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
//        Alamofire.request(mapIcon).responseImage { response in
//            if let catPicture = response.result.value {
//                print("image downloaded: \(catPicture)")
//                let resizedImage = self.resizeImage(image: catPicture, newWidth: 35)
//                let imageData = UIImagePNGRepresentation(resizedImage!)
//                UserDefaults.standard.set(imageData, forKey: "mapIcon")
//            }
//            else
//            {
//                let imageData = UIImagePNGRepresentation(UIImage.init())
//                UserDefaults.standard.set(imageData, forKey: "mapIcon")
//            }
        
        
        
    }
}
