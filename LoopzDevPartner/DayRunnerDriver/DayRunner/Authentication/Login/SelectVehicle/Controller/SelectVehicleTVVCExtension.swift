//
//  SelectVehicleTV.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 23/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension SelectVehicleVC: UITableViewDataSource {
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vehiclesData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"vehicleids") as! VehicleIDsTableViewCell!
        
        if selectedIndex == indexPath.row {
            cell?.selectedButton.isSelected = true
            cell?.backGroundView.backgroundColor = Helper.getUIColor(color: Colors.AppBaseColor)
        }else{
            cell?.selectedButton.isSelected = false
            cell?.backGroundView.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xc2c2c2)
        }
        cell?.vehicleDetails(dict: vehiclesData[indexPath.row])
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 96//UITableView.automaticDimension
    }
}

extension SelectVehicleVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedIndex == indexPath.row{
            selectedIndex = -1
            ConfirmAction.isSelected = false
        }else{
            selectedIndex = indexPath.row
            ConfirmAction.isSelected = true
        }
        
        tableView.reloadData()
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
