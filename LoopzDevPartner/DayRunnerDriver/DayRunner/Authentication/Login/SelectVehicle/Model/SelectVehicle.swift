//
//  SelectVehicle.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 10/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxSwift

class SelectVehicle {
    
     let disposebag = DisposeBag()
    
    func logoutFromApp(completionHandler:@escaping (Bool) -> ()) {
        Helper.showPI(message:"Logging Out..")

//        let params : [String : Any]? =  nil
        let logoutApi =  APIs()
        logoutApi.logoutApi()
        
        logoutApi.subject_response.subscribe(onNext: { (response) in
            Session.expired()
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposebag)
    }
    
    func makeVehicleDefault(params:AuthenticationModel,completionHandler:@escaping (Bool) -> ()) {
        Helper.showPI(message:"Selecting Vehicle..")
        let authenticationApiModel = AuthenticationApis()
        authenticationApiModel.selectVehicle(selectVehicle: params)
        authenticationApiModel.subject_response.subscribe(onNext: { (response) in
            
            completionHandler(true)
            
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposebag)
    }
//        NetworkHelper.requestPOST(serviceName:API.METHOD.VEHICLEDEFAULT,
//                                  params: params,
//                                  success: { (response : [String : Any]) in
//                                    print("signup response : ",response)
//                                    Helper.hidePI()
//                                    if response.isEmpty == false {
//                                        if (response["statusCode"] != nil){
//                                            Helper.hidePI()
//                                            Helper.alertVC(errMSG: "bad request or  internal server error")
//                                        }else{
//                                            let flag:Int = response["errFlag"] as! Int
//                                            if flag == 1{
//                                                Helper.alertVC(errMSG: response["errMsg"] as! String)
//
//                                            }else{
//                                                completionHandler(true)
//                                            }
//                                        }
//
//                                    } else {
//
//                                    }
//        }) { (Error) in
//            Helper.alertVC(errMSG: Error.localizedDescription)
//
//        }
//    }
    
}

