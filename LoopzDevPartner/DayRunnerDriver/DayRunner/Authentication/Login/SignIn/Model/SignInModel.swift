//
//  SignInModel.swift
//  DayRunnerDriverDev
//
//  Created by Vengababu Maparthi on 17/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//
import Foundation
import FirebaseMessaging
import UIKit
import RxCocoa
import RxSwift

protocol SigninModelDelegate{
    func responseOfSigninApi(vehiclesArray: [Signin], sessionToken:String)
}


class Signin:NSObject {
    
    let disposebag = DisposeBag()
    var SIGNINDelegate: SigninModelDelegate! = nil
    
    
    var plateNumber       = ""
    var vehicleType       = ""
    var vehicleModel      = ""
    var iD                = ""
    var vehicleID         = ""
    var goodType          = ""
    var sessionToken      = ""
    var services          = [[String: Any]]()
    var servicesName      = ""
    var vehicleData       = [Signin]()
    var mapIcon           = ""
    var password          = ""
    var userName          = ""
    
    func signinApi(modelName model: AuthenticationModel){
        vehicleData.removeAll()
        
       let signIn =  AuthenticationApis()
       signIn.signInApi(signInModel: model)
       password = model.password
        
       _ = signIn.subject_response.subscribe(onNext: { (response) in
            
            let defaults = UserDefaults.standard
            defaults.set(self.password ,   forKey: USER_INFO.OLDPASSWORD)
//            defaults.set(model.userName, forKey: USER_INFO.SAVEDID)
//            defaults.set(self.password, forKey: USER_INFO.SAVEDPASSWORD)
        
            self.updatesTheVehicleData(dict: response as [String : Any])

        }, onError: { (error) in
            Helper.hidePI()
        }).disposed(by: disposebag)
    }
    
    
    func updatesTheVehicleData(dict:[String:Any]) {
        let defaults = UserDefaults.standard
        let groupDefaults = UserDefaults(suiteName: APPGROUP.groupIdentifier)
        groupDefaults?.set(Utility.appVersion, forKey: "appVersion")
        groupDefaults?.set("", forKey: "bidSum")
        groupDefaults?.set(dict["token"], forKey: "sessionToken")
        
        if let pubTpc  = dict["pushTopic"]   as? String  {
            defaults.set(pubTpc , forKey: USER_INFO.DRIVERPUBCHANNEL)
            Messaging.messaging().subscribe(toTopic: pubTpc)
        }
        
        if let accountType = dict["driverType"] as? Int {
            let defaults = UserDefaults.standard
            defaults.set(accountType, forKey: "storeAccountType")
            defaults.synchronize()
        }
        
        if let deliveryShedule = dict["driverScheduleType"] as? Int {
            let defaults = UserDefaults.standard
            defaults.set(deliveryShedule, forKey: "driverScheduleType")
            defaults.synchronize()
        }else {
            let defaults = UserDefaults.standard
            defaults.set(0, forKey: "driverScheduleType")
            defaults.synchronize()
        }
        
//        if let storeDriverType = dict["storeDriver"] as? Int {
//            let defaults = UserDefaults.standard
//            defaults.set(3, forKey: "storeDriver")
//            defaults.synchronize()
//        }
       
        if let currency = dict["currency"] as? String {
            defaults.set(currency, forKey: "currency")
            defaults.synchronize()
        }
        if let email = dict["email"] as? String {
            defaults.set(email, forKey: "emailString")
            defaults.synchronize()
        }
        if let stripeKey = dict["stripeKey"] as? String {
            defaults.set(stripeKey, forKey: "stripeKey")
            defaults.synchronize()
        }
 
        if let country = dict["country"] as? String {
            defaults.set(country, forKey: "country")
            defaults.synchronize()
        }
        
        if let defaultBankAccount = dict["defaultBankAccount"] as? String {
            defaults.set(defaultBankAccount, forKey: "defaultBankAccount")
            defaults.synchronize()
        }
        
        if let enableBankAccount = dict["enableBankAccount"] as? Bool {
            defaults.set(enableBankAccount, forKey: "enableBankAccount")
            defaults.synchronize()
        }
        
        if let referallCode  = dict["code"]   as? String  {
            defaults.set(referallCode , forKey: USER_INFO.REFERRAL_CODE)
        }
        
        if let tickiteId = dict["requesterId"] as? Int{
            defaults.set(String(tickiteId), forKey: "TickiteID")
        }
        
        if let masterID  = dict["driverId"]  as? String  {
            defaults.set(masterID , forKey: USER_INFO.USER_ID)
        }
        
        if let driverChn  = dict["chn"]    as? String  {
            defaults.set(driverChn , forKey: USER_INFO.DRIVERCHANNEL)
        }
        
        if let cityId  = dict["cityId"]    as? String  {
            defaults.set(cityId , forKey: "cityId")
        }
        
        var newArrayOfZones = [String]()
        newArrayOfZones = dict["serviceZones"] as! [String]
        defaults.set(newArrayOfZones, forKey: "SavedStringArray")
        defaults.synchronize()
        
        
        if let presenceChn  = dict["presence_chn"]   as? String  {
            defaults.set(presenceChn , forKey: USER_INFO.PRESENCECHANNEL)
            groupDefaults?.set(presenceChn, forKey: "presenceTime")
        }
        

        
        defaults.synchronize()
        

        
        if (SIGNINDelegate != nil) {
            if let sessionTok  = dict["token"]   as? String  {

                SIGNINDelegate.responseOfSigninApi(vehiclesArray: vehicleData, sessionToken: sessionTok)
            }
            
        }
    }
}
