//
//  SigninTextFieldVC.swift
//  DayRunnerDriverDev
//
//  Created by Vengababu Maparthi on 17/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension SignInViewController:UITextFieldDelegate{
    
    
    func assignSavedData(){
        if let savedID = Utility.savedID as? String,  let savedPassword = Utility.savedPassword as? String
        {
        emailTextField.text = savedID
        passwrodTextField.text = savedPassword
        }
        else
        {
            
        }

    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        if textField .isEqual(emailTextField){
            passwrodTextField.becomeFirstResponder()
        }
        else{
            signinMethod()
            dismisskeyBord()
        }
        return true
    }
}
