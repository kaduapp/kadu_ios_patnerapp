//
//  SignInViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

enum month {
    case jan,feb, mar, april, may, june, july, aug, sep , Oct, Nov, Dec
}

class SignInViewController: UIViewController, VNHCountryPickerDelegate {

    @IBOutlet weak var dontHaveSignUp: UIButton!
    var activeTextField = UITextField()
    @IBOutlet weak var forgotBtn: UIButton!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var selectLang: UIButton!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var languagePicker: UIPickerView!
    @IBOutlet weak var picketBottomView: NSLayoutConstraint!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var passwrodTextField: HoshiTextField!
    @IBOutlet var emailTextField: HoshiTextField!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet var buttomSignupView: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var countryCodeButton: UIButton!
    @IBOutlet weak var countrySeparaterView: UIView!
    @IBOutlet weak var countryCodeBtnWidth: NSLayoutConstraint!
    var vehicleDetails = [Signin]()
    var SigninModel = Signin()
    var window: UIWindow?
    var countryCodeVar:String = "+57"
    var sessionTkn = String()
    
    let location = LocationManager.sharedInstance
    
    var modelOflan:[LangModel]!
    var apiModel = Languages()
    
    var language:LangModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneButton.setTitleColor(Helper.getUIColor(color: Colors.AppBaseColor), for: .normal)
        emailButton.setTitleColor(Helper.UIColorFromRGB(rgbValue: 0x3B3B3B), for: .normal)
//        emailTextField.placeholder = "Phone Number"
//        emailTextField.keyboardType = .phonePad
        
        phoneButton.setTitle("Phone Number".localized, for: .normal)
        orLabel.text = "OR".localized
        emailButton.setTitle("Email Address".localized, for: .normal)
        passwrodTextField.placeholder = "PASSWORD".localized
        forgotBtn.setTitle("FORGOT PASSWORD ?".localized, for: .normal)
        phoneButton.setTitleColor(Helper.getUIColor(color: Colors.AppBaseColor), for: .normal)
        emailButton.setTitleColor(Helper.UIColorFromRGB(rgbValue: 0x3B3B3B), for: .normal)
        emailTextField.placeholder = "Phone Number".localized
        emailTextField.keyboardType = .phonePad
        selectLang.setTitle("Select Language".localized, for: .normal)
        signInBtn.setTitle("SIGN IN".localized, for: .normal)
        cancelBtn.setTitle("Cancel".localized, for: .normal)
        doneBtn.setTitle("Done".localized, for: .normal)
        if Utility.getSelectedLanguegeCode() == "ar" {
            dontHaveSignUp.setTitle("ليس لديك حساب؟ سجل", for: .normal)
        }else {
             dontHaveSignUp.setTitle("Don't have an account? Sign Up".localized, for: .normal)
        }
       
        self.assignSavedData()
        self.setCountryCode()
        if Utility.getSelectedLanguegeCode() == "ar" {
            emailTextField.textAlignment = .right
            passwrodTextField.textAlignment = .right
        }
        
        self.picketBottomView.constant = -300;
        
        apiModel.languagesAPI { (success) in
            if success{
                self.language = self.apiModel.langData[0]
                self.modelOflan = self.apiModel.langData
                self.languagePicker.reloadAllComponents()
            }
        }
        
    }
    
    
    @IBAction func selectLanButtonAction(_ sender: Any) {
        
//        Utility.setChoosedLanguege(language)
        
        UIView.animate(withDuration: 0.6) {
            self.picketBottomView.constant = 0;
            
        }
        
        //        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelPicker(_ sender: Any) {
        UIView.animate(withDuration: 1) {
            self.picketBottomView.constant = -300;
        }
    }
    @IBAction func donePicker(_ sender: Any) {
        
        Utility.setChoosedLanguege(language)
        
        if Utility.getSelectedLanguegeCode() == "en" {
            OneSkyOTAPlugin.setLanguage("en")
            OneSkyOTAPlugin.checkForUpdate()
        }else {
            OneSkyOTAPlugin.setLanguage("es")
            OneSkyOTAPlugin.checkForUpdate()
        }
        UIView.animate(withDuration: 1) {
            self.picketBottomView.constant = -300;
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - setCountrycode
    func setCountryCode(){
        
        let picker = CountryPicker.dialCode(code: "CO")
        countryCode.text = picker.dialCode
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        location.startUpdatingLocation()
        location.delegate = self
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
//
//        Helper.shadowView(sender: buttomSignupView, width:UIScreen.main.bounds.size.width, height:buttomSignupView.frame.size.height)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        {
            self .moveViewUp(activeView: activeTextField, keyboardHeight: keyboardSize.height)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            // self.moveViewDown()
        }
    }
    
    //*****hide keyboard*****//
    func dismisskeyBord() {
        view.endEditing(true)
    }
    
    /// Move View Up When keyboard Appears
    ///
    /// - Parameters:
    ///   - activeView: View that has Became First Responder
    ///   - keyboardHeight: Keyboard Height
    func moveViewUp(activeView: UIView, keyboardHeight: CGFloat) {
        
        // Get Max Y of Active View with respect their Super View
        var viewMAX_Y = activeView.frame.maxY
        
        // Check if SuperView of ActiveView lies in Nexted View context
        // Get Max for every Super of ActiveTextField with respect to Views SuperView
        if var view: UIView = activeView.superview {
            
            // Check if Super view of View in Nested Context is View of ViewController
            while (view != self.view.superview) {
                viewMAX_Y += view.frame.minY
                view = view.superview!
            }
        }
        
        // Calculate the Remainder
        let remainder = self.view.frame.height - (viewMAX_Y + keyboardHeight)
        
        // If Remainder is Greater than 0 does mean, Active view is above the and enough to see
        if (remainder >= 0) {
            // Do nothing as Active View is above Keyboard
        }
        else {
            // As Active view is behind keybard
            // ScrollUp View calculated Upset
            UIView.animate(withDuration: 0.4,
                           animations: { () -> Void in
                            self.mainScrollView.contentOffset = CGPoint(x: 0, y: -remainder)
            })
        }
        
        // Set ContentSize of ScrollView
        //        var contentSizeOfContent: CGSize = self..frame.size
        //        contentSizeOfContent.height += keyboardHeight
        //        self.mainScrollView.contentSize = contentSizeOfContent
    }
    
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveViewDown() {
        // Resign back to Normal Position having set Content Size as Initial
        self.mainScrollView.contentSize = self.mainScrollView.frame.size
    }

    ///************ segue identifier method****************//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
//        if segue.identifier == "toSelectVehicle" {
//            let nav =  segue.destination as! UINavigationController
//            let nextScene = nav.viewControllers.first as! SelectVehicleVC?
//            nextScene?.vehiclesData = vehicleDetails
//            nextScene?.token = sessionTkn
//        }
//        else
        if(segue.identifier == "toForgotPassword")
        {
            let nav =  segue.destination as! ForgotPasswordWithEmailVC
            nav.typeOfRecovery = sender as! Int
//            nextScene?.vehiclesData = vehicleDetails
//            nextScene?.token = sessionTkn
        } else   if segue.identifier == segues.getCountryCode
        {
            let nav = segue.destination as! UINavigationController
            if let picker: VNHCountryPicker = nav.viewControllers.first as! VNHCountryPicker?
            {
                picker.delegate = self
            }
        }
    }
    
    func didPick(country: VNHCounty) {
        countryCode.text = country.dialCode
        countryCodeVar = country.dialCode
        
    }
    
//    // MARK: - country delegate method
//    internal func didPickedCountry(country: Country)
//    {
//        countryCode.text = country.dialCode
//    }
    
    
    @IBAction func phoneBttnAction(_ sender: UIButton) {
    
        phoneButton.setTitleColor(Helper.getUIColor(color: Colors.AppBaseColor), for: .normal)
        emailButton.setTitleColor(Helper.UIColorFromRGB(rgbValue: 0x3B3B3B), for: .normal)
        emailTextField.placeholder = "PHONE NUMBER*".localized
        emailTextField.keyboardType = .phonePad
        emailTextField.text = ""
        passwrodTextField.text = ""
        countryCode.text = countryCodeVar
        countryCodeBtnWidth.constant = 30.0
        countryCodeButton.isEnabled = true
        countrySeparaterView.isHidden = false
        emailTextField.resignFirstResponder()
    }
    
    @IBAction func emailBttnAction(_ sender: UIButton) {
        
        phoneButton.setTitleColor(Helper.UIColorFromRGB(rgbValue: 0x3B3B3B), for: .normal)
        emailButton.setTitleColor(Helper.getUIColor(color: Colors.AppBaseColor), for: .normal)
        emailTextField.placeholder = "EMAIL*".localized
        emailTextField.keyboardType = .emailAddress
        emailTextField.text = ""
        passwrodTextField.text = ""
        emailTextField.resignFirstResponder()
        countryCodeBtnWidth.constant = 0.0
        countryCode.text = ""
        countryCodeButton.isEnabled = false
        countrySeparaterView.isHidden = true
//        emailTextField.becomeFirstResponder()
    }
    
}

extension SignInViewController:SigninModelDelegate{
    
    func responseOfSigninApi(vehiclesArray: [Signin], sessionToken:String){
        vehicleDetails = vehiclesArray
        sessionTkn =  sessionToken
//        let defaults = UserDefaults.standard
//        defaults.set(sessionTkn, forKey: USER_INFO.SESSION_KEY)
        let defaults = UserDefaults.standard
        defaults.set(sessionTkn, forKey: USER_INFO.SESSION_TOKEN)
        self.moveTohomeVC()
//        self.performSegue(withIdentifier: "toSelectVehicle", sender: nil)
    }
    
    func moveTohomeVC() {
        dismiss(animated: true, completion: nil)
        // create viewController code...
        //        let mapURL = URL(string: mapIcon)
        //        let data = Data(content)
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: homeVC)
        
        //  UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
        
        leftViewController.homeVC = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = homeVC
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate{
                       appDelegate.window!.rootViewController = slideMenuController
                       appDelegate.window!.makeKeyAndVisible()
                   }
    }

}

extension SignInViewController:LocationManagerDelegate{
    
    func locationFound(_ latitude: Double, longitude: Double) {
    }
    
    func locationFoundGetAsString(_ latitude: NSString, longitude: NSString) {
    }
}

extension SignInViewController : UIPickerViewDelegate,UIPickerViewDataSource {
    //MARK: - Pickerview method
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if self.modelOflan != nil  {
            return self.modelOflan!.count
        } else {
            return 1
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if self.modelOflan != nil  {
            return self.modelOflan![row].name
        } else {
            return "English"
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.language = self.modelOflan![row]
    }
}
