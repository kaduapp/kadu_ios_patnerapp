//
//  SigninButtonVC.swift
//  DayRunnerDriverDev
//
//  Created by Vengababu Maparthi on 17/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension SignInViewController {
    
    @IBAction func countryCodeAction(_ sender: UIButton) {
        performSegue(withIdentifier:segues.getCountryCode, sender: nil)
    }
    
    @IBAction func tapGestureAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    //******** Moves to retrive password*********//
    @IBAction func forgotPasswordAction(_ sender: Any) {
        
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please Select Method to Reset Password".localized, message: "Options to select:".localized, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel".localized, style: .cancel) { _ in
            print("Cancel")
        }
        
        let saveActionButton = UIAlertAction(title: "Via Phone Number.".localized, style: .default)
        { _ in
            self.performSegue(withIdentifier: "toForgotPassword", sender: 1)
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        actionSheetControllerIOS8.addAction(cancelActionButton)

        let deleteActionButton = UIAlertAction(title: "Via Email.".localized, style: .default)
        { _ in
            self.performSegue(withIdentifier: "toForgotPassword", sender: 2)
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
        
    }
    
    @IBAction func signInAction(_ sender: Any) {
        dismisskeyBord()
        signinMethod()
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        performSegue(withIdentifier: "toSignup", sender: nil)
//        var signInModel = AuthenticationModel.init(userName: "dhruv@mobifyi.com", password: "321")
//        let signIn =  AuthenticationApis()
//        signIn.signInApi(signInModel: signInModel)
//
//        signIn.subject_response.subscribe(onNext: { (response) in
//
//        }, onError: { (error) in
//            Helper.hidePI()
//        }, onCompleted: {
//
//        }) {
//
//        }
    }
    
    ///*************checking the textfield data*******//
    func signinMethod(){
        if (emailTextField.text?.isEmpty)!{
            self.present(Helper.alertVC(title: "Message".localized, message:"Please enter Email Address or Phone number".localized), animated: true, completion: nil)
        }
        else if (passwrodTextField.text?.isEmpty)!{
            self.present(Helper.alertVC(title: "Message".localized, message:"Please enter Password".localized), animated: true, completion: nil)
        }
        else{
            sendRequestForSignup()
        }
    }
    
    func sendRequestForSignup() {
        
        Helper.showPI(message:"Signing in..")
       let signInModelParams = AuthenticationModel(userName: emailTextField.text!, password: passwrodTextField.text!, countryCode: countryCodeVar)
        SigninModel.SIGNINDelegate = self
        SigninModel.signinApi(modelName: signInModelParams)
        
    }
}
