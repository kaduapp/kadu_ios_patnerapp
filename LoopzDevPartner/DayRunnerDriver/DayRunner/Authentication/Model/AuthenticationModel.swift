//
//  AuthenticationModel.swift
//  KarruPro
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class AuthenticationModel: NSObject {
    
    override init()
    {
        super.init()
    }
    
    //Common Params
    let deviceID: String = Utility.deviceId
    let appVersion: String = Utility.appVersion
    let pushToken: String =  Utility.pushToken
    let devMake: String = "Apple"
    let devModel: String = Utility.modelName
    let deviceTime: String = Helper.currentGMTDateTime
    let mid: String = Utility.userId
    var countryCode: String = ""
    var mobileNo: String = ""
    var fullMobileNo: String = ""
    
    //Sign In params
    var userName: String = ""
    var password: String = ""
    var devType = "1"
    
    init(userName: String, password: String, countryCode:String)
    {
        self.countryCode = countryCode
        self.userName = userName
        self.password = password
    }
    
    //Selected Vehicle
    var vehicleWorkplaceID: String = ""
//    var vehicleID: String! = ""
    var vehicleGoodType: String = ""
    
    init(vehicleWorkplaceID: String, vehicleID: String, vehicleGoodType: String)
    {
        self.vehicleWorkplaceID = vehicleWorkplaceID
//        self.vehicleID = vehicleID
        self.vehicleGoodType = vehicleGoodType
    }
    
    //Verify Refferal Code
    var refferalCode: String = ""
    var latitude: NSNumber!
    var longitude: NSNumber!
    
    init(refferalCode: String, latitude: String, longitude: String)
    {
        self.refferalCode = refferalCode
        self.latitude = NSNumber.aws_number(from: latitude)
        self.longitude = NSNumber.aws_number(from:longitude)
    }
    
    //Verify Email/Phone
    var verifal: String = ""
    var validateType: String = ""

    init(verifal: String, validateType: String)
    {
        self.verifal = verifal
        self.validateType = validateType
    }
    
    init(verifal: String, validateType: String, countryCode: String)
    {
        self.verifal = verifal
        self.validateType = validateType
        self.countryCode = countryCode
    }
    
    //get OTP
    var userType: String = ""
    init(countryCode: String, fullMobileNo: String, userType: String)
    {
        self.countryCode = countryCode
        self.fullMobileNo = fullMobileNo
        self.userType = userType
    }
    
    //verify OTP
    var processType: Int = 0
    var code: Int = 0
    
    init(fullMobileNo: String, countryCode: String, code: String, processType: Int)
    {
        self.fullMobileNo = fullMobileNo
        self.countryCode  = countryCode
        self.code = Int(code)!
        self.processType = processType
    }
    
    //signUP
    var firstName: String = ""
    var lastName: String = ""
    var email: String = ""
    var zipCode: String = "226024"
    var profilePic: String = ""
//    var plateNo: String! = ""
    var type: String = ""
//    var specialities: String! = ""
//    var make: String! = ""
//    var model: String! = ""
//    var vehicleImage: String! = ""
    var driverLicense: String = ""
    var deviceType: String = "1"
//    var zones: [String]!
    var cityId = ""
    var cityName = ""
    var licenseNumber = ""
    var licenseExp =  ""
    var dob = ""
    var selectedZones: [String] = []
//    var insurancePhoto: String! = ""
//    var carriagePhoto: String! = ""
//    var regCert: String! = ""
//    var carrierPermit: String! = ""
    var cityID: String = ""
    var insuranceNo: String = ""
    var serviceArray: [[String:String]]!
}


