//
//  ForgotPasswordModel.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift

class ForgotPasswordModel: NSObject
{
    let disposebag = DisposeBag()
    //Forgot Password API Vars
    var email: String = ""
//    var userType = 2
    var type: Int = 0
    var countryCode: String = ""
    override init()
    {
        super.init()
    }
    
    init(forgotPassword email: String, type: Int, countryCode:String)
    {
        self.email = email
        self.type = type
        self.countryCode = countryCode
    }
    
    func forgotPasswordValidateToCreateNewPassword(params:ForgotPasswordModel,completionHandler:@escaping(Bool) -> ()){
        
        let forgotPassword =  ProfileApi()
        forgotPassword.createNewPassword(params: params)
        forgotPassword.subject_response.subscribe(onNext: { (response) in
            Helper.hidePI()
            if response.isEmpty == false {
                
                if (response["message"] as? String) != nil {
//                    Helper.alertVC(errMSG: responseMessage)
                    completionHandler(true)
                }else{
                    Helper.alertVC(errMSG: response["message"] as! String)
                    completionHandler(false)
                }
                
            } else {
                completionHandler(false)
            }

        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposebag)
    }
}
