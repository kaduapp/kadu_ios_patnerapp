//
//  ForgotPasswordViewAdjust.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension ForgotPasswordWithEmailVC{
    /// Move View Up When keyboard Appears
    ///
    /// - Parameters:
    ///   - activeView: View that has Became First Responder
    ///   - keyboardHeight: Keyboard Height
    func moveViewUp(keyboardHeight: CGFloat) {
        phoneTF.becomeFirstResponder()
//        UIView.animate(withDuration: 0.4,
//                       animations: { () -> Void in
//                        self.buttomConstrain.constant = keyboardHeight
//                        self.view.layoutIfNeeded()
//        })
    }
    
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveViewDown() {
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.buttomConstrain.constant = 0
                        self.view.layoutIfNeeded()
        })
    }
    
}
