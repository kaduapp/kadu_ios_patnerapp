//
//  ForgotPasswordWithEmailVC.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ForgotPasswordWithEmailVC: UIViewController, VNHCountryPickerDelegate {
    
    var forgotEmailOrPhone: Int!
    
    @IBOutlet weak var navTopLabel: UILabel!
    @IBOutlet weak var countryLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelEnterEmailPhoneHeading: UILabel!
    @IBOutlet var countryCode: UILabel!
    @IBOutlet var countryImage: UIImageView!
    
    @IBOutlet weak var separatingView: UIView!
    @IBOutlet weak var getCountryCodebutton: UIButton!
    @IBOutlet weak var labelEnterPhoneNumber: UILabel!
    
    @IBOutlet weak var nextbtn: UIButton!
    @IBOutlet var contentScrollView: UIView!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var phoneTF: UITextField!
    @IBOutlet var buttomConstrain: NSLayoutConstraint!
    var typeOfRecovery:Int = 0
    
    var forgotPasswordModel = ForgotPasswordModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCountryCode()
        navTopLabel.text = "FORGOT PASSWORD ?".localized
        nextbtn.setTitle("CONFIRM".localized, for: .normal)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        self.labelEnterPhoneNumber.text = ""
        self.labelEnterEmailPhoneHeading.text = ""
        switch typeOfRecovery {
        case 1:
            separatingView.isHidden = false
            countryCode.isHidden = false
            getCountryCodebutton.isEnabled = true
            countryLabelConstraint.constant = 0
            self.labelEnterPhoneNumber.text = "Enter your phone number and you will get a verification code to reset Password".localized;            self.labelEnterEmailPhoneHeading.text = "Phone Number".localized
            self.phoneTF.placeholder = "Phone Number".localized
            self.phoneTF.keyboardType = .numberPad
            break
        default:
            separatingView.isHidden = true
            countryCode.isHidden = true
            getCountryCodebutton.isEnabled = false
            countryLabelConstraint.constant = -30
            self.labelEnterPhoneNumber.text = "Enter your email and you will get a verification code to reset password".localized
            self.labelEnterEmailPhoneHeading.text = "Email Address".localized
            self.phoneTF.placeholder = "Email Address".localized
            self.phoneTF.keyboardType = .emailAddress
        }
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyBoard))
        view.addGestureRecognizer(tap)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.phoneTF.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
        
//         self.phoneTF.becomeFirstResponder()
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    //MARK: - Keyboard Methods
    @objc func keyboardWillShow(notification: NSNotification) {
        UIView.animate(withDuration: 0.4, animations: {() -> Void in
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                 self.buttomConstrain.constant = keyboardSize.height
            }
        })
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.moveViewDown()
    }
    
    ///********dismissing the vc*******//
    @IBAction func backToController(_ sender: Any) {
        
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func getCountryCode(_ sender: UIButton) {
        performSegue(withIdentifier: "toGetCountryCode", sender: self)
    }
    
    @IBAction func generateOTP(_ sender: Any) {
        if (phoneTF.text?.isEmpty)! {
            self.present(Helper.alertVC(title: "Message".localized, message:"Please enter Mobile Number or Email ID".localized), animated: true, completion: nil)
        }else{
            self.forgotPasswordRecovery()
        }
    }
    
    //*******hide keyboard***********//
    @IBAction func hideKeyBoard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    // MARK: - setCountrycode
    func setCountryCode(){
        let picker = CountryPicker.dialCode(code: "CO")
        countryCode.text = picker.dialCode
        //        countryImage.image = picker.flag
    }
    
    ///************ segue identifier method****************//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "toVerifyOTP" {
            let nextScene = segue.destination as? VerifyOTPVC
            nextScene?.mobileNumber = phoneTF.text!
            nextScene?.countryCode = countryCode.text!
            nextScene?.defineTheOtp = 2
        } else if segue.identifier == "toGetCountryCode" {
            let nav = segue.destination as! UINavigationController
            if let picker: VNHCountryPicker = nav.viewControllers.first as! VNHCountryPicker?
            {
                picker.delegate = self
            }
        }
    }
    
    // MARK: - country delegate method
    
    func didPick(country: VNHCounty) {
        countryCode.text = country.dialCode
    }
    
    //        func didPickedCountry(country: Country) {
    //                        countryCode.text = country.dialCode
    //        }
    
    
    func forgotPasswordRecovery(){
        
        let testString = phoneTF.text!
        
        
        let phone = testString.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        if phone == testString {
            typeOfRecovery = 1 // for mobile
        }else{
            typeOfRecovery = 2 // for email
        }
        
        print(phone)
        
        Helper.showPI(message:"Loading..")
        
        
        let forgotPassword = ForgotPasswordModel.init(forgotPassword: phoneTF.text!, type: typeOfRecovery, countryCode: countryCode.text! )
        forgotPasswordModel.forgotPasswordValidateToCreateNewPassword(params: forgotPassword, completionHandler: { success in
            if success{
                
                if  self.typeOfRecovery == 1{
                    self.performSegue(withIdentifier: "toVerifyOTP", sender: nil)
                }else{
                    self.present(Helper.alertVC(title: "Message".localized, message:"Email with the reset link has been sent, please check your inbox."), animated: true, completion: nil)
                    _ = self.navigationController?.popToRootViewController(animated: true)
                    
                }
            }else{
                
            }
        })
    }
}

extension ForgotPasswordWithEmailVC:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (phoneTF.text?.isEmpty)! {
            self.present(Helper.alertVC(title: "Message".localized, message:"Please enter Mobile Number or Email ID".localized), animated: true, completion: nil)
        }else{
            self.forgotPasswordRecovery()
        }
        self.view.endEditing(true)
        return true
    }
}
