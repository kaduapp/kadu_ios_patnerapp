//
//  SplashViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import UserNotifications

class SplashViewController: UIViewController {
    var window: UIWindow?
    
    @IBOutlet var splashImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.window                                    = UIApplication.shared.keyWindow
        
        let screenSize                        : CGRect = UIScreen.main.bounds
      
        
        UIApplication.shared.isStatusBarHidden = false
        
        LoopzNotification.sharedInstance.requestPermissionsWithCompletionHandler { (granted) -> (Void) in
            DispatchQueue.main.async {
                if granted {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let defaults = UserDefaults.standard
        defaults.setValue(deviceID, forKey:USER_INFO.DEVICE_ID)
        defaults.synchronize()
        
        Timer.scheduledTimer(timeInterval: 2.0,
                             target: self,
                             selector: #selector(self.methodToBeCalled),
                             userInfo: nil,
                             repeats: false); //
    }
    
    
    
    /// Moves to helpVC
 @objc func methodToBeCalled(){
    if Utility.getSelectedLanguegeCode() == "ar" {
        OneSkyOTAPlugin.setLanguage("ar-SA")
    }else {
        OneSkyOTAPlugin.setLanguage("es")
    }
    OneSkyOTAPlugin.checkForUpdate()
    if  Utility.getSelectedLanguegeCode() == "ar"  {
        UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
    } else {
        UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
    }
    
    
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
            performSegue(withIdentifier: "toSignin", sender: nil)
 
        }else{
            self.moveTohomeVC()
        }
    }
    
    
    func moveTohomeVC() {
        dismiss(animated: true, completion: nil)
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        let right = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: homeVC)
        
        //  UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
        
        leftViewController.homeVC = nvc
        if Utility.getSelectedLanguegeCode() == "ar" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
            let slideMenuController = ExSlideMenuController(mainViewController:nvc, rightMenuViewController: right)
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            slideMenuController.delegate = homeVC
             if let appDelegate = UIApplication.shared.delegate as? AppDelegate{
                           appDelegate.window!.rootViewController = slideMenuController
                           appDelegate.window!.makeKeyAndVisible()
                       }
        }else{
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
            let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController)
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            slideMenuController.delegate = homeVC
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate{
                appDelegate.window!.rootViewController = slideMenuController
                appDelegate.window!.makeKeyAndVisible()
            }
        }
        
        
    }
    
}
