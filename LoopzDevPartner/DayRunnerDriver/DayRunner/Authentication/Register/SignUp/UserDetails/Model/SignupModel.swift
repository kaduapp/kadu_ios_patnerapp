//
//  SignupModel.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift

class SignupModel: NSObject {
    var disposeBag = DisposeBag()
    func verifyingTheReferral(params:AuthenticationModel,completionHandler:@escaping (Bool) -> ()) {
        let refferalApi =  AuthenticationApis()
        refferalApi.verifyRefferalCode(referalCode: params)
        refferalApi.subject_response.subscribe(onNext: { (response) in
            completionHandler(true)
             Helper.hidePI()
            
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
        

    }
    
    
    //Validate Phone number and Email id
    func validateTheEmailIDPhone(params:AuthenticationModel,completionHandler:@escaping (Bool) -> ()) {
        let validateEmailPhone =  AuthenticationApis()
        validateEmailPhone.verifyPhoneOrEmail(phoneOrEmail: params)
        validateEmailPhone.subject_response.subscribe(onNext: { (response) in
            if let errFlag = response["errFlag"] as? Int
            {
                switch errFlag
                {
                case 200:
                    completionHandler(true)
                default:
                    completionHandler(false)
                }
            }
            
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: validateEmailPhone.disposeBag)
        

    }


}
