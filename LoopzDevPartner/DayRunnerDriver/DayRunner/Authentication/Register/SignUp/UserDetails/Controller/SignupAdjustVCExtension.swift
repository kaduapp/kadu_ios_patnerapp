//
//  SignupAdjustViewC.swift
//  GoGoCargoDriver
//
//  Created by Vengababu Maparthi on 23/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension SignUpViewController{
    /// Move View Up When keyboard Appears
    ///
    /// - Parameters:
    ///   - activeView: View that has Became First Responder
    ///   - keyboardHeight: Keyboard Height
    func moveViewUp(activeView: UIView, keyboardHeight: CGFloat) {
        
        // Get Max Y of Active View with respect their Super View
        var viewMAX_Y = activeView.frame.maxY
        
        // Check if SuperView of ActiveView lies in Nexted View context
        // Get Max for every Super of ActiveTextField with respect to Views SuperView
        if var view: UIView = activeView.superview {
            
            // Check if Super view of View in Nested Context is View of ViewController
            while (view != self.view.superview) {
                viewMAX_Y += view.frame.minY
                view = view.superview!
            }
        }
        
        // Calculate the Remainder
        let remainder = self.view.frame.height - (viewMAX_Y + keyboardHeight)
        
        // If Remainder is Greater than 0 does mean, Active view is above the and enough to see
        if (remainder >= 0) {
            // Do nothing as Active View is above Keyboard
        }
        else {
            // As Active view is behind keybard
            // ScrollUp View calculated Upset
            UIView.animate(withDuration: 0.4,
                           animations: { () -> Void in
                            self.mainScrollView.contentOffset = CGPoint(x: 0, y: -remainder)
            })
        }
        
        // Set ContentSize of ScrollView
//        var contentSizeOfContent: CGSize = self.contentScrollView.frame.size
//        contentSizeOfContent.height += keyboardHeight
//        self.mainScrollView.contentSize = contentSizeOfContent
    }
    
    
    /// Move View Down / Normal Position When keyboard disappears
//    func moveViewDown() {
//        self.mainScrollView.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(0))
//        let contentSizeOfContent: CGSize = self.contentScrollView.frame.size
//        self.mainScrollView.contentSize = contentSizeOfContent
//    }
    
    
}
