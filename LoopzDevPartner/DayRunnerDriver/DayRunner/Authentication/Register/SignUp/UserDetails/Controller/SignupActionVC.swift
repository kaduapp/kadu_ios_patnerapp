//
//  SignupActionVC.swift
//  GoGoCargoDriver
//
//  Created by Vengababu Maparthi on 23/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension SignUpViewController{
    
    // MARK: - Tapgesture
    @IBAction func tapGestureAction(_ sender: Any) {
        self.dismisskeyBord()
    }
    
    /// hide keyboard
    func dismisskeyBord() {
        if (referralcodeTF.text?.isEmpty)! {
            
        }else{
            if !referralService {
//                verifyingTheReferral()
            }
        }
        view.endEditing(true)
    }
    
    @IBAction func selectVehicleDoc(_ sender: Any) {
        selectImageTyep = 1 // For licence images tag
        selectImage()
    }
    
    @IBAction func selectProfilePic(_ sender: Any) {
        selectImageTyep = 0 //ForProfilePic tag
        selectImage()
    }
    
    @IBAction func backToController(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    

    
    @IBAction func selectOperator(_ sender: UIButton) {
        
        typeModelMake = 5 // for operator selection tag
        self.view.endEditing(true)
        performSegue(withIdentifier:"toSelectOperator" , sender: nil)
    }
    
    @IBAction func selectZones(_ sender: UIButton) {
        if self.zonesID.isEmpty {
            Helper.alertVC(errMSG: "Please select yo ur city of operation.".localized)
        } else {
            typeModelMake = 6 // for zones selection tag
            self.view.endEditing(true)
            performSegue(withIdentifier:"toSelectOperator" , sender: nil)
        }
    }
    
    @IBAction func goToVehicleDetails(_ sender: Any) {
            self.SignupButton.isEnabled = false
            signupMethod()
    }
    
    
    ///*******Select country mobile code***************//
    @IBAction func selectTheCountryCode(_ sender: Any) {
        
        performSegue(withIdentifier:segues.countryPicker!, sender: nil)
    }
    
}
