//
//  SignupTextFieldVC.swift
//  DayRunnerDriverDev
//
//  Created by Vengababu Maparthi on 17/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

// MARK: - Textfield delegate method
extension SignUpViewController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        if textField == password && (emailAddress.text?.characters.count)!>2{
            
            if !(emailAddress.text?.isValidEmail)!{
                emailAddress.becomeFirstResponder()
                self.present(Helper.alertVC(title: "Message".localized, message:"Enter Valid email id".localized), animated: true, completion: nil)
            }else{
                if !emailService {
                    self.validateEmailPhone(typeValidate: "2")
                }
            }
            
        }else if textField == emailAddress && (phoneNumber.text?.count)!>2{
            
            if !mobileService {
                self.validateEmailPhone(typeValidate: "1")
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumber {
            mobileService = false
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        if textField == emailAddress {
            emailService = false
        }
        
        if textField ==  referralcodeTF{
            referralService = false
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {  //delegate method
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        switch textField {
        case firstNAme:
            lastName.becomeFirstResponder()
            break
        case lastName:
            phoneNumber.becomeFirstResponder()
            break
        case phoneNumber:
            emailAddress.becomeFirstResponder()
            break
            
        case emailAddress:
            password.becomeFirstResponder()
            break
            
        default:
            if (referralcodeTF.text?.isEmpty)! {
                
            }else{
                if !referralService {
//                    verifyingTheReferral()
                }
            }
            
            dismisskeyBord()
            break
        }
        
        return true
    }
}

