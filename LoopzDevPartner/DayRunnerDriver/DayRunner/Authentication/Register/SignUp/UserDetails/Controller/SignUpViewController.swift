//
//  SignUpViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController,UINavigationControllerDelegate,VNHCountryPickerDelegate  {
    
    @IBOutlet weak var drivingLicenceLbl: AlignmentOfLabel!
    @IBOutlet weak var personlbl: AlignmentOfLabel!
    @IBOutlet weak var datePkBottomConstrain: NSLayoutConstraint!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var dobTF: HoshiTextField!
    @IBOutlet weak var licenseExpTF: HoshiTextField!
    @IBOutlet weak var selectZones: HoshiTextField!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var datePickerBottomView: UIView!
    
    @IBOutlet weak var buttonBottomConstraint: NSLayoutConstraint!
    @IBOutlet var selectOperator: UIButton!
//    @IBOutlet var contentScrollView: UIView!
    @IBOutlet var profileImage: UIImageView!
    
//    @IBOutlet var countryImage: UIImageView!
    
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var countryCode: UILabel!
    
    @IBOutlet var collectionView: UICollectionView!
    
    @IBOutlet var SignupButton: UIButton!
    
    @IBOutlet var operatorTextField: UITextField!
    
    @IBOutlet var heightOfOperatorField: NSLayoutConstraint!
    @IBOutlet var lastName: HoshiTextField!
    @IBOutlet var firstNAme: HoshiTextField!
    @IBOutlet var password: HoshiTextField!
    @IBOutlet var emailAddress: HoshiTextField!
    @IBOutlet var phoneNumber: HoshiTextField!
    
    @IBOutlet var referralcodeTF: HoshiTextField!
    @IBOutlet var operatorButton: UIButton!
    @IBOutlet weak var signupLabel: UILabel!
    
    @IBOutlet weak var zonesImage: UIImageView!
    
    @IBOutlet weak var dateOfBirthImage: CustomImageView!
    
    @IBOutlet weak var expiryImage: CustomImageView!
    
    @IBOutlet weak var operatorImage: UIImageView!
    @IBOutlet weak var zonesTF: HoshiTextField!
    //    @IBOutlet var zonesTF: HoshiTextField!
//    @IBOutlet var operatorImage: UIImageView!
    @IBOutlet var topSpaceForOperatorField: NSLayoutConstraint!
    @IBOutlet var freeLancerButton: UIButton!
    var typeModelMake:Int = 0
    var activeTextField = UITextField()
    var selectImageTyep:Int = 0
    var myImages = [UIImage]()
    var signuParameters = [String: Any]()
    var licenceImages = String()
    var profileUrl  = String()
    var pickedImage = UIImageView()
    var latit = String()
    var longit = String()
    var temp = [UploadImage]()
    var signupModel = SignupModel()
    var signUpParams = AuthenticationModel()
    var vehicleDetailsModel = VehicleModel()
    var mobileService = false
    var emailService = false
    var referralService = false
    var licenceAction = false
    
    var timeStamp = String()
    
    let location = LocationManager.sharedInstance
    
//    var zonesID = [String]()
    var zonesID = ""
    var selctedZonesfromCities: [String] = []
    var indexesSelected   = [Int]()
    var cityIndexSelected = [Int]()
    
    override func viewDidLoad() {
        signupLabel.text = "SIGN UP".localized
        personlbl.text =  "PERSONAL DETAILS".localized
        firstNAme.placeholder = "FIRST NAME*".localized//OneSkyOTAPlugin.localizedString(forKey: "FIRST NAME*", value: "FIRST NAME*", table: nil)
        lastName.placeholder = "LAST NAME".localized
        phoneNumber.placeholder = "PHONE NUMBER*".localized
        emailAddress.placeholder = "EMAIL*".localized
        password.placeholder = "PASSWORD*".localized
        zonesTF.placeholder = "CITY*".localized
        selectZones.placeholder = "ZONES*".localized
        dobTF.placeholder = "DATE OF BIRTH*".localized
        licenseExpTF.placeholder = "LICENSE EXPIRY DATE*".localized
        drivingLicenceLbl.text = "DRIVING LICENCE*".localized
        referralcodeTF.placeholder = "LICENSE NUMBER*"
        datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        pickedImage.image = nil
        setCountryCode()
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        datePickerBottomView.isHidden = true
        firstNAme.autocapitalizationType = .words
        lastName.autocapitalizationType = .words;
        location.startUpdatingLocation()
        location.delegate = self
        SignupButton.setTitle("CONFIRM".localized, for: UIControl.State.normal)
        
        if Utility.getSelectedLanguegeCode() == "ar" {
            firstNAme.textAlignment = .right
            lastName.textAlignment = .right
            phoneNumber.textAlignment = .right
            password.textAlignment = .right
            emailAddress.textAlignment = .right
            selectZones.textAlignment = .right
            zonesTF.textAlignment = .right
            dobTF.textAlignment = .right
            licenseExpTF.textAlignment = .right
            operatorImage.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
            zonesImage.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
   
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
        


    }
    
    func moveViewUp(keyboardHeight: CGFloat) {
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.buttonBottomConstraint.constant = keyboardHeight
                        self.view.layoutIfNeeded()
        })
    }
    
    
    
    
//***   Move View Down / Normal Position When keyboard disappears ***//
    
    func moveViewDown() {
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.buttonBottomConstraint.constant = 0
                        self.view.layoutIfNeeded()
        })
    }
    
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        {
            self.moveViewUp(keyboardHeight: keyboardSize.height)
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func dateChanged(_ sender: UIDatePicker) {
            let componenets = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
            if let day = componenets.day, let month = componenets.month, let year = componenets.year {
                dateLabel.text = "\(year)/\(month)/\(day)"
            }
    }
    
    // MARK: - SignupMethod
    func signupMethod(){
        if pickedImage.image == nil{
            self.present(Helper.alertVC(title: "Message".localized, message: "Please upload your profile picture.".localized), animated: true, completion: nil)
            self.SignupButton.isEnabled = true
        }
        else if (firstNAme.text?.isEmpty)!{
            self.present(Helper.alertVC(title:"Message".localized, message:"Please enter your first name.".localized), animated: true, completion: nil)
            self.SignupButton.isEnabled = true
        } else if (phoneNumber.text?.isEmpty)!{
            self.present(Helper.alertVC(title: "Message".localized, message:"Please enter your phone number".localized), animated: true, completion: nil)
            self.SignupButton.isEnabled = true
        } else if ((phoneNumber.text?.count)! < 6) || ((phoneNumber.text?.count)! > 16)
        {
            Helper.alertVC(errMSG: "Please enter a valid phone number".localized)
            self.SignupButton.isEnabled = true
        }
            
        else if (emailAddress.text?.isEmpty)!{
            self.present(Helper.alertVC(title: "Message".localized, message:"Please enter your email address".localized), animated: true, completion: nil)
            self.SignupButton.isEnabled = true
        } else if (password.text?.isEmpty)! {
            self.present(Helper.alertVC(title: "Message".localized, message:"Please enter a password.".localized), animated: true, completion: nil)
            self.SignupButton.isEnabled = true
        } else if (password.text?.count)! < 6 {
            self.present(Helper.alertVC(title: "Message".localized, message:"Please input a minimum of 6 characters with atleast one numeric value(0-9).".localized), animated: true, completion: nil)
            self.SignupButton.isEnabled = true
        }else if !(emailAddress.text?.isValidEmail)!{
            emailAddress.becomeFirstResponder()
            self.present(Helper.alertVC(title: "Message".localized, message:"Enter Valid email id".localized), animated: true, completion: nil)
            self.SignupButton.isEnabled = true
        } else if (zonesTF.text?.isEmpty)! {
            self.present(Helper.alertVC(title: "Message".localized, message:"Please select yo ur city of operation.".localized), animated: true, completion: nil)
            self.SignupButton.isEnabled = true
        } else if (selectZones.text?.isEmpty)! {
            self.SignupButton.isEnabled = true
           self.present(Helper.alertVC(title: "Message".localized, message:"Please select your zones of operation".localized), animated: true, completion: nil)
        }
        else if (dobTF.text?.isEmpty)! {
            self.present(Helper.alertVC(title: "Message".localized, message:"Please select your date of birth.".localized), animated: true, completion: nil)
            self.SignupButton.isEnabled = true
        }
        else if (licenseExpTF.text?.isEmpty)! {
            self.present(Helper.alertVC(title: "Message".localized, message:"Please select your License expiry date.".localized), animated: true, completion: nil)
            self.SignupButton.isEnabled = true
        }
        else if myImages.count == 0{
            self.present(Helper.alertVC(title: "Message".localized, message:"Please upload your driving license.".localized), animated: true, completion: nil)
            self.SignupButton.isEnabled = true
        } else if myImages.count == 1{
            self.present(Helper.alertVC(title: "Message".localized, message:"Please upload your driving license back image.".localized), animated: true, completion: nil)
            self.SignupButton.isEnabled = true
        }
        else{
            temp = [UploadImage]()
            timeStamp = Helper.currentTimeStamp
            temp.append(UploadImage.init(image: myImages[0], path: AMAZONUPLOAD.DRIVERLICENCE +  timeStamp + "_0_07" + ".png"))
            temp.append(UploadImage.init(image: myImages[1], path: AMAZONUPLOAD.DRIVERLICENCE +  timeStamp + "_0_08" + ".png"))
            self.licenceImages = Utility.amazonUrl + AMAZONUPLOAD.DRIVERLICENCE  + timeStamp +   "_0_07" + ".png" + "," + Utility.amazonUrl + AMAZONUPLOAD.DRIVERLICENCE  + timeStamp +   "_0_08" + ".png"
            temp.append(UploadImage.init(image: profileImage.image!, path: AMAZONUPLOAD.PROFILEIMAGE +  timeStamp + "_0_01" + ".png"))
            let upMoadel = UploadImageModel.shared
            upMoadel.uploadImages = temp
            upMoadel.start()
            saveTheParameters()
            let otpAuthentication = AuthenticationModel.init(countryCode: String(describing: signUpParams.countryCode),fullMobileNo: String(describing: signUpParams.mobileNo), userType: "2")
            
            
            vehicleDetailsModel.signUpOtpService(params: otpAuthentication, completionHanlder: { success in
                
                
                self.SignupButton.isEnabled = true
                
                if success{
                    self.performSegue(withIdentifier:"signupVerification" , sender: nil)
                }else {
               
                }
                
            })
//            performSegue(withIdentifier:segues.vehicleDetails! , sender: nil)
        }
    }
    
    
    
    
    func saveTheParameters(){
        
        let profileIMG = Utility.amazonUrl + AMAZONUPLOAD.PROFILEIMAGE +  timeStamp + "_0_01" + ".png"
        signUpParams.firstName = firstNAme.text!
        signUpParams.lastName = lastName.text!
        signUpParams.email = emailAddress.text!
        signUpParams.password = password.text!
        signUpParams.mobileNo = phoneNumber.text!
        signUpParams.countryCode = countryCode.text!
        signUpParams.zipCode = "226024"
        signUpParams.latitude = NSNumber.aws_number(from: latit)
        signUpParams.longitude = NSNumber.aws_number(from:longit)
        signUpParams.profilePic = profileIMG
        signUpParams.driverLicense = self.licenceImages
        signUpParams.cityId = zonesID
        signUpParams.cityName = zonesTF.text!
        signUpParams.licenseNumber = referralcodeTF.text!
        signUpParams.licenseExp  = licenseExpTF.text!
        signUpParams.dob         = dobTF.text!
        signUpParams.selectedZones = selctedZonesfromCities

    }
    
    /// MARK: - actionSheet
    func selectImage() {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select image".localized as String?,
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel".localized as String?,
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: "Gallery".localized as String?,
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: "Camera".localized as String?,
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        //        if pickedImage.image != nil {
        //            let removePhoto: UIAlertAction = UIAlertAction(title: "Remove Photo" as String?,
        //                                                           style: .cancel) { action -> Void in
        //                                                            self.removePhoto()
        //            }
        //            actionSheetController.addAction(removePhoto)
        //        }
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    // MARK: - photo gallery
    func removePhoto()
    {
        profileUrl = ""
        pickedImage.image = nil
        profileImage.image=UIImage(named: "userdefaulticon")
        
    }
    
    
    // MARK: - photo gallery
    func chooseFromPhotoGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    // MARK: - Camera selection
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.camera;
            
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
            noCamera()
        }
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    // MARK: - setCountrycode
    func setCountryCode(){
        let country = NSLocale.current.regionCode
        
        let picker = CountryPicker.dialCode(code: "CO")
        countryCode.text = picker.dialCode
//        countryImage.image = picker.flag
    }
    
    ///************ segue identifier method****************//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == segues.countryPicker! {
            let nav = segue.destination as! UINavigationController
            if let picker: VNHCountryPicker = nav.viewControllers.first as! VNHCountryPicker?
            {
                picker.delegate = self
            }
        } else if segue.identifier == "signupVerification"{
            let nextScene = segue.destination as? VerifyOTPVC
            nextScene?.defineTheOtp = 1   // for signup 1
            nextScene?.mobileNumber =  String(describing: signUpParams.mobileNo)
            nextScene?.countryCode = String(describing: signUpParams.countryCode)
            nextScene?.signupParams = signUpParams
        }
        else if segue.identifier == "toSelectOperator" && typeModelMake == 5 {
            let nextScene = segue.destination as? VehicleTypeModelMakeVC
            nextScene?.delegate = self
//            typeModelMake = 5
            nextScene?.type = typeModelMake
            nextScene?.alreadySelectedIndexes = cityIndexSelected
        }
        else if segue.identifier == "toSelectOperator" && typeModelMake == 6 {
            let nextScene = segue.destination as? VehicleTypeModelMakeVC
            nextScene?.delegate = self
//            typeModelMake = 5
            nextScene?.type = typeModelMake
            nextScene?.selectedCityId = zonesID
            nextScene?.alreadySelectedIndexes = indexesSelected
        }
        else if segue.identifier == segues.vehicleDetails!{
            let nav =  segue.destination as! UINavigationController
            let nextScene = nav.viewControllers.first as! VehicleDetailsVC?
            nextScene?.signupParams = signUpParams
        }
    }
    
    func didPick(country: VNHCounty) {
        countryCode.text = country.dialCode
    }
    
//    // MARK: - country delegate method
//    internal func didPickedCountry(country: Country)
//    {
//        countryCode.text = country.dialCode
////        countryImage.image = country.flag
//    }
    
    @objc func removeProPhotos(_ sender: UIButton) {
        let selectedIndex: Int = sender.tag % 1000
        myImages.remove(at: selectedIndex)
        collectionView.reloadData()
        
    }
    
    
    //MARK: - Validate email and phone
    func validateEmailPhone(typeValidate:String) {
        
        var validate: AuthenticationModel
        if typeValidate == "2"{
            Helper.showPI(message:"Verifying the EmailAddress".localized)
            validate = AuthenticationModel.init(verifal: emailAddress.text!, validateType: typeValidate)
        }else{
            Helper.showPI(message:"Verifying the MobileNumber".localized)
            validate = AuthenticationModel.init(verifal: phoneNumber.text!, validateType: typeValidate, countryCode: countryCode.text!)
        }
        //@locate signupModel
        signupModel.validateTheEmailIDPhone(params: validate, completionHandler: { success in
            if success{
                if typeValidate == "2"{
                    self.emailService = true
                }else{
                   self.mobileService = true
                }
            }else{
                if typeValidate == "2"{
                    self.emailService = false
                    self.emailAddress.text = ""
                    self.emailAddress.becomeFirstResponder()
                }else{
                    self.mobileService = false
                    self.phoneNumber.text = ""
                    self.phoneNumber.becomeFirstResponder()
                }
                
            }
        })
    }
    
    //MARK: - Referal API
    func verifyingTheReferral(){
        
        self.referralService = true
        Helper.showPI(message:"Loading..")
        let dict : [String : Any] =  ["code": referralcodeTF.text!,
                                      "type":1,
                                      "lat" :latit,
                                      "long":longit]
        var refferalCodeParams = AuthenticationModel.init(refferalCode: referralcodeTF.text!, latitude: latit, longitude: longit)
        signupModel.verifyingTheReferral(params: refferalCodeParams, completionHandler: { success in
            if success{
                self.referralService = true
            }else{
                self.referralService = false
                self.referralcodeTF.becomeFirstResponder()
            }
        })
    }
    
    
    @IBAction func cancelDateAction(_ sender: UIButton) {
        datePickerBottomView.isHidden = true
        datePkBottomConstrain.constant = -260
        UIView.animate(withDuration: 1) {
            self.view.layoutIfNeeded()
        }
    
    }
    
    @IBAction func doneDateAction(_ sender: UIButton) {
        datePickerBottomView.isHidden = true
        if licenceAction {
            licenseExpTF.text = dateLabel.text!
        }else {
             dobTF.text = dateLabel.text!
        }
        datePkBottomConstrain.constant = -260
        UIView.animate(withDuration: 1) {
            self.view.layoutIfNeeded()
        }
    
    }
    
    @IBAction func licenseExpAction(_ sender: UIButton) {
        datePickerBottomView.isHidden = false
        licenceAction = true
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let currentDate: Date = Date()
        let components: NSDateComponents = NSDateComponents()
        
        components.year = 0
        let minDate: Date = gregorian.date(byAdding: components as DateComponents, to: currentDate, options: NSCalendar.Options(rawValue: 0))!
        
        components.year = 18
        let maxDate: Date = gregorian.date(byAdding: components as DateComponents, to: currentDate, options: NSCalendar.Options(rawValue: 0))!
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
       
        datePkBottomConstrain.constant = 0
        UIView.animate(withDuration: 1) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func dobAction(_ sender: UIButton) {
         datePickerBottomView.isHidden = false
        licenceAction = false
        
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let currentDate: Date = Date()
        let components: NSDateComponents = NSDateComponents()
        
        components.year = -150
        let minDate: Date = gregorian.date(byAdding: components as DateComponents, to: currentDate, options: NSCalendar.Options(rawValue: 0))!
        
        components.year = -18
        let maxDate: Date = gregorian.date(byAdding: components as DateComponents, to: currentDate, options: NSCalendar.Options(rawValue: 0))!
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        datePkBottomConstrain.constant = 0
        UIView.animate(withDuration: 1) {
            self.view.layoutIfNeeded()
        }
    }
    

    
}

// MARK: - CollectionView datasource method
extension SignUpViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: signupIdentifierds.collectionImage!, for: indexPath as IndexPath) as! ProfileCollectionViewCell
            
            cell.vehicleDetailImage.image = myImages[indexPath.row]
            
            
            
            cell.removeDetailsImage.tag = indexPath.row + 1000
            cell.removeDetailsImage.addTarget(self, action: #selector(self.removeProPhotos), for: .touchUpInside)
            
            return cell
        }else{
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: signupIdentifierds.collectionaddImg! , for: indexPath as IndexPath) as! ProfileAddCollectionViewCell
            
            return cell1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return myImages.count
        }else{
            if myImages.count == 0{
                return 2
            }else{
                return (2 - myImages.count)
            }
        }
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
}


// MARK: - CollectionView delegate method
extension SignUpViewController:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
}

// MARK: - ImagePicker delegate method
extension SignUpViewController : UIImagePickerControllerDelegate {
    
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
      if let image = info[.originalImage] as? UIImage{
        if selectImageTyep == 0{
            pickedImage.image = image
            profileImage.image =  image
        }else{
            myImages.append( image)
            collectionView.reloadData()
        }
        //.resizeImage(size: CGSize(width: 100, height: 100))
        self.dismiss(animated: true, completion: nil)
    }
    }
}


extension SignUpViewController:vehicleDetailsDelegate{
    func didSelectServices(services serviceName: String, serviceID: String, serviceTypeID: String) {
        
    }
    
    func didSelectVehicleSeciality(speciality specialityName: String, specialityID: String) {
        
    }
    
    func didSelectServices(services serviceName: String, serviceID: String) {
        
    }
    
    func didSelectVehicleType(vehicleType vehicleTypeData: VehicleTypeSelected) {
        
    }
    
//    func didSelectCity(city cityData: CitySelected) {
//        
//    }
    
    internal func didSelectMakeModelType(vehicleData: VehicleType)
    {
        zonesTF.text = vehicleData.vehicleTypeMakeModel
        zonesID = vehicleData.idSelected
        cityIndexSelected = vehicleData.selectedIndexes
        selectZones.text = ""
        indexesSelected = []
    }
    
    func didSelectZones(selectedZones: VehicleType) {
        
        selectZones.text = selectedZones.vehicleTypeMakeModel
        selctedZonesfromCities = selectedZones.seletedIDs
        indexesSelected = selectedZones.selectedIndexes
    }

    
}

extension SignUpViewController:LocationManagerDelegate{
    func locationFound(_ latitude: Double, longitude: Double) {
    }
    func locationFoundGetAsString(_ latitude: NSString, longitude: NSString) {
        latit = latitude as String
        longit = longitude as String
    }
}


