//
//  OperatorModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 08/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxSwift

enum zonesResults {
    case success([Zones])
    case failure(Bool)
}

class Zones:NSObject {
    
    var iD  = ""
    var zoneName = ""
    
     let disposeBag = DisposeBag()
    
    func getZonesdetails(cityId:String,completionHandler:@escaping (zonesResults) -> ()) {
        
        Helper.showPI(message:"Loading..")
        let zonesDetails =  AuthenticationApis()
        zonesDetails.getZonesDetails(cityId: cityId)
        _ = zonesDetails.subject_response.subscribe(onNext: { (response) in
            if response.isEmpty == false {
                
                completionHandler(.success(self.getTheZonesData(array: (response["data"] as? [[String: Any]])!)))
                
            } else {
                completionHandler(.failure(false))
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
    }
    
    
    
    //MARK:-  retriving the vehicle types response
    func getTheZonesData(array:[[String:Any]]) -> [Zones] {
        var zonesData = [Zones]()
        for items in array{
//            if let citiesArray: [[String: Any]] = items["cities"] as? Array {
//                for cities in citiesArray {
//                    let model = Zones()
//                    if let id = cities["cityId"] as? String{
//                        model.iD = id
//                    }
//                    if let zoneName = cities["cityName"] as? String{
//                        model.zoneName = zoneName
//                    }
//                    zonesData.append(model)
//                }
//            }
            let model = Zones()
            if let zoneId = items["_id"] as? String {
                model.iD = zoneId
            }
            
            if let zoneName = items["title"] as? String {
                model.zoneName = zoneName
            }
            
            zonesData.append(model)
        }
        return zonesData
    }
    
    
    
}

