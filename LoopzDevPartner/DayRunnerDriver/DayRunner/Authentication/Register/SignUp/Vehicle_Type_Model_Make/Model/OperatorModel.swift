//
//  OperatorModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 08/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxSwift

enum operatorResults {
    case success([Operator])
    case failure(Bool)
}

class Operator:NSObject {
    
    var iD  = ""
    var company = ""
    
    var disposeBag = DisposeBag()
    
    func getOperatorsdetails(serviceName:String,completionHandler:@escaping (operatorResults) -> ()) {
        
        Helper.showPI(message:"Loading..")
        let operatorDetails =  AuthenticationApis()
        operatorDetails.getOperatorDetails()
        operatorDetails.subject_response.subscribe(onNext: { (response) in
            if response.isEmpty == false {
                completionHandler(.success(self.getTheOperatorsData(array: (response["data"] as? [[String: Any]])!)))
                
            } else {
                completionHandler(.failure(false))
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
    }
    
    
    
    //MARK:-  retriving the vehicle types response
    func getTheOperatorsData(array:[[String:Any]]) -> [Operator] {
        var operatorsData = [Operator]()
        for items in array{
            if let citiesArray: [[String: Any]] = items["cities"] as? Array {
                    for cities in citiesArray {
                        let model = Operator()
                        if let id = cities["cityId"] as? String{
                            model.iD = id
                        }
                        if let vehicleName = cities["cityName"] as? String{
                            model.company = vehicleName
                        }
                        operatorsData.append(model)
                    }
                }
        }
        return operatorsData
    }
    
    
    
}

