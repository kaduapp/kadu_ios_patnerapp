//
//  VehicleMakeModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 08/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit
import RxSwift

enum MakeResults {
    case success([Make])
    case failure(Bool)
}


class Make:NSObject {
    
    var makeID =  ""
    var makeName = ""
    var vehicleMake = [Make]()
    let disposeBag = DisposeBag()
    
    func getVehicleMake(serviceName:String,completionHandler:@escaping (MakeResults) -> ()) {
        
        Helper.showPI(message:"Loading..")
        let vehicleMake =  AuthenticationApis()
        vehicleMake.getVehicleMake()
        vehicleMake.subject_response.subscribe(onNext: { (response) in
            if response.isEmpty == false {
                completionHandler(.success(self.getTheVehicleMakeData(dict: (response["data"] as? [[String: Any]])!)))
                
            } else {
                completionHandler(.failure(false))
            }

        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)

//        NetworkHelper.requestGETURL(method: serviceName,
//                                    success: { (response) in
//                                        Helper.hidePI()
//                                        if response.isEmpty == false {
//                                            completionHandler(.success(self.getTheVehicleMakeData(dict: (response["data"] as? [[String: Any]])!)))
//
//                                        } else {
//                                            completionHandler(.failure(false))
//                                        }
//
//        })
//        { (Error) in
//            Helper.hidePI()
//            completionHandler(.failure(false))
//            Helper.alertVC(errMSG: Error.localizedDescription)
//
//        }
    }
    
    //MARK:-  retriving the vehicle types response
    func getTheVehicleMakeData(dict:[[String:Any]]) -> [Make] {
        for items in dict{
            let model = Make()
            
            if  let id = items["_id"] as? String{
                model.makeID = id
            }
            
            if let vehicleName = items["Name"] as? String{
                model.makeName = vehicleName
            }
            vehicleMake.append(model)
        }
        
        return vehicleMake
    }
    
}


enum ModelResults {
    case success([Model])
    case failure(Bool)
}

class Model:NSObject {
     let disposeBag = DisposeBag()
    var modelID  = ""
    var modelName = ""
    
    var  modeldata    = [Model]()
    func getVehicleMakeModelData(serviceName:String,vehicleMakeSelected:String,completionHandler:@escaping (ModelResults) -> ()) {
        
        Helper.showPI(message:"Loading..")
        
        let vehicleMake =  AuthenticationApis()
        vehicleMake.getVehicleMakeModelData()
        vehicleMake.subject_response.subscribe(onNext: { (response) in
            if response.isEmpty == false {
                completionHandler(.success(self.getTheVehicleMakeModelData(dict: (response["data"] as? [[String: Any]])!, makeId: vehicleMakeSelected)))
            } else {
                completionHandler(.failure(false))
            }

            
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
        
//        NetworkHelper.requestGETURL(method: serviceName,
//                                    success: { (response) in
//                                        Helper.hidePI()
//                                        if response.isEmpty == false {
//                                            completionHandler(.success(self.getTheVehicleMakeModelData(dict: (response["data"] as? [[String: Any]])!, makeId: vehicleMakeSelected)))
//                                        } else {
//                                            completionHandler(.failure(false))
//                                        }
//
//        })
//        { (Error) in
//            Helper.hidePI()
//            completionHandler(.failure(false))
//            Helper.alertVC(errMSG: Error.localizedDescription)
//
//        }
    }
    
    //MARK:-  retriving the make model data
    func getTheVehicleMakeModelData(dict:[[String:Any]] , makeId:String) -> [Model] {
        for items in dict{
            
            if let vehicleID = items["_id"] as? String{
                if  let modelsVehicle = items["models"] as? [[String: Any]] {
                    if vehicleID == makeId {
                        for data in modelsVehicle {
                            let model = Model()
                            if  let spID = data["_id"] as? String{
                                model.modelID = spID
                            }
                            
                            if let spName = data["Name"] as? String{
                                model.modelName = spName
                            }
                            modeldata.append(model)
                        }
                    }
                }
            }
        }
        return modeldata
    }
}

