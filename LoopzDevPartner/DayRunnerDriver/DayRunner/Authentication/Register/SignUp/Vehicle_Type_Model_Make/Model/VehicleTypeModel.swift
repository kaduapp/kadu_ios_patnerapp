//
//  VehicleTypeModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 08/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit
import RxSwift
//enum cityResults {
//    case success([CityNameResults])
//    case failure(Bool)
//}


//class CityNameResults:NSObject {
//
//    var cityID = ""
//    var cityName = ""
//    var cityTypes    = [CityNameResults]()
//
//    func getCityType(serviceName:String,completionHandler:@escaping (cityResults) -> ()) {
//
//
//        Helper.showPI(message:"Loading..")
//
//        let getCity =  AuthenticationApis()
//        getCity.getCity()
//        getCity.subject_response.subscribe(onNext: { (response) in
//            Helper.hidePI()
//            if response.isEmpty == false {
//                let data: [String:Any] = response["data"] as! [String:Any]
//                let cityArr: [[String:Any]] = data["cityArr"] as! [[String:Any]]
//
//                completionHandler(.success(self.updateCityType(dict: cityArr)))
//
//            } else {
//                completionHandler(.failure(false))
//            }
//
//        }, onError: { (error) in
//            Helper.hidePI()
//        }, onCompleted: {
//
//        }) {
//
//        }
//
//
////        NetworkHelper.requestGETURL(method: serviceName,
////                                    success: { (response) in
////                                        Helper.hidePI()
////                                        if response.isEmpty == false {
////                                            let data: [String:Any] = response["data"] as! [String:Any]
////                                            let cityArr: [[String:Any]] = data["cityArr"] as! [[String:Any]]
////
////                                            completionHandler(.success(self.updateCityType(dict: cityArr)))
////
////                                        } else {
////                                            completionHandler(.failure(false))
////                                        }
////
////        })
////        { (Error) in
////            Helper.hidePI()
////            completionHandler(.failure(false))
////            Helper.alertVC(errMSG: Error.localizedDescription)
////
////        }
//    }
//
//    //MARK:-  retriving the vehicle types response
//    func updateCityType(dict:[[String:Any]]) -> [CityNameResults] {
//        for items in dict{  // Vehicle specialities
//            let model = CityNameResults()
//
//            if  let id = items["_id"] as? String{
//                model.cityID = id
//            }
//
//            if let vehicleName = items["city"] as? String{
//                model.cityName = vehicleName
//            }
//            cityTypes.append(model)
//        }
//
//        return cityTypes
//    }
//
//}


enum vehicleTypeResults {
    case success([VehType])
    case failure(Bool)
}


class VehType:NSObject {
    
    var VehID = ""
    var VehName = ""
    var VehService = [[String:Any]]()
    var VehSpecialities = [[String:Any]]()
    var vehicleTypes    = [VehType]()
     let disposeBag = DisposeBag()
    func getVehicletype(serviceName:String,completionHandler:@escaping (vehicleTypeResults) -> ()) {

        Helper.showPI(message:"Loading..")
        let getVehicleData =  AuthenticationApis()
        getVehicleData.getVehicletype(serviceName: serviceName)
        getVehicleData.subject_response.subscribe(onNext: { (response) in
//            Helper.hidePI()
            if response.isEmpty == false {
                completionHandler(.success(self.updatesTheVehicleData(dict: (response["data"] as? [[String: Any]])!)))
            } else {
                completionHandler(.failure(false))
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)

//        NetworkHelper.requestGETURL(method: serviceName,
//                                    success: { (response) in
//                                        Helper.hidePI()
//                                        if response.isEmpty == false {
//                                            completionHandler(.success(self.updatesTheVehicleData(dict: (response["data"] as? [[String: Any]])!)))
//                                        } else {
//                                            completionHandler(.failure(false))
//                                        }
//
//        })
//        { (Error) in
//            Helper.hidePI()
//            completionHandler(.failure(false))
//            Helper.alertVC(errMSG: Error.localizedDescription)
//
//        }
    }
    
    //MARK:-  retriving the vehicle types response
    func updatesTheVehicleData(dict:[[String:Any]]) -> [VehType] {
        for items in dict{  // Vehicle specialities
            let model = VehType()
            
            if  let id = items["_id"] as? String{
                model.VehID = id
            }
            
            if let vehicleName = items["type_name"] as? String{
                model.VehName = vehicleName
            }
            
            if let vehicleServices = items["services"] as? [[String : Any]]
            {
                model.VehService = vehicleServices
            }
//            if let vehicleSpecialities = items["sepecialities"] as? [[String : Any]]
//            {
//                model.VehSpecialities = vehicleSpecialities
//            }
            vehicleTypes.append(model)
        }
        
        return vehicleTypes
    }
    
}



//enum specialitesResults {
//    case success([VehSpecial])
//    case failure(Bool)
//}
//
//class VehSpecial:NSObject {
//
//    var VehSpecialID  =  ""
//    var VehSpecialName = ""
//
//    var  specialities    = [VehSpecial]()
//    func getVehicleSpecialities(serviceName:String,vehicleSelected:String,completionHandler:@escaping (specialitesResults) -> ()) {
//
//        Helper.showPI(message:"Loading..")

//        NetworkHelper.requestGETURL(method: serviceName,
//                                    success: { (response) in
//                                        Helper.hidePI()
//                                        if response.isEmpty == false {
//                                            completionHandler(.success(self.updatesTheVehicleSpecialites(dict: (response["data"] as? [[String: Any]])!, selectedType: vehicleSelected)))
//                                        } else {
//                                            completionHandler(.failure(false))
//                                        }
//
//        })
//        { (Error) in
//            Helper.hidePI()
//            completionHandler(.failure(false))
//            Helper.alertVC(errMSG: Error.localizedDescription)
//
//        }
//    }
    
//    //MARK:-  retriving the vehicle specialities response
//    func updatesTheVehicleSpecialites(dict:[[String:Any]] , selectedType:String) -> [VehSpecial] {
//        for items in dict{  // Vehicle specialities
//            
//            if let vehicleID = items["id"] as? String {
//                if  let specialites = items["sepecialities"] as? [[String: Any]] {
//                    if vehicleID == selectedType {
//                        for data in specialites {
//                            let model = VehSpecial()
//                            if  let spID = data["id"] as? String{
//                                model.VehSpecialID = spID
//                            }
//                            
//                            if let spName = data["Name"] as? String{
//                                model.VehSpecialName = spName
//                            }
//                            specialities.append(model)
//                        }
//                    }
//                }
//            }
//        }
//        return specialities
//    }
//}

