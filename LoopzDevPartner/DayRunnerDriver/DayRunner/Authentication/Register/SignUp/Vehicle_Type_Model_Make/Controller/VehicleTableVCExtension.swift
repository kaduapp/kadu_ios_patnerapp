//
//  VehicleTableVC.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

//MARK:- tableview datasource
extension VehicleTypeModelMakeVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionType : VehicleTypeEnum = VehicleTypeEnum(rawValue : type)!
        switch sectionType {
//        case .city:
//            return cityType.count
        case .vehicletype:
            return vehType.count;
        case .vehicleMake:
            return make.count;
        case .vehicleModel:
            return model.count;
//        case .vehicleSpl:
//            return selectSpeciality.count;
//        case .selectService:
//            return self.selectServices.count
        case .zonesEnum:
            return operatorList.count;
        default:
            return zoneList.count;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"typeModelMake") as! VTMMTableViewCell!
        
        let sectionType : VehicleTypeEnum = VehicleTypeEnum(rawValue : type)!
        switch sectionType {
//        case .selectService:
//            cell?.updateTheTableViewRows(data: self.selectServices[indexPath.row]["serviceName"] as! String)
//            break
//        case .city:
//            cell?.updateTheTableViewRows(data: cityType[indexPath.row].cityName)
//            break
        case .vehicletype:
            cell?.updateTheTableViewRows(data: vehType[indexPath.row].VehName) //@locate VTMMTableviewCell
            break
        case .vehicleMake:
            cell?.updateTheTableViewRows(data: make[indexPath.row].makeName) //@locate VTMMTableviewCell
            break
        case .vehicleModel:
            cell?.updateTheTableViewRows(data: model[indexPath.row].modelName) //@locate VTMMTableviewCell
            break
//        case .vehicleSpl:
//            cell?.updateTheTableViewRows(data: self.selectSpeciality[indexPath.row]["Name"] as! String) //@locate VTMMTableviewCell
//            break
        case .zonesEnum:
            cell?.updateTheTableViewRows(data: operatorList[indexPath.row].company)
            break
        default:
            cell?.updateTheTableViewRows(data: zoneList[indexPath.row].zoneName)
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 50;
    }
}


//MARK:- Tableview delegate methods
extension VehicleTypeModelMakeVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        vehicle = nil
        if type != 4 && type != 6 && type != 8{
            for selectedIndexPath: IndexPath in tableView.indexPathsForSelectedRows! {
                if selectedIndexPath.row != indexPath.row {
                    tableView.deselectRow(at: selectedIndexPath, animated: false)
                }
            }
        }
        
        let sectionType : VehicleTypeEnum = VehicleTypeEnum(rawValue : type)!
        switch sectionType {
//        case .city:
//            
//            self.citySelected = CitySelected.init(cityName: cityType[indexPath.row].cityName, type: type, cityID: cityType[indexPath.row].cityID)
//            doneButton.isSelected = true
//            selectedMakeModelType(nil)
//            break
            
        case .vehicletype:
            alreadySelectedIndex = indexPath.row
//            vehicle = VehicleType.init(vehicletyp: vehType[indexPath.row].VehName, typeof:type, selectedID: vehType[indexPath.row].VehID, selectedIdsArray: [],index:indexPath.row,selectedIndex:[])
            vehicleTypeSelected = VehicleTypeSelected.init(vehicleTypeName: vehType[indexPath.row].VehName, vehicleTypeID: vehType[indexPath.row].VehID, vehicleTypeServices: vehType[indexPath.row].VehService, vehicleTypeSpecialities: vehType[indexPath.row].VehSpecialities)
//            print(vehicleTypeSelected)
            doneButton.isSelected = true
            selectedMakeModelType(nil)
            break
        case .vehicleMake:
            alreadySelectedIndex = indexPath.row
            vehicle = VehicleType.init(vehicletyp: make[indexPath.row].makeName, typeof:type, selectedID: make[indexPath.row].makeID, selectedIdsArray: [],index:indexPath.row,selectedIndex:[])
//            VehicleTypeSelected = VehicleTypeSelected.init(vehicleTypeName: vehicl, vehicleTypeID: <#T##String#>, vehicleTypeServices: <#T##[String : Any]#>)
            doneButton.isSelected = true
            selectedMakeModelType(nil)
            break
        case .vehicleModel:
            alreadySelectedIndex = indexPath.row
            vehicle = VehicleType.init(vehicletyp: model[indexPath.row].modelName, typeof:type, selectedID: model[indexPath.row].modelID, selectedIdsArray: [],index:indexPath.row,selectedIndex:[])
            doneButton.isSelected = true
            selectedMakeModelType(nil)
            break
        case .zonesEnum:
            alreadySelectedIndex = indexPath.row
//            vehicle = VehicleType.init(vehicletyp: model[indexPath.row].modelName, typeof:type, selectedID: model[indexPath.row].modelID, selectedIdsArray: [],index:indexPath.row,selectedIndex:[])
//            doneButton.isSelected = true
//            selectedMakeModelType(nil)
            
            
//            let selectedRows: [Any]? = self.vehicleModelTV.indexPathsForSelectedRows
            
//            for index in selectedRows! {
//                var indexpath:IndexPath = index as! IndexPath
//                collectionOfIndex.append(indexpath.row) //selected cells
//                let spcIds = self.operatorList[indexpath.row].iD
//                specialitiesIDs.append(spcIds)
//                if specialitiesSelected.isEmpty {
//                    specialitiesSelected = self.operatorList[indexpath.row].company
//                }else{
//                    specialitiesSelected = specialitiesSelected + "," + self.operatorList[indexpath.row].company
//                }
//            }
            self.vehicle = VehicleType.init(vehicletyp: self.operatorList[indexPath.row].company, typeof:self.type, selectedID: operatorList[indexPath.row].iD, selectedIdsArray: [],index: indexPath.row,selectedIndex:[indexPath.row])
            doneButton.isSelected = true
            selectedMakeModelType(self.vehicle)
//            self.delegate?.didSelectMakeModelType(vehicleData: self.vehicle!) //@locate VehiclTypeModelMakeVC Delegate
            
//            _ = self.navigationController?.popViewController(animated: true)
        default:

            self.doneButton.backgroundColor = Helper.getUIColor(color: Colors.AppBaseColor)
            break
        }
        doneButton.isSelected = true
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let selectedRows: [Any]? = vehicleModelTV.indexPathsForSelectedRows
        if type == 4 || type == 5{
            if selectedRows?.count == nil {
                doneButton.isSelected = false
            }
        }else{
            doneButton.isSelected = false
        }
    }
}
