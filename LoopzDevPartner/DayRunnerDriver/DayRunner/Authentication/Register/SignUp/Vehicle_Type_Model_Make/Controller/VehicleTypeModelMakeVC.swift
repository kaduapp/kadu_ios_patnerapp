//
//  VehicleTypeModelMakeVC.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 26/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

//1:  vehicle types
//2: vehicle make
//4: vehicle model
//4: vehicle Specialities
//5: Zones

enum VehicleTypeEnum : Int {
    case vehicletype   = 1
//    case vehicleSpl    = 4
    case vehicleMake   = 2
    case vehicleModel  = 3
    case zonesEnum     = 5
    case vehicleJob    = 6
//    case city          = 7
//    case selectService = 8
}



protocol vehicleDetailsDelegate:class
{
    func didSelectMakeModelType(vehicleData: VehicleType)
//    func didSelectCity(city cityData: CitySelected)
    func didSelectServices(services serviceName: String, serviceID: String, serviceTypeID: String)
    func didSelectVehicleType(vehicleType vehicleTypeData: VehicleTypeSelected)
    func didSelectVehicleSeciality(speciality specialityName: String, specialityID: String)
    func didSelectZones(selectedZones: VehicleType)
}

//MARK: - Selected data passing to signup r vehicle detailsVc
class VehicleType {
    var vehicleTypeMakeModel = String()
    var idSelected:String = ""
    var type:Int = 0
    var seletedIDs = [String]()
    var selIndex:Int = 0
    var selectedIndexes = [Int]()
    
    
    init(vehicletyp: String, typeof:Int, selectedID:String,selectedIdsArray:[String],index:Int,selectedIndex:[Int]) {
        self.vehicleTypeMakeModel = vehicletyp
        self.type = typeof
        self.idSelected = selectedID
        self.seletedIDs =  selectedIdsArray
        self.selIndex = index
        self.selectedIndexes = selectedIndex
    }
}

class VehicleTypeSelected
{
    var vehicleTypeName = String()
    var vehicleTypeID = String()
    var vehicleTypeServices = [[String: Any]]()
    var vehicleTypeSpecialities = [[String: Any]]()
    init(vehicleTypeName: String, vehicleTypeID: String, vehicleTypeServices: [[String: Any]], vehicleTypeSpecialities: [[String: Any]])
    {
        self.vehicleTypeName = vehicleTypeName
        self.vehicleTypeID = vehicleTypeID
        self.vehicleTypeServices = vehicleTypeServices
        self.vehicleTypeSpecialities = vehicleTypeSpecialities
    }
}

//class CitySelected {
//    var cityName = String()
//    var cityID = String()
//    var type:Int = 0
//
//
//    init(cityName: String, type:Int, cityID:String) {
//        self.cityName = cityName
//        self.type = type
//        self.cityID = cityID
//    }
//}

//MARK: - Select vehicle Type, vehicle specialties, vehicle make, vehicle model.
class VehicleTypeModelMakeVC: UIViewController {
    
    open weak var delegate: vehicleDetailsDelegate?
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var vehicleModelTV: UITableView!
    var vechArray: [[String: AnyObject]] = []
    @IBOutlet var doneButton: UIButton!
    var vehicle: VehicleType? = nil
    var selectedCityId: String = ""
    
    
    var selectServices: [[String: Any]]!
//    var selectSpeciality: [[String: Any]]!
    var vehicleTypeSelected: VehicleTypeSelected? = nil
    var idMakeSeleted =  String()  // Stores the selected vehicle and make
    
    var operatorList    = [Operator]()
    var operatorModel   = Operator()
    
    var zoneList        = [Zones]()
    var zoneModel       = Zones()
    
    var make            = [Make]()
    var makeMdl         = Make()
    
    var model           = [Model]()
    var modelMdl        = Model()
    
    var vehType         = [VehType]()
    var vehModl         =  VehType()
    
    
    
//    var cityType        = [CityNameResults]()
//    var cityModel       = CityNameResults()
    
//    var vehSpecialities = [VehSpecial]()
//    var specialModel    = VehSpecial()
    
    
    var alreadySelectedIndex:Int = -1
    var alreadySelectedIndexes = [Int]()
    
    var type:Int = 0
//    var citySelected: CitySelected!
//    var vehicleTypes: Vehicle
    override func viewDidLoad() {
        super.viewDidLoad()
        vehicleModelTV.allowsMultipleSelectionDuringEditing = true
        vehicleModelTV.tintColor = Helper.UIColorFromRGB(rgbValue: 0x17CA9C)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        let sectionType : VehicleTypeEnum = VehicleTypeEnum(rawValue : type)!
        switch sectionType {
        case .vehicletype:
            titleLabel.text = VehicleMakeModelSelect.titleVehicleType
            doneButton.isHidden = true
            break;
        case .vehicleMake:
            titleLabel.text    = VehicleMakeModelSelect.titleMake
            doneButton.isHidden = true
            break;
        case .vehicleModel:
            titleLabel.text = VehicleMakeModelSelect.titleModel
            doneButton.isHidden = true
            break;
//        case .vehicleSpl:
//            titleLabel.text = VehicleMakeModelSelect.titleSpecialties
//            self.selectSpeciality = UserDefaults.standard.object(forKey: "vehicleTypeSpecialities") as! [[String:Any]]
//            vehicleModelTV.reloadData()
//            break;
//        case .city:
//            titleLabel.text = VehicleMakeModelSelect.titleCity
//            doneButton.isHidden = true
//            break
//        case .selectService:
//            titleLabel.text = VehicleMakeModelSelect.titleSelecteService
//            self.selectServices = UserDefaults.standard.object(forKey: "vehicleTypeServices") as! [[String:Any]]
//            vehicleModelTV.reloadData()
//            break
            
        case .zonesEnum:
            titleLabel.text =  VehicleMakeModelSelect.titleCity
            doneButton.isHidden = true
            break;
        default:
            titleLabel.text =  VehicleMakeModelSelect.titleZone
            if alreadySelectedIndexes.count > 0 {
                self.doneButton.backgroundColor = Helper.getUIColor(color: Colors.AppBaseColor)
            } else {
                self.doneButton.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0x6F6F6F)
            }
            doneButton.isHidden = false
            break;
        }
        vehicleModelTV.setEditing(!vehicleModelTV.isEditing, animated: true)
        getTypeMakeNModel()
    }
    
    func getTypeMakeNModel(){
        var serviceName = String()
        
        let sectionType : VehicleTypeEnum = VehicleTypeEnum(rawValue : type)!
        switch sectionType {
            
//        case .city:
//            serviceName = API.METHOD.GETCITY
//            cityModel.getCityType(serviceName: serviceName, completionHandler: { cityTypeResults in
//                switch cityTypeResults{
//                case .success(let cityType):
//                    self.cityType = cityType
//                    self.updateTheTableView() //show selected rows if the tableview rows already selected
//                    break;
//                case .failure(let error):
//                    print(error)
//                    break
//                }
//            })
            
//            break;
        case .vehicletype:   //Vehicle types
//            let cityID: String = UserDefaults.standard.object(forKey: "selectedCityID") as! String
//            let cityIDAPI: String = "/" + cityID
            serviceName = API.METHOD.vehicleTypes //+ cityIDAPI
            vehModl.getVehicletype(serviceName: serviceName, completionHandler: { vehicleTypeResults in
                switch vehicleTypeResults{
                case .success(let vehicleTypes):
                    self.vehType = vehicleTypes
                    self.updateTheTableView() //show selected rows if the tableview rows already selected
                    break;
                case .failure(let error):
                    print(error)
                    break
                }
            })
            
            break;
//        case .vehicleSpl:   //Vehicle specialities
            
//            serviceName = API.METHOD.vehicleTypes
//            specialModel.getVehicleSpecialities(serviceName: serviceName, vehicleSelected: idMakeSeleted, completionHandler: { specialitesResults in
//                switch specialitesResults{
//                case .success(let specilalites):
//                    self.vehSpecialities = specilalites
//                    self.updateTheTableView() //show selected rows if the tableview rows already selected
//                    break
//                case .failure(let error):
//                    print(error)
//                    break
//                }
//            })
            
//            break;
        case .vehicleMake:  // vehicle make
            
            serviceName = API.METHOD.makeModel
            makeMdl.getVehicleMake(serviceName: serviceName, completionHandler: { MakeResults in
                switch MakeResults{
                case .success(let makeData):
                    self.make = makeData
                    self.updateTheTableView() //show selected rows if the tableview rows already selected
                    break;
                case .failure(let error):
                    print(error)
                    break
                }
            })
            
            break;
        case .vehicleModel:  // vehicle model
            
            serviceName = API.METHOD.makeModel
            modelMdl.getVehicleMakeModelData(serviceName: serviceName, vehicleMakeSelected: idMakeSeleted, completionHandler: { ModelResults in
                switch ModelResults{
                case .success(let modelData):
                    self.model = modelData
                    self.updateTheTableView() //show selected rows if the tableview rows already selected
                    break
                case .failure(let error):
                    print(error)
                    break
                }
            })
            
            break;
        case .zonesEnum:
            serviceName = API.METHOD.getZones
            
            operatorModel.getOperatorsdetails(serviceName: serviceName, completionHandler: { operatorResults in
                switch operatorResults{
                case .success(let operators):
                    self.operatorList = operators
                    self.updateTheTableView() //show selected rows if the tableview rows already selected
                    break;
                case .failure(let error):
                    print(error)
                    break
                }
            })
            break;
            
        default:
            zoneModel.getZonesdetails(cityId: selectedCityId, completionHandler: { zonesResults in
                switch zonesResults{
                case .success(let zones):
                    self.zoneList = zones
                    self.updateTheTableView() //show selected rows if the tableview rows already selected
                    break;
                case .failure(let error):
                    print(error)
                    break
                }
            })
            break;
        }
    }
    
    
    func updateTheTableView(){
        vehicleModelTV.reloadData()
        let sectionType : VehicleTypeEnum = VehicleTypeEnum(rawValue : type)!
        switch sectionType {
//        case .city:
//            doneButton.isSelected = false
//            break;
        case .vehicletype:
            if alreadySelectedIndex != -1 {  //MARK: - Selecting Vehicle Type which is already selected
                let indexPath = IndexPath(row: alreadySelectedIndex, section: 0)
                self.vehicleModelTV.selectRow(at: indexPath, animated: true, scrollPosition: UITableView.ScrollPosition.middle)
                doneButton.isSelected = true
            }
            break;
//        case .vehicleSpl:
//            for index in alreadySelectedIndexes {  //MARK: - Selecting specialities which is already selected
//                let indexPath = IndexPath(row: index, section: 0)
//                self.vehicleModelTV.selectRow(at: indexPath, animated: true, scrollPosition: UITableViewScrollPosition.middle)
//                doneButton.isSelected = true
//            }
//            break;
        case .vehicleMake:
            if alreadySelectedIndex != -1 {         //MARK: - Selecting vehicleMake which is already selected
                let indexPath = IndexPath(row: alreadySelectedIndex, section: 0)
                self.vehicleModelTV.selectRow(at: indexPath, animated: true, scrollPosition: UITableView.ScrollPosition.middle)
                doneButton.isSelected = true
            }
            break;
        case .vehicleModel:
            if alreadySelectedIndex != -1 {             //MARK: - Selecting Vehicle Model which is already selected
                let indexPath = IndexPath(row: alreadySelectedIndex, section: 0)
                self.vehicleModelTV.selectRow(at: indexPath, animated: true, scrollPosition: UITableView.ScrollPosition.middle)
                doneButton.isSelected = true
            }
            break;
        case .zonesEnum:
            for index in alreadySelectedIndexes {      //MARK: - Selecting City which is already selected
                let indexPath = IndexPath(row: index, section: 0)
                self.vehicleModelTV.selectRow(at: indexPath, animated: true, scrollPosition: UITableView.ScrollPosition.middle)
                doneButton.isSelected = true
            }
            break;
        default:
            for index in alreadySelectedIndexes {      //MARK: - Selecting City which is already selected
                let indexPath = IndexPath(row: index, section: 0)
                self.vehicleModelTV.selectRow(at: indexPath, animated: true, scrollPosition: UITableView.ScrollPosition.middle)
                doneButton.isSelected = false
            }
            break;
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}




