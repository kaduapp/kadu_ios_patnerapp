//
//  VehicleDetailsActionVC.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension VehicleTypeModelMakeVC{
    
    
    @IBAction func selectedMakeModelType(_ sender: Any?) {
        if doneButton.isSelected {
            var specialitiesIDs = [String]()
            var specialitiesSelected  = String()
            var collectionOfIndex = [Int]()
            
            
            switch type {
            case 8:
                var selectedServiceID: String! = ""
                var selectedServiceName: String! = ""
                var selectedServiceTypeID: String! = ""
                for indexPath in self.vehicleModelTV.indexPathsForSelectedRows!
                {
                    selectedServiceID = self.selectServices[indexPath.row]["serviceId"] as! String + "," + selectedServiceID
                    selectedServiceName = self.selectServices[indexPath.row]["serviceName"] as! String + "," + selectedServiceName
                    selectedServiceTypeID = String(self.selectServices[indexPath.row]["serviceType"] as! Int) + "," + selectedServiceTypeID
                }
                selectedServiceID = selectedServiceID.substring(to: selectedServiceID.index(before: selectedServiceID.endIndex))
                selectedServiceName = selectedServiceName.substring(to: selectedServiceName.index(before: selectedServiceName.endIndex))
                selectedServiceTypeID = selectedServiceTypeID.substring(to: selectedServiceTypeID.index(before: selectedServiceTypeID.endIndex))
                print("Selected Service ID: \(selectedServiceID)")
                print("Selected Service Name: \(selectedServiceName)")
                print("Selected Servicve TypeID: \(selectedServiceTypeID)")
                delegate?.didSelectServices(services: selectedServiceName!, serviceID: selectedServiceID!, serviceTypeID: selectedServiceTypeID)
                self.navigationController?.popViewController(animated: true)
                //                var truncated = name.substring(to: name.index(before: name.endIndex))
                break
                //            case 7:
                //                delegate?.didSelectCity(city: citySelected)
                //                navigationController?.popViewController(animated: true)
            //                break
            case 4:       //Storing the selected specialties
                //                let selectedRows: [Any]? = vehicleModelTV.indexPathsForSelectedRows
                //                var selectedSpecialityID: String! = ""
                //                var selectedSpecialityName: String! = ""
                //                for indexPath in self.vehicleModelTV.indexPathsForSelectedRows!
                //                {
                //                    selectedSpecialityID = self.selectSpeciality[indexPath.row]["id"] as! String + "," + selectedSpecialityID
                //                    selectedSpecialityName = self.selectSpeciality[indexPath.row]["Name"] as! String + "," + selectedSpecialityName
                //                }
                //                selectedSpecialityID = selectedSpecialityID.substring(to: selectedSpecialityID.index(before: selectedSpecialityID.endIndex))
                //                selectedSpecialityName = selectedSpecialityName.substring(to: selectedSpecialityName.index(before: selectedSpecialityName.endIndex))
                //                print("Selected Service ID: \(selectedSpecialityID)")
                //                print("Selected Service Name: \(selectedSpecialityName)")
                //
                //                delegate?.didSelectVehicleSeciality(speciality: selectedSpecialityName, specialityID: selectedSpecialityID)
                //
                //
                //                _ = navigationController?.popViewController(animated: true)
                break
                
            case 5:      //Storing the Selected zones
                
                //                                let selectedRows: [Any]? = self.vehicleModelTV.indexPathsForSelectedRows
                //
                //                                for index in selectedRows! {
                //                                    var indexpath:IndexPath = index as! IndexPath
                //                                    collectionOfIndex.append(indexpath.row) //selected cells
                //                                    let spcIds = self.operatorList[indexpath.row].iD
                //                                    specialitiesIDs.append(spcIds)
                //                                    if specialitiesSelected.isEmpty {
                //                                        specialitiesSelected = self.operatorList[indexpath.row].company
                //                                    }else{
                //                                        specialitiesSelected = specialitiesSelected + "," + self.operatorList[indexpath.row].company
                //                                    }
                //                                }
                //                                self.vehicle = VehicleType.init(vehicletyp: specialitiesSelected, typeof:self.type, selectedID: "zones", selectedIdsArray: specialitiesIDs,index:0,selectedIndex:collectionOfIndex)
//                self.vehicle = VehicleType.init(vehicletyp: self.operatorList[alreadySelectedIndex].company, typeof:self.type, selectedID: operatorList[alreadySelectedIndex].iD, selectedIdsArray: [],index: alreadySelectedIndex,selectedIndex:[alreadySelectedIndex])
                
                if let selectedCity = sender as? VehicleType {
                    
                    self.delegate?.didSelectMakeModelType(vehicleData: selectedCity) //@locate VehiclTypeModelMakeVC Delegate
                    
                    _ = self.navigationController?.popViewController(animated: true)
                    break
                } else {
                    
                }


                
            case 1:   // storing the selected vehicle
                vehicle = VehicleType.init(vehicletyp: vehType[alreadySelectedIndex].VehName, typeof:type, selectedID: vehType[alreadySelectedIndex].VehID, selectedIdsArray: [],index:alreadySelectedIndex,selectedIndex:[])
                delegate?.didSelectMakeModelType(vehicleData: vehicle!) //@locate VehiclTypeModelMakeVC Delegate
                delegate?.didSelectVehicleType(vehicleType: vehicleTypeSelected!)
                _ = navigationController?.popViewController(animated: true)
                break
                
            case 2:  // storing the selected vehicle MAke
                vehicle = VehicleType.init(vehicletyp: make[alreadySelectedIndex].makeName, typeof:type, selectedID: make[alreadySelectedIndex].makeID, selectedIdsArray: [],index:alreadySelectedIndex,selectedIndex:[])
                
                delegate?.didSelectMakeModelType(vehicleData: vehicle!) //@locate VehiclTypeModelMakeVC Delegate
                _ = navigationController?.popViewController(animated: true)
                break
                
            case 3:  // Storing the selected vehicle model
                vehicle = VehicleType.init(vehicletyp: model[alreadySelectedIndex].modelName, typeof:type, selectedID: model[alreadySelectedIndex].modelID, selectedIdsArray: [],index:alreadySelectedIndex,selectedIndex:[])
                delegate?.didSelectMakeModelType(vehicleData: vehicle!)  //@locate VehiclTypeModelMakeVC Delegate
                _ = navigationController?.popViewController(animated: true)
                break
            case 6:
                                var selectedRows: [Int]  = []
                                var selectedSpecialityID: [String] = []
                                var selectedSpecialityName: String! = ""
                                for indexPath in self.vehicleModelTV.indexPathsForSelectedRows!
                                {
                                    selectedRows.append(indexPath.row)
                                    selectedSpecialityID.append(self.zoneList[indexPath.row].iD)
                                    selectedSpecialityName = self.zoneList[indexPath.row].zoneName + "," + selectedSpecialityName
                                }
//                                selectedSpecialityID = selectedSpecialityID.substring(to: selectedSpecialityID.index(before: selectedSpecialityID.endIndex))
                                selectedSpecialityName = selectedSpecialityName.substring(to: selectedSpecialityName.index(before: selectedSpecialityName.endIndex))
                                print("Selected Service ID: \(selectedSpecialityID)")
                                print("Selected Service Name: \(selectedSpecialityName)")
                                
                                self.vehicle = VehicleType.init(vehicletyp: selectedSpecialityName, typeof: self.type, selectedID: "", selectedIdsArray: selectedSpecialityID, index:alreadySelectedIndex , selectedIndex: selectedRows)
                                
                                delegate?.didSelectZones(selectedZones: self.vehicle!)
                
//                                delegate?.didSelectVehicleSeciality(speciality: selectedSpecialityName, specialityID: selectedSpecialityID)
                
                
                                _ = navigationController?.popViewController(animated: true)
                break
            default:
                break
            }
            
        }else{
            switch type {
            case 1:
                self.present(Helper.alertVC(title: "Message".localized, message:VehicleMakeModelSelect.selectVehicleType), animated: true, completion: nil)
                break;
                
            case 2:
                self.present(Helper.alertVC(title: "Message".localized, message:VehicleMakeModelSelect.vehicleMake), animated: true, completion: nil)
                break;
                
            case 3:
                self.present(Helper.alertVC(title: "Message".localized, message:VehicleMakeModelSelect.selectModel), animated: true, completion: nil)
                break;
            case 4:
                //                self.present(Helper.alertVC(title: "Message".localized, message:VehicleMakeModelSelect.selectSpecialities), animated: true, completion: nil)
                break;
            default:
                self.present(Helper.alertVC(title: "Message".localized, message:VehicleMakeModelSelect.selectZones), animated: true, completion: nil)
                break;
            }
        }
    }
    
    @IBAction func backToVC(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
}
