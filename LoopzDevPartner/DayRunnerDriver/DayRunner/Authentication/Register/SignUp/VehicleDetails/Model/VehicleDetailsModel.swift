//
//  VehicleDetailsModel.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 30/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift

class VehicleModel : NSObject {
    
    let disposebag = DisposeBag()
    //Validating the phone number through otp,SignupOtp API
    func signUpOtpService(params:AuthenticationModel,completionHanlder:@escaping(Bool) -> ()) {
        let otp =  AuthenticationApis()
        otp.getOtp(otp: params)
        otp.subject_response.subscribe(onNext: { (response) in
            if let responseMessage = response["1"] as? String
            {
//                Helper.alertVC(errMSG: responseMessage)
                completionHanlder(true)
            }
            else
            {
                completionHanlder(false)
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposebag)

//        NetworkHelper.requestPOST(serviceName:API.METHOD.SignupGetOTP,
//                                  params: params,
//                                  success: { (response : [String : Any]) in
//                                    print("signup response : ",response)
//                                    Helper.hidePI()
//                                    if response.isEmpty == false {
//                                        let flag:Int = response["errFlag"] as! Int
//                                        if flag == 0{
//                                         completionHanlder(true)
//                                        }else{
//                                            Helper.hidePI()
//                                            Helper.alertVC(errMSG: response["errMsg"] as! String)
//                                            completionHanlder(false)
//                                        }
//                                    } else {
//
//                                    }
//
//        }) { (Error) in
//            Helper.alertVC(errMSG: Error.localizedDescription)
//            completionHanlder(false)
//        }
    }
}
