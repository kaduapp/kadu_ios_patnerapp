//
//  VehicleDetailsAction.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 30/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension  VehicleDetailsVC{
    
    //Select VehicleType
    @IBAction func selectType(_ sender: Any) {
        
        self.view.endEditing(true)
        typeModelMake = 1
        performSegue(withIdentifier: "selectVehicleDetails", sender: nil)
    }
    
    //Move to Signup Vc
    @IBAction func backToController(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    //Make The service call To Get OTP
    @IBAction func doneSignUp(_ sender: Any) {
        finishSignupButton.isEnabled = false
        signupMethod()  //@locate VehicleDetailsValidate
    }
    
    //Select Vehicle Image
    @IBAction func selectVehiclePic(_ sender: Any) {
        selectImageTyep = 0
        selectImage()   //@locate VehicleDetailsVC
    }
    
    //Select vehicle details image
    @IBAction func selectvehicleDoc(_ sender: Any) {
        switch (sender as AnyObject).tag {
        case 1:
            selectImageTyep = 1
        case 2:
            selectImageTyep = 2
        case 3:
            selectImageTyep = 3
        default:
            selectImageTyep = 1
        }
        selectImage() //@locate VehicleDetailsVC
    }
    
    // Gesture action to hide keyboard
    @IBAction func tapGestureAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
    // Select VehicleMake to VehicleMakeModelVc
    @IBAction func selectMake(_ sender: Any) {
        self.view.endEditing(true)
        typeModelMake = 2
        performSegue(withIdentifier: "selectVehicleDetails", sender: nil)
        
    }
    
    //Stores Make id, and display the models under that make
    @IBAction func selectModel(_ sender: Any) { //Need to send Make ID
        self.view.endEditing(true)
        if vehicleTypeID.isEmpty{
            self.present(Helper.alertVC(title: "Message".localized, message:vehicleDetails.vehicleType), animated: true, completion: nil)
        }else{
            typeModelMake = 3
            performSegue(withIdentifier: "selectVehicleDetails", sender: nil)
        }
        
    }
    
//    //Store Vehcile id and displays the specilaties under that vehicle id
//    @IBAction func selectSpecialities(_ sender: Any) {//Need to send vehicle ID
//        if vehicleTypeID.isEmpty{
//            self.present(Helper.alertVC(title: "Message".localized, message:vehicleDetails.vehicleType), animated: true, completion: nil)
//        }else{
//            typeModelMake = 4
//            performSegue(withIdentifier: "selectVehicleDetails", sender: nil)
//        }
//        self.view.endEditing(true)
//    }

    //select the city
//    @IBAction func actionButtonSelectCity(_ sender: UIButton) {
//        typeModelMake = 7
//        self.view.endEditing(true)
//        performSegue(withIdentifier: "selectVehicleDetails", sender: nil)
//    }
    
//    @IBAction func actionButtonSelectServices(_ sender: UIButton) {
//        typeModelMake = 8
//        performSegue(withIdentifier: "selectVehicleDetails", sender: nil)
//    }
    
}

//Selected Vehicle details
extension VehicleDetailsVC:vehicleDetailsDelegate{
    func didSelectZones(selectedZones: VehicleType) {
        
    }
    
    
    func didSelectServices(services serviceName: String, serviceID: String, serviceTypeID: String) {
        self.textFieldSelectedServices.text = serviceName
        self.selectedServiceID = serviceID
        
        let serviceNameArray = serviceName.components(separatedBy: ",")
        let serviceIDArray = serviceID.components(separatedBy: ",")
        let serviceTypeIDArray = serviceTypeID.components(separatedBy: ",")
        
        
        
        for var i in 0..<serviceNameArray.count
        {
        
            let serviceDict = [
                "id": serviceIDArray[i],
                "name": serviceNameArray[i],
                "type": serviceTypeIDArray[i],
                ]
            
            serviceArray.append(serviceDict)
        }
        print(serviceArray)
        
    }
    
    
    
    func didSelectVehicleSeciality(speciality specialityName: String, specialityID: String) {
//        self.vehicleSpecialities.text = specialityName
        self.selectedSpecialityID = specialityID
    }
    
    func didSelectServices(services serviceName: String, serviceID: String) {
        self.textFieldSelectedServices.text = serviceName
        self.selectedServiceID = serviceID
        
    }
    
    func didSelectVehicleType(vehicleType vehicleTypeData: VehicleTypeSelected) {
        self.vehicleType.text = vehicleTypeData.vehicleTypeName
        UserDefaults.standard.set(vehicleTypeData.vehicleTypeServices, forKey: "vehicleTypeServices")
        UserDefaults.standard.set(vehicleTypeData.vehicleTypeSpecialities, forKey: "vehicleTypeSpecialities")
        vehicleTypeID = vehicleTypeData.vehicleTypeID
    }
    
//    func didSelectCity(city cityData: CitySelected) {
//        self.citySelectedData = cityData
//        UserDefaults.standard.set(cityData.cityID, forKey: "selectedCityID")
//        self.textFieldCity.text = self.citySelectedData.cityName
//    }
    
    internal func didSelectMakeModelType(vehicleData: VehicleType)
    {
        if vehicleData.type == 1{   //Vehicle type
            vehicleType.text = vehicleData.vehicleTypeMakeModel
            vehicleTypeID = vehicleData.idSelected
//            vehicleSpecialities.text = ""
            indexesSelected = [Int]()
            vehicleSelected = vehicleData.selIndex
            
        }else if vehicleData.type == 2{  // vehcile make
            vehicleMake.text = vehicleData.vehicleTypeMakeModel
            vehicleMakeID = vehicleData.idSelected
            vehicleModel.text = ""
            modelSelected = -1
            makeSelected = vehicleData.selIndex
            
        }else if vehicleData.type == 3{  // vehicle Model
            vehicleModel.text = vehicleData.vehicleTypeMakeModel
            vehicleModelID = vehicleData.idSelected
            modelSelected = vehicleData.selIndex
        }
        else{  // vehicle specialties
//            vehicleSpecialities.text = vehicleData.vehicleTypeMakeModel
//            specialitiesIds = vehicleData.seletedIDs
            indexesSelected = vehicleData.selectedIndexes
        }
    }
}
