//
//  VehicleDetailsTF.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 30/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension VehicleDetailsVC{
    
    // move view to current position
    func moveViewDown() {
        self.mainSrollView.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(0))
    }
    
    //*******move view according to the textfield to show ********//
    func moveViewUp(_ textfield: UIView, andKeyboardHeight height: Float) {
        
        var  textFieldMaxY:Float = Float(textfield.frame.maxY)+10
        
        var view: UIView? = textfield.superview
        while view != self.view.superview {
            textFieldMaxY += Float((view?.frame.minY)!)
            view = view?.superview
        }
        let remainder: Float? = Float((self.view.window?.frame.height)!) - (textFieldMaxY + height)
        if remainder! >= 0 {
            
        }
        else {
            UIView.animate(withDuration: 0.4, animations: {() -> Void in
                self.mainSrollView.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(-remainder!))
            })
        }
    }
    
    //**************** Textfield delegate methods *******************//
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        dismisskeyBord()
        return true
    }
    
}
