//
//  VehicleDetailsVC.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class VehicleDetailsVC: UIViewController,UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    
    
    @IBOutlet weak var finishSignupButton: UIButton!
    @IBOutlet weak var textFieldInsuranceNumber: HoshiTextField!
    @IBOutlet var contentScrollView: UIView!
    var activeTextField = UITextField()
    var selectImageTyep:Int = 0
    var typeModelMake:Int = 0
    var selectedServiceID:String = ""
    var selectedSpecialityID: String = ""
    var serviceArray = [[String:String]]()
    @IBOutlet weak var textFieldSelectedServices: HoshiTextField!
    @IBOutlet weak var textFieldCity: HoshiTextField!
    @IBOutlet var insuranceImage: UIImageView!
    @IBOutlet var registrationImage: UIImageView!
    @IBOutlet var permitImage: UIImageView!
    @IBOutlet var mainSrollView: UIScrollView!
    @IBOutlet var plateNumber: HoshiTextField!
    @IBOutlet var vehicleModel: HoshiTextField!
    @IBOutlet var vehicleMake: HoshiTextField!
    @IBOutlet var vehicleType: HoshiTextField!
//    @IBOutlet var vehicleSpecialities: HoshiTextField!
    
    
    var signupParams: AuthenticationModel!
    var vehicleDetailsModel = VehicleModel()
    
    var vehicleDocs = String()
    @IBOutlet var vehicleImage: UIImageView!
    
    var vehImage = UIImageView()
    var insImage = UIImageView()
    var regImage = UIImageView()
    var perImage = UIImageView()
    
    var vehicleTypeID     = String()
    var vehicleMakeID     = String()
    var vehicleModelID    = String()
    var specialitiesIds   = [String]()
//    var citySelectedIds   = String()//comment
    
    
    /// for upload purpose
    
    var vehicleTimeStamp = String()
    var temp = [UploadImage]()
    
//    var citySelectedData: CitySelected!//comment
    
   
    var indexesSelected   = [Int]()
    var vehicleSelected:Int = -1
    var makeSelected:Int    = -1
    var modelSelected:Int   = -1
//    var citySelected:Int    = -1//comment
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vehImage.image = nil
        insImage.image = nil
        regImage.image = nil
        perImage.image = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectVehicleDetails" {
            let nextScene = segue.destination as? VehicleTypeModelMakeVC
            nextScene?.delegate = self
            if typeModelMake == 1{
                nextScene?.type = typeModelMake
                nextScene?.alreadySelectedIndex = vehicleSelected
            }else if typeModelMake == 2{
                nextScene?.type = typeModelMake
                nextScene?.alreadySelectedIndex = makeSelected
            }else if typeModelMake == 3{
                nextScene?.type = typeModelMake
                nextScene?.idMakeSeleted = vehicleMakeID
                nextScene?.alreadySelectedIndex = modelSelected
            }else if typeModelMake == 4{
                nextScene?.type = typeModelMake
                nextScene?.idMakeSeleted = vehicleTypeID
                nextScene?.alreadySelectedIndexes = indexesSelected
            }
//            else if typeModelMake == 7{
//                nextScene?.type = typeModelMake
//                nextScene?.alreadySelectedIndex = citySelected
//                nextScene?.idMakeSeleted = citySelectedIds
//            }
            else if typeModelMake == 8
            {
                nextScene?.type = typeModelMake
//                nextScene?.alreadySelectedIndex = citySelected//comment
//                nextScene?.idMakeSeleted = citySelectedIds//comment
            }
        }else if segue.identifier == "signupVerification"{
            let nextScene = segue.destination as? VerifyOTPVC
            nextScene?.defineTheOtp = 1   // for signup 1
            nextScene?.mobileNumber =  String(describing: signupParams.mobileNo)
            nextScene?.countryCode = String(describing: signupParams.countryCode)
            nextScene?.signupParams = signupParams
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let keyboardSize = CGSize(width: 320, height: 240)
        let height: CGFloat = UIDevice.current.orientation.isPortrait ? keyboardSize.height : keyboardSize.width
        UIView.animate(withDuration: 0.4, animations: {() -> Void in
            var edgeInsets: UIEdgeInsets = self.mainSrollView.contentInset
            edgeInsets.bottom = height
            self.mainSrollView.contentInset = edgeInsets
            edgeInsets = self.mainSrollView.scrollIndicatorInsets
            edgeInsets.bottom = height
            self.mainSrollView.scrollIndicatorInsets = edgeInsets
        })
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            mainSrollView.contentInset = contentInset
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //**hide the keyboard**//
    func dismisskeyBord() {
        view.endEditing(true)
        moveViewDown() //@locate VehicleDetailsTF
    }

    
    /*****************************************************************/
    //MARK: - UIImage Picker
    /*****************************************************************/
    
    func selectImage() {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select image".localized as String?,
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: vehicleDetails.cancel as String?,
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: vehicleDetails.gallery as String?,
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: vehicleDetails.camera as String?,
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //********** navigate to gallery*********//
    func chooseFromPhotoGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    //********** navigate to camera*********//
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.camera;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
    /*****************************************************************/
    //MARK: - UIImage Picker Delegate Method
    /*****************************************************************/
    
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
          if let image = info[.originalImage] as? UIImage{
        
        switch selectImageTyep {
        case 0:
            vehicleImage.image = Helper.resizeImage(image: image, newWidth: 200)
            vehImage.image = Helper.resizeImage(image: image, newWidth: 200)
            
            break
        case 1:
            insuranceImage.image = Helper.resizeImage(image: image, newWidth: 200)
            insImage.image = Helper.resizeImage(image: image, newWidth: 200)
            break
        case 2:
            registrationImage.image = Helper.resizeImage(image: image, newWidth: 200)
            regImage.image = Helper.resizeImage(image: image, newWidth: 200)
            break
        case 3:
            permitImage.image = Helper.resizeImage(image: image, newWidth: 200)
            perImage.image = Helper.resizeImage(image: image, newWidth: 200)
            break
        default:
            break
        }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}

