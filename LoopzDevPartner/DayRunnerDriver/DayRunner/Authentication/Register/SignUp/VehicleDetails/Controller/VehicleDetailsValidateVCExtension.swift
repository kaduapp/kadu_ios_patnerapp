 //
//  VehicleDetailsValidate.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 30/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension VehicleDetailsVC{
    /// cheking the vehicle data*********//
    func signupMethod(){
        
        
        if vehImage.image == nil{
            self.present(Helper.alertVC(title: "Message".localized, message:vehicleDetails.vehicleImg), animated: true, completion: nil)
                self.finishSignupButton.isEnabled = true
        }
        else if (plateNumber?.text?.isEmpty)!{
            self.finishSignupButton.isEnabled = true
            self.present(Helper.alertVC(title: "Message".localized, message:vehicleDetails.vehicleNumber), animated: true, completion: nil)
        }
//        else if (vehicleSpecialities.text?.isEmpty)!{
//            self.present(Helper.alertVC(title: "Message".localized, message:vehicleDetails.vehicleColor), animated: true, completion: nil)
//        }
        else if (vehicleType.text?.isEmpty)!{
            self.finishSignupButton.isEnabled = true
            self.present(Helper.alertVC(title: "Message".localized, message:vehicleDetails.vehicleType), animated: true, completion: nil)
        }
            
        else if (vehicleMake.text?.isEmpty)!{
            self.finishSignupButton.isEnabled = true
            self.present(Helper.alertVC(title: "Message".localized, message:vehicleDetails.vehicleMake), animated: true, completion: nil)
        }
        else if (vehicleModel.text?.isEmpty)!{
            self.finishSignupButton.isEnabled = true
            self.present(Helper.alertVC(title: "Message".localized, message:vehicleDetails.vehicleModel), animated: true, completion: nil)
        }
//        else if (textFieldSelectedServices.text?.isEmpty)!
//        {
//            self.present(Helper.alertVC(title: "Message".localized, message:vehicleDetails.insuranceNumber), animated: true, completion: nil)
//        }
            
        else if  regImage.image == nil{
            self.finishSignupButton.isEnabled = true
            self.present(Helper.alertVC(title: "Message".localized, message:vehicleDetails.vehicleRegis), animated: true, completion: nil)
        }
            
        else if insImage.image == nil{
            self.finishSignupButton.isEnabled = true
            self.present(Helper.alertVC(title: "Message".localized, message:vehicleDetails.vehicleIns), animated: true, completion: nil)
        }

        else if perImage.image == nil{
            self.finishSignupButton.isEnabled = true
            self.present(Helper.alertVC(title: "Message".localized, message:vehicleDetails.carriagePermit), animated: true, completion: nil)
        }
        else{
            
            temp = [UploadImage]()
            vehicleTimeStamp = Helper.currentTimeStamp
            
            temp.append(UploadImage.init(image: vehicleImage.image!, path: AMAZONUPLOAD.VEHICLEIMAGE +  vehicleTimeStamp + "_7" + ".png"))
            temp.append(UploadImage.init(image: insuranceImage.image!, path: AMAZONUPLOAD.INSURANCE + vehicleTimeStamp + "_1" + "_ins" + ".png"))
            temp.append(UploadImage.init(image: registrationImage.image!, path: AMAZONUPLOAD.INSURANCE + vehicleTimeStamp + "_3" +  "_reg" + ".png"))
            temp.append(UploadImage.init(image: permitImage.image!, path: AMAZONUPLOAD.INSURANCE +  vehicleTimeStamp + "_2" + "_permit" + ".png"))
            
            //Upload vehicle image and details vehicle to server
            let upMoadel = UploadImageModel.shared  //@locate UploadImageModel
            upMoadel.uploadImages = temp
            upMoadel.start()
            
            
            let vehicleIMG      = Utility.amazonUrl + AMAZONUPLOAD.VEHICLEIMAGE + vehicleTimeStamp + "_7" + ".png"
            let motor_insurance = Utility.amazonUrl + AMAZONUPLOAD.INSURANCE + vehicleTimeStamp + "_1" + "_ins" + ".png"
            let reg_cer         = Utility.amazonUrl + AMAZONUPLOAD.INSURANCE + vehicleTimeStamp + "_3" +  "_reg" + ".png"
            let carrierPermin   = Utility.amazonUrl + AMAZONUPLOAD.INSURANCE +  vehicleTimeStamp + "_2" + "_permit" + ".png"
            
//            signupParams.insurancePhoto = motor_insurance
//            signupParams.regCert = reg_cer
//            signupParams.carrierPermit = carrierPermin
//            signupParams.plateNo = plateNumber.text!
//            signupParams.specialities = selectedSpecialityID
//            signupParams.make = vehicleMakeID
//            signupParams.model = vehicleModelID
//            signupParams.vehicleImage = vehicleIMG
//            signupParams.cityID = citySelectedData.cityID
//            signupParams.insuranceNo = textFieldSelectedServices.text!
//            signupParams.serviceArray = serviceArray
//            signupParams.vehicleID = vehicleTypeID;
            
//            signupParams.updateValue(motor_insurance, forKey: "ent_insurance_photo")
//            signupParams.updateValue(reg_cer, forKey: "ent_reg_cert")
//            signupParams.updateValue(carrierPermin, forKey: "ent_carriage_permit")
//            signupParams.updateValue(plateNumber.text!, forKey: "ent_plat_no")
//            signupParams.updateValue(vehicleTypeID, forKey: "ent_type")
//            signupParams.updateValue(selectedSpecialityID, forKey: "ent_specialities")
//            signupParams.updateValue(vehicleMakeID, forKey: "ent_make")
//            signupParams.updateValue(vehicleModelID, forKey: "ent_model")
//            signupParams.updateValue(vehicleIMG, forKey: "ent_vehicleImage")
//            signupParams.updateValue(citySelectedData.cityID, forKey: "cityId")
//            signupParams.updateValue(textFieldSelectedServices.text!, forKey: "insuranceNumber")
//            signupParams.updateValue(serviceArray, forKey: "services")
            //Locate VehicleDetailsModel
            
            let otpAuthentication = AuthenticationModel.init(countryCode: String(describing: signupParams.countryCode),fullMobileNo: String(describing: signupParams.mobileNo), userType: "2")
            
            
            vehicleDetailsModel.signUpOtpService(params: otpAuthentication, completionHanlder: { success in
                
                self.finishSignupButton.isEnabled = true
                
                if success{
                    self.performSegue(withIdentifier:"signupVerification" , sender: nil)
                }else {
                    
                }
                
            })
            print(vehicleIMG)
            
        }
    }
    
}
