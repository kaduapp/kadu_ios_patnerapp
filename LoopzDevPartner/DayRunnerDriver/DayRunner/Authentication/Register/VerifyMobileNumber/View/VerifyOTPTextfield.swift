//
//  VerifyOtpTextFieldVc.swift
//  GoGoCargoDriver
//
//  Created by Vengababu Maparthi on 22/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension VerifyOTPVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        //print("TextField did end editing method called")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //print("TextField should begin editing method called")
        
        activeTextField = textField
        //        moveViewUp(textField, keyboardHeight: 220)
        
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        //print("TextField should clear method called")
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        //print("TextField should snd editing method called")
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        //print("While entering the characters this method gets called")
        
        // This allows numeric text only, but also backspace for deletes
        if string.length > 0 && !string.isNumber {
            return false
        }
        
        // Hide Keyboard when you enter last digit
        if textField.isEqual(textField4) && !string.isEmpty {
            textField.text = string
            textField.resignFirstResponder()
        }
        
        let oldLength = textField.text!.length
        let replacementLength = string.length
        let rangeLength = range.length
        let newLength = oldLength - rangeLength + replacementLength
        
        
        // This 'tabs' to next field when entering digits
        if (newLength == 1) {
            
            if textField.isEqual(textField1) {
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: textField2, afterDelay: 0.05)
            }
            else if textField.isEqual(textField2) {
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: textField3, afterDelay: 0.05)
            }
            else if textField.isEqual(textField3) {
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: textField4, afterDelay: 0.05)
            }
        }
            //this goes to previous field as you backspace through them, so you don't have to tap into them individually
        else if (oldLength > 0 && newLength == 0) {
            
            if textField.isEqual(textField4) {
                self.perform(#selector(self.setPrevResponder(nextResponder:)), with: textField1, afterDelay: 0.05)
            }
            else if textField.isEqual(textField3) {
                self.perform(#selector(self.setPrevResponder(nextResponder:)), with: textField1, afterDelay: 0.05)
            }
            else if textField.isEqual(textField2) {
                self.perform(#selector(self.setPrevResponder(nextResponder:)), with: textField1, afterDelay: 0.05)
            }
        }else{
            if self.defineTheOtp == 1{  // signup
                processType = 2
                
            }else if self.defineTheOtp == 2{ //forgot password
                processType = 1
            }
            else{
                processType = 2
            }
            
            if (textField1.text?.isEmpty)! || (textField2.text?.isEmpty)! || (textField3.text?.isEmpty)! || (textField4.text?.isEmpty)!{
                self.present(Helper.alertVC(title: "Message".localized, message:"Please fill the OTP".localized), animated: true, completion: nil)
            }else{
                Helper.showPI(message:"Verifying OTP")
          
                let verifyOtp = AuthenticationModel.init(fullMobileNo: mobileNumber, countryCode: countryCode, code: textField1.text!+textField2.text!+textField3.text!+textField4.text!, processType: processType)
                verifyModl.verifyOTP(params: verifyOtp, completionHandler: { success in
                    if success{
                        if self.defineTheOtp == 1{
                            
                            self.verifyModl.sendRequestForSignup(params:self.signupParams, completionHandler: { succeeded in
                                if succeeded{
                                    self.handlingTheSignupResponse()
                                }else{
                                    
                                }
                            })
                            
                        }else if self.defineTheOtp == 2{
                            
                            self.performSegue(withIdentifier: "toCreateNewpassword", sender: nil)
                            
                        }else if self.defineTheOtp == 3{
                            
                            let updateNumber = profileUpdateModel.init(updatePhoneNumber: self.mobileNumber, countryCode: self.countryCode)
                            self.verifyModl.updateNumber(params: updateNumber, completionHandler: { succeeded in
                                if succeeded{
                                    _ = self.navigationController?.popToRootViewController(animated: true)
                                }
                            })
                        }
                    }else{
                        self.emptyTheData()
                    }
                })
            }
        }
        return newLength <= 1
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //print("TextField should return method called")
        
        return true
    }
    
    @objc func setPrevResponder(nextResponder: UITextField) {
        
        textField1.text = ""
        textField2.text = ""
        textField3.text = ""
        textField4.text = ""
        nextResponder.becomeFirstResponder()
    }
    @objc func setNextResponder(nextResponder: UITextField) {
        
        nextResponder.becomeFirstResponder()
    }
}
