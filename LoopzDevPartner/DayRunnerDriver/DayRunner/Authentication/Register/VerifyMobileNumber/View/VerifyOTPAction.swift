//
//  VerifyOTPAction.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 23/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension VerifyOTPVC{
    @IBAction func backToController(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func actionButtonVerifyOTP(_ sender: UIButton) {
        verifyOtpButton.isEnabled = false
        verifyOTP(nil)
    }
    //***********Move to vc to create new password*******//
    @IBAction func verifyOTP(_ sender: Any?) {
        if self.defineTheOtp == 1{  // signup
            processType = 2
        }else if self.defineTheOtp == 2{ //forgot password
            processType = 1
        }
        else{
            processType = 2
        }
        if (textField1.text?.isEmpty)! || (textField2.text?.isEmpty)! || (textField3.text?.isEmpty)! || (textField4.text?.isEmpty)!{
            verifyOtpButton.isEnabled = true
            self.present(Helper.alertVC(title: "Message".localized, message:"Please fill the OTP".localized), animated: true, completion: nil)
        }else{
            
            Helper.showPI(message:"Verifying OTP")
            
            let verifyOtp = AuthenticationModel.init(fullMobileNo: mobileNumber, countryCode: countryCode, code: textField1.text!+textField2.text!+textField3.text!+textField4.text!, processType: processType)
            
            verifyModl.verifyOTP(params: verifyOtp, completionHandler: { success in
                
                self.verifyOtpButton.isEnabled = true
                if success{
                    if self.defineTheOtp == 1{
                        self.verifyModl.sendRequestForSignup(params: self.signupParams , completionHandler: { succeeded in
                            if succeeded{
                                self.handlingTheSignupResponse()
                            }else{
                                
                            }
                        })
                    }
                    else if self.defineTheOtp == 2{
                        Helper.hidePI()
                        self.performSegue(withIdentifier: "toCreateNewpassword", sender: nil)
                    }else if self.defineTheOtp == 3{
//                        let params : [String : Any] =  [
//                            "mobile":self.mobileNumber,
//                            ]
//
                     let params = profileUpdateModel.init(updatePhoneNumber: self.mobileNumber, countryCode: self.countryCode)
                     
                        self.verifyModl.updateNumber(params: params, completionHandler: { succeeded in
                            if succeeded{
                                _ = self.navigationController?.popToRootViewController(animated: true)
                            }
                        })
                        Helper.hidePI()
                    }
                }else{
                    self.emptyTheData()
                }
            })
        }
    }
    
    //******hide the keyboard******//
    @IBAction func hideKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBAction func resendOtp(_ sender: Any) {
        
        let resentOTP = AuthenticationModel.init(countryCode: countryCode, fullMobileNo: mobileNumber, userType: "2")
        verifyModl.resendOtpForSignup(params: resentOTP, completionHandler: { success in
            if success{
                self.count = 60
                self.timer.invalidate()
                self.timer = Timer.scheduledTimer(timeInterval: 1.0
                    , target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
            }else{
                
            }
        })
    }
    
    @objc func update() {
        
        if(count > 0){
            count -= 1
            let minutes = String(count / 60)
            let seconds = String(count % 60)
            timerForResend.text = minutes + ":" + seconds
            resendOTP.isEnabled = false
            resendOTP.setTitleColor(Helper.UIColorFromRGB(rgbValue: 0xCACACA), for: .normal)
            
        }else{
            timerForResend.text = "00:00"
            resendOTP.setTitleColor(Helper.UIColorFromRGB(rgbValue: 0x707070), for: .normal)
            resendOTP.isEnabled = true
            self.timer.invalidate()
        }
    }
    
    //MARK: - Empty the textFields
    //**** error response clears the fields********//
    func emptyTheData(){
        textField1.text = ""
        textField2.text = ""
        textField3.text = ""
        textField4.text = ""
        textField1.becomeFirstResponder()
    }
    
    func handlingTheSignupResponse(){
        
        self.signUpDone()
        self.present(Helper.alertVC(title: "Message".localized, message:"Thanks for signing up. one of our representative will get in touch with you in the next 24 hours to setup your profile and get all the necessary documents."), animated: true, completion: nil)
    }
    
    
}
