//
//  VerifyOtpModel.swift
//  GrocerDriver
//
//  Created by Yogesh Makhija on 22/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift


class VerifyModel:NSObject {
    
    override init() {
        super.init()
    }
     let disposeBag = DisposeBag()
    //resent OTP
    var mobileNumber: String!
    var userType = "2"
    var entType = "1"
    init(mobileNumber: String)
    {
        self.mobileNumber = mobileNumber
    }
    
    //MARK: - Resend OTP API For change password
    func resendOtpForChangePassword(params:VerifyModel ,completionHandler:@escaping (Bool) -> ()) {
        Helper.showPI(message:"Loading..")
        print("params :",params)
        
        let resendOTP =  AuthenticationApis()
        resendOTP.resendOTP(mobileNumber: params)
        
        resendOTP.subject_response.subscribe(onNext: { (response) in
            if response.isEmpty == false {
                let flag:Int = response["errFlag"] as! Int
                if flag == 1{
                    Helper.hidePI()
                    Helper.alertVC(errMSG: response["errMsg"] as! String)
                    completionHandler(false)
                    
                }else{
                    completionHandler(true)
                    Helper.alertVC(errMSG: response["errMsg"] as! String)
                    
                }
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
        
    }
    
    //MARK: - Resend OTP API For SIGNUP
    func resendOtpForSignup(params:AuthenticationModel,completionHandler:@escaping (Bool) -> ()) {
        
        Helper.showPI(message:"Signing up..")
        
        print("getOtp parameters :",params)
        let otp =  AuthenticationApis()
        otp.getOtp(otp: params)
        otp.subject_response.subscribe(onNext: { (response) in
            //            Helper.hidePI()
            //            if response.isEmpty == false {
            if let responseMessage = response["1"] as? String
            {
                Helper.alertVC(errMSG: responseMessage)
                completionHandler(true)
                
            }
            else {
                Helper.alertVC(errMSG: response["errMsg"] as! String)
                completionHandler(false)
            }
            //            }
            
            
            //                if let responseMessage = response["1"] as? String{
            //
            //                }
            //                if flag == 0{
            //
            //                    Helper.hidePI()
            //                    Helper.alertVC(errMSG: response["errMsg"] as! String)
            //                }else{
            //
            //                    Helper.hidePI()
            //                    Helper.alertVC(errMSG: response["errMsg"] as! String)
            //                    completionHandler(true)
            //                }
            
            
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
        
    }
    
    
    
    //MARK: - Verify OTP API
    func verifyOTP(params:AuthenticationModel,completionHandler:@escaping (Bool) -> ()) {
        
        let verifyOTP =  AuthenticationApis()
        verifyOTP.verifyOtp(otp: params)
        
        verifyOTP.subject_response.subscribe(onNext: { (response) in
            let flag:Int = response["errFlag"] as! Int
            if flag == 200{
                completionHandler(true)
            }else{
                Helper.hidePI()
                completionHandler(false)
                //                Helper.alertVC(errMSG: response["errMsg"] as! String)
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
        
        
    }
    
    
    //MARK: - Signup Api
    func sendRequestForSignup(params:AuthenticationModel,completionHandler:@escaping (Bool) -> ()) {
        
        let signUp =  AuthenticationApis()
         signUp.signUP(signUP: params)
        signUp.subject_response.subscribe(onNext: { (response) in
            let flag:Int = response["errFlag"] as! Int
            
            if flag == 200 {
                completionHandler(true)
            }
            else{
                Helper.hidePI()
                //                                                        Helper.alertVC(errMSG: response["errMsg"] as! String)
                //                                                        completionHandler(false)
                
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
        
    }
    
    
    //MARK: - update number APi
    func updateNumber(params: profileUpdateModel,completionHandler:@escaping (Bool) -> ()) {
        Helper.showPI(message:"Loading..")
        
        let updateNumber  =  ProfileApi()
        updateNumber.changeNumber(num: params)
        updateNumber.subject_response.subscribe(onNext: { (response) in
            completionHandler(true)
        }, onError: { (error) in
            Helper.hidePI()
            completionHandler(false)
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
    }
}


