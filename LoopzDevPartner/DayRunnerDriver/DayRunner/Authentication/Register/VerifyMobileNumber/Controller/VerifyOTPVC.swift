//
//  VerifyOTPVC.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class VerifyOTPVC: UIViewController {
    
    @IBOutlet weak var verifyOtpButton: UIButton!
    var activeTextField = UITextField()
    @IBOutlet var textField1: UITextField!
    @IBOutlet var textField2: UITextField!
    @IBOutlet var textField3: UITextField!
    @IBOutlet var textField4: UITextField!
    
    @IBOutlet weak var verifyMobileTopLabel: UILabel!
    @IBOutlet weak var otpTopContentLabel: UILabel!
    @IBOutlet var contentScrollView: UIView!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var buttomConstraint: NSLayoutConstraint!
    @IBOutlet var resendOTP: UIButton!
    var defineTheOtp:Int = 0   // 1: signup, 2:forgot passwrod, 3:change number
    var processType:Int = 0
    var count:Int = 60
    var timer       = Timer()
    
    var countryCode = String()
    var mobileNumber = String()
    var signupParams: AuthenticationModel!
    
    var verifyModl = VerifyModel()
    
    @IBOutlet weak var timerForResend: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField1.text = "1"
        self.textField2.text = "1"
        self.textField3.text = "1"
        self.textField4.text = "1"
        verifyMobileTopLabel.text = "Verifying the MobileNumber".localized
        otpTopContentLabel.text = "A Verification Code has been sent to".localized + " " + mobileNumber + ". Please enter the code below.".localized
        resendOTP.setTitle("RESEND CODE ?".localized, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.textField4.becomeFirstResponder()
        self.timer = Timer.scheduledTimer(timeInterval: 1.0
            , target: self, selector: #selector(update), userInfo: nil, repeats: true)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
         self.view.endEditing(true)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.timer.invalidate()
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func signUpDone(){
        let controller = storyboard?.instantiateViewController(withIdentifier:storyBoardIDs.signIN)
        navigationController?.viewControllers = [controller!]
        
    }
    
    
    ///************ segue identifier method****************//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "toCreateNewpassword" {
            let nextScene = segue.destination as? SetNewpasswordViewController
            nextScene?.secureOtp = textField1.text!+textField2.text!+textField3.text!+textField4.text!
            nextScene?.mobileNumber = mobileNumber
            nextScene?.countryCode = countryCode
        }
    }
    
    
    
}

