//
//  WebViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 20/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet weak var webView: WKWebView!
    
    var urlFrom = ""
    var htmlFrom = ""
    var titleOfController = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titleOfController
        if urlFrom.length != 0 {
            Helper.showPI(message: "Loading..")
            webView.load(NSURLRequest(url: URL(string: urlFrom)!) as URLRequest)
        }else if htmlFrom.length != 0{
            Helper.showPI(message: "Loading..")
            let htmlString:String! = htmlFrom
            webView.loadHTMLString(htmlString, baseURL: nil)
        }else{
            self.present(Helper.alertVC(title: "Message".localized, message:"no proper link provided"), animated: true, completion: nil)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension WebViewController : WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Helper.hidePI()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        Helper.hidePI()
    }
    
    
}
