//
//  InviteViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 15/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Social
import MessageUI

class InviteViewController: UIViewController,MFMailComposeViewControllerDelegate {
    @IBOutlet var referralCode: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        ManageBooking.sharedInstance.currentNavigationController = self.navigationController
        referralCode.text = Utility.referralCode
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func shareCodeWithMessage(_ sender: Any) {
        if (MFMessageComposeViewController.canSendText()) {
            let messageVC = MFMessageComposeViewController()
            messageVC.body = "Hey there! Use my code " + Utility.referralCode  + " to signup and get exclusive deals on Loopz."
            messageVC.recipients =  ["Enter tel-nr"]
            messageVC.messageComposeDelegate = self
            self.present(messageVC, animated: true, completion: nil)
        }else{
            Helper.alertVC(errMSG: "No messaging option available")
        }
    }
    
    
    @IBAction func shareCodeWithEmail(_ sender: Any) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    @IBAction func shareCodeWithTwitter(_ sender: Any) {
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter) {
            
            let tweetShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            
            let referralText = "Hey there! Use my code " + Utility.referralCode  + " to signup and get exclusive deals on Loopz."
            
            tweetShare.setInitialText(referralText)
            
        
            self.present(tweetShare, animated: true, completion: nil)
            
        } else {
            
            let alert = UIAlertController(title: inviteAlerts.Account, message: inviteAlerts.logtwitter, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: inviteAlerts.ok, style: UIAlertAction.Style.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func shareCodeWithFB(_ sender: Any) {
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
            let fbShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
              let referralText = "Hey there! Use my code " + Utility.referralCode  + " to signup and get exclusive deals on Loopz."
            fbShare.setInitialText(referralText)
            
            self.present(fbShare, animated: true, completion: nil)
            
        } else {
            let alert = UIAlertController(title: inviteAlerts.Account, message: inviteAlerts.logFacebook, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: inviteAlerts.ok, style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func menuButton(_ sender: Any) {
          slideMenuController()?.toggleLeft() 
    }
    func showSendMailErrorAlert() {
        
        let sendMailErrorAlert = UIAlertController(title:
            inviteAlerts.cantSentMail, message: inviteAlerts.noEmailAct, preferredStyle: .alert)
        sendMailErrorAlert.addAction(UIAlertAction(title: inviteAlerts.ok, style: .default, handler: nil))
        self.present(sendMailErrorAlert, animated: true, completion:nil)
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        let referralText = "Loopz Referral Code"
        mailComposerVC.setSubject(referralText)
        mailComposerVC.setMessageBody("Hey there! Use my code " + Utility.referralCode  + " to signup and get exclusive deals on Loopz.", isHTML: false)
        
        return mailComposerVC
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
 }


extension InviteViewController : MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
