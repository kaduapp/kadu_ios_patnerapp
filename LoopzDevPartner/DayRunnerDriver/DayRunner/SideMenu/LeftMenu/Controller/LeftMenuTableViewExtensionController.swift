//
//  LeftMenuTableView.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import Kingfisher


//*****************Tableview datasource**********//
extension LeftViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1;
        }else  {
//            let storeType:Int = UserDefaults.standard.integer(forKey: "storeAccountType")
//            if storeType == 3 {
//                return 5
//            }else {
                return 7;
//            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier:"first") as! LeftInitialTableViewCell!
            
            let imageURL = Utility.userImage
            cell?.profileImage.kf.setImage(with: URL(string: imageURL),
                                           placeholder:UIImage.init(named: "menuslider_defaultimag"),
                                           options: [.transition(ImageTransition.fade(1))],
                                           progressBlock: { receivedSize, totalSize in
            },
                                           completionHandler: { image, error, cacheType, imageURL in
            })
            
            
            cell?.driverName.text = Utility.userName
            
            
            return cell!
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier:"second") as! LeftSecondTableViewCell!
            switch indexPath.row {
            case 0:
                cell?.updateThefield(menuName:"Home".localized,menuImage: UIImage(named: "menu_home_icon")!)
                break;
            case 1:
                cell?.updateThefield(menuName:"History".localized,menuImage: UIImage(named: "menu_history_icon")!)
                break;

            case 4:
                cell?.updateThefield(menuName:"Wallet".localized,menuImage: UIImage(named: "wallet_icon")!)
                break;
                
            case 3:
                cell?.updateThefield(menuName:"Cards".localized,menuImage: UIImage(named: "Add_new1")!)
                break;
                
            case 2:
                cell?.updateThefield(menuName:"Support".localized,menuImage: UIImage(named: "menu_support_icon")!)
                break;
                
            case 6:
                
                cell?.updateThefield(menuName: "Language".localized, menuImage: UIImage(named: "lang_icon")!)
                
                break
//            case 2:
//                cell?.updateThefield(menuName:"Live Chat".localized,menuImage: UIImage(named: "menu_logout_icon")!)
//                break;
                
//            case 3:
//                cell?.updateThefield(menuName:"Driver Portal".localized,menuImage: UIImage(named: "Portal")!)
//                break;
//            case 4:
//                cell?.updateThefield(menuName:"Invite".localized,menuImage: UIImage(named: "inviteicon")!)
//                break;
            case 5:
                cell?.updateThefield(menuName:"Bank Details".localized,menuImage: #imageLiteral(resourceName: "bank"))
                break;

//            default:
                //                cell?.updateThefield(menuName:"Bank Details".localized,menuImage: #imageLiteral(resourceName: "bank"))
//                break;
                
            default:
                break
            }
            return cell!
        }
//        else {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "third", for: indexPath)
//            return cell
//        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0 {
            return 160;
        }else{
            let storeType:Int = UserDefaults.standard.integer(forKey: "storeAccountType")
            if storeType == 2 {
                if indexPath.row == 4 {
                    return 0;
                }
            }
            
            let def = UserDefaults.standard
            let saveValue = def.bool(forKey: "enableBankAccount")
            if saveValue {
               return 50.0
            }else {
                switch(indexPath.row) {
                case 4:
                    return 0
                default:
                    return 50.0;
                }
            }
        }
    }
}
//*****************Tableview delegate**********//
extension LeftViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if indexPath.section == 0 {
            let profileController = storyboard?.instantiateViewController(withIdentifier:storyBoardIDs.profileVC!) as! ProfileViewController
            self.ProfileVC = UINavigationController(rootViewController: profileController)
            self.slideMenuController()?.changeMainViewController(self.ProfileVC, close: true)
        }else{
            switch indexPath.row {
            case 0:  //MARK: - HomeviewController
                let homeController = storyboard?.instantiateViewController(withIdentifier:storyBoardIDs.homeVC!) as! HomeViewController
                self.homeVC = UINavigationController(rootViewController: homeController)
                self.slideMenuController()?.changeMainViewController(self.homeVC, close: true)
                break
                
            case 1:  //MARK: - HistoryViewController
                let historyController = storyboard?.instantiateViewController(withIdentifier:storyBoardIDs.historyVC!) as! HistoryViewController
                self.HistoryVC = UINavigationController(rootViewController: historyController)
                self.slideMenuController()?.changeMainViewController(self.HistoryVC, close: true)
                break

            case 2:  //MARK: - SupportViewController
                let supportController = UIStoryboard(name: "Zendesc", bundle: nil).instantiateViewController(withIdentifier:"TicketsViewController") as! TicketsViewController
                self.SupportVC = UINavigationController(rootViewController: supportController)
                self.slideMenuController()?.changeMainViewController(self.SupportVC, close: true)
                break
                
                
//            case 2:  //MARK: - Live Chat
//
//                break
//
//            case 3:  //MARK: - Driver Portal
//
//                break
                
//            case 4://MARK: - InviteViewController
//                let inviteController = storyboard?.instantiateViewController(withIdentifier:storyBoardIDs.inviteVC!) as! InviteViewController
//                self.InviteVC = UINavigationController(rootViewController: inviteController)
//                self.slideMenuController()?.changeMainViewController(self.InviteVC, close: true)
//
//                break
                
            case 5:

                let inviteController = storyboard?.instantiateViewController(withIdentifier:storyBoardIDs.BankDetailsVC!) as! BankAccountDetailsVC
                self.BankDetailsVC = UINavigationController(rootViewController: inviteController)
                self.slideMenuController()?.changeMainViewController(self.BankDetailsVC, close: true)
                break
                
            case 4:
//                let walletStoryboard: UIStoryboard = UIStoryboard(name: "QuickCardVC", bundle: nil)
                let walletController = storyboard?.instantiateViewController(withIdentifier:"WalletHomeVC") as! WalletHomeVC
                self.Wallet = UINavigationController(rootViewController: walletController)
//                let nvc: UINavigationController = UINavigationController(rootViewController: self.Wallet)
                self.slideMenuController()?.changeMainViewController(self.Wallet, close: true)
                break
                
            case 3:
                
                let inviteController = storyboard?.instantiateViewController(withIdentifier:"PaymentVC") as! PaymentVC
                self.CardsList = UINavigationController(rootViewController: inviteController)
                self.slideMenuController()?.changeMainViewController(self.CardsList, close: true)
                break
                
            case 6:  //MARK: - SupportViewController
                let selectLang = storyboard?.instantiateViewController(withIdentifier:
                    storyBoardIDs.selectLanguage!) as! SelectLanguageViewController
                self.SupportVC = UINavigationController(rootViewController: selectLang)
                self.slideMenuController()?.changeMainViewController(self.SupportVC, close: true)
                break
                
                //            case 7:
                //                let inviteController = storyboard?.instantiateViewController(withIdentifier:storyBoardIDs.BankDetailsVC!) as! BankAccountDetailsVC
                //                self.BankDetailsVC = UINavigationController(rootViewController: inviteController)
                //                self.slideMenuController()?.changeMainViewController(self.BankDetailsVC, close: true)
                //                break
                
            default:
                break
            }
        }
    }
}

