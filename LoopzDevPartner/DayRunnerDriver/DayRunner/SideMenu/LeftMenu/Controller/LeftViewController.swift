//
//  LeftViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher


class LeftViewController: UIViewController {
    
    @IBOutlet var leftVCTableView: UITableView!
    
    var homeVC: UIViewController!
    var ProfileVC: UIViewController!
    var HistoryVC: UIViewController!
    var SupportVC: UIViewController!
    var InviteVC: UIViewController!
    var BankDetailsVC: UIViewController!
    var Wallet: UIViewController!
    var CardsList : UIViewController!
    
    @IBOutlet weak var versionLabel: UILabel!
    var leftMenuMdl = LeftMenuModel()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.versionLabel.text = "Version".localized + " " + Utility.appVersion
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let ud = UserDefaults.standard
        ud.set(false, forKey: "cameraOpened")
        
        if USER_INFO.USER_NAME == Utility.userName {
            self.profileDetails()
        }else{
            self.leftVCTableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func profileDetails(){
        //@Locate LeftMenuModel
        leftMenuMdl.profileDetails(completionHandler: { success in
            if success{
                self.leftVCTableView.reloadData()
            }
        })
    }
}


