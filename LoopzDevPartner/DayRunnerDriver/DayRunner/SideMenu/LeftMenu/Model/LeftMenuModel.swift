//
//  LeftMenuModel.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift

class LeftMenuModel: NSObject {
    
    
    var disposeBag = DisposeBag()
    //MARK: - Profile details initially for slide menu or if profile editted
    
    func profileDetails(completionHandler:@escaping(Bool) -> ()){
        
        let profileData =  ProfileApi()
        profileData.getProfileData()
        
        profileData.subject_response.subscribe(onNext: { (response) in
            if let data =  response["data"] as? [String:Any]{
                Helper.hidePI()
                if response.isEmpty == false {
                    if (response["statusCode"] != nil){
                        let statCode:Int = response["statusCode"] as! Int
                        if statCode == 401 {
                            Helper.hidePI()
                            Session.expired() //@locate UIViewController+session
                            Helper.alertVC(errMSG: "Your Session has beed expired")
                            
                        }else
                        {
                            Helper.hidePI()
                            Helper.alertVC(errMSG: "bad request or  internal server error")
                        }
                        completionHandler(false) //Callback
                    }else{
                        let data =  response["data"] as? [String:Any]
                        let image = data?["profilePic"] as? String!
                        let name = data?["Name"] as? String!
                        let ud = UserDefaults.standard
                        ud.set(image, forKey:  USER_INFO.USERIMAGE)
                        ud.set(name, forKey:  USER_INFO.USER_NAME)
                        ud.synchronize()
                        completionHandler(true)//Callback
                    }
                } else {
                    completionHandler(false)//Callback
                }
                //
            }
                
            else {
                
            }
            
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)

    }
    
    //MARK: -Logout API Manually
    func logoutFromApp() {
        Helper.showPI(message:"Logging Out..")
        
//
//        let params : [String : Any]? =  nil
        let logoutApi =  APIs()
        logoutApi.logoutApi()
        
        logoutApi.subject_response.subscribe(onNext: { (response) in
            Session.expired()
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
        
    }
}
