//
//  SelectLanguageViewController.swift
//  LoopzDevPartner
//
//  Created by Rahul Sharma on 12/28/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class SelectLanguageViewController: UIViewController {
    var modelOflan:[LangModel]?
    var apiModel = Languages()
    var language:LangModel!
    
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var selectTableView: UITableView!
    let location = LocationManager.sharedInstance
//    var languageArray: [LanguageModel] = Languages.menuList
//    var language = LanguageModel(name: "ENGLISH", code: "en", isRTL: false)
    var languageString : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.languageLabel.text = "Language".localized
//        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
//        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
      //  languageString = UserDefaults.standard.value(forKey: "LanguageCode") as! String
        apiModel.languagesAPI { (success) in
            if success{
                self.language = self.apiModel.langData[0]
                self.modelOflan = self.apiModel.langData
                self.selectTableView.reloadData()
            }
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        if Utility.getSelectedLanguegeCode() == "ar" {
            slideMenuController()?.toggleRight()
        } else {
            slideMenuController()?.toggleLeft()
        }
    }

}
extension SelectLanguageViewController : UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.modelOflan != nil  {
            return self.modelOflan!.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : LanguageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LanguageTableViewCell") as! LanguageTableViewCell!
        cell.checkImage.isHidden = true
        cell.isUserInteractionEnabled = true
//        en   ca   es
        if languageString == modelOflan![indexPath.row].code {
            cell.checkImage.image = UIImage.init(named: "check_icon_on")
            cell.checkImage.isHidden = false
            cell.isUserInteractionEnabled = false
        }else if languageString == modelOflan![indexPath.row].code {
            cell.checkImage.image = UIImage.init(named: "check_icon_on")
            cell.isUserInteractionEnabled = false
            
        }else if languageString == modelOflan![indexPath.row].code {
           cell.checkImage.image = UIImage.init(named: "check_icon_on")
            cell.isUserInteractionEnabled = false
        }else if languageString == modelOflan![indexPath.row].code {
            cell.checkImage.image = UIImage.init(named: "check_icon_on")
            cell.isUserInteractionEnabled = false
        }
        if self.modelOflan != nil  {
            cell.langLabel.text = self.modelOflan![indexPath.row].name
        } else {
            cell.langLabel.text = ""
        }
       
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Utility.setChoosedLanguege(self.modelOflan![indexPath.row])
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let signIN: SplashViewController? = storyboard.instantiateViewController(withIdentifier: storyBoardIDs.splashVC!) as? SplashViewController
        let window = UIApplication.shared.keyWindow
        window?.rootViewController = signIN
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    
}
