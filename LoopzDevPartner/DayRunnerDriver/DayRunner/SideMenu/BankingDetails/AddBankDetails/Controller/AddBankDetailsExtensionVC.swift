//
//  AddBankDetailsExtensionVC.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 30/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension AddBankDetailsViewController{
    //MARK: - Add Stripe account API
    func makeTheServiceCallToSaveTheBank(){
        if pickedImage.image == nil{
            self.present(Helper.alertVC(title:"Message".localized, message:"Please Add the Photo ID".localized), animated: true, completion: nil)
        }else if (holderName.text?.isEmpty)!{
            self.present(Helper.alertVC(title:"Message".localized, message:"Please Add the account holder first name".localized), animated: true, completion: nil)
        }else if (lastName.text?.isEmpty)!{
            self.present(Helper.alertVC(title:"Message".localized, message:"Please Add the account holder last name".localized), animated: true, completion: nil)
        }
        else if(personalIDTF.text?.isEmpty)!{
            self.present(Helper.alertVC(title:"Message".localized , message:"Please Add the  unique identification number".localized), animated: true, completion: nil)
        }else if(addressTF.text?.isEmpty)!{
            self.present(Helper.alertVC(title:"Message".localized , message:"Please Add the address".localized), animated: true, completion: nil)
        }else if(cityTF.text?.isEmpty)!{
            self.present(Helper.alertVC(title:"Message".localized, message:"Please Add the city".localized), animated: true, completion: nil)
        }else if(stateTF.text?.isEmpty)!{
            self.present(Helper.alertVC(title:"Message".localized , message:"Please Add the state".localized), animated: true, completion: nil)
        }else if(posyalCodeTF.text?.isEmpty)!{
            self.present(Helper.alertVC(title:"Message".localized , message:"Please Add the Postal code".localized), animated: true, completion: nil)
        }else{
            self.sendRequestForSignup() //@Locate AddbankDetailsViewController
        }
    }
}
