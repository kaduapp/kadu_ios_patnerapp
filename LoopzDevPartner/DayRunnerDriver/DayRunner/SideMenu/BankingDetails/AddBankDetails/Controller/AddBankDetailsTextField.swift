//
//  AddBankDetailsTextField.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 30/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation


// MARK: - Textfield delegate method
extension AddBankDetailsViewController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //Format Date of Birth MM/DD/yyyy
        
        //initially identify your textfield
        
        if textField == DateOFB {
            
            // check the chars length dd -->2 at the same time calculate the dd-MM --> 5
            if (DateOFB?.text?.characters.count == 2) || (DateOFB?.text?.characters.count == 5) {
                //Handle backspace being pressed
                if !(string == "") {
                    // append the text
                    DateOFB?.text = (DateOFB?.text)! + "/"
                }
            }
            // check the condition not exceed 9 chars
            return !(textField.text!.characters.count > 9 && (string.characters.count ) > range.length)
        }
        else {
            return true
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {  //delegate method
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        switch textField {
        case holderName:
            lastName.becomeFirstResponder()
            break
        case lastName:
            DateOFB.becomeFirstResponder()
            break
        case DateOFB:
            personalIDTF.becomeFirstResponder()
            break
        case personalIDTF:
            addressTF.becomeFirstResponder()
            break
        case addressTF:
            cityTF.becomeFirstResponder()
            break
        case cityTF:
            stateTF.becomeFirstResponder()
            break
        case stateTF:
            posyalCodeTF.becomeFirstResponder()
            break
        default:
            dismisskeyBord()
            break
        }
        return true
    }
    
}

