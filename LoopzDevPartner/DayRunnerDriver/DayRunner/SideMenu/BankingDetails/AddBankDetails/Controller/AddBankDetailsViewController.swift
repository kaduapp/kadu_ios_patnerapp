//
//  AddBankDetailsViewController.swift
//  DayRunnerDriverDev
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
protocol stripeData:class {
    func stripeComing(value:Bool);
}
class AddBankDetailsViewController: UIViewController,UINavigationControllerDelegate {
    
    @IBOutlet var stateTF: UITextField!
    @IBOutlet var addressTF: UITextField!
    @IBOutlet var holderName: UITextField!
    @IBOutlet var DateOFB: UITextField!
    @IBOutlet var personalIDTF: UITextField!
    @IBOutlet var cityTF: UITextField!
    @IBOutlet var posyalCodeTF: UITextField!
    @IBOutlet var addPhotoID: UIImageView!
      weak var delegate: stripeData?
    @IBOutlet var lastName: UITextField!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var contentScrollView: UIView!
    
    @IBOutlet weak var buttonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var addOrVerifyStripe: UIButton!
    
    @IBOutlet weak var titleView: UILabel!
    var toVerify:Bool = false
    
    var stripeSavedDict:BankDetails?
    var addBankStripeModel = AddbankStripeModel()
    
    var activeTextField = UITextField()
    var pickedImage = UIImageView()
    var temp = [UploadImage]()
    var bankPhotoUrl = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // storeAccountType//
        
      
        pickedImage.image = nil
        if toVerify == true {
            titleView.text = "Verify Stripe Account".localized
            addOrVerifyStripe.setTitle("Verify Stripe".localized, for: .normal)
            DateOFB.text = stripeSavedDict?.dateOFBirth
            holderName.text = stripeSavedDict?.holderName
            lastName.text = stripeSavedDict?.lastName
            cityTF.text = stripeSavedDict?.city
            stateTF.text = stripeSavedDict?.state
            addressTF.text = stripeSavedDict?.address
            posyalCodeTF.text = stripeSavedDict?.postalCode
            
        }else{
            titleView.text = "Add Stripe Account".localized
            addOrVerifyStripe.setTitle("Add Stripe".localized, for: .normal)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func isEmptyLists(dict: [String: Any]) -> Bool {
        if dict.isEmpty {
            return false
        }else{
            return true
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        {
            self .moveViewUp(activeView: activeTextField, keyboardHeight: keyboardSize.height)
        }
    }
    @IBAction func tapGestureToDismissKeyBoard(_ sender: Any) {
        self.dismisskeyBord()
    }
    
    /// Move View Up When keyboard Appears
    ///
    /// - Parameters:
    ///   - activeView: View that has Became First Responder
    ///   - keyboardHeight: Keyboard Height
    func moveViewUp(activeView: UIView, keyboardHeight: CGFloat) {
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.buttonBottomConstraint.constant = keyboardHeight
                        self.view.layoutIfNeeded()
        })
        
//        // Get Max Y of Active View with respect their Super View
//        var viewMAX_Y = activeView.frame.maxY
//
//        // Check if SuperView of ActiveView lies in Nexted View context
//        // Get Max for every Super of ActiveTextField with respect to Views SuperView
//        if var view: UIView = activeView.superview {
//
//            // Check if Super view of View in Nested Context is View of ViewController
//            while (view != self.view.superview) {
//                viewMAX_Y += view.frame.minY
//                view = view.superview!
//            }
//        }
//
//        // Calculate the Remainder
//        let remainder = self.view.frame.height - (viewMAX_Y + keyboardHeight)
//
//        // If Remainder is Greater than 0 does mean, Active view is above the and enough to see
//        if (remainder >= 0) {
//            // Do nothing as Active View is above Keyboard
//        }
//        else {
//            // As Active view is behind keybard
//            // ScrollUp View calculated Upset
//            UIView.animate(withDuration: 0.4,
//                           animations: { () -> Void in
//                            self.mainScrollView.contentOffset = CGPoint(x: 0, y: -remainder)
//            })
//        }
        
        // Set ContentSize of ScrollView
//        var contentSizeOfContent: CGSize = self.contentScrollView.frame.size
//        contentSizeOfContent.height += keyboardHeight
////        self.mainScrollView.contentSize = contentSizeOfContent
//        self.mainScrollView.contentSize
    }
    
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveViewDown() {
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.buttonBottomConstraint.constant = 0
                        self.view.layoutIfNeeded()
        })
//        self.mainScrollView.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(0))
//        let contentSizeOfContent: CGSize = self.contentScrollView.frame.size
//        self.mainScrollView.contentSize = contentSizeOfContent
    }
    
    
    @IBAction func addPhotoID(_ sender: Any) {
        self.selectImage()
    }
    
    @IBAction func backButton(_ sender: Any) {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func saveTheBankDetails(_ sender: Any) {
        self.makeTheServiceCallToSaveTheBank()  //@locate AddbankDetailsExtensionVC
    }
    
    //AddStripe Account API
    func sendRequestForSignup() {
        let name = DateOFB.text!.components(separatedBy: "/")
        Helper.showPI(message: "loading..")
        var bankDetails = [String: Any]()
        var ipAddress = Helper.getIPAddresses()
        
        
        let addBanksParams = AddbankStripeModel.init(addBankCity: cityTF.text!, addBankLine1: addressTF.text!, addBankPostalCode: posyalCodeTF.text!, addBankState: stateTF.text!, addBankMonth: name[0], addBankDay: name[1], addBankYear: name[2], addBankFirstName: holderName.text!, addBankLastName: lastName.text!, addBankPersonalID: personalIDTF.text!, addBankIP: ipAddress[1], addBankDocument: bankPhotoUrl)
        addBankStripeModel.addStripeAccount(params: addBanksParams, completionHanlder: { success in
            if success{
                  self.delegate?.stripeComing(value: true)
                _ = self.navigationController?.popToRootViewController(animated: true)
            }else{
                
            }
        })
    }
    
    func uploadProfileimgToAmazon(){
        
        var url = String()
        
        temp = [UploadImage]()
        let timeStamp = Helper.currentTimeStamp
        url = AMAZONUPLOAD.DOCUMENTS + timeStamp + "_" + "bankPhotoID" + ".png"
        
        temp.append(UploadImage.init(image: addPhotoID.image!, path: url))
        
        let upMoadel = UploadImageModel.shared
        upMoadel.uploadImages = temp
        upMoadel.start()
        
        bankPhotoUrl = Utility.amazonUrl + AMAZONUPLOAD.DOCUMENTS + timeStamp +  "_" + "bankPhotoID" + ".png"
    }
    
    
    func dismisskeyBord(){
        self.view.endEditing(true)
    }
    
    /// MARK: - actionSheet
    func selectImage() {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select image".localized as String?,
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel".localized as String?,
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: "Gallery".localized as String?,
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: "Camera".localized as String?,
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    // MARK: - photo gallery
    func chooseFromPhotoGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    // MARK: - Camera selection
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.camera;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
        }
    }
}


// MARK: - ImagePicker delegate method
extension AddBankDetailsViewController : UIImagePickerControllerDelegate {
    
      func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
      if let image = info[.originalImage] as? UIImage{
          if let image = info[.originalImage] as? UIImage{
        addPhotoID.image = Helper.resizeImage(image: image, newWidth: 200)
        pickedImage.image = Helper.resizeImage(image: image, newWidth: 200)
        bankPhotoUrl = ""
        self.uploadProfileimgToAmazon()
        self.dismiss(animated: true, completion: nil)
    }
        }
    }
}

