//
//  AddBankViewExtension.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 30/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension AddBankViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        switch textField {
        case accountHolderName:
            accountNumber.becomeFirstResponder()
            break
        case accountNumber:
            routingNumber.becomeFirstResponder()
            break
        default:
            dismisskeyBord()
            break
        }
        return true
    }
    
}
