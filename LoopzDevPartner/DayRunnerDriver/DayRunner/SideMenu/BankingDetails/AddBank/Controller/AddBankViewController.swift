//
//  AddBankViewController.swift
//  DayRunnerDriverDev
//
//  Created by Vengababu Maparthi on 18/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

protocol FooTwoViewControllerDelegate:class {
    func addBank(value:Bool);
}

class AddBankViewController: UIViewController {
    @IBOutlet weak var routingNumber: UITextField!
    @IBOutlet weak var addAccountNumber: UIButton!
    @IBOutlet weak var accountNumber: UITextField!
    @IBOutlet weak var accountHolderName: UITextField!
    weak var delegate: FooTwoViewControllerDelegate?
    var addBankModel = AddBankModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToVc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func AddAccountAction(_ sender: Any) {
        addBankAccount()
    }
    
    func addBankAccount(){
        if (accountHolderName.text?.isEmpty)!{
            self.present(Helper.alertVC(title:"Message".localized, message:"Please Add the account holder  name".localized), animated: true, completion: nil)
        }
        else if(accountNumber.text?.isEmpty)!{
            self.present(Helper.alertVC(title:"Message".localized , message:"Please Add the Iban number".localized), animated: true, completion: nil)
        }
            //        else if(routingNumber.text?.isEmpty)!{
            //            self.present(Helper.alertVC(title:"Message".localized, message:"Please Add the routing number".localized), animated: true, completion: nil)
            //        }
        else{
            sendRequestForSignup()
        }
        
    }
    
    
    
    func sendRequestForSignup() {
        Helper.showPI(message: "loading..")
        var bankDetails = [String: Any]()
        
        let addAccount = AddBankModel.init(addBankAccountRoutingNumber: routingNumber.text! , addBankAccountAccountNumber: accountNumber.text!, addBankAccountAccountHolderName: accountHolderName.text!, addCurrency: UserDefaults.standard.object(forKey: USER_INFO.CURRENCYSYMBOL) as! String)
        addBankModel.addBankAccountAfterStripeVerified(params: addAccount, completionHanlder: { success in
            if success{
                self.delegate?.addBank(value: true)
                _ = self.navigationController?.popToRootViewController(animated: true)
            }else{
                
            }
        })
    }
    
    func dismisskeyBord(){
        self.view.endEditing(true)
    }
}



