//
//  SelectDefaultBank.swift
//  DayRunnerDriverDev
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
protocol UpdateBankDataDelegate:class
{
    func getCurrentBankDetails()
}

class SelectDefaultBank: UIView {
    open weak var delegate: UpdateBankDataDelegate?
    @IBOutlet var contentView: UIView!
    @IBOutlet var holderName: UILabel!
    @IBOutlet var accountNumber: UILabel!
    @IBOutlet var routingNumber: UILabel!
    
    var deleteBankModel = DeleteBankModel()
    var defaultBankModel = DefaultBank()
    var isShown:Bool = false
    private static var share: SelectDefaultBank? = nil
    var bankID = String()
    
    static var instance: SelectDefaultBank {
        if (share == nil) {
            share = Bundle(for: self).loadNibNamed("SelectDefaultBank",
                                                   owner: nil,
                                                   options: nil)?.first as? SelectDefaultBank
        }
        return share!
    }
    
    
    @IBAction func makeTheAccountDefault(_ sender: Any) {
        self.makeAccountDefault()
    }
    
    @IBAction func deleteTheAccount(_ sender: Any) {
        self.deleteTheAcount()
    }
    
    @IBAction func removeThePopup(_ sender: Any) {
        self.hide()
    }
    func makeAccountDefault(){
        Helper.showPI(message:"Loading..")
    
        var defaultBankAccount = DefaultBank.init(bankID: bankID)
        
        defaultBankModel.defaultBankAccount(params: defaultBankAccount, completionHanlder: { success in
            if success{
                self.hide()
                self.delegate?.getCurrentBankDetails()
            }else{
                
            }
        })
    }
    
    func deleteTheAcount(){
        Helper.showPI(message:"Loading..")
        let deleteAccount = DeleteBankModel.init(bankID: bankID)
        deleteBankModel.deleteBankAccount(params: deleteAccount, completionHanlder: { success in
            if success{
                self.hide()
                self.delegate?.getCurrentBankDetails()
            }else{
                
            }
            
        })
    }
    
    
    
    
    /// Show Network
    func show(bankData:BankAccountDataModel) {
    
        bankID = bankData.iD
        self.holderName.text = bankData.holderName
        self.accountNumber.text = bankData.accountNum
        self.routingNumber.text = bankData.routingNum
        
        
        if isShown == false {
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
            
            self.alpha = 0.0
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 1.0
            })
        }
    }
    
    /// Hide
    func hide() {
        
        if isShown == true {
            isShown = false
            
            UIView.animate(withDuration: 0.5,
                           animations: {
                            SelectDefaultBank.share = nil
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
        }
    }
    
}
