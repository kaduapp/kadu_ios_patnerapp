//
//  BankTableViewCell.swift
//  DayRunnerDriverDev
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class BankTableViewCell: UITableViewCell {
    
    @IBOutlet var topView: UIView!
    @IBOutlet var defaultButton: UIButton!
    @IBOutlet var acountNumber: UILabel!
    @IBOutlet var holderName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Helper.shadowView(sender: topView, width:UIScreen.main.bounds.size.width-30, height:topView.frame.size.height)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateBankAccount(bankData:BankAccountDataModel) {
        acountNumber.text = bankData.accountNum
        holderName.text = bankData.holderName
    }
}
