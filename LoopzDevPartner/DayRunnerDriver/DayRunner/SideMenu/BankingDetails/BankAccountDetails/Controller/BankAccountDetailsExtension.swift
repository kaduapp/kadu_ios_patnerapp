//
//  BankAccountDetailsExtension.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 30/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation


// //MARK: - TableView Datasource method

extension BankAccountDetailsVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if bankDate.bankAccountData.count != 0 && self.bankDate.address.length > 0 {
            footerView.isHidden = false
            noBankDetailsLabel.isHidden = true
            addStripeDetails.isHidden = true
            return 2
        }else if (self.bankDate.address.length > 0) {
            noBankDetailsLabel.isHidden = true
            addStripeDetails.isHidden = true
            if verifiedorNot == "verified".localized {
                footerView.isHidden = false
            }else{
                footerView.isHidden = true
            }
            return 1;
        }else{
            noBankDetailsLabel.isHidden = false
            addStripeDetails.isHidden = false
            footerView.isHidden = true
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
            
        case 0:
            return 2
        default:
            
            return bankDate.bankAccountData.count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let header = tableView.dequeueReusableCell(withIdentifier:"bankheader") as! BankHeaderTableViewCell!
                header?.bankHeaderLabel.text = "Stripe Account".localized
                return header!
                
            default:
                let stripe = tableView.dequeueReusableCell(withIdentifier:"stripe") as! StripeTableViewCell!
                
                if verifiedorNot == "verified".localized {
                    stripe?.stripeStatus.text = "verified".localized
                }else{
                    stripe?.stripeStatus.text = "Unverified".localized
                }
                stripe?.stripeAccountNumber.text = bankDate.holderName + " " + bankDate.lastName
                return stripe!
            }
            
        default:
            switch indexPath.row {
            case 0:
                let header = tableView.dequeueReusableCell(withIdentifier:"bankheader") as! BankHeaderTableViewCell!
                header?.bankHeaderLabel.text = "Bank Accounts".localized
                return header!
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier:"bankDetails") as! BankTableViewCell!
                
                cell?.updateBankAccount(bankData: bankDate.bankAccountData[indexPath.row - 1])
                
                if bankDate.bankAccountData[indexPath.row - 1].defaultBank == 1{
                    cell?.defaultButton.isSelected = true
                }else{
                    cell?.defaultButton.isSelected = false
                }
                return cell!
            }
        }
    }
}

//MARK: - TableView Delegate method

extension BankAccountDetailsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 1 {
            if verifiedorNot != "verified".localized {
                toVerify = true
                self.performSegue(withIdentifier: "toAddStripe", sender: nil)
            }
        }
        
        if indexPath.section == 1 && indexPath.row != 0 {
            let Vc:SelectDefaultBank = SelectDefaultBank.instance
            Vc.delegate = self
            Vc.show(bankData:bankDate.bankAccountData[indexPath.row - 1])
        }
        tableView.deselectRow(at: indexPath, animated: false)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0 :
            return 44
        case 1:
            return 110
        case 2:
            return 110
        default:
            return 110
        }
        
    }
}

