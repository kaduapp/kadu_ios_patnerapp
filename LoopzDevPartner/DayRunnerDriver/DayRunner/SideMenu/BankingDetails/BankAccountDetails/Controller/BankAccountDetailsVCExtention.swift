//
//  BankAccountDetailsVC.swift
//  DayRunnerDriverDev
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class BankAccountDetailsVC: UIViewController,FooTwoViewControllerDelegate,stripeData {
    
    @IBOutlet var bankDetailsTableView: UITableView!
    @IBOutlet var noBankDetailsLabel: UILabel!
    @IBOutlet weak var addStripeDetails: UIButton!
    var bankDate = BankDetails()
    var toVerify = false
    var verifyBank = false;
    var stripComing = false;
    @IBOutlet weak var bankDetailslabel: UILabel!
    @IBOutlet weak var addbankButton: UIButton!
    var verifiedorNot = String()
    var StripeHolderName = String()
    
    @IBOutlet weak var linkBankButton: UIButton!
    
    @IBOutlet weak var footerView: UIView!
    
    override func viewDidLoad() {
       
        super.viewDidLoad()
        stripComing = false;
        bankDetailslabel.text =  "Bank Details".localized
        noBankDetailsLabel.text = "No Stripe Account Details !!".localized
        addStripeDetails.setTitle("Add Stripe Account".localized, for: .normal)
        ManageBooking.sharedInstance.currentNavigationController = self.navigationController
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
        
        bankDetailsTableView.estimatedRowHeight = 10
        bankDetailsTableView.rowHeight = UITableView.automaticDimension
//        self.footerView.isHidden = true;
        self.linkBankButton.layer.borderColor =  UIColor.orange.cgColor
          self.getBankDate()
        // Do any additional setup after loading the view.
    }
    
    func addBank(value: Bool) {
        verifyBank = value
    }
    
    func stripeComing(value: Bool) {
        stripComing = value
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if verifyBank {
            self.getBankDate()
        }else {
            print("don't call API...")
        }
        
        if(stripComing) {
            self.getBankDate()
        }else {
            print("don't call API...")
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuButton(_ sender: Any) {
        if Utility.getSelectedLanguegeCode() == "ar" {
            slideMenuController()?.toggleRight()
            
        }else {
           slideMenuController()?.toggleLeft()
        }
    }
    
    @IBAction func addTheBankDetails(_ sender: Any) {
        performSegue(withIdentifier: "toAddBank", sender: nil)
    }
    
    @IBAction func addStripeDetailsAction(_ sender: Any) {
        toVerify = false
        performSegue(withIdentifier: "toAddStripe", sender: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "toAddStripe":
            if let addStripe = segue.destination as? AddBankDetailsViewController {
                addStripe.toVerify = toVerify
                addStripe.stripeSavedDict = bankDate
                addStripe.delegate = self
            }
            break
        case "toAddBank":
            if let addBank = segue.destination as? AddBankViewController {
                addBank.delegate = self
            }
            break
        default:
            break
        }
    }
    
    func getBankDate(){
        self.bankDate.bankAccountData.removeAll()
        bankDate.getBankDate(completionHandler: { bankResults in
            switch bankResults{
            case .success(let bankDetails):
                self.bankDate = bankDetails
                self.verifiedorNot = self.bankDate.verifiedRNot
                self.bankDetailsTableView.reloadData()
            case .failure(let error):
                print(error)
                break
            }
        })
    }
    
}


extension BankAccountDetailsVC : UpdateBankDataDelegate{
    func getCurrentBankDetails() {
        self.getBankDate()
    }
}
