//
//  BankData.swift
//  DayRunnerDriverDev
//
//  Created by Rahul Sharma on 14/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit
import RxSwift
enum bankResults {
    case success(BankDetails)
    case failure(Bool)
}

class BankDetails {
    var city        = ""
    var state       = ""
    var address     = ""
    var postalCode  = ""
    var dateOFBirth = ""
    var holderName  = ""
    var lastName    = ""
    var verifiedRNot = ""
    
    //    override init() {
    //        super.init()
    //    }
    let disposeBag = DisposeBag()
    var bankAccountData = [BankAccountDataModel]()
    
    func getBankDate(completionHandler:@escaping(bankResults) -> ()){
        let stripeDataModel = BankDetails()
        Helper.showPI(message:"Loading..")
        let bankingAPI =  BankingApi()
        bankingAPI.getBankAccounts()
        
        bankingAPI.subject_response.subscribe(onNext: { (response) in
            if (response["errNum"] != nil){
                let statCode:Int = response["errNum"] as! Int
                if statCode == 400 {
                    Helper.alertVC(errMSG:response["errMsg"] as! String)
                }
                completionHandler(.failure(false))
            }else{
                if let accountData = response["data"] as? [String:Any] {
                    if let stripeData = accountData["legal_entity"] as? [String : Any]{
                        
                        if let dict = stripeData["verification"] as? [String : Any]{
                            if let verifyRNot = dict["status"] as? String{
                                stripeDataModel.verifiedRNot = verifyRNot
                            }
                        }
                        
                        if let firstName = stripeData["first_name"] as? String{
                            stripeDataModel.holderName = firstName
                        }
                        
                        
                        
                        
                        if let latName = stripeData["last_name"] as? String{
                            stripeDataModel.lastName = latName
                        }
                        
                        if let dateofBirth = stripeData["dob"] as? [String:Any]{
                            stripeDataModel.dateOFBirth =  String(describing:dateofBirth["month"]!) + "/" +  String(describing:dateofBirth["day"]!)
                                + "/" + String(describing:dateofBirth["year"]!)
                        }
                        
                        if let addressDict =  stripeData["address"] as? [String:Any]{
                            stripeDataModel.city       = String(describing:addressDict["city"]!)
                            stripeDataModel.state       = String(describing:addressDict["state"]!)
                            stripeDataModel.address     = String(describing:addressDict["line1"]!)
                            stripeDataModel.postalCode = String(describing:addressDict["postal_code"]!)
                        }
                    }
                    
                    if let currency = accountData["default_currency"] as? String {
                        
                        UserDefaults.standard.set(currency, forKey: USER_INFO.CURRENCYSYMBOL)
                        
                        
                    }
                    
                    if  let dict = accountData["external_accounts"] as? [String : Any]{
                        if let data =  dict["data"] as? [[String:Any]]{
                            stripeDataModel.bankAccountData = self.handlingTheBankData(accounts: data)
                        }
                    }
                    
                    completionHandler(.success(stripeDataModel))
                }
            }
            
            
            
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
            }.disposed(by: disposeBag)
    }
    
    
    
    func handlingTheBankData(accounts:[[String:Any]]) -> [BankAccountDataModel]{
        
        for dict in accounts {
            let bankModel = BankAccountDataModel()
            
            if let routing = dict["routing_number"] as? String{
                bankModel.routingNum = routing
            }
            
            if let holderNam = dict["account_holder_name"] as? String{
                bankModel.holderName = holderNam
            }
            
            if let account = dict["last4"] as? String{
                bankModel.accountNum = "XXXX " +  account
            }
            
            if let defaultBank = dict["default_for_currency"] as? Int{
                bankModel.defaultBank = defaultBank
            }
            
            if let id = dict["id"] as? String{
                bankModel.iD = id
            }
            bankAccountData.append(bankModel)
        }
        
        return bankAccountData
    }
}


class BankAccountDataModel: NSObject {
    var iD  = ""
    var accountNum = ""
    var routingNum = ""
    var holderName = ""
    var defaultBank:Int = 0
}



class AddbankStripeModel: NSObject {
    
    override init() {
        super.init()
    }
    let disposeBag = DisposeBag()
    //and bankdetails
    var addBankCity: String = ""
    var addBankLine1: String = ""
    var addBankPostalCode: String = ""
    var addBankState: String = ""
    var addBankMonth: String = ""
    var addBankDay: String = ""
    var addBankYear: String = ""
    var addBankFirstName: String = ""
    var addBankLastName: String = ""
    var addBankPersonalID: String = ""
    var addBankIP: String = ""
    var addBankDate = Helper.currentDateTime
    
    var addBankCountry = UserDefaults.standard.value(forKey: "country") as? String
    
    var addBankDocument: String = ""
    
    init(addBankCity: String, addBankLine1: String, addBankPostalCode: String, addBankState: String, addBankMonth: String, addBankDay: String, addBankYear: String, addBankFirstName: String,addBankLastName: String, addBankPersonalID: String, addBankIP: String, addBankDocument: String)
    {
        self.addBankCity = addBankCity
        self.addBankLine1 = addBankLine1
        self.addBankPostalCode = addBankPostalCode
        self.addBankState = addBankState
        self.addBankMonth = addBankMonth
        self.addBankDay = addBankDay
        self.addBankYear = addBankYear
        self.addBankFirstName = addBankFirstName
        self.addBankLastName = addBankLastName
        self.addBankPersonalID = addBankPersonalID
        self.addBankIP = addBankIP
        self.addBankDocument = addBankDocument
    }
    
    func addStripeAccount(params:AddbankStripeModel,completionHanlder:@escaping(Bool)->()) {
        
        let addStripeAccount =  BankingApi()
        addStripeAccount.addBank(bankDetails: params)
        addStripeAccount.subject_response.subscribe(onNext: { (response) in
            Helper.hidePI()
            
            if let errFlag = response["errFlag"] as? Int  {
                if(errFlag == 1){
                    Helper.hidePI()
                    completionHanlder(true)
                }
            } else {
                Helper.hidePI()
                completionHanlder(false)
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
            }.disposed(by: disposeBag)
    }
}



class AddBankModel: NSObject {
    
    override init() {
        super.init()
    }
    let disposeBag = DisposeBag()
    var addBankAccountCountry = UserDefaults.standard.value(forKey: "country") as? String
    var addBankAccountRoutingNumber: String = ""
    var addBankAccountAccountNumber: String = ""
    var addBankAccountAccountHolderName: String = ""
    
    init(addBankAccountRoutingNumber: String, addBankAccountAccountNumber: String, addBankAccountAccountHolderName: String,addCurrency: String)
    {
        self.addBankAccountRoutingNumber = addBankAccountRoutingNumber
        self.addBankAccountAccountNumber = addBankAccountAccountNumber
        self.addBankAccountAccountHolderName = addBankAccountAccountHolderName
        self.addBankAccountCountry = UserDefaults.standard.value(forKey: "country") as? String
    }
    
    
    
    func addBankAccountAfterStripeVerified(params:AddBankModel,completionHanlder:@escaping(Bool)->()) {
        
        let addBankAccount =  BankingApi()
        addBankAccount.addAccount(accountDetails: params)
        addBankAccount.subject_response.subscribe(onNext: { (response) in
            completionHanlder(true)
        }, onError: { (error) in
            Helper.hidePI()
            completionHanlder(false)
        }, onCompleted: {
            
        }) {
            
            }.disposed(by: disposeBag)
        
        
        //        NetworkHelper.requestPOST(serviceName:API.METHOD.ADDBANKAFTERSTRIPE,
        //                                  params: params,
        //                                  success: { (response : [String : Any]) in
        //                                    print("Bankdetails response : ",response)
        //                                    Helper.hidePI()
        //                                    if response.isEmpty == false {
        //                                        if (response["statusCode"] != nil){
        //                                            let statCode:Int = response["statusCode"] as! Int
        //                                            if statCode == 401 {
        //                                                Helper.hidePI()
        //                                                Session.expired()
        //                                                Helper.alertVC(errMSG: "Your Session has beed expired")
        //
        //                                            }else
        //                                            {
        //                                                Helper.alertVC(errMSG: "bad request or  internal server error")
        //                                                Helper.hidePI()
        //                                            }
        //                                            completionHanlder(false)
        //                                        }else{
        //                                            let flag:Int = response["errFlag"] as! Int
        //
        //                                            if flag == 0{
        //                                                Helper.hidePI()
        //                                                Helper.alertVC(errMSG: response["errMsg"] as! String)
        //                                                completionHanlder(true)
        //
        //                                            }else{
        //                                                Helper.hidePI()
        //                                                completionHanlder(false)
        //                                                Helper.alertVC(errMSG: response["errMsg"] as! String)
        //                                            }
        //                                        }
        //                                    } else {
        //
        //                                    }
        //
        //        }) { (Error) in
        //            Helper.hidePI()
        //            completionHanlder(false)
        //            Helper.alertVC(errMSG: Error.localizedDescription)
        //        }
    }
}




class DeleteBankModel: NSObject {
    let disposeBag = DisposeBag()
    var bankID: String!
    override init() {
        super.init()
    }
    
    init(bankID: String)
    {
        self.bankID = bankID
    }
    
    func deleteBankAccount(params:DeleteBankModel,completionHanlder:@escaping(Bool)->()) {
        
        let deleteAccount =  BankingApi()
        deleteAccount.deleteBankAccount(deleteBank: params)
        deleteAccount.subject_response.subscribe(onNext: { (response) in
            print("deleteCard Response response : ",response)
            Helper.hidePI()
            if response.isEmpty == false {
                Helper.alertVC(errMSG: response["errMsg"] as! String)
                completionHanlder(true)
            } else {
                completionHanlder(false)
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
            }.disposed(by: disposeBag)
        
        
        //        NetworkHelper.requestDELETEURL(method:API.METHOD.DELETEBANK,
        //                                       params: params,
        //                                       success: { (response : [String : Any]) in
        //                                        print("deleteCard Response response : ",response)
        //                                        Helper.hidePI()
        //                                        if response.isEmpty == false {
        //                                            Helper.alertVC(errMSG: response["errMsg"] as! String)
        //                                            completionHanlder(true)
        //                                        } else {
        //                                            completionHanlder(false)
        //                                        }
        //
        //        })
        //        { (Error) in
        //            Helper.hidePI()
        //            completionHanlder(false)
        //            Helper.alertVC(errMSG: Error.localizedDescription)
        //
        //        }
    }
}


class DefaultBank:NSObject{
    
    let disposeBag = DisposeBag()
    var bankID: String!
    override init() {
        super.init()
    }
    
    init(bankID: String)
    {
        self.bankID = bankID
    }
    
    func defaultBankAccount(params:DefaultBank,completionHanlder:@escaping(Bool)->()) {
        
        let defaultBank =  BankingApi()
        defaultBank.makeDefaultBankAccount(defaultBank: params)
        defaultBank.subject_response.subscribe(onNext: { (response) in
            print("UpdateDriverStatus response : ",response)
            Helper.hidePI()
            if response.isEmpty == false {
                //                Helper.alertVC(errMSG: response["errMsg"] as! String)
                completionHanlder(true)
            } else {
                completionHanlder(false)
            }
            
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
            }.disposed(by: disposeBag)
    }
}










