//
//  ProfileModel.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift


enum ProfileResults {
    case success([String:Any])
    case failure(Bool)
}

class ProfileModel: NSObject {
    
    override init() {
        super.init()
    }
    var disposeBag = DisposeBag()
    //MARK: - Get profile data API
    func profileDetails(completionHandler:@escaping(ProfileResults) ->()){
        Helper.showPI(message:"Loading..")
        let profileData =  ProfileApi()
        profileData.getProfileData()
        
        profileData.subject_response.subscribe(onNext: { (response) in
            if let data =  response["data"] as? [String:Any]{
                completionHandler(.success(data))
            }
                
            else {
                completionHandler(.failure(false))
            }
            
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
        
    }
    
    //MARK: -Logout API Manually
    func logoutFromApp() {
        Helper.showPI(message:"Logging Out..")
        
        //
        //        let params : [String : Any]? =  nil
        let logoutApi =  APIs()
        logoutApi.logoutApi()
        
        logoutApi.subject_response.subscribe(onNext: { (response) in
            Session.expired()
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
    }
    
    //profile ImageVar
    var profileImageURL: String = ""
    init(profileImageURL: String)
    {
        self.profileImageURL = profileImageURL
    }
    
    //This Func update profile pic to server.
    func updateProfileImage(params:ProfileModel,completionHanler:@escaping(Bool) -> ()) {
        
        let updatePic =  ProfileApi()
        updatePic.updateProfilePic(newPic: params)
        updatePic.subject_response.subscribe(onNext: { (response) in
            Helper.hidePI()
            if response.isEmpty == false {
                completionHanler(true)
                
            } else {
                completionHanler(false)
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
    }
}

