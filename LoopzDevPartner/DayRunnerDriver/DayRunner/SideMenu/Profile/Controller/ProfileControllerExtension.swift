//
//  ProfileControllerExtension.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import Kingfisher
//Handling the  response
extension ProfileViewController{
    
    //MARK: - Get profile data API
    func profileDetails(){
        Helper.showPI(message:"Loading..")
        if isCameraOpened == false {
            
            profileMdl.profileDetails(completionHandler: { ProfileResults in
                switch ProfileResults{
                case .success(let profileData):
                    self.handlingTheProfileData(data: profileData)
                    break
                case .failure(let error):
                    print(error)
                    break
                    
                }
            })
        }
    }
    
    
    //MARK: - Handling The Profile Data
    func handlingTheProfileData(data:[String:Any]){
        let ud = UserDefaults.standard
        self.firstName.text = data["Name"] as! String
        if let phoneNumber = data["phone"] as? String{
            self.mobileNumber.text = phoneNumber
            ud.set(phoneNumber, forKey: USER_INFO.SAVEDID)
        }
        
        if let vehicleType = data["vehicleTypeName"] as? String {
            self.vehicleType.text = vehicleType
        }
        
        
        
        if let platNo = data["email"] as? String {
            self.vehicleNumber.text = platNo
        }
        if let accountType = data["driverType"] as? Int {
 
            if accountType == 2 {
                if let storeName = data["storeName"] as? String {
                    storeLabel.text = "STORE"
                    self.planTF.text = storeName
                }
            } else if accountType == 1 {
                storeLabel.text = "COMMISSION PLAN".localized
                self.planTF.text = data["planName"] as! String
            }
        }
        self.email = data["email"] as! String
        let imageURL = data["profilePic"] as! String
        self.profileImage.kf.setImage(with: URL(string: imageURL),
                                      placeholder:UIImage.init(named: "profile_defaultimage"),
                                      options: [.transition(ImageTransition.fade(1))],
                                      progressBlock: { receivedSize, totalSize in
        },
                                      completionHandler: { image, error, cacheType, imageURL in
        })
        
        let image = data["profilePic"] as! String
        let name = data["Name"] as! String
        
        ud.set(image, forKey:  USER_INFO.USERIMAGE)
        ud.set(name, forKey:  USER_INFO.USER_NAME)
        ud.synchronize()
    }
}
