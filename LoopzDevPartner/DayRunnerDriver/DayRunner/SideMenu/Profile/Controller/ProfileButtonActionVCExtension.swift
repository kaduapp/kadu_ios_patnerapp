//
//  ProfileButtonAction.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 29/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension ProfileViewController{
    
    //MARK: - dismissViewAction
    @IBAction func tapGestureAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    //MARK: - change name action
    @IBAction func changeName(_ sender: Any) {
        
        performSegue(withIdentifier: "changeName", sender: nil)
    }
    
    //MARK: - change number action
    @IBAction func changePhoneNumber(_ sender: Any) {
        
        performSegue(withIdentifier: "changePhone", sender: nil)
    }
    
    //MARK: - change password action
    @IBAction func changePassword(_ sender: Any) {
        CheckOldPassword.instance.show()
        CheckOldPassword.instance.delegate = self
        
    }
    
    //MARK: - change profile image action
    @IBAction func changeProfilePic(_ sender: Any) {
        selectImage()
    }
    
    //MARK: - slider menu View action
    @IBAction func menuAction(_ sender: Any) {
        if Utility.getSelectedLanguegeCode() == "ar" {
            slideMenuController()?.toggleRight()
            
        }else {
            slideMenuController()?.toggleLeft()
        }
    }

}
