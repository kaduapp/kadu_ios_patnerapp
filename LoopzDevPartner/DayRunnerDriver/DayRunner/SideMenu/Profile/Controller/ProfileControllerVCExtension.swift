//
//  ProfileControllerExtn.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation


extension ProfileViewController{
    // MARK: - Selet profile Image
    func selectImage() {
        isCameraOpened = true
        if profilePicUrl == ""{
            //Create the AlertController and add Its action like button in Actionsheet
            let actionSheetController: UIAlertController = UIAlertController(title: "Please Select Image".localized,
                                                                             message: "",
                                                                             preferredStyle: .actionSheet)
            
            let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel".localized,
                                                                  style: .cancel) { action -> Void in
            }
            actionSheetController.addAction(cancelActionButton)
            
            let saveActionButton: UIAlertAction = UIAlertAction(title: "Gallery".localized,
                                                                style: .default) { action -> Void in
                                                                    self.chooseFromPhotoGallery()
            }
            actionSheetController.addAction(saveActionButton)
            
            let deleteActionButton: UIAlertAction = UIAlertAction(title: "Camera".localized,
                                                                  style: .default) { action -> Void in
                                                                    self.chooseFromCamera()
            }
            actionSheetController.addAction(deleteActionButton)
            self.present(actionSheetController, animated: true, completion: nil)
        }else{
            let actionSheetController: UIAlertController = UIAlertController(title: "Please Select Image".localized,
                                                                             message: "",
                                                                             preferredStyle: .actionSheet)
            
            let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel".localized,
                                                                  style: .cancel) { action -> Void in
            }
            actionSheetController.addAction(cancelActionButton)
            
            let saveActionButton: UIAlertAction = UIAlertAction(title: "Gallery".localized,
                                                                style: .default) { action -> Void in
                                                                    self.chooseFromPhotoGallery()
            }
            actionSheetController.addAction(saveActionButton)
            
            let deleteActionButton: UIAlertAction = UIAlertAction(title: "Camera".localized,
                                                                  style: .default) { action -> Void in
                                                                    self.chooseFromCamera()
            }
            actionSheetController.addAction(deleteActionButton)
            let removeActionButton: UIAlertAction = UIAlertAction(title: "Remove image".localized,
                                                                  style: .default) { action -> Void in
                                                                    self.removeProfilePic()
            }
            actionSheetController.addAction(removeActionButton)
            self.present(actionSheetController, animated: true, completion: nil)
            
        }
    }
    
    //MARK: - Remove profile pic
    func removeProfilePic(){
        profileImage.image = UIImage.init(named: "profile_defaultimage")
        profilePicUrl = ""
    }
    
    //MARK: - open Photo Gallary
    func chooseFromPhotoGallery()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    //MARK: - Open Camera
    func chooseFromCamera()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.camera;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
}

//MARK: - Imagepicker delegate
extension ProfileViewController:UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
          if let image = info[.originalImage] as? UIImage{
        
        if(profileImage.image?.isEqual(Helper.resizeImage(image: image, newWidth: 200)))! {
            print("Repeated")
            return
        }
        
        profileImage.image = Helper.resizeImage(image: image, newWidth: 200)
        self.uploadProfileimgToAmazon()
        profilePicUrl = "profile_profile_default_image"
        self.dismiss(animated: true, completion: nil)
    }
    }
}
