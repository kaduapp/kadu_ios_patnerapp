//
//  ProfileViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher


class ProfileViewController: UIViewController ,UINavigationControllerDelegate {
    var profilePicUrl = String()
    var activeTextField = UITextField()
    
    @IBOutlet var passwordTF: UITextField!
    @IBOutlet var contentSrollView: UIView!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var profileTopLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet var vehicleNumber: UITextField!
    @IBOutlet var vehicleType: UITextField!
    @IBOutlet var mobileNumber: UITextField!
    @IBOutlet var firstName: UITextField!
    @IBOutlet var topView: UIView!
    @IBOutlet var planTF: UITextField!
    @IBOutlet weak var storeLabel: UILabel!
    
    @IBOutlet weak var logoutbtn: UIButton!
    var email = String()
    var isCameraOpened = false
    
    var profileMdl = ProfileModel()
    
    @IBOutlet var profileImage: UIImageView!
    var profileUrl = String()
    
    var temp = [UploadImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ManageBooking.sharedInstance.currentNavigationController = self.navigationController
        topView = Helper.setViewBackgroundGradient(sender: topView, Helper.UIColorFromRGB(rgbValue: 0x3EBA74), Helper.UIColorFromRGB(rgbValue: 0x17CA9C))
        profilePicUrl = ""
        passwordLabel.text = "PASSWORD*".localized
        emailLabel.text = "EMAIL*".localized
        firstNameLabel.text =  "FIRST NAME".localized
        phoneLabel.text = "PHONE NUMBER*".localized
        logoutbtn.setTitle("LOGOUT".localized, for: .normal)
        //
        //COMMISSION PLAN
        self.profileTopLabel.text = "Profile".localized
        storeLabel.text = "COMMISSION PLAN".localized
        makeTextfieldsDisable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.profileDetails()
        let defaults = UserDefaults.standard
        defaults.set(Utility.checkOldPassword, forKey: USER_INFO.SAVEDPASSWORD)
        passwordTF.text = Utility.checkOldPassword
        self.navigationController?.isNavigationBarHidden = true
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissView(){
        self.view.endEditing(true)
    }
    
    
    //MARK: - Removed the user interation to all textfields
    func makeTextfieldsDisable(){
        firstName.isUserInteractionEnabled = false
        mobileNumber.isUserInteractionEnabled = false
        vehicleType.isUserInteractionEnabled = false
        vehicleNumber.isUserInteractionEnabled = false
        passwordTF.isUserInteractionEnabled = false
        planTF.isUserInteractionEnabled = false
    }

    
    //MARK: - Storing updated profileImage to Amazon
    func uploadProfileimgToAmazon(){
        
        var url = ""
        
        temp = [UploadImage]()
        let timeStamp  = Helper.currentTimeStamp
        
        url =  AMAZONUPLOAD.PROFILEIMAGE +  timeStamp + "_0_01" + ".png"
        
        temp.append(UploadImage.init(image: profileImage.image!, path: url))
        
        let upMoadel = UploadImageModel.shared
        upMoadel.uploadImages = temp
        upMoadel.start()
        
        Helper.showPI(message:"Loading..")
        profileUrl = Utility.amazonUrl  + AMAZONUPLOAD.PROFILEIMAGE + timeStamp + "_0_01" + ".png"
        self.updateimage(proUrl: profileUrl)
        let ud = UserDefaults.standard
        ud.set(profileUrl, forKey:  USER_INFO.USERIMAGE)
        ud.synchronize()
    }
    
    @IBAction func emailBtnAction(_ sender: UIButton) {
        
         performSegue(withIdentifier: "changeEmail", sender: nil)
    }
    
    
    //MARK: - Update profileImage
    func updateimage(proUrl:String){

        let params = ProfileModel.init(profileImageURL: proUrl)
        profileMdl.updateProfileImage(params: params, completionHanler: { success in
            if success{
                self.isCameraOpened = false
            }
        })
        
    }
    
    
    @IBAction func logoutAction(_ sender: UIButton) {
        profileMdl.logoutFromApp()
    }
}


extension ProfileViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        dismissView()
        return true
    }
}

//MARK: - ChangePassword delegate
extension ProfileViewController:CheckPasswordDelegate{
    func AuthorisedToChangeThePassword() {
        performSegue(withIdentifier: "toChangePassword", sender: nil)
    }
}

