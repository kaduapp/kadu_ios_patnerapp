//
//  ChangeNumberViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 17/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ChangeNumberViewController: UIViewController {
    
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    @IBOutlet var countryImage: UIImageView!
    @IBOutlet var countryCode: UILabel!
    @IBOutlet var buttonContraint: NSLayoutConstraint!
    @IBOutlet var phoneTF: UITextField!
    @IBOutlet weak var changeNumberButton: UIButton!
    var verifyNumberModel =  profileUpdateModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
        self.setCountryCode()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    

    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(true)
        DispatchQueue.main.async {
            self.phoneTF.becomeFirstResponder()
        }
        
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    
    //MARK: - Keyboard Methods
    @objc func keyboardWillShow(notification: NSNotification) {
        tapGesture.isEnabled = true
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        {
            self.moveViewUp(keyboardHeight: keyboardSize.height)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        tapGesture.isEnabled = false

        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
    }
    
    //Pick country code segue
    @IBAction func pickTheCountryCode(_ sender: Any) {
        performSegue(withIdentifier:"pickCountryCodeFromFP", sender: nil)
    }
    
    //Verify mobile number action
    @IBAction func verifyOTP(_ sender: Any) {
        self.moveViewDown()
        self.view.endEditing(true)
        if (phoneTF.text?.isEmpty)! {
            self.present(Helper.alertVC(title: "Message".localized, message:"Please enter the number"), animated: true, completion: nil)
        } else {
             //1:Slave 2:Master
            let updatePhoneNumber = profileUpdateModel.init(updatePhoneNumber:phoneTF.text!, countryCode: countryCode.text!)
            verifyNumberModel.updateNewPhoneNumber(params: updatePhoneNumber, completionHandler: { success in
                if success{
                    self.performSegue(withIdentifier:"toVerifyTheNumber", sender: nil)
                }else{
                    
                }
            })
        }
    }
    
    // MARK: - setCountrycode
    func setCountryCode(){
       
        let picker = CountryPicker.dialCode(code: "CO")
        countryCode.text = picker.dialCode
       
    }
    
    
    //MARK: - back to profile vc
    @IBAction func backToVC(_ sender: Any) {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: - Hide keyboard action
    @IBAction func hideKeyboard(_ sender: Any) {
        self.view.endEditing(true)
        validateEmailPhone(typeValidate: "1")
        
    }
    
    ///************ segue identifier method****************//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "pickCountryCodeFromFP"
        {
            let nav = segue.destination as! UINavigationController
            if let picker: VNHCountryPicker = nav.viewControllers.first as! VNHCountryPicker?
            {
                picker.delegate = self
            }
        }else if segue.identifier == "toVerifyTheNumber" {
            let nextScene = segue.destination as? VerifyOTPVC
            nextScene?.mobileNumber = phoneTF.text!
            nextScene?.countryCode = countryCode.text!
            nextScene?.defineTheOtp = 3
        }
    }
    

    
    func moveViewUp(keyboardHeight: CGFloat) {
        self.phoneTF.becomeFirstResponder()
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.buttonContraint.constant = keyboardHeight
                        self.view.layoutIfNeeded()
        })
    }
    
    
    
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveViewDown() {
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.buttonContraint.constant = 0
                        self.view.layoutIfNeeded()
        })
    }
    
    
    
}

extension ChangeNumberViewController:VNHCountryPickerDelegate{
    // MARK: - country delegate method
    
    func didPick(country: VNHCounty) {
        countryCode.text = country.dialCode
    }
//    internal func didPickedCountry(country: Country)
//    {
//        countryCode.text = country.dialCode
//        countryImage.image = country.flag
//    }
}

extension ChangeNumberViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        self.validateEmailPhone(typeValidate:"1")
    }
    //MARK: - Validate email and phone
    func validateEmailPhone(typeValidate:String) {
        
        if (phoneTF.text?.count)! > 6 && (phoneTF.text?.count)! < 16 {
            
            var validate: AuthenticationModel
            let signupModel = SignupModel()
            Helper.showPI(message:"Verifying the MobileNumber".localized)
            validate = AuthenticationModel.init(verifal: phoneTF.text!, validateType: typeValidate, countryCode: countryCode.text!)
            
            //@locate signupModel
            signupModel.validateTheEmailIDPhone(params: validate, completionHandler: { success in
                if success{
                    self.changeNumberButton.isEnabled = true
                    self.changeNumberButton.backgroundColor = Helper.getUIColor(color: Colors.AppBaseColor)
                }else{
                    self.changeNumberButton.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0x6f6f6f)
                    self.changeNumberButton.isEnabled  = false
                    self.phoneTF.becomeFirstResponder()
                }
            })
        } else {
            Helper.alertVC(errMSG: "Please enter a valid phone number")
        }

    }

    
}


