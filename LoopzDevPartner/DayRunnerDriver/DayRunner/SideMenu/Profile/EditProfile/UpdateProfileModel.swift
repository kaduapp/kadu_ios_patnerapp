//
//  UpdateProfileModel.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift
class profileUpdateModel: NSObject {
      let disposeBag = DisposeBag()
    override init()
    {
        super.init()
    }
    //updateName
    var updateProfileName: String = ""
    init(updateProfileName: String)
    {
        self.updateProfileName = updateProfileName
    }
    
    //updatePhoneNumber
    var updatePhoneNumber: String = ""
    var updatePhoneUserType: String = ""
    var countryCode: String = ""
    init(updatePhoneNumber: String)
    {
         self.updatePhoneNumber = updatePhoneNumber
    }
    init(updatePhoneNumber: String, countryCode: String)
    {
        self.updatePhoneNumber = updatePhoneNumber
        self.countryCode = countryCode
    }
    
    //MARK: - update name 
    func updateName(params:profileUpdateModel,completionHandler:@escaping (Bool) -> ()) {
        Helper.showPI(message:"Loading..")
        
        let updateName  =  ProfileApi()
        updateName.updateName(updatedName: params)
        updateName.subject_response.subscribe(onNext: { (response) in
            completionHandler(true)
        }, onError: { (error) in
            Helper.hidePI()
            completionHandler(false)
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
    }
    
    //MARK: get otp from new number and verify
    func updateNewPhoneNumber(params:profileUpdateModel,completionHandler:@escaping (Bool) -> ()) {
        Helper.showPI(message:"Signing up..")
        
        let updateNumber =  ProfileApi()
        updateNumber.updateNumber(number: params)
        updateNumber.subject_response.subscribe(onNext: { (response) in
            if response.isEmpty == false {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
    }
}
