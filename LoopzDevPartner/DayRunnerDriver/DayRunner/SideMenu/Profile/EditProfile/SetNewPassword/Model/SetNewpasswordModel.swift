//
//  ChangePasswordModel.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift

//MARK: - Create new password when u forgot and
class SetNewPassword: NSObject {
    
    override init() {
        super.init()
    }
    
    var secureNumber: Int = 0
    var password: String = ""
    var userType = 2
    var mobileNumber: String = ""
    var countryCode: String = ""
      let disposeBag = DisposeBag()
    
    init(secureNumber: String, password: String, mobileNumber: String, countryCode: String) {
        self.secureNumber = Int(secureNumber)!
        self.password = password
        self.mobileNumber = mobileNumber
        self.countryCode = countryCode
    }
    
    
    
    func updateNewPassword(params:SetNewPassword,completionHandler:@escaping(Bool) ->()){
        let updatePassword =  AuthenticationApis()
        updatePassword.updatePassword(password: params)
        updatePassword.subject_response.subscribe(onNext: { (response) in
            completionHandler(true)
        }, onError: { (error) in
            Helper.hidePI()
            completionHandler(false)
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
        
    }
}
