//
//  SetNewpasswordViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 19/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

//MARK: - controller to create newpassword, when u forgotpassword

class SetNewpasswordViewController: UIViewController {
    
    @IBOutlet var contentScrollView: UIView!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var bottomContraint: NSLayoutConstraint!
    @IBOutlet var confirmPassword: HoshiTextField!
    @IBOutlet var newPassword: HoshiTextField!
    var activeTextField = UITextField()
    var secureOtp :String = ""
    var mobileNumber :String = ""
    var countryCode :String = ""
    
    @IBOutlet weak var thankYouLabel: UILabel!
    @IBOutlet weak var changepasswordTopLabel: UILabel!
    var setNewpasswordModl = SetNewPassword()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changepasswordTopLabel.text = "Change Password".localized
        thankYouLabel.text = "Change Password".localized
        newPassword.placeholder =  "NEW PASSWORD".localized
        confirmPassword.placeholder = "RE-ENTER PASSWORD".localized
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        newPassword.becomeFirstResponder()
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    
    //MARK: - Keyboard Methods
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        {
            self.moveViewUp(keyboardHeight: keyboardSize.height)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
    }
    
    //Create new passwrod using verification
    func setNewpassword() {
        
        
        Helper.showPI(message:"")
        
        let updatePassword = SetNewPassword.init(secureNumber: secureOtp, password: newPassword.text!, mobileNumber: mobileNumber, countryCode: countryCode)
        setNewpasswordModl.updateNewPassword(params: updatePassword, completionHandler: { success in
            if success{
                Helper.alertVC(errMSG: "Your password has been successfully updated.")
                let defaults = UserDefaults.standard
                defaults.set(self.newPassword.text! , forKey: USER_INFO.OLDPASSWORD)
                defaults.synchronize()
                _ = self.navigationController?.popToRootViewController(animated: true)
            }else{
                
            }
        })
    }
    
    
    //********* Hide the keyboard********//
    @IBAction func hideKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    //*********back to root vc*************//
    @IBAction func backToVC(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func saveThePAssword(_ sender: Any) {
        self.changePassword()
    }
    
    func changePassword(){
        if (newPassword.text?.isEmpty)! {
            self.present(Helper.alertVC(title: "Message".localized, message:passwordViewControl.newpassword), animated: true, completion: nil)
        }else if (confirmPassword.text?.isEmpty)!{
            self.present(Helper.alertVC(title: "Message".localized, message:passwordViewControl.reenterPassword), animated: true, completion: nil)
        }else if newPassword.text! != confirmPassword.text!{
            self.present(Helper.alertVC(title: "Message".localized, message:passwordViewControl.mismatchpassword), animated: true, completion: nil)
        }else{
            self.setNewpassword()
        }
    }
    
    func moveViewUp(keyboardHeight: CGFloat) {
        newPassword.becomeFirstResponder()
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.bottomContraint.constant = keyboardHeight
                        self.view.layoutIfNeeded()
        })
    }
    
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveViewDown() {
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.bottomContraint.constant = 0
                        self.view.layoutIfNeeded()
        })
    }
    
    func dismisskeyBord(){
        self.view.endEditing(true)
    }
    
}

// MARK: - Textfield delegate method
extension SetNewpasswordViewController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        switch textField {
        case newPassword:
            confirmPassword.becomeFirstResponder()
            break
        case confirmPassword:
            dismisskeyBord()
            break
        default:
            self.changePassword()
            dismisskeyBord()
            break
        }
        
        return true
    }
}

