//
//  EditProfileController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 20/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
//MARK: - change name controller

class EditProfileController: UIViewController {
    
    @IBOutlet weak var buttonBottomConst: NSLayoutConstraint!
    var name : NSString = ""
    var text : NSString = ""
    var activeTextField = UITextField()
    var updateProModel = profileUpdateModel()
    
    @IBOutlet weak var changenameTopLabel: UILabel!
    @IBOutlet weak var textfield: HoshiTextField!
    
    @IBOutlet weak var changeYourName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        changenameTopLabel.text = "Change Name".localized
        changeYourName.text = "CHANGE YOUR NAME".localized
        textfield.placeholder = "Enter Name".localized
        if Utility.getSelectedLanguegeCode() == "ar" {
            self.textfield.textAlignment = .right
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        DispatchQueue.main.async {
          self.textfield.becomeFirstResponder()
        }
       
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
 
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }

    
    @IBAction func saveName(_ sender: Any) {
        self.moveViewDown()
        if (textfield.text?.isEmpty)! {
            self.present(Helper.alertVC(title: "Message".localized, message:"Field shouldn't be empty"), animated: true, completion: nil)
            
        }else{
            
            let updateName = profileUpdateModel.init(updateProfileName: textfield.text!)
            updateProModel.updateName(params: updateName, completionHandler: {success in
                if success{
                    self.dismiss(animated: true, completion: nil)
                }else{
                    
                }
            })
        }
    }
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Keyboard Methods
    @objc func keyboardWillShow(notification: NSNotification) {
        UIView.animate(withDuration: 0.4, animations: {() -> Void in
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                self.buttonBottomConst.constant = keyboardSize.height
            }
        })
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.moveViewDown()
    }
    
    //MARK:-Move View Up
    func moveViewUp(keyboardHeight: CGFloat) {
        self.activeTextField.becomeFirstResponder()
//        UIView.animate(withDuration: 0.4,
//                       animations: { () -> Void in
//                        self.buttonBottomConst.constant = keyboardHeight
//                        self.view.layoutIfNeeded()
//        })
    }
    
    
    //MARK:- Move View Down
    func moveViewDown() {
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.buttonBottomConst.constant = 0
                        self.view.layoutIfNeeded()
        })
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}

