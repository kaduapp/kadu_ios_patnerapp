//
//  ChangePasswordModel.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 28/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxSwift

//MARK: - update new password from profile
class NewPasswordModel: NSObject {
    
    override init() {
        super.init()
    }
      let disposeBag = DisposeBag()
    var newPassword: String!
    init(newPassword: String)
    {
        self.newPassword = newPassword
    }
    
    func updateNewPassword(params:NewPasswordModel,completionHandler:@escaping(Bool) ->()){
        let updatePassword =  ProfileApi()
        updatePassword.changePassword(newPassword: params)
        updatePassword.subject_response.subscribe(onNext: { (response) in
            completionHandler(true)
        }, onError: { (error) in
            Helper.hidePI()
            completionHandler(false)
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)

    }
}
