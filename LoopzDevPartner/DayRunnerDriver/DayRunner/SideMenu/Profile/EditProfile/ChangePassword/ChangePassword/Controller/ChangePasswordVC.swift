//
//  ChangePasswordVC.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 19/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {
    
    @IBOutlet var reEnterPassword: HoshiTextField!
    @IBOutlet var newPassword: HoshiTextField!
    
    var newPasswordModel = NewPasswordModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //************* hide the keyboard************//
    @IBAction func hideKeyboard(_ sender: Any) {
        self.view.endEditing(true)
        
    }
    
    
    ///Save the new password*******//
    @IBAction func saveTheNewpassword(_ sender: Any) {
        if (newPassword.text?.isEmpty)! {
            self.present(Helper.alertVC(title: "Message".localized, message:passwordViewControl.newpassword), animated: true, completion: nil)
        }else if (reEnterPassword.text?.isEmpty)! {
            self.present(Helper.alertVC(title: "Message".localized, message:passwordViewControl.reenterPassword), animated: true, completion: nil)
        }else if newPassword.text  != reEnterPassword.text{
            self.present(Helper.alertVC(title: "Message".localized, message:passwordViewControl.mismatchpassword), animated: true, completion: nil)
        }else{
            self.updateNewPassword()
        }
    }
    
    @IBAction func backToVC(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Update new password
    func updateNewPassword(){
        Helper.showPI(message:"Loading..")

        var newPassword = NewPasswordModel.init(newPassword: self.newPassword.text!)
        
        //@locate ChangePasswordModel
        newPasswordModel.updateNewPassword(params: newPassword, completionHandler: { success in
            if success{
                let defaults = UserDefaults.standard
                defaults.set(self.newPassword.text! , forKey: USER_INFO.OLDPASSWORD)
                defaults.synchronize()
                Helper.alertVC(errMSG: "Your password has been successfully updated.")
                self.dismiss(animated: true, completion: nil)
            }else{
                
            }
        })
    }
    
    func dismissView(){
        self.view.endEditing(true)
    }
}



// MARK: - Textfield delegate method
extension ChangePasswordVC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        switch textField {
        case newPassword:
            reEnterPassword.becomeFirstResponder()
            break
            
        default:
            
            dismissView()
            break
        }
        
        return true
    }
}




