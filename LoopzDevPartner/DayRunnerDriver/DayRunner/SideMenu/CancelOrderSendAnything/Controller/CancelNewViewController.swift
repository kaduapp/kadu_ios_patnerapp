//
//  CancelNewViewController.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 8/14/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit
protocol dismissPopUp:class {
    func goToHome()
}

class CancelNewViewController: UIViewController {

    @IBOutlet weak var cancelTableView: UITableView!
    weak var delegate: dismissPopUp?
    var modelOflan:[CancelModelNew]!
    var apiModel = CancelApi()
    var shipment:Shipment!
    var language:CancelModelNew!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        apiModel.cancelAPI { (success) in
            if success{
                self.language = self.apiModel.langData[0]
                self.modelOflan = self.apiModel.langData
                self.cancelTableView.reloadData()
            }
        }
        // Do any additional setup after loading the view.
    }

    @IBAction func cancelBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension CancelNewViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.modelOflan != nil  {
            return self.modelOflan!.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ReasonTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: ReasonTableViewCell.self), for: indexPath) as! ReasonTableViewCell
        if self.modelOflan != nil  {
            cell.cancelLabel.text = self.modelOflan[indexPath.row].isRTL
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var OrderId: Double!
        if let orderId = shipment.bookingId as? NSNumber {
            OrderId = Double(orderId)
        }
        
        let alertController = UIAlertController(title: "Message".localized, message: "Are you sure you want to cancel order ?", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) {
            UIAlertAction in

            let ipAddress = Utility.getIPAddress()
            let params = Shipment.init(orderId: OrderId, ipAddress: ipAddress)
            self.shipment.makeServiceCallToCancelOrder(shipmentModel: params, completionHanler: { success in
                if success{
                    self.dismiss(animated: true, completion: {
                          self.delegate?.goToHome()
                    })
                  
                   
                }
            })
        }
        
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
}
