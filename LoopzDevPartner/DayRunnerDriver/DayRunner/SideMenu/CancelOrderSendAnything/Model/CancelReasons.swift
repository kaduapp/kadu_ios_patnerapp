//
//  CancelReasons.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 8/14/19.
//  Copyright © 2019 3Embed. All rights reserved.
//



import Foundation

struct CancelModelNew {
    var name: String!
    var code: String!
    var isRTL : String!
    init(data:[String:Any])
    {
        
        if let name = data["_id"] as? String{
            self.name = name
        }
        if let code = data["res_id"] as? String {
            self.code = code
        }
        if let isRTL = data["reasons"] as? String {
            self.isRTL = isRTL
        }
        
    }
    
}

class CancelApi {
    
    var langData:[CancelModelNew]!
    
    func cancelAPI(completion:@escaping(Bool)->()){
        Helper.showPI(message:"Loading..")
        let getLanguage =  APIs()
        getLanguage.getCancelReasons()
        
        getLanguage.subject_response.subscribe(onNext: { (response) in
            Helper.hidePI()
            if let data =  response["data"] as? [String:Any]{
                if let reason = data["reasons"] as? [[String:Any]] {
                    self.langData = reason.map{
                        CancelModelNew.init(data:$0)
                    }
                    completion(true)
                }
               
            }
            
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }
        
        
    }
}
