//
//  SlotsStoreViewController.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 7/19/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class SlotsStoreViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var shipmentModel: Shipment!
    var detailsData = [Shipment]()
    @IBOutlet weak var dateStringLabel: UILabel!
    var dateString = ""
    var productDetails = [String:[Shipment]]()
     var slotsMultiArray = [(key : Int , value : [Shipment] )]()
    var dict = [String:[Shipment]]()
    var startTimeInd = 0
    var status:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        status = (shipmentModel?.orderStatus)!
        for (index, value) in slotsMultiArray.enumerated()
        {
            if startTimeInd == value.key
            {
                detailsData = slotsMultiArray[index].value
                break
            }
            
        }
        
        dict = Dictionary.init(grouping:detailsData, by:{ $0.storeId
        })
        print(dict)
        
        dateStringLabel.text = dateString
        print(detailsData[0].pickupStore)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
    }
    
    @IBAction func actionbuttonBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let nextScene = segue.destination as? StoreOrdersViewController {
            nextScene.detailsData = dict.values.first!
            nextScene.shipmentModel = shipmentModel
            nextScene.dateAndTimeString = dateString
        }
    }
}

extension SlotsStoreViewController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dict.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        for k in dict.keys {
        //             return dict[k]!.count
        //        }
        return dict.keys.map{ return (self.dict[$0]?.count)!}.first!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderTableViewCell.self)) as! HeaderTableViewCell
        if(section == 0)
        {
            cell.headerView.isHidden = true
        }
        cell.controller = false
        let aar:[String] = Array(dict.keys)
        let shipment:Shipment = dict[aar[section]]![section]
        cell.storeNameLabel.text = shipment.pickupStore
        cell.storeAddressLabel.text = shipment.pickupAddress
        let rectShape = CAShapeLayer()
        rectShape.bounds = cell.shadowView.frame
        rectShape.position = cell.shadowView.center
        rectShape.path = UIBezierPath(roundedRect: cell.shadowView.bounds, byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 20, height: 20)).cgPath
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor(red: 0.71, green: 0.72, blue: 0.82, alpha: 0.3).cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 1, height: 1)
        view.layer.shadowRadius = 1.0
        cell.shadowView.layer.mask = rectShape
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderIdTableViewCell") as! OrderIdTableViewCell
        let aar = dict.values.first
        let shipment:Shipment = aar![indexPath.row]
        cell.orderIdLabel.text = "OID :" + "\(shipment.bidID)"
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "slotStoreToStores", sender: nil)
    }
}
