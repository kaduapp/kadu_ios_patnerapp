//
//  HeaderTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 7/19/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {
   
    @IBOutlet weak var storeAddressLabel: UILabel!
    @IBOutlet weak var storeNameLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var headerView: UIView!
    var controller:Bool = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func googleMapAction(_ sender: Any) {
        let con = controller as? SlotsStoreViewController
        let lat: Double
        let log: Double
        if con!.status <= 11 {
            
            let latLongs = con!.shipmentModel.storeLatLng
            
            let name = latLongs.components(separatedBy: ",")
            
            lat = Double(name[0])!
            log = Double(name[1])!
            
        }else{
            let latLongs = con!.shipmentModel.customerLatLng
            
            let name = latLongs.components(separatedBy: ",")
            
            lat = Double(name[0])!
            log = Double(name[1])!
            
        }
        MapNavigation.navgigateTogoogleMaps(latit: lat, logit: log) //@Locate MapNavigation
    }
}

