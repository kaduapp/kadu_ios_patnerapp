//
//  RTModel.swift
//  DayRunner
//
//  Created by apple on 9/11/17.
//  Copyright Â© 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift

class RTModel: NSObject {
    
    static let sharedInstance = RTModel()
    
    let disposeBag = DisposeBag()
    
    ///Get Recent Transactions Data
    ///
    /// - Parameters:
    /// - index: Index number
    /// - success: return an array of RTModel
    func recentTractionsData(index: Int,
                             success:@escaping (RTDataType?) -> Void) {
        
        
        //        if !ReachabilityHelper.shared.isReachable {
        
        //            Helper.showAlertVC(errMSG: alertMsgCommom.NoNetwork, title: alertMsgCommom.OOPS)
        //            Helper.alertVC(errMSG: "OOPS!, No Internet Connection.")
        //            return
        //        }
        
        let walletAPI =  WalletAPI()
        walletAPI.getWalletTransactionsServiceAPICall(pageIndex: index)
        walletAPI.subject_response.subscribe(onNext:{ (response) in
            //        walletAPI.apiResponse.subscribe(onNext: { (response) in
            
            //            if(response.status == true)
            //            {
            if let data = response["data"] as? [String:Any] {
                success(RTDataType(data: data))
            }
            
            //            } else {
            //
            //                if let message = response.data["message"] as? String {
            //
            //                    Helper.alertVC(errMSG: message)
            //                }
            //            }
            
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }).disposed(by: disposeBag)
        
    }
}
