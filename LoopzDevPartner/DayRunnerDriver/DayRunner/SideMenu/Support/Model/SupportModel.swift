//
//  SupportModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 10/06/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit
import RxSwift

protocol SupportDelegate {
    func supportDelegate(data: [Support])
}

class Support:NSObject {
    
    var SupportDelegate: SupportDelegate! = nil
    
    var ArrayData   = [Support]()
    var SubData     = [Support]()
    
    var supportName = ""
    var supportUrl  = ""
    var supportDesc = ""
      let disposeBag = DisposeBag()
    
    func getTheSupportData(){
        Helper.showPI(message:"Loading..")
        
        let support =  SupportApi()
        support.supportApi()
        support.subject_response.subscribe(onNext: { (response) in
            self.supportGetData(dict: (response["data"] as? [[String: Any]])! as [AnyObject])
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)

        
//        NetworkHelper.requestGETURL(method: API.METHOD.SUPPORT  + "0/" + "2",
//                                    success: { (response) in
//                                        Helper.hidePI()
//                                        if response.isEmpty == false {
//
//                                            if (response["statusCode"] != nil){
//                                                let statCode:Int = response["statusCode"] as! Int
//                                                if statCode == 401 {
//                                                    Helper.hidePI()
//                                                    Session.expired()
//                                                    Helper.alertVC(errMSG: "Your Session has beed expired")
//                                                }else
//                                                {
//                                                    Helper.hidePI()
//                                                    Helper.alertVC(errMSG: "bad request or  internal server error")
//                                                }
//                                            }else{
//
//                                                self.supportGetData(dict: (response["data"] as? [[String: Any]])! as [AnyObject])
//                                            }
//
//                                        } else {
//
//                                        }
//        })
//        { (Error) in
//            Helper.alertVC(errMSG: "Message".localized)
//
//        }
    }
    
    func supportGetData(dict: [AnyObject]) {
        print(dict)
        if dict.count != 0{
            for data in dict {
                if let data = data as? [String:Any]{
                    let model = Support()
                    if let titleTemp = data["name"] as? String {
                        model.supportName = titleTemp
                    }
                    if let titleTemp = data["link"] as? String {
                        model.supportUrl = titleTemp
                    }
                    if let titleTemp = data["desc"] as? String {
                        model.supportDesc = titleTemp
                    }
                    if (data["subcat"] != nil) {
                        let titleTemp: [AnyObject] = data["subcat"] as! [AnyObject]
                        for dataSub in titleTemp {
                            if let dataSub = dataSub as? [String:Any] {
                                let subModel = Support()
                                if let titleTemp = dataSub["name"] as? String {
                                    subModel.supportName = titleTemp
                                }
                                if let titleTemp = dataSub["link"] as? String {
                                    subModel.supportUrl = titleTemp
                                }
                                if let titleTemp = dataSub["desc"] as? String {
                                    subModel.supportDesc = titleTemp
                                }
                                model.SubData.append(subModel)
                            }
                        }
                    }
                    ArrayData.append(model)
                }
            }
        }
        if (SupportDelegate != nil) {
            SupportDelegate.supportDelegate(data: ArrayData)
        }
    }

}
