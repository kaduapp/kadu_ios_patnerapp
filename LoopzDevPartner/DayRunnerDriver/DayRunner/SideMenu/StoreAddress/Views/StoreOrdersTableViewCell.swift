//
//  StoreOrdersTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 7/17/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class StoreOrdersTableViewCell: UITableViewCell {
    @IBOutlet weak var pickedLabel: UILabel!
    
    @IBOutlet weak var innerShadowView: UIView!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var delivryTimeLabel: UILabel!
    
    @IBOutlet weak var currentDateAndTimeLabel: UILabel!
    @IBOutlet weak var customerNameLabel: UILabel!
    
    @IBOutlet weak var typeOfOrderLabel: UILabel!
    @IBOutlet weak var customerAddressLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        let rectShape = CAShapeLayer()
//        rectShape.bounds = self.innerShadowView.frame
//        rectShape.position = self.innerShadowView.center
//        rectShape.path = UIBezierPath(roundedRect: self.innerShadowView.bounds, byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 20, height: 20)).cgPath
//        self.innerShadowView.layer.masksToBounds = false
//        self.innerShadowView.layer.cornerRadius = 12.0
//        self.innerShadowView.layer.shadowColor = UIColor(red: 0.71, green: 0.72, blue: 0.82, alpha: 0.3).cgColor
//        self.innerShadowView.layer.mask = rectShape
        
        
         Helper.shadowView(view: self.shadowView, scale: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
