//
//  StoreNameTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 7/17/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class StoreNameTableViewCell: UITableViewCell {

    @IBOutlet weak var deliverysView: UIView!
    @IBOutlet weak var heightOfImage: NSLayoutConstraint!
    @IBOutlet weak var navigationImage: UIImageView!
    @IBOutlet weak var youAreHereLabel: UILabel!
    @IBOutlet weak var youAreAddressLabel: UILabel!
    var controller:UIViewController? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func googleMapAction(_ sender: Any) {
        let con = self.controller as? StoreOrdersViewController
        let lat: Double
        let log: Double
        if con!.status <= 11 {
            
            let latLongs = con!.shipmentModel.storeLatLng
            
            let name = latLongs.components(separatedBy: ",")
            
            lat = Double(name[0])!
            log = Double(name[1])!
            
        }else{
            let latLongs = con!.shipmentModel.customerLatLng
            
            let name = latLongs.components(separatedBy: ",")
            
            lat = Double(name[0])!
            log = Double(name[1])!
            
        }
        MapNavigation.navgigateTogoogleMaps(latit: lat, logit: log) //@Locate MapNavigation
    }
    

}
