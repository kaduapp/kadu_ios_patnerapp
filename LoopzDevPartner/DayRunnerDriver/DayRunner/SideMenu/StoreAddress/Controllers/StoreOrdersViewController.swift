//
//  StoreOrdersViewController.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 7/17/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit
import JaneSliderControl

class StoreOrdersViewController: UIViewController {
    
    @IBOutlet weak var bottomSliderViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var leftSlider: SliderControl!
    @IBOutlet weak var ordersTableView: UITableView!
    @IBOutlet weak var shipmentStartedButton: UIButton!
    
    @IBOutlet weak var bottomConstantOfSlider: NSLayoutConstraint!
    @IBOutlet weak var dateStringLabel: UILabel!
    
    var dateAndTimeString = ""
    
    var detailsData = [Shipment]()
    var shipmentModel: Shipment!
    var selectedIndex:Int = 0
    var status:Int = 0
    var reloadSection:Bool = true
    var dict = [Int:[Shipment]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dateStringLabel.text = dateAndTimeString
        
        
        status = (shipmentModel?.orderStatus)!
        if(status == 8) {
            
        }else {
            self.bottomSliderViewConstraint.constant = 0
        }
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
        dict = Dictionary.init(grouping:detailsData, by:{ $0.orderStatus
            
        })
        print(dict.keys.count)
        if(dict.keys.count == 1 && dict.keys.first == 12) {
            //            self.bottomSliderViewConstraint.constant = 65
        }
        self.ordersTableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if let nextScene = segue.destination as? PickingItemViewController {
            
            nextScene.shipmentModel = detailsData[selectedIndex]
            
            if(detailsData[selectedIndex].orderStatus == 11){
                nextScene.status = 1
            }else if(detailsData[selectedIndex].orderStatus == 12){
                nextScene.status = 1
            }else if(detailsData[selectedIndex].orderStatus == 8){
                nextScene.status = 3
            }else{
                nextScene.status = 2
            }
            nextScene.comingFromStore = false
            
        }else if let nextScene = segue.destination as? ProviderInvoiceViewController{
            nextScene.shipmentModel = sender as? Shipment
        }else if let nextScene = segue.destination as? OnBookingViewController {
            var shipmentData = sender as! Shipment
            nextScene.shipmentModel  = shipmentData
            if(shipmentData.orderStatus == 10){
                nextScene.status = 1
            }else{
                nextScene.status = 2
            }
        }else if let nav =  segue.destination as? UINavigationController
        {
            if let nextScene = nav.viewControllers.first as? JobDetailsController?{
                nextScene?.shipmentModel = detailsData[selectedIndex]
            }
        }
    }
    @IBAction func actionbuttonBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sliderFineshed(_ sender: SliderControl) {
        // shipmentStartedButton.isEnabled = false
        
        let bookingId = Double(detailsData[0].bookingId)
        
        let updateDeliveryStatusParams = Shipment.init(orderId: bookingId, slotID:detailsData[0].slotId ,orderStatus: 11 , custRating: Float(4.0), custSignature: " " )
        
        resetSlider()
        shipmentModel.makeServiceCallToUpdateStatus(shipmentModel: updateDeliveryStatusParams, completionHanler: { success in
            
            //                self.shipmentStartedButton.isEnabled = true
            if success == true{
                self.shipmentModel.orderStatus = 11
                self.shipmentModel.updateTheBookingsString(bookingID: self.shipmentModel.bookingId, status: 11)
                self.reloadSection = false
                self.bottomSliderViewConstraint.constant = 0; self.ordersTableView.reloadSections([0], with: .none)
            }
            
        })
        
    }
    func resetSlider()
    {
        self.leftSlider.reset()
    }
    
    @IBAction func slideCanceled(_ sender: SliderControl) {
        print("Cancelled")
    }
    
    
    @IBAction func sliderChanged(_ sender: SliderControl) {
        print("Changed")
    }
    
}

extension StoreOrdersViewController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0) {
            return 1;
        }else if(section == 1) {
            return detailsData.count;
        }else {
            return 7
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0) {
            let cell: StoreNameTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: StoreNameTableViewCell.self), for: indexPath) as! StoreNameTableViewCell
            if(dict.keys.count == 1 && dict.keys.first == 12) {
                cell.deliverysView.isHidden = false
            }else {
                cell.deliverysView.isHidden = true
            }
            
            if(reloadSection) {
                if(status == 8) {
                    cell.controller = self
                    cell.youAreHereLabel.text = detailsData[indexPath.row].pickupStore
                    cell.youAreAddressLabel.text = detailsData[indexPath.row].pickupAddress
                }else {
                    cell.controller = self
                    cell.youAreHereLabel.text = "YOU ARE HERE"
                    cell.heightOfImage.constant = 0
                    
                    let myString:NSString = detailsData[indexPath.row].pickupStore + " : " + detailsData[indexPath.row].pickupAddress as NSString
                    var myMutableString = NSMutableAttributedString()
                    
                    myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Helvetica", size: 16.0)!])
                    myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:detailsData[indexPath.row].pickupStore.length + 3))
                    
                    cell.navigationImage.isHidden = true
                    cell.youAreAddressLabel.attributedText = myMutableString
                }
                
            }else {
                
                cell.controller = self
                cell.youAreHereLabel.text = "YOU ARE HERE"
                cell.heightOfImage.constant = 0
                
                let myString:NSString = detailsData[indexPath.row].pickupStore + " : " + detailsData[indexPath.row].pickupAddress as NSString
                var myMutableString = NSMutableAttributedString()
                
                myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Helvetica", size: 16.0)!])
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:detailsData[indexPath.row].pickupStore.length + 3))
                
                cell.navigationImage.isHidden = true
                cell.youAreAddressLabel.attributedText = myMutableString
            }
            return cell
        }else if(indexPath.section == 1){
            let cell: StoreOrdersTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: StoreOrdersTableViewCell.self), for: indexPath) as! StoreOrdersTableViewCell
            if(detailsData[indexPath.row].orderStatus == 12) {
                cell.pickedLabel.isHidden = false
                cell.pickedLabel.text = "Picked"
            }else if(detailsData[indexPath.row].orderStatus == 13){
                cell.pickedLabel.isHidden = false
                cell.pickedLabel.text = "Picked"
            }else if(detailsData[indexPath.row].orderStatus == 14){
                cell.pickedLabel.isHidden = false
                cell.pickedLabel.text = "Picked"
            }else if(detailsData[indexPath.row].orderStatus == 8){
                cell.pickedLabel.isHidden = true
            }else {
                cell.pickedLabel.isHidden = false
                cell.pickedLabel.text = "Not Picked"
            }
            
            cell.orderIdLabel.text = "\(detailsData[indexPath.row].bidID)"
            cell.totalAmountLabel.text = Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(detailsData[indexPath.row].grandTotal) , digits: 2))
            cell.customerNameLabel.text = detailsData[indexPath.row].customerName
            cell.customerAddressLabel.text = detailsData[indexPath.row].deliveryAddress
            cell.currentDateAndTimeLabel.text = Helper.UTCToLocal(date:detailsData[indexPath.row].timeLeftToPick)
            cell.typeOfOrderLabel.text = detailsData[indexPath.row].storeMessage
            let date1 = NSDate(timeIntervalSince1970: TimeInterval(detailsData[indexPath.row].slotEndTime))
            let dayTimePeriodFormatter1 = DateFormatter()
            dayTimePeriodFormatter1.dateFormat = "hh:mm a"
            let dateString1 = dayTimePeriodFormatter1.string(from: date1 as Date)
            cell.delivryTimeLabel.text = dateString1
            return cell
        }else {
            return UITableViewCell()
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
            if(dict.keys.count == 1 && dict.keys.first == 12) {
                return 70;
            }else {
                return 100;
            }
            
        }else if(indexPath.section == 1) {
            return 250;
        }else {
            return 0;
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(dict.keys.count == 1 && dict.keys.first == 12) {
            performSegue(withIdentifier: "toBookingVC", sender: detailsData[indexPath.row])
        }else if(shipmentModel.orderStatus == 13) {
            performSegue(withIdentifier: "editItems", sender: detailsData[indexPath.row])
        }else if(shipmentModel.orderStatus == 14) {
            performSegue(withIdentifier: "goToDeliverScreen", sender: detailsData[indexPath.row])
        }else {
            
            if(reloadSection) {
                if(status == 8) {
                    selectedIndex = indexPath.row
                    performSegue(withIdentifier: "shipmentDetails", sender: detailsData[indexPath.row])
                }else {
                    selectedIndex = indexPath.row
                    performSegue(withIdentifier: "editItems", sender: detailsData[indexPath.row])
                }
                
            }else {
                selectedIndex = indexPath.row
                performSegue(withIdentifier: "editItems", sender: detailsData[indexPath.row])
            }
            
        }
    }
    
}



