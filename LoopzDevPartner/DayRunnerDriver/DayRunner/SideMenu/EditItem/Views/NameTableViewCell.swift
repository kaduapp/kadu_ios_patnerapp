//
//  NameTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 7/17/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class NameTableViewCell: UITableViewCell {

    @IBOutlet weak var customerNameLabel: UILabel!
    var controller:UIViewController? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    @IBAction func callAction(_ sender: Any) {
         let con = controller as? EditItemviewViewController
        if con!.status <= 11 {
            let dropPhone = "tel://" + ((con?.shipment.storePhone)!)
            if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.openURL(url as URL)
            }
        }else{
            let dropPhone = "tel://" + ((con?.shipment.customerPhone)!)
            if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.openURL(url as URL)
            }
        }
        
    }
    
    @IBAction func chatAction(_ sender: Any) {
          let con = controller as? EditItemviewViewController
        con!.performSegue(withIdentifier: "toChatIP", sender: self)
    }
    
}
