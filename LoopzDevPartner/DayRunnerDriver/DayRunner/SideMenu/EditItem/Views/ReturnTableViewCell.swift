//
//  ReturnTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 7/17/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class ReturnTableViewCell: UITableViewCell {

    @IBOutlet weak var replaceImage: UIImageView!
    @IBOutlet weak var returnCompleteImage: UIImageView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var cancelLabel: UILabel!
    
    @IBOutlet weak var replaceLabel: UILabel!
    @IBOutlet weak var returnView: UIView!
    @IBOutlet weak var returnButton: UIButton!
    
    @IBOutlet weak var replaceButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        Helper.shadowView(view: returnView,scale:true)
        Helper.shadowView(view: cancelView,scale:true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
