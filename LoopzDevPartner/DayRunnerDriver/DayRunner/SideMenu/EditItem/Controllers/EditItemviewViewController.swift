//
//  EditItemviewViewController.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 7/17/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit


class EditItemviewViewController: UIViewController {

    @IBOutlet weak var areYouSureLabel: UILabel!
    
    @IBOutlet weak var editTableView: UITableView!
    
    
    @IBOutlet weak var alertView: UIView!
    var shipmentModel:[String: Any] = [:]
    var shipment:Shipment!
    var status:Int = 0
    var selectedIndex:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        if(self.shipment.orderStatus == 13) {
           
        }
         status = (shipment?.orderStatus)!
        DispatchQueue.main.async {
             self.alertView.isHidden = true
        }
        
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
    }
    
    @IBAction func actionbuttonBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func replace(sender: UIButton){
        
        if(shipment.orderStatus == 13){
//            let selectedIndex = sender.tag
            UpdateQuantityPopup.instance.show()
            
            UpdateQuantityPopup.instance.productDetailsInit(details: shipment, ItemNo: selectedIndex)
            UpdateQuantityPopup.instance.delegate = self
        }else {
            shipment.updateType = "2"
            performSegue(withIdentifier: "editItemViewController", sender: nil)
        }
    }
    @objc func returnBtn(sender: UIButton){
        
            if(self.shipment.orderStatus == 13) {
                if(shipment.productDetails.count == 1) {
                    
                    var OrderId: Double!
                    if let orderId = shipment.bookingId as? NSNumber {
                        OrderId = Double(orderId)
                    }
                    
                    let alertController = UIAlertController(title: "Message".localized, message: "This is the last item in the order, cancelling this item will cancel the entire order. Please confirm to proceed ?", preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        
                        let ipAddress = Utility.getIPAddress()
                        let params = Shipment.init(orderId: OrderId, ipAddress: ipAddress)
                        self.shipment.makeServiceCallToCancelOrder(shipmentModel: params, completionHanler: { success in
                            if success{
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                        })
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                        UIAlertAction in
                        NSLog("Cancel Pressed")
                    }
                    
                    alertController.addAction(okAction)
                    alertController.addAction(cancelAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }else {
                
                DispatchQueue.main.async {
                    self.areYouSureLabel.text = "Would you like to remove item completely ?"
                    self.alertView.isHidden = false
                    self.navigationController?.navigationBar.isHidden = true
                }
            }
            }else {
                if(shipment.productDetails.count == 1) {
                    let alertController = UIAlertController(title: "Message".localized, message: "Last item you can't remove", preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        
                    }
                    alertController.addAction(okAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }else {
                    DispatchQueue.main.async {
                        self.areYouSureLabel.text = "Would you like to remove item completely ?"
                        self.alertView.isHidden = false
                        self.navigationController?.navigationBar.isHidden = true
                    }
                }
        }

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nextScene = segue.destination as? SearchBarItemViewController
        {
            nextScene.shipment = shipment
            nextScene.shipmentModel = shipmentModel
        } else if let nav =  segue.destination as? UINavigationController
        {
            if let viewController: ChatVC = nav.viewControllers.first as! ChatVC? {
                viewController.bookingID = "\(shipment.bookingId!)"
                viewController.custName = shipment.customerName
                viewController.customerID = shipment.customerId
                viewController.custImage = shipment.customerPic
            }
            
            
        }
    }
    @IBAction func noButtonAction(_ sender: Any) {
        DispatchQueue.main.async {
            self.alertView.isHidden = true
            self.editTableView.reloadData()
            self.navigationController?.navigationBar.isHidden = false
            
        }
    }
    
    @IBAction func yesButtonAction(_ sender: Any) {
        if(self.shipment.orderStatus == 13) {
            
            let newDict = ["addedToCartOn" : shipmentModel["addedToCartOn"] ?? 0,
                           "childProductId": shipmentModel["childProductId"] ?? "",
                           "unitId" : shipmentModel["unitId"] ?? 0 ,
                           "unitPrice" : shipmentModel["unitPrice"] ?? 0,
                           "appliedDiscount" : shipmentModel["appliedDiscount"] ?? 0,
                           "finalPrice" : shipmentModel["finalPrice"] ?? 0,
                           "quantity" : 0,
                           "oldQuantity" : shipmentModel["quantity"] ?? 0
                ] as [String:Any]
            let oldDict = ["addedToCartOn" : shipmentModel["addedToCartOn"] ?? 0,
                           "childProductId": shipmentModel["childProductId"] ?? "",
                           "unitId" : shipmentModel["unitId"] ?? 0 ,
                           "unitPrice" : shipmentModel["unitPrice"] ?? 0,
                           "appliedDiscount" : shipmentModel["appliedDiscount"] ?? 0,
                           "finalPrice" : shipmentModel["finalPrice"] ?? 0,
                           "quantity" : 0,
                           "oldQuantity" : shipmentModel["quantity"] ?? 0
                ] as [String:Any]
            
            var OrderId: Double!
            if let orderId = shipment.bookingId as? NSNumber {
                OrderId = Double(orderId)
            }
            let params = Shipment.init(oldItems: oldDict, newItems: newDict, orderId: OrderId, updateType: "3")
            shipment.makeServiceCallUpdateOrder(withParams: params, completionHanler: { (success,gTotal,subtotal) in
                if success {
                    self.shipment.grandTotal = gTotal
                    self.shipment.subTotal = subtotal
                    self.navigationController?.popViewController(animated: true)
                } else {
                    
                }
            })
        }else {
            
            if(shipment.productDetails.count == 1){
                
                let alertController = UIAlertController(title: "Message".localized, message: "Last item you can't remove", preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    self.alertView.isHidden = true
                    self.navigationController?.navigationBar.isHidden = false
                }
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion: nil)
                
            }else {
                
                let newDict = ["addedToCartOn" : shipmentModel["addedToCartOn"] ?? 0,
                               "childProductId": shipmentModel["childProductId"] ?? "",
                               "unitId" : shipmentModel["unitId"] ?? 0 ,
                               "unitPrice" : shipmentModel["unitPrice"] ?? 0,
                               "appliedDiscount" : shipmentModel["appliedDiscount"] ?? 0,
                               "finalPrice" : shipmentModel["finalPrice"] ?? 0,
                               "quantity" : 0,
                               "oldQuantity" : shipmentModel["quantity"] ?? 0
                    ] as [String:Any]
                let oldDict = ["addedToCartOn" : shipmentModel["addedToCartOn"] ?? 0,
                               "childProductId": shipmentModel["childProductId"] ?? "",
                               "unitId" : shipmentModel["unitId"] ?? 0 ,
                               "unitPrice" : shipmentModel["unitPrice"] ?? 0,
                               "appliedDiscount" : shipmentModel["appliedDiscount"] ?? 0,
                               "finalPrice" : shipmentModel["finalPrice"] ?? 0,
                               "quantity" : 0,
                               "oldQuantity" : shipmentModel["quantity"] ?? 0
                    ] as [String:Any]
                
                var OrderId: Double!
                if let orderId = shipment.bookingId as? NSNumber {
                    OrderId = Double(orderId)
                }
                let params = Shipment.init(oldItems: oldDict, newItems: newDict, orderId: OrderId, updateType: "3")
                shipment.makeServiceCallUpdateOrder(withParams: params, completionHanler: { (success,gTotal,subtotal) in
                    if success {
                        self.shipment.grandTotal = gTotal
                        self.shipment.subTotal = subtotal
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        
                    }
                })
            }
            
        }
       
    }
    
}
extension EditItemviewViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell: ProductDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProductDetailsTableViewCell.self), for: indexPath) as! ProductDetailsTableViewCell
            if let name = shipmentModel["itemName"] as? String {
                cell.productName.text = name
            }
            if let qty = shipmentModel["quantity"] as? Int {
                cell.quantityLabel.text = "Qty : " + "\(qty)"
            }
            if let unitPrice = shipmentModel["unitPrice"] as? Double {
                
                let myString:NSString = "Unit Cost : " + Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(unitPrice) , digits: 2)) as NSString
                let newStr = String(describing: Helper.clipDigit(value: Float(unitPrice) , digits: 2)) as NSString
                var myMutableString = NSMutableAttributedString()
                
                myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Helvetica", size: 16.0)!])
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: Helper.UIColorFromRGB(rgbValue: 0x555555), range: NSRange(location:12,length:newStr.length + 1))
                cell.unitCostLabel.attributedText = myMutableString
            }
            if let finalPrice = shipmentModel["finalPrice"] as? Int {
                
                let myString:NSString = "Total Price : " + Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(finalPrice) , digits: 2)) as NSString
                var myMutableString = NSMutableAttributedString()
                let newStr1 = String(describing:Helper.clipDigit(value: Float(finalPrice) , digits: 2)) as NSString
                myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Helvetica", size: 16.0)!])
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: Helper.UIColorFromRGB(rgbValue: 0x555555), range: NSRange(location:14,length:newStr1.length + 1))
                cell.totalPriceLabel.attributedText = myMutableString
            }else if let finalPrice = shipmentModel["finalPrice"] as? Double {
                let myString:NSString = "Total Price : " + Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(finalPrice) , digits: 2)) as NSString
                var myMutableString = NSMutableAttributedString()
                let newStr1 = String(describing:Helper.clipDigit(value: Float(finalPrice) , digits: 2)) as NSString
                myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Helvetica", size: 16.0)!])
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: Helper.UIColorFromRGB(rgbValue: 0x555555), range: NSRange(location:14,length:newStr1.length + 1))
                cell.totalPriceLabel.attributedText = myMutableString
            }
            
            return cell
        case 1:
            let cell: NameTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: NameTableViewCell.self), for: indexPath) as! NameTableViewCell
            cell.controller = self
            cell.customerNameLabel.text = shipment.customerName
            return cell
        case 2:
            let cell: ReturnTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: ReturnTableViewCell.self), for: indexPath) as! ReturnTableViewCell
            if(shipment.orderStatus == 13){
                cell.replaceLabel.text = "Return       Partially"
                cell.replaceImage.image = UIImage.init(named: "return partically")
                cell.cancelLabel.text = "Return   completely"
            }
            cell.replaceButton.addTarget(self, action: #selector(replace(sender:)), for: .touchUpInside)
             cell.returnButton.addTarget(self, action: #selector(returnBtn(sender:)), for: .touchUpInside)
            return cell
        default:
           return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 180
        case 1:
            return 100
        case 2:
            return 120
        default:
           return 100
        }
    }
    
}

extension EditItemviewViewController: UpdateQuantityDelegate {
    
    func updateOrderDetails(updatedOrder: Shipment) {
        var OrderId: Double!
        if let orderId = updatedOrder.bookingId as? NSNumber {
            OrderId = Double(orderId)
        }
        
        if self.shipment.productDetails.count > 0 {
            let newInt = shipmentModel["quantity"] as! Int
            
            let newValue = shipment.productDetails[selectedIndex]["quantity"] as! Int
            
                let newDict = ["addedToCartOn" : shipmentModel["addedToCartOn"] ?? 0,
                               "childProductId": shipmentModel["childProductId"] ?? "",
                               "unitId" : shipmentModel["unitId"] ?? 0 ,
                               "unitPrice" : shipmentModel["unitPrice"] ?? 0,
                               "appliedDiscount" : shipmentModel["appliedDiscount"] ?? 0,
                               "finalPrice" : shipmentModel["finalPrice"] ?? 0,
                               "quantity" : 0,
                               "oldQuantity" : shipmentModel["quantity"] ?? 0
                    ] as [String:Any]
                let oldDict = ["addedToCartOn" : shipmentModel["addedToCartOn"] ?? 0,
                               "childProductId": shipmentModel["childProductId"] ?? "",
                               "unitId" : shipmentModel["unitId"] ?? 0 ,
                               "unitPrice" : shipmentModel["unitPrice"] ?? 0,
                               "appliedDiscount" : shipmentModel["appliedDiscount"] ?? 0,
                               "finalPrice" : shipmentModel["finalPrice"] ?? 0,
                               "quantity" : newInt - newValue,
                               "oldQuantity" : shipmentModel["quantity"] ?? 0
                    ] as [String:Any]
                
                var OrderId: Double!
                if let orderId = shipment.bookingId as? NSNumber {
                    OrderId = Double(orderId)
                }
                let params = Shipment.init(oldItems: oldDict, newItems: newDict, orderId: OrderId, updateType: "1")
                shipment.makeServiceCallUpdateOrder(withParams: params, completionHanler: { (success,gTotal,subtotal) in
                    if success {
                        self.shipment.grandTotal = gTotal
                        self.shipment.subTotal = subtotal
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        
                    }
                })
        }
//        } else {
//
////            let alertController = UIAlertController(title: "Message".localized, message: "You are about to cancel order, press Okay to cancel Order", preferredStyle: .alert)
////
////            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
////                UIAlertAction in
////
////                let ipAddress = Utility.getIPAddress()
////                let params = Shipment.init(orderId: OrderId, ipAddress: ipAddress)
////                self.shipment.makeServiceCallToCancelOrder(shipmentModel: params, completionHanler: { success in
////                    if success{
////                        //removing the product from array of product details
////                        self.shipment = updatedOrder
////                        self.navigationController?.popToRootViewController(animated: true)
////                    } else{
////
////                    }
////                })
////            }
//
//            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
//                UIAlertAction in
//                NSLog("Cancel Pressed")
//            }
//
//            alertController.addAction(okAction)
//            alertController.addAction(cancelAction)
//
//            self.present(alertController, animated: true, completion: nil)
//        }
        
    }
}
