//
//  HistModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 23/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import Foundation
import RxSwift

struct Sale {
    var month: String
    var value: Double
}

class DataGenerator {
    
    static var randomizedSale: Double {
        return Double(arc4random_uniform(10000) + 1) / 10
    }
    
    static func data() -> [Sale] {
        let months =  ["MON","TUE","WED","THU","FRI","SAT","SUN"]
        var sales = [Sale]()
        for month in months {
            let sale = Sale(month: month, value: randomizedSale)
            sales.append(sale)
        }
        return sales
    }
}

protocol HistoryModelDelegate{
    //    func completedBookingData(data: [HistoryModel])
    func completedBookingData(data: [HistoryModel], totalEarnDict:[String:Any])
}

class HistoryModel: NSObject {
    var HISTDelegate: HistoryModelDelegate! = nil
    
    var pickAddress = ""
    var customerPickupAddress = ""
    var dropAddress = ""
    var bookingDateNtime = ""
    var timeLeftToPick = ""
    //    var pickupStore = ""
    var bookingAmt: NSNumber = 0
    var subTotal = 0.00
    var taxAmount = 0.00
    var discount = 0.00
    var deliveryCharge = 0.00
    //    var bookingId:String = ""
    var bookingId: NSNumber = 0
    var statCode = 0
    var pickPhone = ""
    var dropPhone = ""
    var dropCustName = ""
    var pickCustName = ""
    var pickLatlog = ""
    var dropLatlog = ""
    var goodType = ""
    var quantity = ""
    var notes = ""
    var signature = ""
    var vehicleDocuments = ""
    var custRate = ""
    var paymentType: Int = 0
    var storType:Int = 0
    var storeTypeMsg = ""
    var  payByWallet: Int = 0
    
    var paymentCard = 0.00
    var paymentCash = 0.00
    var paymentWallet = 0.00
    var shipImage:[String] = []
    var custChn = ""
    var invoiceData = BookingModel()
    var  bookingsData    = [HistoryModel]()
    var pickTime = ""
    var deliveryTime = ""
    var driverEarning = 0.00
    //Product Details
    var productDetails: [[String:Any]]!
    var distanceDriver  = ""
    var distanceTime    = 0
    var storeLogo = ""
    var appCommission = 0.00
    var storeEarning = 0.00
    let disposeBag = DisposeBag()
    func historyBookingsAPI(params:[String:Any]){
        bookingsData = []
        Helper.showPI(message:"Loading..")
        let historyApi =  HistoryApi()
        historyApi.historyApi(params: params)
        historyApi.subject_response.subscribe(onNext: { (response) in
            
            if let data =  response["orders"] as? [[String:Any]] {
                if let totalEarning = response["total"] as? [String:Any] {
                    self.parsingTheServiceResponse(responseData: data, totalEarning: totalEarning )
                }
            }else {
                
                let myNewDictArray: [[String:Int]] = []
                let totalEarning:[String:Int] = [:]
                self.parsingTheServiceResponse(responseData: myNewDictArray, totalEarning: totalEarning )
            }
            
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
            }.disposed(by: disposeBag)
    }
    
    func parsingTheServiceResponse(responseData:[[String: Any]], totalEarning: [String:Any] ) {
        
        for dict in responseData {
            
            let model = HistoryModel()
            
            if let appCommission = dict["appCommission"] as? Double {
                model.appCommission = appCommission
            }
            
            if let storeEarning = dict["storeEarning"] as? Double {
                model.storeEarning = storeEarning
            }
            
            if let distanceTime    = dict["journeyStartToEndTime"] as? Int {
                model.distanceTime = distanceTime
            }
            
            if  let storeAddress   = dict["storeAddress"] as? String {
                model.pickAddress = storeAddress
            }
            
            if  let dropAddress   = dict["dropAddress"] as? String {
                model.dropAddress = dropAddress
            }
            
            if  let PickTime      = dict["bookingDate"] as? String {
                model.bookingDateNtime = PickTime
            }
            
            if let distance = dict["distanceDriver"] as? String {
                model.distanceDriver = distance
            }
            
            if let storeLogo = dict["storeLogo"] as? String {
                model.storeLogo = storeLogo
            }
            
            if  let storeName      = dict["storeName"] as? String {
                model.pickCustName = storeName
            }
            
            if let driverEarning = dict["driverEarning"] as? Double {
                model.driverEarning = driverEarning
            }
            
            if  let custName  = dict["customerName"] as? String {
                model.dropCustName = custName
            }
            
            if let custPhone     = dict["customerPhone"] as? String {
                model.pickPhone = custPhone
            }
            
            if  let pickLat       = dict["pickupLat"] as? String {
                model.pickLatlog = pickLat
            }
            
            if  let dropLat       = dict["dropLat"] as? String {
                model.dropLatlog   = dropLat
            }
            
            if  let bid           = dict["orderId"] as? NSNumber {
                model.bookingId = bid
            }
            
            if  let status        = dict["statusCode"] as? NSNumber{
                model.statCode = Int(status)
            }
            
            if  let subTotal      =  dict["subTotalAmount"] as? Double {
                model.subTotal = subTotal
            }
            
            if  let taxAmount      =  dict["tax"] as? Double {
                model.taxAmount = taxAmount
            }
            
            if let discount   = dict["discount"] as? Double {
                model.discount = discount
            }
            
            if  let deliveryCharge      =  dict["deliveryCharge"] as? Double {
                model.deliveryCharge = deliveryCharge
            }
            
            if  let amount      =  dict["totalAmount"] as? NSNumber {
                model.bookingAmt = amount
            }
            
            if let paymentType = dict["paymentType"] as? NSNumber {
                model.paymentType = Int(paymentType)
            }
            
            if let storeType = dict["storeType"] as? NSNumber {
                model.storType = Int(storeType)
            }
            if let storeTypeMsg = dict["storeTypeMsg"] as? String {
                model.storeTypeMsg = storeTypeMsg
            }
            
            if let paybyWallet = dict["payByWallet"] as? NSNumber {
                model.payByWallet = Int(paybyWallet)
            }
            
            if let paymentCard = dict["paidByCard"] as? Double {
                model.paymentCard = paymentCard
            }
            
            if let paymentCash = dict["paidByCash"] as? Double {
                model.paymentCash = paymentCash
            }
            
            if let paymentWallet = dict["paidByWallet"] as? Double {
                model.paymentWallet = paymentWallet
            }
            if let customerPickAddress = dict["pickAddress"] as? String {
                model.customerPickupAddress = customerPickAddress
            }
            
            if let signature = dict["signatureUrl"] as? String {
                model.signature = signature
            }
            
            if let rating = dict["rating"] as? NSNumber {
                let num = rating
                model.custRate = "\(num)"
            }
            
            if let pickTime = dict["pickedupTime"] as? String {
                model.pickTime = Helper.changeDateFormatForHome(pickTime)
            }
            
            if let dropTime = dict["deliveredTime"] as? String {
                model.deliveryTime = Helper.changeDateFormatForHome(dropTime)
            }
      
            model.productDetails = []
            for shipments in (dict["items"] as? [[String: Any]])!  {
                
                var productData: [String:Any]!
                productData = [:]
                
                if   let good     = shipments["itemName"] as? String {
                    productData["itemName"] = good
                }
                
                if let appliedDiscount = shipments["appliedDiscount"] as? Int{
                    
                    productData["appliedDiscount"] = appliedDiscount
                }
                
                if let itemImageURL = shipments["itemImageURL"] as? String {
                    
                    productData["itemImageURL"] = itemImageURL
                }
                
                if let finalPrice = shipments["finalPrice"] as? Int{
                    
                    productData["finalPrice"] = finalPrice
                }
                
                if let quant     = shipments["quantity"] as? NSNumber {
                    productData["quantity"] = Int(quant)
                }
                
                if let unitLbl = shipments["unitName"] as? String {
                    productData["unitName"] = unitLbl
                }
                
                if  let shipImage = shipments["itemImageURL"] as? String {
                    productData["itemImageURL"] = shipImage
                }
                
                if let totalPrice = shipments["unitPrice"] as? NSNumber {
                    productData["unitPrice"] = Int(totalPrice)
                }
                
                if let id = shipments["childProductId"] as? String {
                    productData["childProductId"] = id
                }
                
                model.productDetails.append(productData)
            }
            
            bookingsData.append(model)
        }
        
        if HISTDelegate != nil{
            HISTDelegate.completedBookingData(data: bookingsData, totalEarnDict: totalEarning)
        }
    }
}
