//
//  HistHeaderTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 21/06/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class HistHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var thisWeeklabel: UILabel!
    
    @IBOutlet var totalAmt: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.thisWeeklabel.text = "THIS WEEK".localized
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
