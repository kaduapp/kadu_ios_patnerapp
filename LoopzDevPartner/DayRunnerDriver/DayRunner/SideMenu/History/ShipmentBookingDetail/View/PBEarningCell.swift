//
//  PBEarningCell.swift
//  LoopzDriver
//
//  Created by Rahul Sharma on 06/03/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class PBEarningCell: UITableViewCell {

    @IBOutlet weak var appComission: UILabel!
    @IBOutlet weak var driverEarning: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
