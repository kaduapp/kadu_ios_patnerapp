//
//  PBHeaderTableViewCell.swift
//  DayRunnerDriverDev
//
//  Created by Rahul Sharma on 17/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class PBHeaderTableViewCell: UITableViewCell {

    @IBOutlet var sectionHeader: UILabel!
    @IBOutlet weak var paidByReciever: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateHeaderField(header:String){
        sectionHeader.text = header
    }

}
