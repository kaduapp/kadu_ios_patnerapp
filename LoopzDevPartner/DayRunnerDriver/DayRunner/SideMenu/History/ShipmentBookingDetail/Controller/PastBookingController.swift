//
//  PastBookingController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 11/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

enum PBSectionType : Int {
    case PBEarnings = 0
    case PBDriverDetails = 1
    case PBBillDetails = 2
    case PBGrandTot = 3
    case PBPaymentType = 4
    case PBReceiversDetails = 5
}

enum PBRowType : Int {
    case PBHeaderSec = 0
    case PBRowDefault = 1
    case PBHeader = 2
    case PBThirdRow = 3
    case PBFourthRow = 4
    case PBFifthRow = 5
    case PBSixthRow = 6
    case PBSeventhRow = 7
}


class PastBookingController: UIViewController {
    
    @IBOutlet var bookingID: UILabel!
    @IBOutlet var dateOfBooking: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var bookingArray:HistoryModel?
    var invoiceDetails = BookingModel()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let num = bookingArray?.bookingId
        bookingID.text = "Id: \(num!)"
        dateOfBooking.text = Helper.changeDateFormatForHome((bookingArray?.bookingDateNtime)!)
        
        tableView.estimatedRowHeight = 10
        tableView.rowHeight = UITableView.automaticDimension
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func helpView(_ sender: Any) {
        self.performSegue(withIdentifier: "fromHistory", sender:  nil)
    }
    
}

extension PastBookingController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionType : PBSectionType = PBSectionType(rawValue: section)!
        
        switch sectionType {
            
        case .PBEarnings:
            return 2
            
        case .PBDriverDetails:
            return 3
            
        case .PBBillDetails :
            return (bookingArray?.productDetails.count)!
            
        case .PBGrandTot:
            return 2
            
        case .PBPaymentType :
            return 2
            
        case .PBReceiversDetails :
            return 2
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionType : PBSectionType = PBSectionType(rawValue: indexPath.section)!
        let sectionCell : PBSectionCell = tableView.dequeueReusableCell(withIdentifier: "PBSectionCell") as! PBSectionCell
        
        let header: PBHeaderNewViewCell = tableView.dequeueReusableCell(withIdentifier: "header") as! PBHeaderNewViewCell
        
        let rowType : PBRowType = PBRowType(rawValue: indexPath.row)!
        
        switch sectionType {
            
        case .PBEarnings:
            switch rowType{
            case .PBHeaderSec:
                let cell: PBEarningCell = tableView.dequeueReusableCell(withIdentifier: "PBEarningCell") as! PBEarningCell
                let total = bookingArray?.bookingAmt
                cell.driverEarning.text  = Utility.currencySymbol + "\(total!)";
                cell.appComission.text = Utility.currencySymbol + "\(String(describing: bookingArray?.driverEarning))"
                return cell
                
            case .PBRowDefault:
                return sectionCell
                
            default:
                break
            }
        case .PBDriverDetails:
            
            switch rowType {
            case .PBHeaderSec:
                let cell: PBDriverDetailsCell = tableView.dequeueReusableCell(withIdentifier: "PBDriverDetailsCell") as! PBDriverDetailsCell
                let total = bookingArray?.bookingAmt
                if bookingArray?.distanceTime != 0 {
                    let min = (bookingArray?.distanceTime)! / 60
                    let sec = ((bookingArray?.distanceTime)! - (min * 60))
                    if sec < 10 {
                        cell.deliveryTime.text = "0\(min) : 0\(sec) Minutes"
                    } else {
                        cell.deliveryTime.text = "0\(min) : \(sec) Minutes"
                    }
                } else {
                    cell.deliveryTime.text = "- : - Minutes"
                }
//                cell.deliveryTime.text = bookingArray?.deliveryTime
                //invoiceDetails.distanceTravelled
//                cell.refButton.layer.borderWidth = 0.5
//                cell.refButton.layer.borderColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1).cgColor
//                cell.refButton.layer.shadowOffset = CGSize.zero
//                cell.refButton.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.07).cgColor
//                cell.refButton.layer.shadowOpacity = 1
//                cell.refButton.layer.shadowRadius = 5
                //                cell.distance.text = invoiceDetails.distanceTravelled
                cell.dropAddress.text  = bookingArray?.dropAddress
                let address = "\((bookingArray?.pickCustName)!): \((bookingArray?.pickAddress)!)"
                let range = NSMakeRange((bookingArray?.pickCustName.count)!, (address.count - (bookingArray?.pickCustName.count)!))
                cell.pickAddress.attributedText = Helper.historyAttributedString(from: address, nonBoldRange: range)
//                cell.pickAddress.text  = bookingArray?.pickAddress
                cell.pickUpTime.text = (bookingArray?.distanceDriver)! + Utility.DistanceUnits
                //invoiceDetails.durationTravlled
                //                cell.timeDuration.text = invoiceDetails.durationTravlled
                cell.deliveryTimeLbl.text = "Time"
                cell.rating.text       = bookingArray?.custRate
//                cell.totalAmount.text  = Utility.currencySymbol + "\(total!)";
//                cell.driverEarning.text = Utility.currencySymbol + (bookingArray?.driverEarning)!
                cell.senderPhone.text  = bookingArray?.pickPhone
                cell.senderName.text   = bookingArray?.dropCustName
                cell.distanceUnits.text = "Distance"
                return cell
                
            case .PBRowDefault:
                return sectionCell
                
            case .PBHeader:
                return header
                
            default:
                break
            }
            
        case .PBBillDetails:
            let cell :PBBillDetailsCell = tableView.dequeueReusableCell(withIdentifier: "PBBillDetailsCell") as! PBBillDetailsCell
            
            if let products = bookingArray?.productDetails[indexPath.row] as? [String: Any] {
                
                if let itemName = products["itemName"] as? String {
                    var quantity = 0
                    if let quant = products["quantity"] as? Int {
                        quantity = quant
                    }
                    var unitName = ""
                    if let name = products["unitName"] as? String {
                        unitName = name
                    }
                    cell.key.text = itemName + " (\(quantity) \(unitName))"
                }
                
                if let itemPrice = products["finalPrice"] as? Int {
                    cell.value.text = Utility.currencySymbol + "\(itemPrice)"
                }
            }
            
            return cell
            
        case .PBGrandTot:
            switch rowType {
            case .PBHeaderSec:
                let cell : GrandTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "grandtotal") as! GrandTotalTableViewCell
                let total = bookingArray?.bookingAmt
                cell.grandTotalLbl.text = "Grand Total: "
                cell.grandTotal.text = Utility.currencySymbol + "\(total!)";
                return cell
            case .PBRowDefault:
                let cell: PBSectionCell = tableView.dequeueReusableCell(withIdentifier: "PBSectionCell") as! PBSectionCell
                cell.upperborder.isHidden = true
                return cell
                
            default:
                break
            }
            
            
        case .PBPaymentType:
            
            switch rowType {
            case .PBHeaderSec:
                let cell :PBPaymentTypeCell = tableView.dequeueReusableCell(withIdentifier: "PBPaymentTypeCell") as! PBPaymentTypeCell
                let paymentType = bookingArray?.paymentType
                let paymentTypeByWallet = bookingArray?.payByWallet
                
                //  if paymentType == 1  && payByWallet==1{
//                cell.cashOrCardOption.text = "CARD + Wallet"
//            }
                
                if paymentTypeByWallet == 0 {
                    if paymentType == 1 {
                       cell.cashOrCardOption.text = "CARD"
                    }else if paymentType == 2 {
                       cell.cashOrCardOption.text = "CASH"
                    }else {
                        cell.cashOrCardOption.text = "WALLET"
                    }
                }else {
                    if paymentType == 1 {
                        cell.cashOrCardOption.text = "CARD + WALLET"
                    }else if paymentType == 2 {
                        cell.cashOrCardOption.text = "CASH + WALLET"
                    }else {
                        cell.cashOrCardOption.text = "WALLET"
                    }
                }
              
                return cell
                
                
            case .PBRowDefault:
                return sectionCell
                
            default:
                break
            }
            
        case .PBReceiversDetails:
            
            switch rowType {
            case .PBHeaderSec:
                let cell :PBReceiversDetailsCell = tableView.dequeueReusableCell(withIdentifier: "PBReceiversDetailsCell") as! PBReceiversDetailsCell
                
                cell.signatureImage.kf.setImage(with: URL(string: (bookingArray?.signature)!),
                                                placeholder:UIImage.init(named: "profile"),
                                                options: [.transition(ImageTransition.fade(1))],
                                                progressBlock: { receivedSize, totalSize in
                },
                                                completionHandler: { image, error, cacheType, imageURL in
                })
                
                cell.receiverName.text  = bookingArray?.dropCustName
                cell.receiverNumber.text  = bookingArray?.pickPhone
                return cell
                
            case .PBRowDefault:
                return sectionCell
                
            default:
                break
            }
        }
        
        return UITableViewCell()
    }
}

extension PastBookingController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension PastBookingController : showTheVehicleDocPopupDelegate{
    func showVehicleGalley(gallery: INSPhotosViewController) {
        self.present(gallery, animated: true, completion: nil)
    }
}
