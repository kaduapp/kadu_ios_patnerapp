//
//  PastBookingDetailsVCViewController.swift
//  GrocerPartner
//
//  Created by Rahul Sharma on 20/06/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class PastBookingDetailsVC: UIViewController {
    
    @IBOutlet weak var orderCompleted: UILabel!
    @IBOutlet weak var bookingDateTimeLbl: UILabel!
    @IBOutlet weak var bookingIdLbl: UILabel!
    var bookingArray:HistoryModel?
    var invoiceDetails = BookingModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        bookingDateTimeLbl.text = Helper.changeDateFormatForHistory((bookingArray?.bookingDateNtime) ?? "2018-07-10 15:45:48" )
        bookingIdLbl.text = String(describing: bookingArray?.bookingId ?? 0)
        self.orderCompleted.text = "Order Completed".localized
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
