//
//  PastBookingDetailsTVExtension.swift
//  GrocerPartner
//
//  Created by Rahul Sharma on 20/06/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import Kingfisher

extension PastBookingDetailsVC: UITableViewDataSource {
    
    enum PastBookingCellsType: Int {
        case Amount = 0
        case Address = 3
        case OrderDetailHeader = 4
        case OrderDetail = 5
        case customerDetails = 2
        case Charges = 6
        case PaymentType = 7
        case EarningAndCommision = 8
        case TypeOfDelivery = 1
        static var count: Int {
            return PastBookingCellsType.EarningAndCommision.rawValue + 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return PastBookingCellsType.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionType : PastBookingCellsType = PastBookingCellsType(rawValue: section)!
        switch sectionType {
            
        case .Amount:
            return 1
        case .Address:
            return (1 + 1)
        case .OrderDetailHeader:
            return 1
        case .OrderDetail:
            return (bookingArray?.productDetails.count) ?? 0
        case .Charges:
            return 8
        case .PaymentType:
            let paymentType = bookingArray?.paymentType
            let payByWallet = bookingArray?.payByWallet
            
            if payByWallet == 0 {
                if paymentType == 1 {
                    return 3
                }else if paymentType == 2 {
                    return 3
                }else {
                    return 3
                }
                
            }else {
                if paymentType == 1 {
                    return 4
                }else if paymentType == 2 {
                    return 4
                }else {
                    return 4
                }
            }
            
        case .EarningAndCommision:
            return (3 + 1)
        case .TypeOfDelivery:
            return 2
            
        case .customerDetails:
            return 2
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! PastHeaderTableViewCell
        
        let sectionType : PastBookingCellsType = PastBookingCellsType(rawValue: indexPath.section)!
        switch sectionType {
            
        case .Amount:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AmountCell", for: indexPath) as! PastAmountTableViewCell
            
            cell.amountLbl.text = Utility.currencySymbol + String(describing: bookingArray?.bookingAmt ?? 0)
            cell.distanceLbl.text = "Distance : " + String(describing:bookingArray?.distanceDriver ?? "0") + " " + Utility.DistanceUnits
            
            if bookingArray?.distanceTime != 0 {
                var min = 0
                var sec = 0
                var hour = 0
                
                sec = (bookingArray?.distanceTime)!
                
                if sec > 60 {
                    min = sec / 60
                    sec = (sec - (min * 60))
                }
                
                if min > 60 {
                    hour = min / 60
                    min = (min - (hour * 60))
                }
                
                if hour > 0 {
                    if hour < 10 {
                        if min < 10 {
                            if sec < 10 {
                                cell.timeLbl.text = "Time : " + "0\(hour) hr : 0\(min) min : 0\(sec) sec"
                            } else {
                                cell.timeLbl.text = "Time : " + "0\(hour) hr : 0\(min) min : \(sec) sec"
                            }
                        } else {
                            if sec < 10 {
                                cell.timeLbl.text = "Time : " + "0\(hour) hr : \(min) min : 0\(sec) sec"
                            } else {
                                cell.timeLbl.text = "Time : " + "0\(hour) hr : \(min) min : \(sec) sec"
                            }
                        }
                    } else {
                        if min < 10 {
                            if sec < 10 {
                                cell.timeLbl.text = "Time : " + "\(hour) hr : 0\(min) min : 0\(sec) sec"
                            } else {
                                cell.timeLbl.text = "Time : " + "\(hour) hr : 0\(min) min : \(sec) sec"
                            }
                        } else {
                            if sec < 10 {
                                cell.timeLbl.text = "Time : " + "\(hour) hr : \(min) min : 0\(sec) sec"
                            } else {
                                cell.timeLbl.text = "Time : " + "\(hour) hr : \(min) min : \(sec) sec"
                            }
                        }
                    }
                } else {
                    if min < 10 {
                        if sec < 10 {
                            cell.timeLbl.text = "Time : " + "0\(min) min : 0\(sec) sec"
                        } else {
                            cell.timeLbl.text = "Time : " + "0\(min) min : \(sec) sec"
                        }
                    } else {
                        if sec < 10 {
                            cell.timeLbl.text = "Time : " + "\(min) min : 0\(sec) sec"
                        } else {
                            cell.timeLbl.text = "Time : " + "\(min) min : \(sec) sec"
                        }
                    }
                }
            } else {
                cell.timeLbl.text = "Time : " + "- : - Minutes"
            }
            
            return cell
        case .Address:
            switch indexPath.row {
                
            case 0:
                headerCell.titleLbl.text = "ORDER DETAILS".localized
                return headerCell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath) as! PastAddressTableViewCell
                if(bookingArray?.storType == 7) {
                      cell.storeName.text = "Pick Up"
                    cell.storeAddress.text = bookingArray!.customerPickupAddress
                    cell.customerName.text = "Delivery"
                }else {
                      cell.storeName.text = bookingArray?.pickCustName ?? ""
                    cell.storeAddress.text = bookingArray?.pickAddress ?? ""
                    cell.customerName.text = bookingArray?.dropCustName ?? ""
                }
                cell.deliveryAddress.text = bookingArray?.dropAddress ?? ""
                
                return cell
            default:
                return UITableViewCell()
            }
        case .OrderDetailHeader:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailHeaderCell", for: indexPath) as! PastOrderDetailTableViewCell
            if(bookingArray?.storType == 7) {
               cell.priceLabelCurrency.text = " QUANTITY "
                cell.QTYLabel.isHidden = true
            }else {
                cell.priceLabelCurrency.text = " PRICE ".localized + "( " + Utility.currencySymbol + " )"
            }
            cell.storeName.text = bookingArray?.pickCustName ?? ""
            cell.storeAddress.text = bookingArray?.pickAddress ?? ""
            
            cell.storeImage.kf.setImage(with: URL(string: bookingArray?.storeLogo ?? ""),
                                        placeholder:UIImage.init(named: "menuslider_defaultimag"),
                                        options: [.transition(ImageTransition.fade(1))],
                                        progressBlock: { receivedSize, totalSize in
            },
                                        completionHandler: { image, error, cacheType, imageURL in
            })
            
            return cell
        case .OrderDetail:
         
            let cell = tableView.dequeueReusableCell(withIdentifier: "MainOrderCell", for: indexPath) as! PastMainOrderDetailTableViewCell
            
            if(bookingArray?.storType == 7) {
                cell.quantity.isHidden = true
                cell.price.textAlignment = .center
            }
            
            if let products = bookingArray?.productDetails[indexPath.row]{
                
                if let itemName = products["itemName"] as? String {
                    
                    var unitName = ""
                    if let name = products["unitName"] as? String {
                        unitName = name
                    }
                    
                    cell.productName.text = itemName + " \(unitName)"
                }
                
                if let quant = products["quantity"] as? Int {
                    cell.price.text = String(describing: quant)
                }
                
                if let itemImageURL = products["itemImageURL"] as? String{
                    if itemImageURL == ""{
                        
                    }else{
                    let url = URL(string: itemImageURL)
                    DispatchQueue.global().async { [weak self] in
                        if let data = try? Data(contentsOf: url!) {
                            if let image = UIImage(data: data) {
                                DispatchQueue.main.async {
                                    cell.iteamImageView.image = image
                                }
                            }
                        }
                    }
                    }
                }
                
                if let itemPrice = products["finalPrice"] as? Int {
                    if(bookingArray?.storType == 7) {
                       
                    }else {
                        cell.price.text = Utility.currencySymbol + String(format: "%0.2f ",Double(itemPrice))
                    }
                }
            }
            
            
            return cell
        case .Charges:
            
            
            switch indexPath.row {
                
            case 0:
                headerCell.titleLbl.text = "PAYMENT BREAKDOWN".localized
                return headerCell
                
            case 1:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "PriceValueCell", for: indexPath) as! PastPriceValueTableViewCell
                cell.amountKey.text = "Sub Total".localized
                cell.amount.text = Utility.currencySymbol + String(format: "%0.2f", Double(bookingArray?.subTotal ?? 0.00))
                
                return cell
                
            case 2:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "PriceValueCell", for: indexPath) as! PastPriceValueTableViewCell
                cell.amountKey.text = "Tax".localized
                cell.amount.text = Utility.currencySymbol + String(format: "%0.2f ",Double(bookingArray?.taxAmount ?? 0.00))
                return cell
                
            case 3:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "PriceValueCell", for: indexPath) as! PastPriceValueTableViewCell
                cell.amountKey.text = "Delivery Charges".localized
                cell.amount.text = Utility.currencySymbol +
                    String(format: "%0.2f ",Double(bookingArray?.deliveryCharge ?? 0.00))
                return cell
                
            case 4:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "PriceValueCell", for: indexPath) as! PastPriceValueTableViewCell
                cell.amountKey.text = "Discount".localized
                cell.amount.text = Utility.currencySymbol + String(format: "%0.2f ",Double(bookingArray?.discount ?? 0.00))
                return cell
            case 5:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "PriceValueCell", for: indexPath) as! PastPriceValueTableViewCell
                cell.amountKey.text = "Weight".localized
                cell.amount.text = "2" + "KG"
                return cell
            case 6:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "PriceValueCell", for: indexPath) as! PastPriceValueTableViewCell
                cell.amountKey.text =  "Cleaning Fee".localized
                cell.amount.text = Utility.currencySymbol +  String(format: "%0.2f", Double(bookingArray?.subTotal ?? 0.00))
                return cell
                
            default:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "PriceValueCell", for: indexPath) as! PastPriceValueTableViewCell
                cell.amountKey.text = "Total".localized
                let newS = bookingArray?.bookingAmt ?? 0
                cell.amount.text = Utility.currencySymbol + String(format: "%.2f", newS.doubleValue)
                
                //                    Utility.currencySymbol + String(describing: bookingArray?.bookingAmt ?? 0)
                return cell
                
            }
        case .PaymentType:
            switch indexPath.row {
                
            case 0:
                headerCell.titleLbl.text = "PAYMENT METHOD".localized
                return headerCell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTypeCell", for: indexPath) as! PastPaymentTypeTableViewCell
                
                cell.paymentType.text = "Cash".localized
                cell.paymentTypeImage.image = #imageLiteral(resourceName: "cash")
                //                cell.amount.text = Utility.currencySymbol + " " + String(describing: bookingArray?.paymentCash ?? 0)
                cell.amount.text = Utility.currencySymbol + String(format: "%0.2f ",Double(bookingArray?.paymentCash ?? 0.00))
                
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTypeCell", for: indexPath) as! PastPaymentTypeTableViewCell
                
                cell.paymentType.text = "Card".localized
                cell.paymentTypeImage.image = #imageLiteral(resourceName: "card")
                cell.amount.text = Utility.currencySymbol + String(format: "%0.2f ",Double(bookingArray?.paymentCard ?? 0.00))
                
                
                return cell
                
            default:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTypeCell", for: indexPath) as! PastPaymentTypeTableViewCell
                
                cell.paymentType.text = "Wallet".localized
                cell.paymentTypeImage.image = #imageLiteral(resourceName: "wallet_icon")
                cell.amount.text = Utility.currencySymbol + String(format: "%0.2f ",Double(bookingArray?.paymentWallet ?? 0.00))
                
                return cell
            }
        case .EarningAndCommision:
            switch indexPath.row {
                
            case 0:
                headerCell.titleLbl.text = "EARNINGS AND COMMISSION".localized
                return headerCell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PriceValueCell", for: indexPath) as! PastPriceValueTableViewCell
                
                cell.amountKey.text = "Driver earning".localized
                cell.amount.text = Utility.currencySymbol + " " + String(format: "%0.2f ",Double(bookingArray?.driverEarning ?? 0.00))
                
                return cell
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PriceValueCell", for: indexPath) as! PastPriceValueTableViewCell
                
                cell.amountKey.text = "Store earning"
                cell.amount.text = Utility.currencySymbol + " " + String(describing: bookingArray?.storeEarning ?? 0.00)
                
                return cell
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PriceValueCell", for: indexPath) as! PastPriceValueTableViewCell
                
                cell.amountKey.text = "App commission"
                cell.amount.text = Utility.currencySymbol + " " + String(describing: bookingArray?.appCommission ?? 0)
                
                return cell
            }
        case .TypeOfDelivery:
            switch indexPath.row {
            case 0:
                headerCell.titleLbl.text = "ORDER DETAILS".localized
                return headerCell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PriceValueCell", for: indexPath) as! PastPriceValueTableViewCell
                cell.amountKey.text = "Type Of Delivery".localized
                if bookingArray?.storType == 5 {
                    cell.amount.text = bookingArray?.storeTypeMsg
                }else if bookingArray?.storType == 1 {
                    cell.amount.text = bookingArray?.storeTypeMsg
                }else {
                    cell.amount.text = bookingArray?.storeTypeMsg
                }
                return cell
            }
            
        case .customerDetails:
            switch indexPath.row {
                
            case 0:
                headerCell.titleLbl.text = "CUSTOMER DETAILS".localized
                return headerCell
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PriceValueCell", for: indexPath) as! PastPriceValueTableViewCell
                
                cell.amountKey.text = "Customer Name"
                cell.amount.text = bookingArray?.dropCustName ?? ""
                return cell
            }
        }
    }
    
}

extension PastBookingDetailsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let sectionType : PastBookingCellsType = PastBookingCellsType(rawValue: indexPath.section)!
        switch sectionType {
            
        case .Amount:
            return 60
        case .Address:
            switch indexPath.row {
                
            case 0:
                return 40
            case 1:
                return 140
            default:
                return 44
            }
        case .OrderDetailHeader:
            //            return 60
            if bookingArray?.storType == 5 {
                return 0
            }else {
                return 90
            }
        case .OrderDetail:
            if bookingArray?.storType == 5 {
                return 0
            }else {
                return 60
            }
            
        case .Charges:
            if bookingArray?.storType == 5 {
                switch indexPath.row {
                    
                    
                case 0:
                    return 40
                case 1,2,4,5 :
                    return 0
                default:
                    return 40
                }
            }else if bookingArray?.storType == 7 {
                switch indexPath.row {
                case 1,5,6 :
                    return 0
                default:
                    return 40
                }
            }else {
                switch indexPath.row {
                    
                case 0:
                    return 40
                case 5:
                    return 0
                case 6:
                    return 0
                default:
                    return 40
                }
            }
            
        case .PaymentType:
            
            let paymentType = bookingArray?.paymentType
            let payByWallet = bookingArray?.payByWallet
            
            if payByWallet == 0 {
                if paymentType == 1 {
                    switch indexPath.row {
                        
                    case 0:
                        return 40
                    case 1:
                        return 0
                    case 2:
                        return 40
                    default:
                        return 0
                    }
                }else if paymentType == 2 {
                    switch indexPath.row {
                        
                    case 0:
                        return 40
                    case 1:
                        return 40
                    case 2:
                        return 0
                    default:
                        return 0
                    }
                }else {
                    switch indexPath.row {
                        
                    case 0:
                        return 40
                    case 1:
                        return 0
                    case 2:
                        return 0
                    default:
                        return 40
                    }
                }
                
            }else {
                if paymentType == 1 {
                    switch indexPath.row {
                        
                    case 0:
                        return 40
                    case 1:
                        return 0
                    case 2:
                        return 40
                    default:
                        return 40
                    }
                }else if paymentType == 2 {
                    switch indexPath.row {
                        
                    case 0:
                        return 40
                    case 1:
                        return 40
                    case 2:
                        return 40
                    default:
                        return 40
                    }
                }else {
                    switch indexPath.row {
                        
                    case 0:
                        return 40
                    case 1:
                        return 0
                    case 2:
                        return 0
                    default:
                        return 40
                    }
                }
            }
            
            
        case.EarningAndCommision:
            switch indexPath.row {
                
            case 0:
                return 40
            case 1:
                return 40
            default:
                return 0
            }
        case .TypeOfDelivery:
            switch indexPath.row {
            case 0:
                return 0
            default:
                return 44
            }
        case .customerDetails:
            
            if bookingArray?.storType == 7 {
                return 44
            }
            return 0
        }
        
    }
    
    
}


