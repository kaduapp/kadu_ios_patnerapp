//
//  PastMainOrderDetailTableViewCell.swift
//  GrocerPartner
//
//  Created by Rahul Sharma on 02/07/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class PastMainOrderDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var iteamImageView: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var price: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
