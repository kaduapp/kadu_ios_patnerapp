//
//  PastAmountTableViewCell.swift
//  GrocerPartner
//
//  Created by Rahul Sharma on 20/06/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class PastAmountTableViewCell: UITableViewCell {

    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
