//
//  PastPaymentTypeTableViewCell.swift
//  GrocerPartner
//
//  Created by Rahul Sharma on 02/07/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class PastPaymentTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var paymentTypeImage: UIImageView!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var paymentType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
