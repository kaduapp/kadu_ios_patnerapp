//
//  PastHeaderTableViewCell.swift
//  GrocerPartner
//
//  Created by Rahul Sharma on 20/06/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class PastHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
