//
//  PastAddressTableViewCell.swift
//  GrocerPartner
//
//  Created by Rahul Sharma on 20/06/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class PastAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var deliveryAddress: UILabel!
    @IBOutlet weak var storeAddress: UILabel!
    @IBOutlet weak var storeName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
//        let shapelayer = CAShapeLayer()
//        let path = UIBezierPath()
//        path.move(to: CGPoint(x: 35, y: 51))
//        path.addLine(to: CGPoint(x: 35, y: 81))
//
//        shapelayer.strokeColor = UIColor.black.cgColor
//        shapelayer.lineWidth = 1.0 / UIScreen.main.scale
//        shapelayer.lineJoin = kCALineJoinMiter
//        shapelayer.lineDashPattern = [1, 1]
//        shapelayer.path = path.cgPath
//        layer.addSublayer(shapelayer)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
