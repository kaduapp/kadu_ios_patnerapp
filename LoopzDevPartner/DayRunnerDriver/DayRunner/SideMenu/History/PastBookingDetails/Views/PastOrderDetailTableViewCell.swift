//
//  PastOrderDetailTableViewCell.swift
//  GrocerPartner
//
//  Created by Rahul Sharma on 20/06/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class PastOrderDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var hideDetailButton: UIButton!
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var itemDetailsLabel: UILabel!
    
    @IBOutlet weak var QTYLabel: UILabel!
    @IBOutlet weak var itemsLabel: UILabel!
    @IBOutlet weak var priceLabelCurrency: UILabel!
    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var storeAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        hideDetailButton.layer.borderWidth = 1.0
        if #available(iOS 11.0, *) {
            hideDetailButton.layer.borderColor = Colors.baseColor.cgColor
            hideDetailButton.setTitleColor(Colors.baseColor, for: .normal)
        
        } else {
            // Fallback on earlier versions
        }
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
