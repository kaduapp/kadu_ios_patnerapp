//
//  HistoryChartVC.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 25/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import Charts

extension HistoryViewController{
    
    
    func makeTheTotalForWeeks(responseData:[String: Any]){
        let weekStartEnd = arrayOfWeeks[selectedWeek]
        weeksTotal = [Double]()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var today = dateFormatter.date(from: weekStartEnd.first!)
        
        
        var dateArray = [String]()
        for _ in 1...7{
            let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today!)
            let date = DateFormatter()
            date.dateFormat = "yyyy-MM-dd"
            let stringDate : String = date.string(from: today!)
            today = tomorrow!
            dateArray.append(stringDate)
        }
        
        for val in 0...6{
            let data = dateArray[val]
            let total = responseData[data] as? Double
            if total != nil {
                weeksTotal.append(total!)
            } else {
                weeksTotal.append(0.00)
            }
            
            
        }
        
        months = [HistoryAlerts.Sun,"MON".localized,"TUE".localized,"WED".localized,
                  "THU".localized,"FRI".localized,"SAT".localized]
        
        setChart(dataPoints: months, values: weeksTotal, maxValue: weeksTotal.max()!)
        barGraph.animate(yAxisDuration: 1.5, easingOption: .easeInOutQuart)
    }
    
    
    // this function use it to set up the chart
    func setChart(dataPoints:[String], values:[Double] , maxValue:Double)  {
        barGraph.noDataText = "You need to provide data for the chart.".localized
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x:Double(i), yValues: [Double(values[i])])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(entries: dataEntries, label: "Booking data".localized + "(" + Utility.currencySymbol + ")")
        chartDataSet.colors = [Helper.getUIColor(color: Colors.AppBaseColor)]
        let chartData = BarChartData(dataSets: [chartDataSet]) //BarChartData()
        
        chartData.barWidth = 0.9
        barGraph.data = chartData
        barGraph.xAxis.valueFormatter = IndexAxisValueFormatter(values:dataPoints)
        barGraph.xAxis.granularityEnabled = true
        barGraph.xAxis.drawGridLinesEnabled = false
        barGraph.xAxis.labelPosition = .bottom
        barGraph.xAxis.granularity = 2
        barGraph.leftAxis.enabled = true
        
        barGraph.data?.setDrawValues(false)
        barGraph.pinchZoomEnabled = true
        barGraph.scaleYEnabled = true
        barGraph.scaleXEnabled = true
        barGraph.highlighter = nil
        barGraph.doubleTapToZoomEnabled = true
        barGraph.chartDescription?.text = ""
        barGraph.rightAxis.enabled = false
        
        barGraph.leftAxis.axisMinimum = 0.0
        
        if maxValue < 100 {
            barGraph.leftAxis.axisMaximum = 100
        }else{
            barGraph.leftAxis.axisMaximum = maxValue
        }
        barGraph.xAxis.axisMaximum = 7.0
        barGraph.xAxis.axisMinimum = -1.0
        
    } // end of setchart function
    
    
}
