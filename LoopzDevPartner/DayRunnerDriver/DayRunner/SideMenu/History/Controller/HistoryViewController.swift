//
//  HistoryViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Charts




class HistoryViewController: UIViewController {
    
    
    @IBOutlet var noBookingHistLabel: UILabel!
    @IBOutlet var historyTableView: UITableView!
    @IBOutlet var menuButton: UIButton!
    @IBOutlet var weeksCollectionView: UICollectionView!
    @IBOutlet var barGraph: BarChartView!
    @IBOutlet weak var bookingHistoryLabel: UILabel!
    
    var bookingArray = [HistoryModel]()
    var historyMDL = HistoryModel()
    var dateSelected:Int = 0
    var selectedIndex:Int = 0
    var stardDate = String()
    var bookingID: NSNumber = 0
    var endDateSelected = String()
    var searchBar = UISearchBar()
    
    var arrayOfDates = [String]()
    var arrayOfWeeks = [[String]]()
    
    var months: [String]!
    var mount:[Float]!
    var selectedWeek:Int = 0
    
    var weeksTotal = [Double]()
    
    
    var bookingsHist = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ManageBooking.sharedInstance.currentNavigationController = self.navigationController
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
        self.noBookingHistLabel.text = "No Booking history found in this week !!".localized
        self.bookingHistoryLabel.text = "Booking History".localized
        menuButton.setImage(#imageLiteral(resourceName: "whiteMenuIcon"), for: .normal)
        getWeekCount()
        if(Utility.getSelectedLanguegeCode() == "ar") {
            weeksCollectionView.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.weeksCollectionView.scrollToItem(at: IndexPath(item: selectedWeek, section: 0), at: .right, animated: false)
        
    }
    
    
    func AssignedTrips(){
        let weekStartEnd = arrayOfWeeks[selectedWeek]
        let dict : [String : Any] =  ["pageIndex": 0,
                                      "startDate":weekStartEnd.first! + " 00:00:00",
                                      "endDate":weekStartEnd.last! + " 23:59:59"]
        historyMDL.HISTDelegate = self
        historyMDL.historyBookingsAPI(params:dict)
        self.weeksCollectionView.scrollToItem(at: IndexPath(item: selectedWeek, section: 0), at: .right, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuAction(_ sender: Any) {
        if Utility.getSelectedLanguegeCode() == "ar" {
            slideMenuController()?.toggleRight()
        }else {
            slideMenuController()?.toggleLeft()
        }
    }
    
    @IBAction func showTheWeekTransaction(_ sender: Any) {
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "toHistoryDetails"{
            let nav =  segue.destination as! UINavigationController
            let nextScene = nav.viewControllers.first as! PastBookingController?
            nextScene?.bookingArray = bookingArray[selectedIndex]
            nextScene?.invoiceDetails = bookingArray[selectedIndex].invoiceData
        } else if segue.identifier == "toPastDetails" {
            
            let nextScene = segue.destination as! PastBookingDetailsVC
            nextScene.bookingArray = bookingArray[selectedIndex]
            nextScene.invoiceDetails = bookingArray[selectedIndex].invoiceData
        }
    }
}


extension Date {
    var startOfWeek: Date {
        let date = Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
        let dslTimeOffset = NSTimeZone.local.daylightSavingTimeOffset(for: date)
        return date.addingTimeInterval(dslTimeOffset)
    }
    
    var endOfWeek: Date {
        return Calendar.current.date(byAdding: .second, value: 604799, to: self.startOfWeek)!
    }
}

extension HistoryViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "weeks", for: indexPath as IndexPath) as! WeeksCollectionViewCell
        
        if selectedWeek == indexPath.row {
            cell.weeksButton.isSelected = true
        }else{
            
            cell.weeksButton.isSelected = false
        }
        let startDate = Helper.changeDateFormatWithMonth(arrayOfWeeks[indexPath.row][0])
        let endDate = Helper.changeDateFormatWithMonth(arrayOfWeeks[indexPath.row][arrayOfWeeks[indexPath.row].count - 1])
        
        cell.weeksButton.setTitle(startDate + "-" + endDate, for: .normal)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayOfWeeks.count
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let screenSize: CGRect = UIScreen.main.bounds
        return CGSize(width: screenSize.width/3, height: 60)
    }
}

// MARK: - CollectionView delegate method
extension HistoryViewController:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell: WeeksCollectionViewCell = collectionView.cellForItem(at: indexPath) as! WeeksCollectionViewCell
        cell.weeksButton.isSelected = true
        selectedWeek = indexPath.row
        self.AssignedTrips()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if let cell: WeeksCollectionViewCell = collectionView.cellForItem(at: indexPath) as? WeeksCollectionViewCell {
            cell.weeksButton.isSelected = false
        }
    }
}

extension HistoryViewController:HistoryModelDelegate {
    func completedBookingData(data: [HistoryModel], totalEarnDict: [String : Any])
    {
        bookingArray = data
        self.makeTheTotalForWeeks(responseData:totalEarnDict)
        self.historyTableView.reloadData()
    }
}

