//
//  HistoryGetWeeksArrayVC.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 25/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

extension HistoryViewController{
    
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    
    func getWeekCount(){
        let yearStarts = ["2017-01-01","2017-01-02","2017-01-03","2017-01-04","2017-01-05","2017-01-06","2017-01-07"]
        for initialDate in yearStarts {
            let startWeek = self.getDayOfWeek(initialDate)
            if startWeek != 1{
                
            }else{
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let result = formatter.string(from: date)
                arrayOfDates = Dates.printDatesBetweenInterval(Dates.dateFromString(initialDate), Dates.dateFromString(result))
                
                print(arrayOfDates.count)
                var weekDict = [String]()
                for index in 0...arrayOfDates.count-1 {
                    if ((index % 7) == 0) && ((index * 1) != 0) {
                        arrayOfWeeks.append(weekDict)
                        weekDict = [String]()
                        weekDict.append(arrayOfDates[index])
                    }else
                    {
                        weekDict.append(arrayOfDates[index])
                        if index == arrayOfDates.count - 1{
                            arrayOfWeeks.append(weekDict)
                        }
                    }
                }
            }
            
        }
        weeksCollectionView.reloadData()
        
        selectedWeek = arrayOfWeeks.count-1
        let indexPath = IndexPath(item: selectedWeek, section: 0)
 
        self.weeksCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        
    //    weeksCollectionView.scrollToItem(at: IndexPath(row: selectedWeek, section: 0), at: .right, animated: true)
        
        self.AssignedTrips()
    }
    
}
