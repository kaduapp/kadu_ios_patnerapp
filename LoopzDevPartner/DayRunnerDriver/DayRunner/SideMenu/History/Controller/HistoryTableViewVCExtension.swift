//
//  HistoryTableViewVC.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 25/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation


///****** tableview datasource*************//
extension HistoryViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if bookingArray.count != 0 {
            noBookingHistLabel.isHidden = true;
        }else{
            
            noBookingHistLabel.isHidden = false;
        }
        
        return bookingArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"history") as! HistoryTableViewCell!
        
        cell?.bid.text = String(describing:bookingArray[indexPath.row].bookingId)
        
        cell?.dropAddress.text = bookingArray[indexPath.row].dropAddress
        cell?.amount.text = Utility.currencySymbol + Helper.clipDigit(value: Float(bookingArray[indexPath.row].bookingAmt) , digits: 2)
//        "\()"
        cell?.bookingTime.text = Helper.changeDateFormatForHistory(bookingArray[indexPath.row].bookingDateNtime)
        
        if(bookingArray[indexPath.row].storType == 7) {
            cell?.storeName.text = "Pick Up"
            cell?.pickAddress.text = bookingArray[indexPath.row].customerPickupAddress
            cell?.customerName.text = "Delivery"
        }else {
            cell?.storeName.text = bookingArray[indexPath.row].pickCustName
            cell?.pickAddress.text = bookingArray[indexPath.row].pickAddress
            cell?.customerName.text = bookingArray[indexPath.row].dropCustName
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 170;
    }
    
}

///***********tableview delegate methods**************//
extension HistoryViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tableView.deselectRow(at: indexPath, animated: false)
        //performSegue(withIdentifier: "toHistoryDetails", sender: nil)///*** moves to booking history***//
        performSegue(withIdentifier: "toPastDetails", sender: nil)
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier:"headerTableCell") as! HistHeaderTableViewCell!
        cell?.totalAmt.text = "\(Int(weeksTotal.reduce(0, +)))"
        
        return cell?.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 56
    }
    
}

