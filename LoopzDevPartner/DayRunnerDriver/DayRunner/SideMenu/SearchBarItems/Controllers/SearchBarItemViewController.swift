//
//  SearchBarItemViewController.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 7/18/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

protocol pickedItem:class {
    func pickedItem();
}

class SearchBarItemViewController: UIViewController {
    
   @IBOutlet weak var heightOfView: NSLayoutConstraint!
   
    @IBOutlet weak var addItemTableView: UITableView!
    @IBOutlet weak var addItemView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBarTableView: UITableView!
     var shipment:Shipment!
    var shipmentModel:[String:Any] = [:]
    var newDictOfItems:[String:Any] = [:]
    var oldDictOfItems:[String:Any] = [:]
    var delegate:pickedItem?
    var actionHandler: (() -> Void)?
    @IBOutlet weak var hideImage: UIImageView!
     var selectedIndex = -1
    var searchItemModel:[searchItem]!
    var apiModel = searchItems()
    var offset = 0
    var limit = 15
    var searchTextValue = ""
    var language:searchItem!
    var count = 0
    var newItemModel:Item?
//    var myDictionary = Dictionary<String, String>()
//    var oldItemDictionary = Dictionary<String, String>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addItemView.isHidden = true
        searchBar.delegate = self
         self.heightOfView.constant = -200
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
        self.searchBar.becomeFirstResponder()
        self.navigationController?.navigationBar.barTintColor = Helper.getUIColor(color: Colors.AppBaseColor)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
    }
    
    @IBAction func popCloseBtn(_ sender: Any) {
        DispatchQueue.main.async {
            self.addItemView.isHidden = true
            self.navigationController?.navigationBar.isHidden = false
        }
    }
    
    
    @IBAction func actionbuttonBack(_ sender: UIButton) {
//        let controllers = self.navigationController?.viewControllers
//        for vc in controllers! {
//            if vc is PickingItemViewController {
//                _ = self.navigationController?.popToViewController(vc as! PickingItemViewController, animated: true)
//            }
//        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmBtnAction(_ sender: Any) {
        
        
        let myDict:[String:Any] = ["addedToCartOn" : newItemModel?.addedToCartOn ?? 0,
                      "childProductId": newItemModel?.childProductId ?? "",
                      "unitId" : newItemModel?.unitId ?? 0,
                      "unitPrice" : newItemModel?.unitPrice ?? 0,
                      "appliedDiscount" : newItemModel?.appliedDiscount ?? 0,
                      "finalPrice" : newItemModel?.finalPrice ?? 0,
                      "quantity" : newItemModel?.quantity ?? 0,
                      "oldQuantity" : shipmentModel["quantity"] ?? 0
            ]
        
        let oldDict:[String:Any] = ["addedToCartOn" : shipmentModel["addedToCartOn"] ?? 0,
                       "childProductId": shipmentModel["childProductId"] ?? "",
                       "unitId" : shipmentModel["unitId"] ?? 0 ,
                       "unitPrice" : shipmentModel["unitPrice"] ?? 0,
                       "appliedDiscount" : shipmentModel["appliedDiscount"] ?? 0,
                       "finalPrice" : shipmentModel["finalPrice"] ?? 0,
                       "quantity" : newItemModel?.quantity ?? 0,
                       "oldQuantity" : shipmentModel["quantity"] ?? 0
            ]
        

        
        var OrderId: Double!
        if let orderId = shipment.bookingId as? NSNumber {
            OrderId = Double(orderId)
        }
        let params = Shipment.init(oldItems: oldDict ,newItems: myDict,orderId: OrderId,updateType:"2")
            shipment.makeServiceCallUpdateOrder(withParams: params, completionHanler: { (success,gTotal,subtotal) in
                if success {
                    self.shipment.grandTotal = gTotal
                    self.shipment.subTotal = subtotal
                    
                    let controllers = self.navigationController?.viewControllers
                    for vc in controllers! {
                        if vc is PickingItemViewController {
                            
                            _ = self.navigationController?.popToViewController(vc as! PickingItemViewController, animated: true)
                        }
                    }
                } else {
                    
                }
            })
        
        
    }
    
    func reloadData(index:Int) {
        searchBarTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
    }

}
extension SearchBarItemViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchTextValue = searchText
        if searchText.length == 3 {
            apiModel.getSearchItems(storeId: shipment.storeId, index: ("\(self.offset)"), limit: ("\(self.limit)"), search: searchTextValue) { (success) in
                if success{
                    //                    self.language = self.apiModel.searchItemData[0]
                    self.searchItemModel = self.apiModel.searchItemData
                    self.searchBarTableView.reloadData()
                }
            }
           
        }
        if searchText.length == 0 {
           self.searchItemModel.removeAll()
             self.searchBarTableView.reloadData()
//            heightOfView.constant = -200
        }
      
    }
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    
    
    
}
extension SearchBarItemViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == self.addItemTableView) {
           if self.searchItemModel == nil {
                
            }else {
                return searchItemModel[section].units.count
            }
           
        }else {
            
            if self.searchItemModel != nil  {
                if self.searchItemModel.count == 0{
                    self.hideImage.isHidden = false
                }else {
                    self.hideImage.isHidden = true
                    print("items are thereeee")
                    return self.searchItemModel!.count
                }
                
            } else {
                return 0
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == self.addItemTableView) {
             let cell: AddItemTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddItemTableViewCell.self), for: indexPath) as! AddItemTableViewCell
            cell.controller = self
            cell.addBtn.tag = indexPath.row
            if let str = searchItemModel[indexPath.row].units[indexPath.row] as? [String:Any] {
                cell.unitProductName.text = str["name"] as? String
                let tempNumber = str["floatValue"] as! Double
                let stringTemp = String(tempNumber)
                cell.unitPriceCost.text = "Unit Cost : " + Utility.currencySymbol + stringTemp
                
            }
            return cell
        }else {
            let cell: SearchItemTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: SearchItemTableViewCell.self), for: indexPath) as! SearchItemTableViewCell
            cell.controller = self
            cell.addBtn.tag = indexPath.row
            if self.searchItemModel != nil  {
                cell.productNameLabel.text = searchItemModel[indexPath.row].name
                if let str = searchItemModel[indexPath.row].units[0] as? [String:Any] {
                    //                                let tempNumber = str["availableQuantity"] as! Int
                    //                                let stringTemp = String(tempNumber)
                    let unitCost = str["floatValue"] as! Double
                    let unitString = String(unitCost)
                    cell.priceOfProduct.text = "Unit Cost : " + Utility.currencySymbol + unitString
                    //                cell.unitCostLabel.text = "Unit Cost : " + Utility.currencySymbol + " " + unitString
                    //                cell.quantyLabel.text = "\(searchItemModel[indexPath.row].currentQuantity)"
                    cell.addBtn.isHidden = false
                    cell.plusMinusView.isHidden = true
                }
            }
            return cell
        }
//        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(tableView == self.addItemTableView) {
            
        }else {
            if indexPath.row == self.searchItemModel.count - 1 && self.searchItemModel.count > (self.limit * self.offset) + (self.limit - 1) { //you might decide to load sooner than -1 I guess...
                self.offset = self.offset + 1
                apiModel.getSearchItems(storeId: shipment.storeId, index: ("\(self.offset)"), limit: ("\(self.limit)"), search: searchTextValue) { (success) in
                    if success{
                        //                    self.language = self.apiModel.searchItemData[0]
                        self.searchItemModel = self.apiModel.searchItemData
                        self.searchBarTableView.reloadData()
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80;
    }
    
    
    
}

extension SearchBarItemViewController:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.searchBar.resignFirstResponder()
    }
}
