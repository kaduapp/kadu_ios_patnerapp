//
//  AddItemTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 7/24/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class AddItemTableViewCell: UITableViewCell {

    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var unitProductName: UILabel!
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var addBtn: UIButton!
    
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var unitPriceCost: UILabel!
    var controller:UIViewController? = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addBtn.isHidden = false
        self.addView.isHidden = true
        self.addView.layer.borderWidth = 1
        self.addView.layer.borderColor = Helper.UIColorFromRGB(rgbValue: 0x3498DB).cgColor
        // Initialization code
    }

    @IBAction func addBtnAction(_ sender: Any) {
        let con = controller as? SearchBarItemViewController
        con?.selectedIndex = (sender as AnyObject).tag
        let availabilityItems = con?.searchItemModel[(con?.selectedIndex)!].units[(con?.selectedIndex)!]["availableQuantity"] as? Int
        if("\(availabilityItems ?? 0)" == "0"){
            
        }else {
            quantityLabel.text = "1"
            
            if(con?.selectedIndex == -1) {
                con?.selectedIndex = (sender as AnyObject).tag
            }else {
                con?.reloadData(index:(con?.selectedIndex)!)
                con?.selectedIndex = (sender as AnyObject).tag
            }
            self.addView.isHidden = false
        }
      
        
        
    }
    
    @IBAction func minusBtnAction(_ sender: Any) {
        let newCount = Int(quantityLabel.text ?? "0")
        quantityLabel.text = "\(newCount! - 1)"
          let con = controller as? SearchBarItemViewController
        if quantityLabel.text == "0" {
            self.addBtn.isHidden = false
            self.addView.isHidden = true
            self.minusBtn.isUserInteractionEnabled = false
            let con = controller as? SearchBarItemViewController
            con?.searchBar.resignFirstResponder()
           
        }else {
            self.addBtn.isHidden = true
            self.addView.isHidden = false
            self.minusBtn.isUserInteractionEnabled = true
        }
         con?.newItemModel = Item.init(addedToCartOn: 0, childProductId: (con?.searchItemModel[(con?.selectedIndex)!].parentProductId)!, unitId: con?.searchItemModel[(con?.selectedIndex)!].units[(con?.selectedIndex)!]["unitId"] as! String, unitPrice: Int(con?.searchItemModel[(con?.selectedIndex)!].units[(con?.selectedIndex)!]["floatValue"] as! NSNumber), appliedDiscount: 0, finalPrice: Int(con?.searchItemModel[(con?.selectedIndex)!].units[(con?.selectedIndex)!]["floatValue"] as! NSNumber), quantity: Int(quantityLabel.text!) ?? 0 , oldQuantity: con?.shipmentModel["quantity"] as! Int)
        
    }
    
    @IBAction func plusBtnAction(_ sender: Any) {
         let con = controller as? SearchBarItemViewController
        let availabilityItems = con?.searchItemModel[(con?.selectedIndex)!].units[(con?.selectedIndex)!]["availableQuantity"] as? Int
        if("\(availabilityItems ?? 0)" == quantityLabel.text){
            let alert = UIAlertController(title: "Message", message: "These many quantity is available for this product.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            con?.present(alert, animated: true, completion: nil)
        }else if("\(availabilityItems ?? 0)" == "0"){
            let alert = UIAlertController(title: "Message", message: "Products are outofstock", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            con?.present(alert, animated: true, completion: nil)
        }else {
            let newCount = Int(quantityLabel.text ?? "0")
            quantityLabel.text = "\(newCount! + 1)"
            self.minusBtn.isUserInteractionEnabled = true
            self.plusBtn.isUserInteractionEnabled = true
        }
        
        
        con?.newItemModel = Item.init(addedToCartOn: 0, childProductId: (con?.searchItemModel[(con?.selectedIndex)!].parentProductId)!, unitId: con?.searchItemModel[(con?.selectedIndex)!].units[(con?.selectedIndex)!]["unitId"] as! String, unitPrice: Int(con?.searchItemModel[(con?.selectedIndex)!].units[(con?.selectedIndex)!]["floatValue"] as! NSNumber), appliedDiscount: 0, finalPrice: Int(con?.searchItemModel[(con?.selectedIndex)!].units[(con?.selectedIndex)!]["floatValue"] as! NSNumber), quantity: Int(quantityLabel.text!) ?? 0 , oldQuantity: con?.shipmentModel["quantity"] as! Int)
       
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
