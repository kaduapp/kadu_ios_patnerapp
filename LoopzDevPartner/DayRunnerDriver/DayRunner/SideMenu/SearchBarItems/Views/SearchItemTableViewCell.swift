//
//  SearchItemTableViewCell.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 7/18/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import UIKit

class SearchItemTableViewCell: UITableViewCell {

    @IBOutlet weak var quantyLabel: UILabel!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusMinusView: UIView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var priceOfProduct: UILabel!
    
    @IBOutlet weak var productNameLabel: UILabel!
    var controller:UIViewController? = nil
    
    var count:Int = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addBtn.isHidden = false
        self.plusMinusView.isHidden = true
        self.plusMinusView.layer.borderWidth = 1
        self.plusMinusView.layer.borderColor = Helper.UIColorFromRGB(rgbValue: 0x3498DB).cgColor
    }
    
    func setData(data:[String:Any]) {
        
    }

    @IBAction func addBtnAction(_ sender: Any) {
        
        
        DispatchQueue.main.async() {
            let con = self.controller as? SearchBarItemViewController
            con!.searchBar.resignFirstResponder()
            con?.navigationController?.navigationBar.isHidden = true
            con!.addItemView.isHidden = false
            con!.addItemTableView.reloadData()
        }
       
//         let con = self.controller as? SearchBarItemViewController
//        con?.selectedIndex = (sender as AnyObject).tag
//        if((con?.searchItemModel[(con?.selectedIndex )!].units.count)! <= 0) {
//            let con = controller as? SearchBarItemViewController
//            con?.selectedIndex = -1;
//            if(con?.selectedIndex == -1) {
//                con?.selectedIndex = (sender as AnyObject).tag
//            }else {
//                con?.reloadData(index:(con?.selectedIndex)!)
//                con?.selectedIndex = (sender as AnyObject).tag
//            }
//            self.plusMinusView.isHidden = false
//
//        }else {
//
//
//        }

    }
    @IBAction func plushBtnAction(_ sender: Any) {
        
        
        let newCount = Int(quantyLabel.text ?? "0")
        quantyLabel.text = "\(newCount! + 1)"
         self.minusBtn.isUserInteractionEnabled = true
        self.plusBtn.isUserInteractionEnabled = true
        let con = controller as? SearchBarItemViewController
        con?.searchBar.resignFirstResponder()
        con?.heightOfView.constant = -200
    }
    
    @IBAction func minusBtnAction(_ sender: Any) {
         let newCount = Int(quantyLabel.text ?? "0")
          quantyLabel.text = "\(newCount! - 1)"
        
        if quantyLabel.text == "0" {
            self.addBtn.isHidden = false
            self.plusMinusView.isHidden = true
            self.minusBtn.isUserInteractionEnabled = false
            let con = controller as? SearchBarItemViewController
            con?.searchBar.resignFirstResponder()
            con?.heightOfView.constant = -200
        }else {
            self.addBtn.isHidden = true
            self.plusMinusView.isHidden = false
            self.minusBtn.isUserInteractionEnabled = true
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
