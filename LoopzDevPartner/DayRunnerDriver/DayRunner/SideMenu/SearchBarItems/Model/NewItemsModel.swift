//
//  NewItemsModel.swift
//  FlexyPartner
//
//  Created by Rahul Sharma on 7/25/19.
//  Copyright © 2019 3Embed. All rights reserved.
//

import Foundation

struct Item {
    var addedToCartOn:Int
    var childProductId:String
    var unitId:String
    var unitPrice:Int
    var appliedDiscount:Int
    var finalPrice:Int
    var quantity:Int
    var oldQuantity:Int
    
    init(addedToCartOn:Int,childProductId:String,unitId:String, unitPrice:Int, appliedDiscount:Int, finalPrice:Int, quantity:Int, oldQuantity:Int) {
        self.addedToCartOn = addedToCartOn
        self.childProductId = childProductId
        self.unitId = unitId
        self.unitPrice = unitPrice
        self.appliedDiscount = appliedDiscount
        self.finalPrice = finalPrice
        self.quantity = quantity
        self.oldQuantity = oldQuantity
    }
    
}
