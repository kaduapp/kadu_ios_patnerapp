//
//  HomeViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import Firebase
import RxCocoa
import RxSwift

class HomeViewController: UIViewController {
    
    @IBOutlet weak var viewShipment: UIView!
    @IBOutlet var menuButton: UIButton!
    @IBOutlet var historyDetails: UIButton!
    @IBOutlet var offlineOnlineButton: UIButton!
    @IBOutlet var currentLocationButton: UIButton!
    
    @IBOutlet weak var noSlotsShiftLabel: UILabel!
    @IBOutlet weak var slotsSheduleView: UIView!
    @IBOutlet weak var homeSheduleView: UIView!
    @IBOutlet weak var shiftTableView: UITableView!
    
    
    @IBOutlet var currentTableView: UITableView!
    @IBOutlet var heightOfMapView: NSLayoutConstraint!
    
    @IBOutlet var mapView: GMSMapView!
//    var newShipment:shipment!
    var bookingArray = [Home]()
    var pickMarkers: GMSMarker?
    var infoWindow = MapInfoWindow()
    let model   = Home.init() //Home model
    var selectedSectionIndex = 0
   var selectedSectionKey  = 0
    
//    var emptyArray = []
    
    var dateStringFirst = ""
    var dateStringSecond = ""
    var fullString = ""
    
    var dictOfString = [String:[Shipment]]()
    
    var currentLocationMarker: GMSMarker!
    
    var didFindMyLocation = false
    
    //    let locationManager = CLLocationManager()
    var timer       = Timer()
    
    var pickMarkView: UIImageView?
    var tableReloadTime    = Timer()
    var bookings = [[String:Any]]()
    var updateLocIntervals:Int = 0
    
    var window: UIWindow?
    
    var modelOflan:[CancelModelNew]!
    var apiModel = CancelApi()
    
    var language:CancelModelNew!
    
    
    var newBookingMDL = NewBookingData()
    
    var lat:Double = 0.00
    var log:Double = 0.00
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
    
    var rxString: String!
    let  obserca  =  Variable<String> ("")
    
    let manager = CBLManager.sharedInstance()
    var helloSequence = Observable.just("Hello Rx")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noSlotsShiftLabel.isHidden = true
        helloSequence.subscribe { (mm) in
            print(mm)
        }
        
        
   
        let delivertSheduleType:Int = UserDefaults.standard.integer(forKey: "driverScheduleType")

        if delivertSheduleType == 1 {
            DispatchQueue.main.async {
                self.slotsSheduleView.isHidden = false
                self.homeSheduleView.isHidden = true
            }
        }else {
            DispatchQueue.main.async {
                self.slotsSheduleView.isHidden = true
                 self.homeSheduleView.isHidden = false
            }
        }
        
     ManageBooking.sharedInstance.currentNavigationController = self.navigationController
        initiateMap()
        UserDefaults.standard.set(true, forKey: "onHome")
        UserDefaults.standard.set(false, forKey: "noBookings")
        updateLocIntervals = Int(Utility.apiInterval)
        if(MQTT.sharedInstance.isConnected == false)
        {
            MQTT.sharedInstance.createConnection()
        }
        
    }
    
    @IBAction func unwindToContainerVC(segue: UIStoryboardSegue) {
        
    }

    override func viewWillAppear(_ animated: Bool) {
     self.makeTheHomeServiceCall()
        let delivertSheduleType:Int = UserDefaults.standard.integer(forKey: "driverScheduleType")
        if delivertSheduleType == 1 {
            
        }else {
             self.currentTableView.reloadData()
        }
        self.navigationController?.isNavigationBarHidden = true
        super.viewWillAppear(animated)
        locationtracker.delegate = self;
        locationtracker.startLocationTracking()
        newBookingMDL.getConfigData()
        if bookingArray.count == 0{
            self.ifnoBookings()
        }
        // Register to receive notification

        
//        let serviceType = UserDefaults.standard.object(forKey: "serviceType") as! String
//        if(serviceType != "3")
//        {
//            viewServiceType.isHidden = true
//            constraintViewServiceType.constant = 0
//            if(serviceType == "2")
//            {
//                actionButtonShowRide(nil)
//            }
//            else
//            {
//                actionButtonShowShipment(nil)
//            }
//        }
        //        Messaging.messaging().subscribe(toTopic:"Dhruv")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(makeTheHomeServiceCall), name: Notification.Name("gotNewBooking"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(bookingCancelHandling(_:)), name: Notification.Name("cancelledBooking"), object: nil)
        self.mapView.bringSubviewToFront(self.offlineOnlineButton)
//        self.mapView.bringSubview(toFront: self.historyDetails)
        self.mapView.bringSubviewToFront(self.menuButton)
//        self.mapView.bringSubview(toFront: self.currentLocationButton)
        checkTheAppVersion()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        mapView.clear()
        // Stop listening notification
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "gotNewBooking"), object: nil);
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "cancelledBooking"), object: nil);
        self.tableReloadTime.invalidate()
        
    } 
    
    
    @IBAction func unwindToVC1(segue:UIStoryboardSegue) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    ////MARK: - Initiate map
    func initiateMap() {
        //        locationManager.delegate = self
        //        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.animate(toZoom: 15)
        
        let ud = UserDefaults.standard
        if let lat = ud.value(forKey: "currentLat") as? Double{
            if lat == 0.0{
                let position = CLLocationCoordinate2D(latitude: 13.028823, longitude: 77.58963)
                print(position)
                mapView.animate(toLocation: position)
                 currentLocationMarker = GMSMarker(position: position)
            }else{
                let position = CLLocationCoordinate2D(latitude: ud.double(forKey: "currentLat"), longitude: ud.double(forKey:"currentLog" ))
                print(position)
                mapView.animate(toLocation: position)
                 currentLocationMarker = GMSMarker(position: position)
            }
        }else{
            let position = CLLocationCoordinate2D(latitude: 13.028823, longitude:77.58963)
            print(position)
            mapView.animate(toLocation: position)
             currentLocationMarker = GMSMarker(position: position)
        }
       
        
        currentLocationMarker.map = mapView
        currentLocationMarker.groundAnchor = CGPoint(x: 0.5,y: 0.5)
        
//        if let iconData: Data = UserDefaults.standard.object(forKey: "mapIcon") as? Data
//        {
//            currentLocationMarker.icon = UIImage.init(data: iconData, scale: 1)
//            currentLocationMarker.map = mapView
//        }
        //        let iconData = UserDefaults.standard.object(forKey: "mapIcon") as?Data
        
    }
    
    
    
    
    // makes the assigned trip service call and retrives the data
    @objc func makeTheHomeServiceCall(){
        model.assignedBookingAPI() //@locate Home model class
        model.handleTimer(interval: self.updateLocIntervals) //@locate Home model class
        model.HMVCDelegate = self
        
//       let dict = Dictionary.init(grouping:model.productModelData, by:{ $0.slotStartTime
//        })
//
//        print(dict)
//
        
    }
    
    @objc func bookingCancelHandling(_ notification: NSNotification) {
        let bid = "Bid:" + String(describing:notification.userInfo!["bid"]!)
        let msg = notification.userInfo?["msg"] as! String
        Helper.alertVC(errMSG: bid + " " + msg)
    }
    
    //*** reloads the table view to update the time(time to pickup , time to drop) on each booking
    func reloadTableView() {
        self.tableReloadTime.invalidate()
        self.tableReloadTime = Timer.scheduledTimer(timeInterval:60,
                                                    target: self,
                                                    selector: #selector(reloadTableEveryMinutes),
                                                    userInfo: nil,
                                                    repeats: true)
        
    }
    
    
    //*****table reload happens only when bookings available****//
    @objc func reloadTableEveryMinutes(){
        if bookingArray.count != 0 {
            self.currentTableView.reloadData()
        }
    }
    
    ///************ segue identifier method****************//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        updateLocIntervals = Int(Utility.bookingInterval)
        model.handleTimer(interval: updateLocIntervals)
        
        switch segue.identifier! {
            
        case "toInvoiceFromHome":
        if let nextScene = segue.destination as? ProviderInvoiceViewController{
            nextScene.shipmentModel = sender as? Shipment
            }
            break
        case "toBookingVC":
            if let nextScene = segue.destination as? OnBookingViewController {
                var shipmentData = sender as! Shipment
                nextScene.shipmentModel  = shipmentData
                if(shipmentData.orderStatus == 10){
                    nextScene.status = 1
                }else{
                    nextScene.status = 2
                }
            }
            break
        case "homeToInvoice":
            if let nextScene = segue.destination as? InvoiceViewController {
                nextScene.bookingDict   = bookingArray[sender as! Int]
                nextScene.invoiceDetails = bookingArray[sender as! Int].invoiceData
                nextScene.lastLat = lat
                nextScene.lastLog = log
            }
            break
        case "toPickItemFromHome":
            if let nextScene = segue.destination as? PickingItemViewController {
                var shipmentData = sender as! Shipment
                nextScene.shipmentModel  = shipmentData
                if(shipmentData.orderStatus == 11){
                    nextScene.status = 1
                }else if(shipmentData.orderStatus == 8){
                    nextScene.status = 3
                }else{
                    nextScene.status = 2
                }
            }
        case "laundraToPickItemFromHome":
            if let nextScene = segue.destination as? LaundraPickingItemViewController {
                var shipmentData = sender as! Shipment
                nextScene.shipmentModel  = shipmentData
                if(shipmentData.orderStatus == 26){
                    nextScene.status = 1
                }else{
                    nextScene.status = 2
                }
            }
            
        case "toLaundraStart":
            if let nextScene = segue.destination as? LaundraJobStartedVC
            {
                nextScene.shipmentModel =  sender as! Shipment
            }
            
        case "slotsToStores":
            if let nextScene2 = segue.destination as? SlotsStoreViewController
            {
                var shipmentData = model.productModelData[0] as Shipment
                nextScene2.detailsData = model.productModelData
                nextScene2.dateString = fullString
                nextScene2.shipmentModel = shipmentData
                nextScene2.startTimeInd = selectedSectionKey
                nextScene2.slotsMultiArray = model.multiArray
//                nextScene.productDetails = dictOfString
            }
        default:
            if let nextScene = segue.destination as? JobStartedViewController
            {
                nextScene.shipmentModel =  sender as! Shipment
            }
            
            break
        }
    }
    
    func ifnoBookings(){ // if no booking need to clear the booking data and @locate HomeControllerTV
//        let screenSize: CGRect = UIScreen.main.bounds
        let ud = UserDefaults.standard
        ud.removeObject(forKey: USER_INFO.SELBID)
        ud.removeObject(forKey: USER_INFO.BOOKSTATUS)
        ud.removeObject(forKey:  USER_INFO.SELCHN)
        ud.set(false, forKey: "noBookings")
        ud.synchronize()
        heightOfMapView.constant = 0.0
        self.view.layoutIfNeeded()
    }
}


// //MARK: - GMSMapView Delegate
extension HomeViewController: GMSMapViewDelegate
{
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
    
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.infoWindow.removeFromSuperview()
        print("willMove gesture")
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print(marker)
    }
    
    //Tapping marker shows address, bid and call option
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        if marker.userData == nil {
            
        }else {
             print(marker)
            print(marker.userData as Any)
            pickMarkers = marker
            infoWindow.removeFromSuperview()
            infoWindow = loadNiB()
            guard let location = pickMarkers?.position else {
                print("locationMarker is nil")
                return false
            }
            //        if marker == nil {
            //
            //        }else {
            //
            //        }
            
            let dict = pickMarkers?.userData as! [String:Any]
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.addressLabel.text = (dict["address"] as! String)
            infoWindow.titleInfo.text = "Bid :" + String(dict["bid"] as! Int)
            infoWindow.mobileNume = (dict["mobileNum"] as! String)
            //Assigning the data(bid, phone , address) to each marker
            self.mapView.addSubview(infoWindow)
            return true
        }
        return true
 
    }
    
    // MARK: Needed to create the custom info window
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.infoWindow.removeFromSuperview()
    }
}


//Mark: - Home model delegate method
extension HomeViewController : HomeModelDelegate{
    
    
    //Assigned bookings response
    func assignedTripsSucceeded(data: [Home]) {
        bookingArray = data
        if bookingArray.count == 0{
            self.ifnoBookings()
        }
        currentTableView.reloadData()
        shiftTableView.reloadData()
        self.placeTheMarkersOnMap()
        self.reloadTableView()
    }
    
    // check for last updated driver status
    func checkTheDriverStatus(data:Int){
        if data == 3
        {
            UserDefaults.standard.set(3, forKey: USER_INFO.STATUS)
            self.offlineOnlineButton.isSelected = false
            
        }else{
            UserDefaults.standard.set(4, forKey: USER_INFO.STATUS)
            self.offlineOnlineButton.isSelected = true
        }
        
    }
    
    //** Updated driver status reponse
    func updateTheDriverStatus(status: String ,success:Bool) {
        if status == "3" && success {
            self.offlineOnlineButton.isSelected = false
            Helper.playSound(soundName: "Online")
        }else if status == "4" && !success{
            self.offlineOnlineButton.isSelected = false
            Helper.playSound(soundName: "Offline")
        }else{
            self.offlineOnlineButton.isSelected = true
        }
    }
}

//version check delegate
extension HomeViewController:HarpyDelegate{
    func harpyDidShowUpdateDialog() {
        print( #function)
    }
    func harpyUserDidLaunchAppStore() {
        print( #function)
    }
    func harpyUserDidSkipVersion() {
        print( #function)
    }
    
    func harpyUserDidCancel() {
        print( #function)
    }
    
    func harpyDidDetectNewVersionWithoutAlert(_ message: String!) {
        print(message)
    }
}


