//
//  HomebuttonsVC.swift
//  DayRunnerDriverDev
//
//  Created by Vengababu Maparthi on 17/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import GoogleMaps


extension HomeViewController{
    
    //************ open the left menu**************//
    @IBAction func menuAction(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "onHome")
        if Utility.getSelectedLanguegeCode() == "ar" {
            slideMenuController()?.toggleRight()
            
        }else {
            slideMenuController()?.toggleLeft()
        }
    }
    
    //**********get the current location action************//
    @IBAction func getCurrentLocation(_ sender: Any) {
        guard let lat = self.mapView.myLocation?.coordinate.latitude,
            let lng = self.mapView.myLocation?.coordinate.longitude else { return }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 16)
        self.mapView.animate(to: camera)
        
        ////can change the map color "Style" the json file
        
        //        do {
        //
        //            if let styleURL = Bundle.main.url(forResource: "Style", withExtension: "json") {
        //                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
        //            } else {
        //                NSLog("Unable to find style.json")
        //            }
        //        } catch {
        //            NSLog("One or more of the map styles failed to load. \(error)")
        //        }
        
    }
    
    //************ opens the bookingVC**************//
    @IBAction func historyDetailsAction(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "onHome")
        performSegue(withIdentifier: "toBookingHistory", sender: nil)
    }
    //MARK: - Animation Part
    @IBAction func touchDown(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.offlineOnlineButton.frame.size = CGSize(width:self.offlineOnlineButton.frame.size.width + 40, height:self.offlineOnlineButton.frame.size.height + 10)
        }, completion:{ _ in
            UIView.animate(withDuration: 0.3){
                self.offlineOnlineButton.frame.size = CGSize(width:self.offlineOnlineButton.frame.size.width - 40, height:self.offlineOnlineButton.frame.size.height - 10)
            }
        })
    }
    
    @IBAction func goOfflineOnlineAction(_ sender: Any) {  
        
        if offlineOnlineButton.isSelected {
            model.updateMasterStatus(status: "3")
            
//             offlineOnlineButton.setTitle("Go Offline..", for: .normal)
            Helper.showPI(message:"Go Online..")
        }else{
            model.updateMasterStatus(status: "4")
//             offlineOnlineButton.setTitle("Go Online..", for: .normal)
            Helper.showPI(message:"Go Offline..")
        }
    }
    
    // MARK: Needed to create the custom info window (this is optional)
    func loadNiB() -> MapInfoWindow{
        let infoWindow = MapInfoWindow.instanceFromNib() as! MapInfoWindow
        return infoWindow
    }
    
    
    //Storing data into markers
    func placeTheMarkersOnMap(){
        if bookingArray.count != 0 {
            for val in 0...bookingArray.count-1{
                let params : [String : Any] =  [
                    "address": bookingArray[val].pickAddress,
                    "bid":bookingArray[val].bookingId,
                    "mobileNum":bookingArray[val].pickPhone]
                
                let latLongs = bookingArray[val].pickLatlog
                let name = latLongs.components(separatedBy: ",")
                let latit = Double(name[0])
                let logit = Double(name[1])
                let position = CLLocationCoordinate2D(latitude: latit!, longitude: logit!)
                let house = UIImage(named: "home_map_pin_icon")!.withRenderingMode(.alwaysTemplate)
                let markerView = UIImageView(image: house)
                markerView.tintColor = .red
                pickMarkView = markerView
                var marker = GMSMarker()
                marker = GMSMarker(position: position)
                marker.userData = params
                marker.icon = #imageLiteral(resourceName: "home_map_pin_icon")
                marker.map = mapView
            }
        }
    }
    
    //MARK: - Check the app version with server requested version, according to the shows mandatory update or optional update
    func checkTheAppVersion(){
        
        let driverVersion = Utility.appVersion.replacingOccurrences(of: ".", with: "")
        let driverServerVersion = (Utility.serverdriverVer ).replacingOccurrences(of: ".", with: "")
        if Int(driverVersion )! < Int(driverServerVersion )!{
            self.updateTheAppVersion(version: true)
        }else{
            self.updateTheAppVersion(version: false)
        }
        Harpy.sharedInstance().delegate = self
        Harpy.sharedInstance().presentingViewController = self
    }
    
    //Option update or mandatory update
    func updateTheAppVersion(version:Bool){
        if version {
            Harpy.sharedInstance().alertType = HarpyAlertType.force
        }else{
            Harpy.sharedInstance().alertType = HarpyAlertType.option
        }
        Harpy.sharedInstance().isDebugEnabled = true
        Harpy.sharedInstance().checkVersion()
    }
}
