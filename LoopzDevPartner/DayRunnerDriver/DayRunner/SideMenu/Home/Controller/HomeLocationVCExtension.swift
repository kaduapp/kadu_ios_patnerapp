//
//  HomeLocationVC.swift
//  DayRunnerDriverDev
//
//  Created by Vengababu Maparthi on 17/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import GoogleMaps

//// MARK: - Cllocation delegate method
extension HomeViewController: CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse
        {
//            locationManager.startUpdatingLocation()
//            mapView.isMyLocationEnabled = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        print(coord.longitude)
        print(coord.latitude)
        lat = Double(coord.latitude)
        log = Double(coord.longitude)
        UserDefaults.standard.set(lat, forKey:"currentLat")
        UserDefaults.standard.set(log, forKey:"currentLog")
        
        didFindMyLocation = false
        if let location = locations.first
        {
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 16, bearing: 0, viewingAngle: 0)
//            locationManager.stopUpdatingLocation()
        }
    }
}
