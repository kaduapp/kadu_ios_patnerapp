//
//  HomeControllerTV.swift
//  RunnerLive
//
//  Created by Vengababu Maparthi on 30/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

// //MARK: - TableView Datasource method
extension HomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let delivertSheduleType:Int = UserDefaults.standard.integer(forKey: "driverScheduleType")
        
        if delivertSheduleType == 1 {
            if(model.multiDict.reversed().count == 0){
                self.noSlotsShiftLabel.isHidden = false
            }else{
                self.noSlotsShiftLabel.isHidden = true
                return model.multiDict.reversed().count
            }
        }else {
            
        }
        return 1
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let delivertSheduleType:Int = UserDefaults.standard.integer(forKey: "driverScheduleType")
        
        if delivertSheduleType == 1 {
            if(tableView == currentTableView) {
                
            }else {
                if(model.multiDict.reversed().count == 0){
                    return ""
                }else {
                    let sectionHeader = Helper.withDate(from: model.multiDict[section].key)
                    return sectionHeader.0
                }
            }
        }else {
       
        }
        return ""
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let delivertSheduleType:Int = UserDefaults.standard.integer(forKey: "driverScheduleType")
        
        if delivertSheduleType == 1 {
            if(model.multiDict.reversed().count == 0){
                
            }else {
                let filterArray = model.multiDict.reversed()[section].value.compactMap {
                    $0.slotStartTime
                }
                
                let newArray = Array(Set(filterArray))
                var slotsArraySections:[[Shipment]] = []
                for date in newArray {
                    let dateKey = date
                    let filterArray1 = model.multiDict.reversed()[section].value.filter { $0.slotStartTime == dateKey }
                    print(filterArray1)
                    slotsArraySections.append(filterArray1)
                }
                var finalslotsArraySections:[Shipment] = []
                for index in slotsArraySections {
                    finalslotsArraySections.append(index[0])
                }
                
                if(tableView == currentTableView) {
                    let ud = UserDefaults.standard
                    if bookingArray.count == 0{
                        self.ifnoBookings()
                        return model.productModelData.count
                    }else{
                        ud.set(true, forKey: "noBookings")
                        heightOfMapView.constant = 300
                        self.view.layoutIfNeeded()
                        return model.productModelData.count
                    }
                }else if(tableView == shiftTableView){
                    //             self.ifnoBookings()
                    if(slotsArraySections.count == 0) {
                        self.noSlotsShiftLabel.isHidden = false
                    }else {
                        self.noSlotsShiftLabel.isHidden = true
                        return slotsArraySections.count
                    }
                    return 0
                    
                }else {
                    return 10;
                }
            }
        }else {
            if(tableView == currentTableView) {
                let ud = UserDefaults.standard
                if bookingArray.count == 0{
                    self.ifnoBookings()
                    return model.productModelData.count
                }else{
                    ud.set(true, forKey: "noBookings")
                    heightOfMapView.constant = 300
                    self.view.layoutIfNeeded()
                    return model.productModelData.count
                }
            }else if(tableView == shiftTableView){
               
                
            }else {
                return 10;
            }
        }
        return 0;
  
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if(tableView == currentTableView) {
            if tableView.tag == 99{
                let cell = tableView.dequeueReusableCell(withIdentifier:"current") as! CurrentTableViewCell!
                
                let bookingDetails = model.productModelData
                if bookingDetails.count > 0 {
                    let array = bookingDetails[indexPath.row]
                    cell?.updatingTheData(homeDict: array) //@locate currentTableviewCell
                }
                
                return cell!
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier:"upcoming") as! UpcomingTableViewCell!
                return cell!
            }
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier:"ShiftTableViewCell") as! ShiftTableViewCell!
            
            let bookingDetails = model.multiDict.reversed().reversed()[indexPath.section].value
            
            
            if bookingDetails.count > 0 {

                let dateStringFirst =   Helper.dayDifference(from: TimeInterval(bookingDetails[indexPath.row].slotStartTime))
                let dateStringSecond = Helper.dayDifference(from: TimeInterval(bookingDetails[indexPath.row].slotEndTime))
                print("string first"  + "\(dateStringFirst.0)" +  "\(dateStringFirst.1) ")
                print("string first"  + "\(dateStringSecond.0)" +  "\(dateStringSecond.1) ")
                cell?.timeSlotLabel.text = dateStringFirst.1 + " - " + dateStringSecond.1
                
            }
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        let delivertSheduleType:Int = UserDefaults.standard.integer(forKey: "driverScheduleType")
        
        if delivertSheduleType == 1 {
            if(tableView == currentTableView) {
              
            }else {
                return 120
            }
        }else {
            if(tableView == currentTableView) {
                if model.productModelData[indexPath.row].storeType == 5 && !model.productModelData[indexPath.row].bookingTypeLaundra {
                    return 160;
                }
                return 210;
                
            }
        }
        return 0
    }
}

//MARK: - TableView Delegate method
extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(tableView == currentTableView) {
            
            let storeType:Int = UserDefaults.standard.integer(forKey: "storeAccountType")
            
            if storeType == 2 {
                
                let bookingStatus: BookingStatus = BookingStatus(rawValue: model.productModelData[indexPath.row].orderStatus)!
                
                switch  bookingStatus{
                    
                case .onTheWayToPickup:
                    performSegue(withIdentifier: "toPickItemFromHome", sender: model.productModelData[indexPath.row])
                    break
                    
                case .arrivedAtPickup:
                    fallthrough
                case .reachedDropLoc:
                    performSegue(withIdentifier: "toBookingVC", sender: model.productModelData[indexPath.row])
                    break
                case .laundraMart:
                    performSegue(withIdentifier: "toBookingVC", sender: model.productModelData[indexPath.row])
                    break
                case .laundryPickup:
                    performSegue(withIdentifier: "laundraToPickItemFromHome", sender: model.productModelData[indexPath.row])
                    break
                    
                case .deliveryStarted:
                    fallthrough
                case .jobCompleted:
                    
                    if model.productModelData[indexPath.row].storeType == 5 {
                        performSegue(withIdentifier: "laundraToPickItemFromHome", sender: model.productModelData[indexPath.row])
                    }else {
                        performSegue(withIdentifier: "toPickItemFromHome", sender: model.productModelData[indexPath.row])
                    }
                    
                    break
                    
                case .finished:
                    performSegue(withIdentifier: "toInvoiceFromHome", sender: model.productModelData[indexPath.row])
                    break
                    
                case .jobDoneByDriver:
                    
                    performSegue(withIdentifier: "toInvoiceFromHome", sender: model.productModelData[indexPath.row])
                    break
                    
                default:
                    break
                }
            }else {
                
                let bookingStatus: BookingStatus = BookingStatus(rawValue: model.productModelData[indexPath.row].orderStatus)!
                
                switch  bookingStatus{
                    
                case .onTheWayToPickup:
                    
                    if model.productModelData[indexPath.row].storeType == 5 {
                        performSegue(withIdentifier: "toLaundraStart", sender: model.productModelData[indexPath.row])
                    }else {
                        performSegue(withIdentifier: "toStartJob", sender: model.productModelData[indexPath.row])
                    }
                    break
                    
                case .arrivedAtPickup:
                    fallthrough
                case .reachedDropLoc:
                    performSegue(withIdentifier: "toBookingVC", sender: model.productModelData[indexPath.row])
                    break
                case .laundraMart:
                    performSegue(withIdentifier: "toBookingVC", sender: model.productModelData[indexPath.row])
                    break
                case .laundryPickup:
                    performSegue(withIdentifier: "laundraToPickItemFromHome", sender: model.productModelData[indexPath.row])
                    break
                    
                case .deliveryStarted:
                    fallthrough
                case .jobCompleted:
                    
                    if model.productModelData[indexPath.row].storeType == 5 {
                        performSegue(withIdentifier: "laundraToPickItemFromHome", sender: model.productModelData[indexPath.row])
                    }else {
                        performSegue(withIdentifier: "toPickItemFromHome", sender: model.productModelData[indexPath.row])
                    }
                    
                    break
                    
                case .finished:
                    performSegue(withIdentifier: "toInvoiceFromHome", sender: model.productModelData[indexPath.row])
                    break
                    
                case .jobDoneByDriver:
                    
                    performSegue(withIdentifier: "toInvoiceFromHome", sender: model.productModelData[indexPath.row])
                    break
                    
                default:
                    break
                }
            }
        }else {
            if(model.multiDict.reversed().count == 0) {
                
            }else {
                selectedSectionIndex = indexPath.section
                
                let date = NSDate(timeIntervalSince1970: TimeInterval(model.multiDict[indexPath.section].value[indexPath.row].slotStartTime))
                let dayTimePeriodFormatter = DateFormatter()
                dayTimePeriodFormatter.dateFormat = "hh:mm a"
                dateStringFirst = dayTimePeriodFormatter.string(from: date as Date)
                
                let date1 = NSDate(timeIntervalSince1970: TimeInterval(model.multiDict[indexPath.section].value[indexPath.row].slotEndTime))
                let dayTimePeriodFormatter1 = DateFormatter()
                dayTimePeriodFormatter1.dateFormat = "hh:mm a"
                dateStringSecond = dayTimePeriodFormatter1.string(from: date1 as Date)
                fullString = dateStringFirst + "-" + dateStringSecond
                
                print(model.productModelData[0])
                print(model.multiArray )
                
                selectedSectionKey =  model.multiDict[indexPath.section].value[indexPath.row].slotStartTime
                
                for ( i , item ) in model.multiArray.enumerated() {
                    if i == indexPath.section {
                        selectedSectionIndex = indexPath.row
                        model.productModelData = item.value
                        
                        performSegue(withIdentifier: "slotsToStores", sender:model.productModelData)
                    }
                    //                    selectedSectionIndex = indexPath.row
                    //                    model.productModelData = item.value
                    
                    //                }
                }
                
            }
        }
      
    }
}

////MARK: - Slidemenu delegate
extension HomeViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
