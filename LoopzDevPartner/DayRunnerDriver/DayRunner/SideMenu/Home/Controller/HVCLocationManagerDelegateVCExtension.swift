//
//  HVCLocationManagerDelegate.swift
//  RunnerDev
//
//  Created by Rahul Sharma on 25/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

extension HomeViewController: LocationTrackerDelegates
{
    func didUpdateLocation(withLatitude latitude: Float, andLongitude longitude: Float) {
        currentLocationMarker.position = CLLocationCoordinate2D.init(latitude: Double(latitude), longitude: Double(longitude))
        mapView.animate(toLocation: CLLocationCoordinate2D.init(latitude: Double(latitude), longitude: Double(longitude)))
    }

    func didUpdateHeading(to heading: Float) {
//        currentLocationMarker.rotation = Double(heading)
    }
}
