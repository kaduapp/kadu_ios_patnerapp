//
//  MapInfoWindow.swift
//  GoogleMapsCustomInfoWindow
//
//  Created by Sofía Swidarowicz Andrade on 11/5/17.
//  Copyright © 2017 Sofía Swidarowicz Andrade. All rights reserved.
//

import UIKit

class MapInfoWindow: UIView {

    var mobileNume = ""
    
    @IBOutlet weak var titleInfo: UILabel!

    @IBOutlet var addressLabel: UILabel!
    
    @IBAction func callAction(_ sender: Any) {
        let dropPhone = "tel://" + mobileNume
        if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
 
    }
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "MapInfoWindowView", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }
}
