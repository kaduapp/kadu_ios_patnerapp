//
//  ManageBooking.swift
//  DayRunnerDriverDev
//
//  Created by Rahul Sharma on 26/07/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxSwift
public protocol ManageBookingDelegate : NSObjectProtocol {
    func assignedTripResponse(_ response: [String: Any])
}

final class ManageBooking: NSObject {
    
    static let sharedInstance = ManageBooking()
    var checkShipmentBooking = PublishSubject<[[String: Any]]>()
    var currentNavigationController: UINavigationController!
    public var delegate: ManageBookingDelegate?
      let disposeBag = DisposeBag()
    private override init() {
        super.init()
        
        //adding this class for the new booking observation
//        let notificationName = Notification.Name("gotNewBooking")
//        NotificationCenter.default.addObserver(self, selector: #selector(makeServiceCallToGetAppointmentDetails), name: notificationName, object: nil)
    }
    
    func makeServiceCallToGetAppointmentDetails()
    {
        Helper.showPI(message:"Loading..")
        let assignedTrips =  APIs()
        assignedTrips.getAssignedTrips()
        assignedTrips.subject_response.subscribe(onNext: { (response) in
            if response.isEmpty == false {
                   
                if (response["statusCode"] != nil){
                    let statCode:Int = response["statusCode"] as! Int
                    if statCode == 401 {
                        Helper.hidePI()
                        Session.expired()
                        Helper.alertVC(errMSG: "Your Session has beed expired")
                        
                    }else
                    {
                        Helper.alertVC(errMSG: "bad request or  internal server error")
                        Helper.hidePI()
                    }
                    
                }else{
                    Helper.hidePI()
                    let data =  response["data"] as? [String:Any]
                    
                    let status:Int = data!["DriverStatus"] as! Int
//                    if (self.HMVCDelegate != nil) {
//                        self.HMVCDelegate.checkTheDriverStatus(data: status)
//                    }
                    
                    let rideAppointment = data?["appointments"] as! [[String : Any]]
//                    let shipmentAppointment  = data?["delivery_appointments"] as! [[String : Any]]
                    if(rideAppointment.count > 0)
                    {
                        self.parsingTheRideResponse(responseData: rideAppointment[0])
                    }
//                    self.checkShipmentBooking.onNext(shipmentAppointment)
                }
            } else {
                
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
    }
    
    func parsingTheRideResponse(responseData:[String: Any]){
        //        let rideBookingData = OnRideBookingModel.init(bookingData: responseData)
        //
        //        let storyboard = UIStoryboard(name: "Ride", bundle: nil)
        //        let rideBookingController = storyboard.instantiateViewController(withIdentifier: "rideBookingController") as! OnRideBookingViewController;
        //        rideBookingController.rideBookingModel = rideBookingData;
        //        self.currentNavigationController?.pushViewController(rideBookingController, animated: true)
        
    }
    
    func parsingTheRideServiceResponse(responseData:[[String: Any]])
    {
        
        //        var jobData = responseData as Array<Any>
        //        if(jobData.count > 0)
        //        {
        //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //            let rideBookingController = storyboard.instantiateViewController(withIdentifier: "rideBookingController") as! OnRideBookingViewController
        //
        //            rideBookingController.rideBookingModel = OnRideBookingModel.init(bookingData: jobData[0] as! [String:Any])
        //            self.currentNavigationController.pushViewController(rideBookingController, animated: true)
        //        }
    }
    
    func sessionExpried(){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let signIN: SplashViewController? = storyboard.instantiateViewController(withIdentifier: storyBoardIDs.splashVC!) as? SplashViewController
        
        let window = UIApplication.shared.keyWindow
        window?.rootViewController = signIN
        _ = Utility.deleteSavedData()
    }
    
    
}
