//
//  MainModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 18/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import RxSwift
protocol HomeModelDelegate{
    func assignedTripsSucceeded(data: [Home])
    
    func checkTheDriverStatus(data: Int)
    //    func rideTripData(dataModel: OnRideBookingModel)
    func updateTheDriverStatus(status: String, success:Bool)
}


class Home:NSObject {
    
    var HMVCDelegate: HomeModelDelegate! = nil
      let disposeBag = DisposeBag()
    var pickAddress = ""
    var dropAddress = ""
    var estimatedPackageValue = 0.00
    var extraNote = ""
    var bookingDateNtime = ""
    var timeLeftToPick = ""
    var bookingAmt: Int = 0
    var subtotalAmount:Int = 0
    var slotId = ""
    var bidID:Int = 0
    var bookingId = 0
    var statCode = 0
    var pickPhone = ""
    var dropPhone = ""
    var dropCustName = ""
    var pickCustName = ""
    var pickLatlog = ""
    var dropLatlog = ""
    var goodType = ""
    var quantity = 0
    var notes = ""
    var bookingType = ""
    var driverTip = 0.00
    var shipImage:[String] = []
    var custChn = ""
    var invoiceData = BookingModel()
    var bookingsData    = [Home]()
    var helpersCount = 0
    var paidByWhom = 0
    var statusMessage = ""
    var deliveryCharge = 0.00
    var discount = 0.00
    var cashCollect = 0.00
    var storeType = 0
    var storeId = ""
    var slotEndTime = 0
    var slotStartTime = 0
    var expressType:Bool = true
    var storeMessage = ""
    var weightPicked = 0
    var weightText = ""
    var bookingtypeForLaundra:Bool = true
    var bookingtypeMsg = ""
    var productModelData = [Shipment]()
    var multiArray = [(key : Int , value : [Shipment] )]()
     var exculsiveTax: [[String:Any]] = []
    var deliverySplits: [String:Any] = [:]
     var multiDict = [(key: Date, value: [Shipment])]()
    override init()
    {
        super.init()
//        let notificationName = Notification.Name("gotNewBooking")
//        NotificationCenter.default.addObserver(self, selector: #selector(assignedBookingAPI), name: notificationName, object: nil)
        ManageBooking.sharedInstance.checkShipmentBooking.subscribe(onNext: { (reponse) in
            self.parsingTheShipmentResponse(responseData: reponse)
        }, onError: { (Error) in
            
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
    }
    
    var driverStatus: Int = 0
    init(driverStatus: String)
    {
        self.driverStatus = Int(driverStatus)!
    }
    
    
    var  locationtracker:LocationTracker = LocationTracker.sharedInstance() as! LocationTracker
    
    var timer       = Timer()
    
    func updateMasterStatus(status:String) {
        
        //Status: 3 driver online
        // status: 4 driver offline
        
        //        let params : [String : Any] =  [
        //            "ent_status": status]
        
        let params = Home.init(driverStatus: status)
        let homeApi =  HomeApi()
        homeApi.updateStatus(statusCode: params)
        
        homeApi.subject_response.subscribe(onNext: { (response) in
            if response.isEmpty == false {
                if (response["statusCode"] != nil){
                    let statCode:Int = response["statusCode"] as! Int
                    if statCode == 401 {
                        Helper.hidePI()
                        Session.expired()
                        Helper.alertVC(errMSG: "Your Session has beed expired")
                    }else
                    {
                        Helper.hidePI()
                        Helper.alertVC(errMSG: "bad request or  internal server error")
                        
                    }
                }else{
                    if (self.HMVCDelegate != nil) {
                        self.HMVCDelegate.updateTheDriverStatus(status: status, success:true)
                    }
                    if status == "3"
                    {
                        UserDefaults.standard.set(3, forKey: USER_INFO.STATUS)
                         
//                        if MQTT.sharedInstance.isConnected == false {
//                            MQTT.sharedInstance.createConnection()
//                        }
//                        self.handleTimer(interval: Int(Utility.apiInterval))
                        
                    }else{
                        UserDefaults.standard.set(4, forKey: USER_INFO.STATUS)
//                        ///unsubscribing Message channel
//                        if !(MQTT.sharedInstance.manager.subscriptions.isEmpty) {
//                            MQTT.sharedInstance.manager.subscriptions = nil
//                        }
//                        MQTT.sharedInstance.manager.connectToLast()
//                        MQTT.sharedInstance.disconnectMQTTConnection()
//                        self.stopLocationUpdates()
                    }
                }
            } else {
                if (self.HMVCDelegate != nil) {
                    self.HMVCDelegate.updateTheDriverStatus(status: status, success: false)
                }
                if status == "3"
                {
                    UserDefaults.standard.set(3, forKey: USER_INFO.STATUS)
//                    self.handleTimer(interval: Int(Utility.apiInterval))
                    
                }else{
                    UserDefaults.standard.set(4, forKey: USER_INFO.STATUS)
//                    self.stopLocationUpdates()
                }
            }
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
        
        
    }
    
    
    func stopLocationUpdates()  {
        self.locationtracker.stopLocationUpdateTimer();
        
    }
    
    func updateLcocationToServerChannel()  {
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
            self.timer.invalidate()
            //            self.pub.unsubscribeFromPresenceChannel()
            //            self.pub.unsubscribeFromMyChannel()
        }else{
            self.locationtracker.updateLocationToServer()
        }
    }
    
    func handleTimer(interval:Int) {
        //        self.timer.invalidate()
        //                self.pub.unsubscribeFromMyChannel()
        //                self.pub.subscribeToMyChannel()
        //                self.pub.subscribeToPresenceMyChannel()
        //        self.locationtracker.startLocationTracking()
        //        self.timer = Timer.scheduledTimer(timeInterval:TimeInterval(interval),
        //                                          target: self,
        //                                          selector: #selector(updateLcocationToServerChannel),
        //                                          userInfo: nil,
        //                                          repeats: true)
        self.locationtracker.startLocationUpdateTimer(Int32(interval))
        
    }
    
    
    
    func assignedBookingAPI(){
        bookingsData = []
        Helper.showPI(message:"Loading..")
        let assignedTrips =  APIs()
        assignedTrips.getAssignedTrips()
        
        assignedTrips.subject_response.subscribe(onNext: { (response) in
            Helper.hidePI()
            let data =  response["data"] as? [String:Any]
            let checkData = data?["appointments"] as![[String : Any]]
            if checkData.count == 0 || checkData.count == nil {
                UserDefaults.standard.set("0", forKey: "transit") //Bool
            }else {
                UserDefaults.standard.set("1", forKey: "transit") //Bool
            }
            let status:Int = data!["driverStatus"] as! Int
            
            if (self.HMVCDelegate != nil) {
                self.HMVCDelegate.checkTheDriverStatus(data: status)
            }

            self.parsingTheShipmentResponse(responseData: (data?["appointments"] as! [[String : Any]]))
            
        }, onError: { (error) in
            Helper.hidePI()
        }, onCompleted: {
            
        }) {
            
        }.disposed(by: disposeBag)
        
        
    }
    
    func parsingTheRideResponse(responseData:[String: Any]){
        
        ////        let rideBookingData = OnRideBookingModel.init(bookingData: responseData)
        //        if(HMVCDelegate != nil)
        //        {
        //            HMVCDelegate.rideTripData(dataModel: rideBookingData)
        //        }
        
    }
    
    func parsingTheShipmentResponse(responseData:[[String: Any]]){
        
        productModelData = []
        
        for dict in responseData {
            
            let model = Home()
            let bookingData = Shipment()
            
            if  let custAddress   = dict["pickUpAddress"] as? String {
                model.pickAddress = custAddress
                bookingData.pickupAddress = custAddress
            }
        
            let userDef = UserDefaults.standard
            
            if let currencySymbol = dict["currencySymbol"] as? String {
                userDef.set(currencySymbol, forKey: USER_INFO.CURRENCYSYMBOL)
            } 
            
            if let mileageMetric = dict["mileageMetric"] as? String {
                userDef.set(mileageMetric, forKey: USER_INFO.DISTANCE)
            }
            
            if let bookType = dict["appt_type"] as? String{
                model.bookingType = bookType
            }
            
            if let custId = dict["customerId"] as? String {
                bookingData.customerId = custId
            }
            
            if  let dropAddress   = dict["dropAddress"] as? String {
                model.dropAddress = dropAddress
                bookingData.deliveryAddress = dropAddress
            }
            
            if let statusMessage = dict["statusMessage"] as? String {
                model.statusMessage = statusMessage
                bookingData.statusMessage = statusMessage
            }
            
            if let storeType = dict["storeType"] as? NSNumber {
                model.storeType = Int(storeType)
                bookingData.storeType = Int(storeType)
            }
            
            if let slotEndTime = dict["slotEndTime"] as? NSNumber {
                model.slotEndTime = Int(slotEndTime)
                bookingData.slotEndTime = Int(slotEndTime)
            }
            
            if let estimatedPackageValue = dict["estimatedPackageValue"] as? NSNumber {
                model.estimatedPackageValue = Double(estimatedPackageValue)
                bookingData.estimatedPackageValue = Double(estimatedPackageValue)
            }
            
            if let extraNote = dict["extraNote"] as? String {
                model.extraNote = extraNote
                bookingData.extraNote = extraNote
            }else {
                model.extraNote = "Empty Note"
                bookingData.extraNote = "Empty Note"
            }
            if let slotStartTime = dict["slotStartTime"] as? NSNumber {
                model.slotStartTime = Int(slotStartTime)
                bookingData.slotStartTime = Int(slotStartTime)
            }
            
            if let expressType = dict["expressDelivery"] as? Bool {
                model.expressType = expressType
                bookingData.expressType = expressType
            }
            
            if let weightPicked = dict["weight"] as? NSNumber {
                model.weightPicked = Int(weightPicked)
                bookingData.weightPicked = Int(weightPicked)
            }
            
            if let weightText = dict["weightMetricText"] as? String {
                model.weightText = weightText
                bookingData.weightText = weightText
            }
            
            
            if let storeTypeMsg = dict["storeTypeMsg"] as? String {
                model.storeMessage = storeTypeMsg
                bookingData.storeMessage = storeTypeMsg
            }
            
            
            if let custPic = dict["customerPic"] as? String {
                bookingData.customerPic = custPic
            }
            
        
            
            if let paymentType = dict["paymentType"] as? NSNumber {
                bookingData.paymentType = Int(paymentType)
            }
            
            if let payByWallet = dict["payByWallet"] as? NSNumber {
                bookingData.payByWallet = Int(payByWallet)
            }
            
            if let onWayTime        = dict["onWayTime"] as? String {
                
                bookingData.bookingTime = onWayTime
            }else {
                 bookingData.bookingTime = "3 July 2019"
            }
            if  let PickTime      = dict["orderDatetime"] as? String {
                model.bookingDateNtime = PickTime
                bookingData.bookingDateNtime = PickTime
                
            }
            
            if  let dropTime      = dict["dueDatetime"] as? String {
                model.timeLeftToPick = dropTime
                bookingData.timeLeftToPick = dropTime
            }
            
            
            if  let bookingTypemsg      = dict["bookingTypeMsg"] as? String {
                model.bookingtypeMsg = bookingTypemsg
                bookingData.bookingTypemsg = bookingTypemsg
            }
            
            if  let storeID      = dict["storeId"] as? String {
                model.storeId = storeID
                bookingData.storeId = storeID
            }
            
            if  let bookingTypeLaundra      = dict["isCominigFromStore"] as? Bool {
                model.bookingtypeForLaundra = bookingTypeLaundra
                bookingData.bookingTypeLaundra = bookingTypeLaundra
            }
            
            if  let custName      = dict["storeName"] as? String {
                model.pickCustName = custName
                bookingData.pickupStore = custName
            }
            
            if let itemPickedTime   = dict["pickedupTime"] as? String {
                
                bookingData.itemPickedTime = itemPickedTime
            }
            
            if let custPhone     = dict["customerPhone"] as? String {
                model.pickPhone = custPhone
                bookingData.customerPhone = custPhone
            }
            
            if let storePhone     = dict["storePhone"] as? String {
                //                model.pickPhone = custPhone
                bookingData.storePhone = storePhone 
            }
            
            
            if  let pickLat       = dict["pickUpLatLng"] as? String {
                model.pickLatlog = pickLat
                bookingData.storeLatLng = pickLat
            }
            
            if  let dropLat       = dict["dropLatLng"] as? String {
                model.dropLatlog   = dropLat
                bookingData.customerLatLng = dropLat
                UserDefaults.standard.set("\(dropLat)", forKey: "dropLatLng")
                UserDefaults.standard.synchronize()
            }
            
            if let bid              = dict["bid"] as? NSNumber {
                let bookingId = Int(bid)
                bookingData.bookingId = bid
                model.bookingId = bookingId
            }
            
            if  let status        = dict["orderStatus"] as? NSNumber{
                model.statCode = Int(status)
                bookingData.orderStatus = Int(status)
            }
            
            if  let notes         = dict["extraNotes"] as? String {
                model.notes = notes
            }
            
            if  let custChn       = dict["customerChn"] as? String {
                model.custChn = custChn
                bookingData.custChn = custChn
            }
            
            //            if let helpers       = dict["helpers"] as? NSNumber{
            //                model.helpersCount = Int(helpers)
            //            }
            //            if let paidBy       = dict["paidByReceiver"] as? NSNumber{
            //                model.paidByWhom = Int(paidBy)
            //            }
            
            if  let fare      =  dict["totalAmount"] as? NSNumber {
                model.bookingAmt = Int(fare)
                bookingData.grandTotal = Double(fare)
            }
            
            
            if let driverTipData = dict["driverTip"] as? Double {
                model.driverTip = driverTipData
                bookingData.drivertip = driverTipData
            }
            
            if  let subTotal     =  dict["subTotalAmount"] as? NSNumber {
                model.subtotalAmount = Int(subTotal)
                bookingData.subTotal = Double(subTotal)
            }
            if let slotId = dict["slotId"] as? String {
                model.slotId = slotId
                bookingData.slotId = slotId
            }
            if let bidID = dict["bid"] as? Int {
                model.bidID = bidID
                bookingData.bidID = bidID
            }
            if  let deliveryCharge     =  dict["deliveryCharge"] as? Double {
                model.deliveryCharge = deliveryCharge
                bookingData.deliveryCharge = deliveryCharge
            }
            
            
            if  let discount     =  dict["discount"] as? Double {
                model.discount = discount
                bookingData.discount = discount
            }
            
            if  let dropName  = dict["customerName"] as? String {
                model.dropCustName = dropName
                bookingData.customerName = dropName
            }
            if let cashCollect = dict["cashCollect"] as? Double {
                 model.cashCollect = cashCollect
                bookingData.cashCollect = cashCollect
            }
            
       
            
//            var cart: [[String: Any]] = []
//            cart = dict["shipmentDetails"] as! [[String : Any]]
//            UserDefaults.standard.set(cart, forKey: "myCart")
//
            bookingsData.append(model)
            
            bookingData.productDetails = []
            for shipments in (dict["shipmentDetails"] as? [[String: Any]])!  {
                var productData: [String:Any]!
                productData = [:]
                
                
                if  let status       = shipments["status"] as? Int {
                    productData["status"] = status
                }
                
                if let addedToCartOn = shipments["addedToCartOn"] as? NSNumber {
                    productData["addedToCartOn"] = addedToCartOn
                }
                
                if let finalPrice = shipments["finalPrice"] as? Double {
                    productData["finalPrice"] = finalPrice
                }
                
                if   let good     = shipments["itemName"] as? String {
                    productData["itemName"] = good
                }
                
                if let quant     = shipments["quantity"] as? NSNumber {
                    productData["quantity"] = Int(truncating: quant)
                }
                
                if  let shipImage = shipments["itemImageURL"] as? String {
                    productData["itemImageURL"] = shipImage
                }
                
                if let totalPrice = shipments["unitPrice"] as? NSNumber {
                    productData["unitPrice"] = Double(totalPrice)
                }
                
                if let unitName = shipments["unitName"] as? String {
                    productData["unitName"] = unitName
                }
                
                if let id = shipments["childProductId"] as? String {
                    productData["childProductId"] = id
                }
                
                if let appliedDiscount = shipments["appliedDiscount"] as? Int {
                    productData["appliedDiscount"] = appliedDiscount
                }
                
                if let finalPrice = shipments["finalPrice"] as? Int {
                    productData["finalPrice"] = finalPrice
                }
                
                if let offerId = shipments["offerId"] as? String{
                    productData["offerId"] = offerId
                }
                
                
//                if let productName = shipments["productName"] as? String{
//                    productData["productName"] = productName
//                }
                
                if let parentId = shipments["parentProductId"] as? String {
                    productData["parentProductId"] = parentId
                }
                
                if let latitude = shipments["unitId"] as? String {
                    productData["unitId"] = latitude
                }
                
                if let taxes = shipments["taxes"] as? [[String:Any]] {
                    productData["taxes"] = taxes
                }
                
                if let addOns = shipments["addOns"] as? [[String:Any]] {
                    productData["addOns"] = addOns
                    print(addOns.count)
                }
                
                if let isAddOnAvailable = shipments["addOnAvailable"] as? Bool {
                    productData["addOnAvailable"] = isAddOnAvailable
                }

                
                bookingData.productDetails.append(productData)
            }
            
            if let newExculsive = dict["exclusiveTaxes"] as? [[String:Any]] {
                
                if newExculsive.count == 0 {
                  
                }else {
                    bookingData.exculsiveTax = newExculsive
                   
                }
                
            }
            
            
            if let deliverySplits = dict["deliveryChargeSplit"] as? [String:Any] {
                model.deliverySplits = deliverySplits
                bookingData.deliverySplits = deliverySplits
            }
            
            bookingData.dateOfSlot  = Helper.dayDifference(from: TimeInterval(bookingData.slotStartTime)).2
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            let date = dateFormatter.date(from: bookingData.dateOfSlot.string(with: "MMM dd, yyyy"))
            print(date)
            bookingData.dateOfSlot = date!
            productModelData.append(bookingData)
  
        }
        
        let delivertSheduleType:Int = UserDefaults.standard.integer(forKey: "driverScheduleType")
        
        if delivertSheduleType == 1 {
            let groupOrders = Dictionary.init(grouping: productModelData, by: {
                $0.slotStartTime
            })
            
            let groupOrders1 = Dictionary.init(grouping: productModelData, by: {
                $0.dateOfSlot
            })
            print(groupOrders1)
            
            let dateSort = groupOrders1.sorted(by: {  $0.0  <  $1.0 })
            
            let dataDict = groupOrders.sorted(by: {  $0.0  <  $1.0 })
            multiArray = dataDict
            
            multiDict = dateSort
            
            //        print(Helper.dayDifference(from: TimeInterval(multiArray[0].key)))
            
            print(dataDict)
            productModelData.removeAll()
            
            for index in dateSort {
                for (i , item) in index.value.enumerated() {
                    if i == 0{
                        productModelData.append(item)
                    }
                    
                }
            }
            
            
            
            print(productModelData)
        }

        self.handlingThePublishData()
        
        if (HMVCDelegate != nil) {
            HMVCDelegate.assignedTripsSucceeded(data: bookingsData)
        }
        
    }
    
    func handlingThePublishData(){
        var bidSum = String()
        for dict  in productModelData {
            if bidSum.isEmpty {
                bidSum = String(describing:dict.bookingId!) + "|" + dict.custChn + "|" + String(describing:dict.orderStatus)
            }
            else{
                bidSum = bidSum + "," + String(describing:dict.bookingId!) + "|" + dict.custChn + "|" + String(describing:dict.orderStatus)
            }
        }
        
        let ud = UserDefaults.standard
        ud.set(bidSum, forKey: USER_INFO.SELBID)
        ud.synchronize()
    }
    
}

extension Date {
    func string(with format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
