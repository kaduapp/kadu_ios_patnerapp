//
//  CurrentTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 14/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class CurrentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var expressImage: UIImageView!
    @IBOutlet weak var expressDelivery: UIButton!
    @IBOutlet weak var orderStatus: UILabel!
    @IBOutlet var topView: UIView!
    @IBOutlet var bookingDateNTime: UILabel!
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet var dropAddress: UILabel!
    @IBOutlet var pickUpAddress: UILabel!
    @IBOutlet var timeLeftToPickBooking: UILabel!
    @IBOutlet var bookingAmt: UILabel!
    @IBOutlet var bookingID: UILabel!
    
    @IBOutlet weak var orderTypeName: UILabel!
    @IBOutlet weak var deliveryTimeLabel: UILabel!
    @IBOutlet weak var pickupHeight: NSLayoutConstraint!
    @IBOutlet weak var storeHeight: NSLayoutConstraint!
    
    @IBOutlet weak var pickupImageHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Helper.shadowView(sender: topView, width:UIScreen.main.bounds.size.width-30, height:topView.frame.size.height)
    }
    
    func updatingTheData(homeDict: Shipment) {
        
        
        
        if(homeDict.storeType == 5 && !homeDict.bookingTypeLaundra){
            
            var feeValue = 0
            if let fee = homeDict.deliverySplits["deliveryPriceFromCustomerToLaundromat"] as? NSNumber {
                feeValue = Int(fee)
            }
            bookingAmt.text = Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(feeValue) , digits: 2))
            if homeDict.expressType {
                
                self.expressDelivery.isHidden = false
                  self.expressImage.isHidden = false
                
            }else {
                
                self.expressDelivery.isHidden = true
                self.expressImage.isHidden = true
            }
            let date = NSDate(timeIntervalSince1970: TimeInterval(homeDict.slotStartTime))
            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "hh:mm a"
            let dateString = dayTimePeriodFormatter.string(from: date as Date)
            
            let date1 = NSDate(timeIntervalSince1970: TimeInterval(homeDict.slotEndTime))
            let dayTimePeriodFormatter1 = DateFormatter()
            dayTimePeriodFormatter1.dateFormat = "hh:mm a"
            let dateString1 = dayTimePeriodFormatter1.string(from: date1 as Date)
    
            self.timeLeftToPickBooking.text =  dateString + "-" + dateString1
            self.pickupHeight.constant = 0
             self.storeHeight.constant = 0
             self.pickupImageHeight.constant = 0
            self.deliveryTimeLabel.text = "Pickup Time:".localized
            
            orderTypeName.text = homeDict.storeMessage
            
            
            customerName.text = homeDict.customerName
            
            pickUpAddress.text = homeDict.pickupAddress
            
            storeName.text = homeDict.pickupStore
            
            dropAddress.text = homeDict.deliveryAddress
            
            bookingID.text = "ID: " + "\(homeDict.bookingId!)"
            
            orderStatus.text = homeDict.statusMessage
            
        }else if(homeDict.storeType == 5 && homeDict.bookingTypeLaundra){
            
            var laundraToCusFee = 0
            if let fee = homeDict.deliverySplits["deliveryPriceFromLaundromatToCustomer"] as? NSNumber {
                laundraToCusFee = Int(fee)
            }
            if homeDict.expressType {
                
                self.expressDelivery.isHidden = false
                self.expressImage.isHidden = false
                
            }else {
                self.expressDelivery.isHidden = true
                self.expressImage.isHidden = true
            }
            
            let date = NSDate(timeIntervalSince1970: TimeInterval(homeDict.slotStartTime))
            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "hh:mm a"
            let dateString = dayTimePeriodFormatter.string(from: date as Date)
            
            let date1 = NSDate(timeIntervalSince1970: TimeInterval(homeDict.slotEndTime))
            let dayTimePeriodFormatter1 = DateFormatter()
            dayTimePeriodFormatter1.dateFormat = "hh:mm a"
            let dateString1 = dayTimePeriodFormatter1.string(from: date1 as Date)
            self.timeLeftToPickBooking.text =  dateString + "-" + dateString1
            
            bookingAmt.text = Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(laundraToCusFee) , digits: 2))
            self.pickupHeight.constant = 16
            self.storeHeight.constant = 16
            self.pickupImageHeight.constant = 16
            
            orderTypeName.text = homeDict.storeMessage
            
            
            customerName.text = homeDict.customerName
            
            pickUpAddress.text = homeDict.pickupAddress
            
            storeName.text = homeDict.pickupStore
            
            dropAddress.text = homeDict.deliveryAddress
            
            bookingID.text = "ID: " + "\(homeDict.bookingId!)"
            
            orderStatus.text = homeDict.statusMessage
            
        }else if(homeDict.storeType == 7) {
            bookingDateNTime.text = Helper.UTCToLocal(date: homeDict.bookingDateNtime)
            timeLeftToPickBooking.text = Helper.UTCToLocal(date:homeDict.timeLeftToPick)
            
            bookingAmt.text = Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(homeDict.grandTotal!) , digits: 2))
            self.pickupHeight.constant = 16
            self.storeHeight.constant = 16
            self.pickupImageHeight.constant = 16
            
            self.expressDelivery.isHidden = true
            self.expressImage.isHidden = true
            
            orderTypeName.text = homeDict.storeMessage
            
            
            customerName.text = homeDict.customerName
            
            pickUpAddress.text = homeDict.pickupAddress
            
            storeName.text = "Pickup"
            
            dropAddress.text = homeDict.deliveryAddress
            
            bookingID.text = "ID: " + "\(homeDict.bookingId!)"
            
            orderStatus.text = homeDict.statusMessage
            
        }else {
            bookingDateNTime.text = Helper.UTCToLocal(date: homeDict.bookingDateNtime)
            timeLeftToPickBooking.text = Helper.UTCToLocal(date:homeDict.timeLeftToPick)
            
            bookingAmt.text = Utility.currencySymbol + String(describing: Helper.clipDigit(value: Float(homeDict.grandTotal!) , digits: 2))
            self.pickupHeight.constant = 16
            self.storeHeight.constant = 16
            self.pickupImageHeight.constant = 16
            
            self.expressDelivery.isHidden = true
            self.expressImage.isHidden = true
            
            orderTypeName.text = homeDict.storeMessage
            
            
            customerName.text = homeDict.customerName
            
            pickUpAddress.text = homeDict.pickupAddress
            
            storeName.text = homeDict.pickupStore
            
            dropAddress.text = homeDict.deliveryAddress
            
            bookingID.text = "ID: " + "\(homeDict.bookingId!)"
            
            orderStatus.text = homeDict.statusMessage
            
        }
        
       
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
