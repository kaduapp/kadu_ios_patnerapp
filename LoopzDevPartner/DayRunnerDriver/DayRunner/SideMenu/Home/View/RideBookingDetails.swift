////
////  RideBookingDetails.swift
////  KarruPro
////
////  Created by Rahul Sharma on 17/11/17.
////  Copyright © 2017 3Embed. All rights reserved.
////
//
//import UIKit
//
//protocol RideBookingDetailsDelegate{
//    func moveToOnRideBookingContoller(dataModel: OnRideBookingModel)
//}
//
//class RideBookingDetails: UIView {
//
//    @IBOutlet weak var labelBookingID: UILabel!
//    @IBOutlet weak var labelStatus: UILabel!
//    @IBOutlet weak var labelCustName: UILabel!
//    @IBOutlet weak var labelAddress: UILabel!
//    @IBOutlet weak var labelAddressHeading: UILabel!
//    
//    var rideBookingDetailsDelgate: RideBookingDetailsDelegate! = nil
//    var rideDataModel: OnRideBookingModel!
//    
//    func updateRideBookingDetails(dataModel: OnRideBookingModel)
//    {
//        self.isHidden = false
//        rideDataModel = dataModel
//        
//        self.labelBookingID.text = String(dataModel.bid)
//        self.labelCustName.text = dataModel.customerName
//        self.labelStatus.text = dataModel.statusString
//        
//        switch dataModel.status {
//        case 6:
//            self.labelStatus.text = "On The Way"
//            self.labelAddressHeading.text = "Pickup Address:"
//            self.labelAddress.text = dataModel.pickupAddress
//            break
//            
//        case 7:
//            self.labelStatus.text = "Arrived"
//            self.labelAddressHeading.text = "Drop Address:"
//            self.labelAddress.text = dataModel.pickupAddress
//            break
//            
//        default:
//            self.labelStatus.text = "Trip Started"
//            self.labelAddressHeading.text = "Drop Address:"
//            self.labelAddress.text = dataModel.dropAddress
//        }
//    }
//    
//    @IBAction func actionButtonMoveToOnRideBookingController(_ sender: UIButton?) {
//        rideBookingDetailsDelgate.moveToOnRideBookingContoller(dataModel: rideDataModel)
//    }
//    
//}

