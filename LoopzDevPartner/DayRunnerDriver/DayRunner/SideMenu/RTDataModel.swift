//
//  RTDataModel.swift
//  DayRunner
//
//  Created by apple on 9/14/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class RTDataModel: NSObject {

    var amount:Double = 0.00
    var closingBal = ""
    var comment = ""
    var currency = ""
    var intiatedBy = ""
    var openingBal = ""
    var paymentTxnId = ""
    var paymentType = ""
    var timestamp:Int64 = 0
    var trigger = ""
    var txnId = ""
    var txnType = ""
    var tripId = ""
    
    override init() {
        super.init()
    }
    
    init(dictionary: [String: Any]) {
        super.init()
        if let amount = dictionary["amount"] as? Double {
            self.amount = amount
        }
        
        self.closingBal = dictionary["closingBal"] as? String ?? ""
        self.comment = dictionary["comment"] as? String ?? ""
        self.currency = dictionary["currency"] as? String ?? ""
        self.intiatedBy = dictionary["intiatedBy"] as? String ?? ""
        self.openingBal = dictionary["openingBal"] as? String ?? ""
        self.paymentTxnId = dictionary["paymentTxnId"] as? String ?? ""
        self.paymentType = dictionary["paymentType"] as? String ?? ""
        if let time = dictionary["txnDate"] as? NSNumber{
            self.timestamp = Int64(truncating: time)
        }
        self.trigger = dictionary["trigger"] as? String ?? ""
        self.txnId = dictionary["txnId"] as? String ?? ""
        self.txnType = dictionary["txnType"] as? String ?? ""
        self.tripId = String(describing:dictionary["tripId"] ?? 0)
    }
}

struct RTDataType {
    
    var credits: [RTDataModel] = []
    var debits: [RTDataModel] = []
    var credits_debits: [RTDataModel] = []
    var walletBalance: Double = 0.00
    init(data: [String: Any]) {
        
        if let creditArr = data["creditTransctions"] as? [[String: Any]] {
            for credit in creditArr {
                credits.append(RTDataModel(dictionary: credit))
            }
        }
        
        if let debitArr = data["debitTransctions"] as? [[String: Any]] {
            for debit in debitArr {
                debits.append(RTDataModel(dictionary: debit))
            }
        }
        
        if let creditDebitArr = data["creditDebitTransctions"] as? [[String: Any]] {
            for creditDebit in creditDebitArr {
                credits_debits.append(RTDataModel(dictionary: creditDebit))
            }
        }
        
        let userDef = UserDefaults.standard
        
        if let currencySymbol = data["currencySymbol"] as? String {
            userDef.set(currencySymbol, forKey: USER_INFO.CURRENCYSYMBOL)
        }
        
        if let newWalletBalance = data["walletBalance"] as? Double {
            walletBalance = newWalletBalance
        }
    }
}
