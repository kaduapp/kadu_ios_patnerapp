//
//  APICalls.swift
//  UFly
//
//  Created by 3Embed on 27/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import ReachabilitySwift

class APICalls: NSObject {
    static let disposeBag = DisposeBag()
    enum ErrorCode: Int {
        case success                = 200
        case created                = 201
        case deleted                = 202 //Accepted forgotPW
        case updated                = 203
        case NoUser                 = 204
        case badRequest             = 400
        case Unauthorized           = 401
        case Required               = 402
        case Forbidden              = 403
        case NotFound               = 404
        case MethodNotAllowed       = 405
        case NotAcceptable          = 406
        case Banned                 = 408
        case ExpiredOtp             = 410
        case PreconditionFailed     = 412
        case RequestEntityTooLarge  = 413
        case TooManyAttemt          = 429
        case ExpiredToken           = 440
        case InvalidToken           = 498
        case InternalServerError    = 500
        case InternalServerError2   = 502
        
    }
    static let LogoutInfoTo = PublishSubject<Bool>()
    //Refresh Token
    class func refreshToken() {
//        if !AppConstants.Reachable {
////            Helper.showNoInternet()
//            return
//        }
//        let header = APIErrors.getAOTHHeader()
//        RxAlamofire.requestJSON(.get, APIEndTails.RefreshToken, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
//            Helper.hidePI()
//            let bodyIn = body as! [String:Any]
//            let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
//            if errNum == .success {
//                let newToken = bodyIn[APIResponceParams.Data] as! String
//                Utility.saveSession(token: newToken)
//            }else{
//                APICalls.basicParsing(data:bodyIn,status:head.statusCode)
//            }
//        }, onError: { (Error) in
//            Helper.hidePI()
//        }).disposed(by: APICalls.disposeBag)
    }
    
    
    class func configure() ->Observable<Bool> {
        let header = APIErrors.getAOTHHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, APIEndTails.Configure, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as! [String:Any]
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
//                    let data = bodyIn[APIResponceParams.Data] as! [String:Any]
//                    Utility.setAppConfig(data: data)
                }
                observer.onNext(true)
                observer.onCompleted()
            }, onError: { (Error) in
                Helper.hidePI()
                observer.onNext(false)
                observer.onCompleted()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }
    }
    
    class func basicParsing(data:[String:Any],status: Int) {
        Helper.hidePI()
        let errNum = ErrorCode(rawValue: status)!
        switch errNum {
        case .success:
//            Helper.showAlert(message: data[APIResponceParams.Message] as! String, head: StringConstants.Success, type: 0)
            break
        case .ExpiredToken:

//            if let temp = data[APIResponceParams.Data] as? String {
//                Utility.saveSession(token: temp)
//                APICalls.refreshToken()
//            }else {
//                Session.expired()
//            }


            break
        case .InvalidToken,.NoUser:
             Session.expired()
            break
        case .NotFound:
            break
        default:
//            var message = ""
//            if let msg = data[APIResponceParams.Message] as? String {
//                message = msg
//            }
//            Helper.showAlert(message: message, head: StringConstants.Error, type: 1)
            break
        }
    }
    
}
