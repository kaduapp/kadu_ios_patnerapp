//
//  AddNewCardView.swift
//  UFly
//
//  Created by 3 Embed on 17/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import Stripe

class AddNewCardView: UIView ,STPPaymentCardTextFieldDelegate {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardNumSeparator: UIView!
    @IBOutlet weak var expireTFSeparator: UIView!
    @IBOutlet weak var cvvSeparator: UIView!
    @IBOutlet weak var addCardView: UIView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var cardNumberTF: HoshiTextField!
    @IBOutlet weak var expiresTF: HoshiTextField!
    @IBOutlet weak var cvvTF: HoshiTextField!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var orSeparator1: UIView!
    @IBOutlet weak var orSeparator2: UIView!
    
    @IBOutlet weak var scanView: UIView!
    @IBOutlet weak var scanButton: UIButton!
    //SAVE CARD DETAILS
    
    
    var paymentTextField: STPPaymentCardTextField?
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        cardNumberTF.Semantic()
//        expiresTF.Semantic()
//        cvvTF.Semantic()
        initialSetup()
    }

    /// initial viewsetup
    func initialSetup() {
        setUp()
        cardImage.image = STPImageLibrary.brandImage(for: STPCardBrand.unknown)
//        Helper.setButton(button: addBtn,view:addCardView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
//        Helper.setButtonTitle(normal: StringConstants.AddNewCard(), highlighted: StringConstants.AddNewCard(), selected: StringConstants.AddNewCard(), button: addBtn)
//        Helper.setButtonTitle(normal: StringConstants.ScanCard(), highlighted: StringConstants.ScanCard(), selected: StringConstants.ScanCard(), button: scanButton)
        
        addBtn.setTitle("SAVE CARD DETAILS".localized, for: .normal)
        scanButton.setTitle("", for: .normal)
      paymentTextField?.becomeFirstResponder()
        Fonts.setPrimaryMedium(addBtn)
        Fonts.setPrimaryRegular(cardNumberTF)
        Fonts.setPrimaryRegular(expiresTF)
        Fonts.setPrimaryRegular(cvvTF)
        
        cardNumberTF.textColor =  UIColor.black
        expiresTF.textColor =  UIColor.black
        cvvTF.textColor = UIColor.black
        
        cardNumberTF.placeholder = "Card number".localized
        cvvTF.placeholder = "CVV"
        expiresTF.placeholder = "Expires".localized  //Expira
        orLabel.text = "OR".localized
        orLabel.textColor =  Colors.baseColor
        orSeparator1.backgroundColor =  Colors.baseColor
        orSeparator2.backgroundColor = Colors.baseColor
        Helper.setButton(button: scanButton, view: scanView, primaryColour:  UIColor.black, seconderyColor:.white, shadow: false)
    }
    
    func setUp() {
        paymentTextField               = STPPaymentCardTextField()
        paymentTextField?.delegate     = self
        paymentTextField?.cursorColor  = UIColor.blue
        paymentTextField?.borderColor  = UIColor.clear
        paymentTextField?.borderWidth  = 1
        paymentTextField?.font         = UIFont.boldSystemFont(ofSize: 12)
        paymentTextField?.textColor    = UIColor.black;
        paymentTextField?.cornerRadius = 2.0;
        paymentTextField?.frame        = CGRect(x: 0,y: 0,width: cardNumberTF.frame.size.width,height: 40)
    }
    
    
}
