
//
// NotificationService.swift
// FlagitDriverNotificationServiceExtension
//
// Created by Rahul Sharma on 17/03/18.
// Copyright © 2018 3Embed. All rights reserved.
//

import UserNotifications
import UIKit
import CoreLocation
import Foundation


struct NotificationLinks {
    static let AcknowledgeService  = "https://api.deliv-x.com/driver/bookingAck"
    static let locationUpdates     = "https://api.deliv-x.com/driver/location"
    static let groupIdentifier     = "group.com.delivX.partner"
    static let googleServerKey     = "AIzaSyCwnMQoJoHIzFKLPoulX8E0iiebHVIjzgI"
    static let googleApikey        = "https://fcm.googleapis.com/fcm/send"
}

class NotificationService: UNNotificationServiceExtension,CLLocationManagerDelegate {
    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    var locationManager: CLLocationManager?
    
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        bestAttemptContent?.badge = 0
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        if let bestAttemptContent = bestAttemptContent {
            // Modify the notification content here...
            bestAttemptContent.title = "Location notification..."
            let dict = request.content.userInfo
            print("push data \(dict)")
            if let parseData = dict as? [String:Any] {
                print("booking data2", dict)
                var aValue = 506
                if let a = parseData["a"] as? String {
                    if !a.isEmpty {
                        aValue = Int(a)!
                    }
                }
                print("identifier :", request.identifier)
                if aValue == 506 {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.locationManager = CLLocationManager()
                        self.locationManager?.delegate = self
                        self.locationManager?.requestAlwaysAuthorization()
                        self.locationManager?.startMonitoringSignificantLocationChanges()
                        self.locationManager?.startUpdatingLocation()
                    })
                } else {
                    if let bookingPushData = parseData["bookingData"] as? String {                    self.acknowledgingThebookingServer(pushData:self.convertToDictionary(text: bookingPushData)!)
                        UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.set(self.convertToDictionary(text: bookingPushData)!, forKey: "bookingData")
                        let distanceTime = Date().timeIntervalSince1970
                        UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.set(Int(distanceTime), forKey: "timeStamp")
                    }
                }
            }
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func acknowledgingThebookingServer(pushData:[String:Any]) {
        let params:[String:Any] = [
            "bookingId": String(describing:pushData["bid"]!),
            ]
        let jsondata: Data? = try? JSONSerialization.data(withJSONObject: params,
                                                          options: .prettyPrinted)
        var sessionToken = ""
        if let sessToken = UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.object(forKey:"session_token")! as? String {
            sessionToken = sessToken
        }
        let confi = URLSessionConfiguration.default
        confi.httpAdditionalHeaders = [
            "authorization":sessionToken,
            "lan" : "en"
        ]
        let session = URLSession.init(configuration: confi)
        let todoEndpoint: String = NotificationLinks.AcknowledgeService
        let url = URL(string: todoEndpoint)!
        var request1 = URLRequest(url: url)
        request1.httpMethod = "POST"
        request1.httpBody = jsondata
        request1.timeoutInterval = 15.0
        request1.addValue("application/json", forHTTPHeaderField:"Content-Type")
        let task1 = session.dataTask(with: request1) { data, response, error in
            guard let data = data, error == nil else {
                print("error=\(String(describing: error))")
                return
            }
            print("response = \(String(describing: response))")
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            self.contentHandler!(self.bestAttemptContent!)
            self.clearNotification()
        }
        task1.resume()
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent = bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
}

extension NotificationService {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        self.updateTheLocationToService(latitude: Double(coord.latitude), longitude: Double(coord.longitude))
        DispatchQueue.main.async(execute: {() -> Void in
            self.locationManager?.stopUpdatingLocation()
        })
    }
    
    func updateTheLocationToService(latitude:Double,longitude:Double) {
        var transist = 0
        let ud = UserDefaults.standard
        if UserDefaults.standard.object(forKey: "distLat") != nil {
            let locA = CLLocation(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
            let locB = CLLocation(latitude: CLLocationDegrees(ud.double(forKey: "distLat")), longitude:CLLocationDegrees(ud.double(forKey: "distLog")))
            let distance: CLLocationDistance = locA.distance(from: locB)
            if distance >= 0 {
                transist = 1
                ud.set(latitude, forKey: "distLat")
                ud.set(longitude, forKey: "distLog")
                ud.synchronize()
            } else {
                transist = 0
            }
        } else {
            ud.set(latitude, forKey: "distLat")
            ud.set(longitude, forKey: "distLog")
            transist = 0
            ud.synchronize()
        }
        var presence: String! {
            if let pre: String = UserDefaults.standard.value(forKey: "60") as! String? {
                return pre
            }
            return "60"
        }
        var locationCheck = "1"
        if UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.object(forKey:"locationCheck") != nil {
            if let locCheck = UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.object(forKey:"locationCheck")! as? Bool {
                if !locCheck {
                    locationCheck = "0"
                }
            }
        }
        let dict :[String:Any] = [
            "latitude": latitude,
            "longitude": longitude,
            "batteryPer": "\(UIDevice.current.batteryLevel * 100)",
            "appVersion": UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.object(forKey:"appVersion")! as Any,
            "locationCheck": locationCheck,
            "locationHeading": "0",
            "presenceTime" : presence,
            "transit":transist
        ]
        let jsondata: Data? = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        var sessionToken = ""
        if let sessToken = UserDefaults(suiteName: NotificationLinks.groupIdentifier)!.object(forKey:"session_token")! as? String {
            sessionToken = sessToken
        }
        let confi = URLSessionConfiguration.default
        confi.httpAdditionalHeaders = ["authorization":sessionToken,"lan" : "en"]
        let session = URLSession.init(configuration: confi)
        let todoEndpoint: String = NotificationLinks.locationUpdates
        let url = URL(string: todoEndpoint)!
        var request1 = URLRequest(url: url)
        request1.httpMethod = "POST"
        request1.httpBody = jsondata
        request1.timeoutInterval = 15.0
        request1.addValue("application/json", forHTTPHeaderField:"Content-Type")
        let task1 = session.dataTask(with: request1) { data, response, error in
            guard let data = data, error == nil else {
                print("error=\(String(describing: error))")
                return
            }
            print("response = \(String(describing: response))")
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            if let httpResponse = response as? HTTPURLResponse {
                print("statusCode: \(httpResponse.statusCode)")
                if httpResponse.statusCode == 200 {
                    self.bestAttemptContent?.title = "Location Update Success!"
                    self.clearNotification()
                    self.contentHandler!(self.bestAttemptContent!)
                    self.clearNotification()
                }
            } else {
                self.bestAttemptContent?.title = "Location Service Error..."
            }
        }
        task1.resume()
    }
    
    func clearNotification() {
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
    }
}

